﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using sos.core;
using sos.core.cards;
using HutongGames.PlayMaker;

public class SoS_Album : SoS_Base 
{
	#region PUBLICS
	public UIScrollView _scrollView;
	public UIGrid 		_playerGrid;
	public GameObject 	_playerCardTemplate;
	#endregion
	#region PRIVATES
	List<PlayerCard> _players = new List<PlayerCard> ();
	#endregion
	#region MONO_STUFF
	void Start()
	{
		if (!SoS_IsInitialed)
			return;
		_players = new List<PlayerCard>( GetGeneral.GetCurrentCollection.PlayerCards );
		_players = _players.OrderBy(o=>o.Role.Id).ToList();
		foreach(var player in _players)
		{
			CreateTestCard(player,School.DEFENCE);
		}
		foreach(var player in _players)
		{
			CreateTestCard(player,School.PLAYMAKING);
		}
		foreach(var player in _players)
		{
			CreateTestCard(player,School.PUNK);
		}
		foreach(var player in _players)
		{
			CreateTestCard(player,School.ATTACK);
		}
	}
	void CreateTestCard(PlayerCard card,School school)
	{
		if (card.SchoolName != school.Name)
			return;
		GameObject playerObj=Instantiate(_playerCardTemplate);
		playerObj.GetComponent<SoS_PlayerCard>().InitPlayer(card);
		playerObj.AddComponent<UIDragScrollView>();
		playerObj.GetComponent<UIDragScrollView>().scrollView=_scrollView;
		_playerGrid.AddChild(playerObj.transform);
	}
	#endregion
}