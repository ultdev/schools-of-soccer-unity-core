﻿using UnityEngine;
using System.Collections;

using sos.data;
using sos.core;
using sos.core.cards;


public class SoS_PlayerCard : SoS_Card 
{
	#region PRIVATES
	PlayerCard _playerCard;
	#endregion
	#region PUBLIC
	public void InitPlayer(PlayerCard card)
	{
		_playerCard = card;
		name = card.Name;
		EndInit ();
		UpdateCard ();
	}
	#endregion
	#region MONO_STUFF
	void Update()
	{
		if (!_initialized)
			return;
	}
	#endregion
	#region PL_Actions
	private void UpdateCard()
	{
		_fsmCard.FsmVariables.GetFsmString ("Name").Value = _playerCard.Name;
		_fsmCard.FsmVariables.GetFsmString ("Role").Value = _playerCard.Role.Name;
		_fsmCard.FsmVariables.GetFsmString ("Dribbling").Value = _playerCard.Stats.Dribbling.ToString();
		_fsmCard.FsmVariables.GetFsmString ("Passing").Value = _playerCard.Stats.Passing.ToString();
		_fsmCard.FsmVariables.GetFsmString ("Scoring").Value = _playerCard.Stats.Scoring.ToString();
		_fsmCard.FsmVariables.GetFsmString ("Defense").Value = _playerCard.Stats.Defense.ToString();
		_fsmCard.FsmVariables.GetFsmString ("Stamina").Value = _playerCard.Stats.Stamina.ToString();
		_fsmCard.FsmVariables.GetFsmString ("School").Value = _playerCard.SchoolName;
		_fsmCard.SendEvent (UPDATE_CARD);
	}
	#endregion
}