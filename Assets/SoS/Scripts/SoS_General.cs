﻿using UnityEngine;
using sos.core;
using sos.core.cards;
using sos.core.helpers;
using sos.data.providers.user;
using HutongGames.PlayMaker;
using sos.logging;
using sos.data.providers.card;
using sos.data.providers.quest;

public class SoS_General : SoS_Base 
{
	#region PUBLICS
	public User 				GetCurrentUser{ get { return _currentUser; } }
	public Collection 			GetCurrentCollection{ get { return _currentCollection; } }
	public float 				GetCardManager{get {return 0f;}}
	public static SoS_General 	GetSoS_General{ get{ return GameObject.Find ("SoS_General").GetComponent<SoS_General> ();}}
	#endregion
	#region PRIVATES
	private User 			_currentUser=null;
	private Collection 		_currentCollection=null;
	private UserProvider 	_userProvider;
    private CardProvider 	_cardProvider;
    private QuestProvider 	_questProvider;
	#endregion
	#region PL_ACTIONS
	public void Init()
	{
		Application.targetFrameRate = 30;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
        sos.logging.Logger.Debug ("Sos Init");

        _cardProvider = gameObject.AddComponent<CardProvider>();
        _cardProvider.LoadDefinitionsStart += CardProvider_LoadDefinitionsStart;
        _cardProvider.LoadDefinitionsFailed += CardProvider_LoadDefinitionsFailed;
        _cardProvider.LoadDefinitionsDone += CardProvider_LoadDefinitionsDone;

        _questProvider = gameObject.AddComponent<QuestProvider>();
        _questProvider.LoadDefinitionsStart += QuestProvider_LoadDefinitionsStart;
        _questProvider.LoadDefinitionsDone += QuestProvider_LoadDefinitionsDone;
        _questProvider.LoadDefinitionsFailed += QuestProvider_LoadDefinitionsFailed;

		_userProvider=gameObject.AddComponent<UserProvider> ();
		_userProvider.LoginStart += UserProvider_LoginStarted;
		_userProvider.LoginDone += UserProvider_LoginDone;
		_userProvider.LoginFailed += UserProvider_LoginFailed;



        // Startup
        _cardProvider.LoadDefinitions();
        _questProvider.LoadDefinitions();
	}

    private void QuestProvider_LoadDefinitionsFailed(QuestProviderErrorEventArgs args)
    {
        sos.logging.Logger.Debug("UI > QuestProvider > LoadDefinitionsFailed EVENT catched");
    }

    private void QuestProvider_LoadDefinitionsDone(QuestProviderEventArgs args)
    {   
        sos.logging.Logger.Debug("UI > QuestProvider > LoadDefinitionsDone EVENT catched");
    }

    private void QuestProvider_LoadDefinitionsStart(QuestProviderEventArgs args)
    {
        sos.logging.Logger.Debug("UI > QuestProvider > LoadDefinitionsStard EVENT catched");
    }

    private void CardProvider_LoadDefinitionsStart(CardProviderEventArgs args)
    {
        sos.logging.Logger.Debug("UI > CardProvider > LoadDefinitionsStard EVENT catched");
    }

    private void CardProvider_LoadDefinitionsDone(CardProviderEventArgs args)
    {
        sos.logging.Logger.Debug("UI > CardProvider > LoadDefinitionsDone EVENT catched");
    }

    private void CardProvider_LoadDefinitionsFailed(CardProviderErrorEventArgs args)
    {
        sos.logging.Logger.Debug("UI > CardProvider > LoadDefinitionsFailed EVENT catched");
    }

    public void Login(string username,string password)
	{
        sos.logging.Logger.Debug ("SoS_General.Login() > Provided arguments: Username: {0}, password: {1}", username, password);

        // TO REMOVE! Arguments forcing!
        //username = "Clouddancer";
        //password = "3333";
        // TO REMOVE! Arguments forcing!

        sos.logging.Logger.Debug ("SoS_General.Login() > Arguments forced!: Username: {0}, password: {1}", username, password);

		_userProvider.Login (username,password);
	}
	public void GetUserCollection()
	{
		_currentCollection = CardHelper.GetCollection ();
	}
	#endregion

	#region SoS_CORE_CALLBACK
	private void UserProvider_LoginStarted(UserProviderEventArgs e)
	{
		sos.logging.Logger.Debug("UI > UserProvider > LoginStart EVENT catched");
		PlayMakerFSM.BroadcastEvent (LOGIN_STARTED);
	}
	private void UserProvider_LoginDone(UserProviderEventArgs e)
	{
		sos.logging.Logger.Debug("UI > UserProvider > LoginDone EVENT catched");
		_currentUser = e.User;

		FsmString username = FsmVariables.GlobalVariables.FindFsmString(GVR_USERNAME);
		username.Value = _currentUser.Username;
		FsmInt credits = FsmVariables.GlobalVariables.FindFsmInt (GVR_CREDITS);
		credits.Value = (int)_currentUser.Credits;

		//TEMP
		GetUserCollection ();

		PlayMakerFSM.BroadcastEvent (LOGIN_DONE);
	}
	public void UserProvider_LoginFailed(UserProviderErrorEventArgs e)
	{
		sos.logging.Logger.Debug("UI > UserProvider > LoginFailed EVENT catched");
		PlayMakerFSM.BroadcastEvent (LOGIN_FAILED);
	}
	#endregion
}
