﻿using UnityEngine;
using System.Collections;

using sos.core.cards;

public class SoS_Test : SoS_Base 
{
	public GameObject _playerCardPrefab;
	void Awake()
	{
		GameObject playerCard = Instantiate (_playerCardPrefab);
		playerCard.GetComponent<SoS_PlayerCard> ().InitPlayer (sos.core.helpers.CardHelper.PLAYER_CARD_01);
	}
}
