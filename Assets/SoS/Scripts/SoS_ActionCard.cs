﻿using UnityEngine;
using System.Collections;

using sos.data;
using sos.core;
using sos.core.cards;

public class SoS_ActionCard : SoS_Card 
{
	#region PRIVATES
	ActionCard _actionCard;
	#endregion
	#region PUBLIC
	public void InitAction(ActionCard card)
	{
		_actionCard = card;
		name = card.Name;
		EndInit ();
		UpdateCard ();
	}
	#endregion
	#region MONO_STUFF
	void Update()
	{
		if (!_initialized)
			return;
	}
	#endregion
	#region PL_Actions
	private void UpdateCard()
	{
		_fsmCard.FsmVariables.GetFsmString ("Name").Value = _actionCard.Name;
		_fsmCard.FsmVariables.GetFsmString ("ActionText").Value = _actionCard.ActionText;
		_fsmCard.FsmVariables.GetFsmString ("Cost").Value = _actionCard.Cost.ToString();
		_fsmCard.SendEvent (UPDATE_CARD);
	}
	#endregion
}
