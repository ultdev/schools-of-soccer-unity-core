﻿using UnityEngine;
using System.Collections;

using sos.data;
using sos.core;
using sos.core.cards;

public class SoS_Card : SoS_Base 
{
	#region PROTECTED
	protected PlayMakerFSM 	_fsmCard;
	protected bool			_initialized=false;
	protected void EndInit()
	{
		_fsmCard = GetComponent<PlayMakerFSM> ();
		_initialized = true;
	}
	#endregion
	#region MONO_STUFF
	protected void Awake()
	{
		GetComponent<UIPanel> ().alpha = 0f;
	}
	#endregion
}