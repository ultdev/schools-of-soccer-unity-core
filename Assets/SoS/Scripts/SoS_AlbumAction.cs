﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using sos.core;
using sos.core.cards;
using HutongGames.PlayMaker;

public class SoS_AlbumAction : SoS_Base 
{
	#region PUBLICS
	public UIScrollView _scrollView;
	public UIGrid 		_playerGrid;
	public GameObject 	_actionCardTemplate;
	#endregion
	#region PRIVATES
	List<ActionCard> _actions = new List<ActionCard> ();
	#endregion
	#region MONO_STUFF
	void Start()
	{
		if (!SoS_IsInitialed)
			return;
		_actions = new List<ActionCard> ( GetGeneral.GetCurrentCollection.ActionCards );
		_actions = _actions.OrderBy(o=>o.SchoolName).ToList();
		foreach(var action in _actions)
		{
			CreateTestCard(action);
		}
	}
	void CreateTestCard(ActionCard card)
	{
		GameObject actionObj=Instantiate(_actionCardTemplate);
		actionObj.GetComponent<SoS_ActionCard>().InitAction(card);
		actionObj.AddComponent<UIDragScrollView>();
		actionObj.GetComponent<UIDragScrollView>().scrollView=_scrollView;
		_playerGrid.AddChild(actionObj.transform);
	}
	#endregion
}