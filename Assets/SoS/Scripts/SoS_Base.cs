﻿using UnityEngine;
using System.Collections;

using HutongGames.PlayMaker;


public class SoS_Base : MonoBehaviour 
{
	#region PROTECTED
	
	protected const string  CALL_INIT_ACTION   		= "Call_Init";
	protected const string  LOGIN_STARTED   		= "Login_Started";
	protected const string  LOGIN_DONE	  			= "Login_Done";
	protected const string  LOGIN_FAILED	  		= "Login_Failed";
	protected const string  LOGIN_ERROR	  			= "Login_Error";
	protected const string  UPDATE_CARD	  			= "Update_Card";

	protected const string 	GVR_GENERAL				= "SoS_Singleton";
	protected const string 	GVR_INITIALIZED			= "SoS_Initialized";
	protected const string 	GVR_USERNAME			= "SoS_Username";
	protected const string 	GVR_PASSWORD			= "SoS_Password";
	protected const string 	GVR_CREDITS				= "SoS_Credits";

	protected bool SoS_IsInitialed
	{
		get
		{
			return FsmVariables.GlobalVariables.GetFsmBool(GVR_INITIALIZED).Value;
		}
	}

	protected SoS_General GetGeneral
	{ 
		get 
		{ 
			if(!SoS_IsInitialed)
				return null;
			else
				return FsmVariables.GlobalVariables.GetFsmGameObject (GVR_GENERAL).Value.GetComponent<SoS_General> (); 
		} 
	}
	
	#endregion
}