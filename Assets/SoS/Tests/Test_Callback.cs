﻿using UnityEngine;
using sos.logging;
using sos.core.cards;
using sos.data.providers.user;
using sos.data.providers.card;
using sos.data.providers.quest;
using sos.core;
using sos.core.game.client;

public class Test_Callback : MonoBehaviour 
{

    private CardProvider _cardProvider;
    private QuestProvider _questProvider;
    private UserProvider _userProvider;
    private User _user;
    private GameClient _client;

	// Use this for initialization
	void Start () 
    {

        // Creates the client
        _client = gameObject.AddComponent<GameClient>();
        _client.ConnectionStart += GameClient_ConnectionStart;
        _client.ConnectionStop += GameClient_ConnectionStop;
        _client.ConnectionLost += GameClient_ConnectionLost;
        _client.ConnectionFailed += GameClient_ConnectionFailed;
	
        // Init cad provider 
        _cardProvider = gameObject.AddComponent<CardProvider>();
        _cardProvider.LoadDefinitionsDone += _cardProvider_LoadDefinitionsDone;
        _cardProvider.LoadDefinitionsStart += _cardProvider_LoadDefinitionsStart;
        _cardProvider.LoadDefinitionsFailed += _cardProvider_LoadDefinitionsFailed;
        // Load cards definitions
        _cardProvider.LoadDefinitions();

        // Init cad provider 
        _questProvider = gameObject.AddComponent<QuestProvider>();
        _questProvider.LoadDefinitionsDone += _questProvider_LoadDefinitionsDone;
        _questProvider.LoadDefinitionsStart += _questProvider_LoadDefinitionsStart;
        _questProvider.LoadDefinitionsFailed += _questProvider_LoadDefinitionsFailed;
        // Load quest definitions
        _questProvider.LoadDefinitions();
    }


    // Update is called once per frame
    void Update ()
    {
	    var clientStatusLabel = GameObject.Find("Label_Client_Status").GetComponent<UILabel>();
        var clientHeartbeatCountLabel = GameObject.Find("Label_Client_HeartbeatCount").GetComponent<UILabel>();
        var userLabel = GameObject.Find("Label_User").GetComponent<UILabel>();

        clientStatusLabel.text = "Client Status: " + _client.Status.Name;
        clientHeartbeatCountLabel.text = "Heartbeat count: " + _client.HeartbeatCount;

        userLabel.text = "User: " + ( (_user != null) ? _user.Username : "Not Logged" );

	}

    public void Init_OnClick()
    {

        // SoS_General g = GameObject.Find("Sos_General").GetComponent<SoS_General>();
        // g.Init();

        // Check definitions loading
        if (!CardProvider.AreDefinitionsLoaded)
        {
            sos.logging.Logger.Info("CardDefinitions are not loaded");
            return;
        }

        // Check definitions loading
        if (!QuestProvider.AreDefinitionsLoaded)
        {
            sos.logging.Logger.Info("QuestDefinitions are not loaded");
            return;
        }

        // LOGIN TEST
        _userProvider = gameObject.AddComponent<UserProvider>();
        _userProvider.LoginDone += UserProvider_LoginDone;
        _userProvider.LoginStart += UserProvider_LoginStart;
        _userProvider.LoginFailed += UserProvider_LoginFailed;
        _userProvider.Login("loustrong", "1111");

    }

    public void EngineClient_Connect_OnClick()
    {
        // Check user logged
        if (_user == null)
        {
            sos.logging.Logger.Info("User not logged!");
            return;
        }

        // Starts the connection
        GameClientOptions options = new GameClientOptions();
        _client.Connect(_user, options);
        

    }

    public void EngineClient_Disconnect_OnClick()
    {

        if (!_client.IsConnected)
        {
            sos.logging.Logger.Info("GameClient is not connected!");
            return;
        }

        // Disconnects the client
        _client.Disconnect();

    }

    #region EVENT HANDLERS

    #region GameClient handlers

    private void GameClient_ConnectionFailed(GameClientEventArgs args)
    {
        sos.logging.Logger.Debug("GameClient.ConnectionFailed()");
    }

    private void GameClient_ConnectionLost(GameClientEventArgs args)
    {
        sos.logging.Logger.Debug("GameClient.ConnectionLost()");
    }

    private void GameClient_ConnectionStop(GameClientEventArgs args)
    {
        sos.logging.Logger.Debug("GameClient.ConnectionStop()");
    }

    private void GameClient_ConnectionStart(GameClientEventArgs args)
    {
        sos.logging.Logger.Debug("GameClient.ConnectionStart()");
    }

    #endregion GameClient handlers

    #region UserProvider handlers

    private void UserProvider_LoginFailed(UserProviderErrorEventArgs args)
    {
        sos.logging.Logger.Debug("UserProvider.Login() FAILED : {0}", args.ErrorMessage);
    }

    private void UserProvider_LoginStart(UserProviderEventArgs args)
    {
        sos.logging.Logger.Debug("UserProvider.Login() START");
    }

    private void UserProvider_LoginDone(UserProviderEventArgs args)
    {
        // DEBUG
        sos.logging.Logger.Debug("USER {0}", args.User.ToString());

        _user = args.User;

        sos.logging.Logger.Debug("UserProvider.Login() DONE");
    }

    #endregion UserProvider handlers

    private void _cardProvider_LoadDefinitionsFailed(CardProviderErrorEventArgs args)
    {
        sos.logging.Logger.Debug("CardProvider.LoadDefinition() FAILED : {0}", args.ErrorMessage);
    }

    private void _cardProvider_LoadDefinitionsStart(CardProviderEventArgs args)
    {
        sos.logging.Logger.Debug("CardProvider.LoadDefinition() start");
    }

    private void _cardProvider_LoadDefinitionsDone(CardProviderEventArgs args)
    {
        sos.logging.Logger.Debug("CardProvider.LoadDefinition() DONE > {0}", CardProvider.GetDefinitions().ToString());
    }

    private void _questProvider_LoadDefinitionsStart(QuestProviderEventArgs args)
    {
        sos.logging.Logger.Debug("QuestProvider.LoadDefinition() start");
    }

    private void _questProvider_LoadDefinitionsDone(QuestProviderEventArgs args)
    {
        sos.logging.Logger.Debug("QuestProvider.LoadDefinition() DONE > {0}", QuestProvider.GetDefinitions().ToString());
    }

    private void _questProvider_LoadDefinitionsFailed(QuestProviderErrorEventArgs args)
    {
        sos.logging.Logger.Debug("CardProvider.LoadDefinition() FAILED : {0}", args.ErrorMessage);
    }

    #endregion EVENT HANDLERS

}
