﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.GUI)]
	[Tooltip("Assing an NGUI Button Action to an Event.")]
	public class NGUI_ButtonClickListener : FsmStateAction
	{
		[CheckForComponent(typeof(UIButton))]
		public FsmOwnerDefault gameObject;
		[RequiredField]
		public FsmEvent eventToSend;
		
		public override void Reset()
		{
		}
		public void ButtonAction(GameObject obj)
		{
			Fsm.Event(eventToSend);
			Finish();
		}
		public override void OnEnter()
		{
			UIEventListener.Get(Fsm.GetOwnerDefaultTarget(gameObject)).onClick+=ButtonAction;
		}
	}
}