﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.GUI)]
	[Tooltip("Get an NGUI Label.")]
	public class NGUI_GetLabel : FsmStateAction
	{
		[CheckForComponent(typeof(UILabel))]
		public FsmOwnerDefault gameObject;
		[RequiredField]
		public FsmString stringText;
		public bool everyFrame;
		
		public override void Reset()
		{
		}
		
		public override void OnEnter()
		{
			DoSetLabel();
			
			if (!everyFrame)
			{
				Finish();
			}
		}
		
		public override void OnUpdate()
		{
			DoSetLabel();
		}
		
		void DoSetLabel()
		{
			UILabel go = Fsm.GetOwnerDefaultTarget(gameObject).GetComponent<UILabel>();
			stringText.Value = go.text;
		}
	}
}