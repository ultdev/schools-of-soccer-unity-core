﻿using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.GUI)]
	[Tooltip("Set an NGUI Input Focus.")]
	public class NGUI_SetInputFocus : FsmStateAction
	{
		[CheckForComponent(typeof(UIInput))]
		public FsmOwnerDefault gameObject;
		[RequiredField]
		public bool focus;
		
		public override void Reset()
		{
		}
		
		public override void OnEnter()
		{
			UIInput go = Fsm.GetOwnerDefaultTarget(gameObject).GetComponent<UIInput>();
			go.isSelected = focus;
			Finish();
		}
	}
}