﻿using UnityEngine;
using UnityEngine.UI;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.GUI)]
	[Tooltip("Get an uGUI text from InputField.")]
	public class uGUI_GetInputFieldText : FsmStateAction
	{
		[CheckForComponent(typeof(InputField))]
		public FsmOwnerDefault gameObject;
		[RequiredField]
		public FsmString stringText;
		public bool everyFrame;
		
		public override void Reset()
		{
		}
		
		public override void OnEnter()
		{
			DoSetLabel();
			
			if (!everyFrame)
			{
				Finish();
			}
		}
		
		public override void OnUpdate()
		{
			DoSetLabel();
		}
		
		void DoSetLabel()
		{
			InputField inputField=Fsm.GetOwnerDefaultTarget(gameObject).GetComponent<InputField>();
			stringText.Value = inputField.text;
		}
	}
}