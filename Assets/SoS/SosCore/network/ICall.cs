﻿using System.Collections.Generic;

namespace sos.network
{
    /// <summary>
    /// Interface for implementting SoS server calls
    /// </summary>
    /// <typeparam name="TDataFormat">Datatype of the returned data. Used to define CallResult.Data datatype</typeparam>
    public interface ICall<TDataFormat> where TDataFormat : class
    {

		#region PROPERTIES

        /// <summary>
        /// True if the call has completed the async execution
        /// </summary>
        bool IsExecuted { get; }

        /// <summary>
        /// True if the call has been executed succesfully or with an handled error
        /// </summary>
        bool IsSuccesfull { get; }

        /// <summary>
        /// True if the call has been executed but any kind of internal errore has occoured
        /// and the response cannot be used. Check Status property to knowk the error reason and
        /// Result.ErrorMessage and Result.ErrorMessage for the details
        /// </summary>
        bool IsInError { get; }

        /// <summary>
        /// Current Call status
        /// </summary>
        CallStatus Status { get; }

        /// <summary>
        /// Results obtained from the SoS server call
        /// </summary>
        CallResult<TDataFormat> Result { get; }

		#endregion PROPERTIES

		#region PUBLIC METHODS

        /// <summary>
        /// 
        /// </summary>
        void Execute(string url);

        /// <summary>
        /// 
        /// </summary>
        void Execute(string url, uint timeout);

        /// <summary>
        /// 
        /// </summary>
        void Execute(string url, Dictionary<string, string> parameters);

        /// <summary>
        /// 
        /// </summary>
        void Execute(string url, uint timeout, Dictionary<string, string> parameters);

		#endregion PUBLIC METHODS

    }
}
