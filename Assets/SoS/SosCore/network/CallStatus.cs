﻿namespace sos.network
{
    /// <summary>
    /// 
    /// </summary>
    public enum CallStatus
    {
        /// <summary>
        /// Call ready to be excuted
        /// </summary>
        READY          = 10,
        /// <summary>
        /// Call in execution
        /// </summary>
        EXECUTING      = 20,
        /// <summary>
        /// Call executed with success
        /// </summary>
        SUCCEEDED      = 30,
        /// <summary>
        /// Call executed with an handled error
        /// </summary>
        FAILED         = 40,
        /// <summary>
        /// Call not executed, execution lasts over given timeout
        /// </summary>
        TIMEOUT        = 50,
        /// <summary>
        /// Call not executed, data received not valid or in the wrong format 8depending by the encoding (probably JSON)
        /// </summary>
        DATA_ERROR     = 60,
        /// <summary>
        /// Call not executed, unexpected error during client execution
        /// </summary>
        CLIENT_ERROR   = 70,
        /// <summary>
        /// Call not executed, network error occourred (WWW.error)
        /// </summary>
        NETWORK_ERROR  = 80,
    }
}
