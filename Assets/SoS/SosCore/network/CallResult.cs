﻿using sos.utils;

namespace sos.network
{

    /// <summary>
    /// Results obtained by executiona a call to the SoS server
    /// </summary>
    public class CallResult<TDataFormat> where TDataFormat : class
    {

        #region CONSTANTS

        // Default values
        protected const int     DEFAULT_RESULT                  = 0;
        protected const int     DEFAULT_ERROR_CODE              = Defaults.INVALID_CALL_ERROR_CODE;
        protected const string  DEFAULT_ERROR_MESSAGE           = Defaults.INVALID_CALL_ERROR_MESSAGE;

        #endregion CONSTANTS

        #region FIELDS

        private CallResultValue _result;
        private TDataFormat _data;
        private int _errorCode;
		private string _errorMessage;

        #endregion FIELDS

        #region CONSTRUCTORS

        private CallResult()
        {
            Clear();
        }

        /// <summary>
        /// Succesfull call result constructor
        /// </summary>
        /// <param name="result"></param>
        /// <param name="data"></param>
        public CallResult(CallResultValue result, TDataFormat data) : this()
        {
            _result = result;
            _data = data;
        }

        /// <summary>
        /// Failed call result constructor
        /// </summary>
        /// <param name="result"></param>
        /// <param name="data"></param>
        public CallResult(CallResultValue result, int errorCode, string errorMessage) : this()
        {
            _result = result;
            _errorCode = errorCode;
            _errorMessage = errorMessage;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public CallResultValue Result
        {
            get { return _result; }
        }

        /// <summary>
        /// Raw data returned by the call
        /// </summary>
        public TDataFormat Data
        {
            get { return _data; }
        }

        /// <summary>
        /// Code that identify the error occoured during the call execution
        /// </summary>
        public int ErrorCode
        {
            get { return _errorCode; }
        }

        /// <summary>
        /// Message that describe the error occoured during the call execution
        /// </summary>
        public string ErrorMessage
        {
            get { return _errorMessage; }
        }

        /// <summary>
        /// True if the response represents a successfull call to SoS server
        /// </summary>
        public bool IsSuccesfull
        {
            get { return _result == CallResultValue.EXEC_WITH_SUCCESS; }
        }

        /// <summary>
        /// True if the result represents a failed call with an error
        /// </summary>
        public bool IsError
        {
            get { return _result != CallResultValue.EXEC_WITH_SUCCESS; }
        }

        /// <summary>
        /// True if the result represents an handler error returend from the SoS server
        /// </summary>
        public bool IsHandledError
        {
            get
            {
                return _result == CallResultValue.EXEC_WITH_ERROR;
            }
        }

        /// <summary>
        /// True if the result represents an client unhandler error occourred during the call execution
        /// </summary>
        /// <returns></returns>
        public bool IsClientError
        {
            get
            {
                return _result == CallResultValue.CLIENT_ERROR;
            }
        }

        /// <summary>
        /// True if the result represents an server unhandler error occourred during the call server-side execution
        /// </summary>
        public bool IsServerError
        {
            get { return _result == CallResultValue.SERVER_ERROR; }
        }

        /// <summary>
        /// True if the response contains data received from the SoS server
        /// </summary>
        public bool HasData
        {
            get { return _data != null; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        public bool Equals(CallResult<TDataFormat> result)
        {
            return  _result         == DEFAULT_RESULT       &&
                    _data           == result.Data          &&    
                    _errorCode      ==  result.ErrorCode    &&
		            _errorMessage   == result.ErrorMessage;
        }

        public void Clear()
        {
            _result         = DEFAULT_RESULT;
            _data           = null;
            _errorCode      = DEFAULT_ERROR_CODE;
		    _errorMessage   = DEFAULT_ERROR_MESSAGE;
        }

        public void Copy(CallResult<TDataFormat> result)
        {
            _result         = result.Result;
            _data           = result.Data;
            _errorCode      = result.ErrorCode;
		    _errorMessage   = result.ErrorMessage;
        }

        public CallResult<TDataFormat> Clone()
        {
            CallResult<TDataFormat> result = new CallResult<TDataFormat>();
            result.Copy(this);
            return result;
        }

        #endregion PUBLIC METHODS

    }
}
