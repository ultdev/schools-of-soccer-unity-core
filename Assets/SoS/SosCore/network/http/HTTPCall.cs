﻿using System;
using System.Collections;
using System.Collections.Generic;
using sos.logging;
using sos.utils;
using SimpleJSON;
using UnityEngine;
using Newtonsoft.Json.Linq;

namespace sos.network.http
{
    /// <summary>
    /// 
    /// </summary>
    public class HTTPCall : MonoBehaviour, ICall<string>
    {

        #region CONSTANTS

        // URLs
        public const string     URL_FAKE_DOWNLOAD                           = "http://download.thinkbroadband.com/1MB.zip"; 
        // protected const string  BASE_URL                                 = "http://ec2-52-28-181-145.eu-central-1.compute.amazonaws.com/sos/ClientComm/Flash/";
        public const string     URL_BASE                                    = "http://ec2-52-28-181-145.eu-central-1.compute.amazonaws.com/sos_unity_v1/ClientComm/Unity/";

        // User URLs
        public const string     URL_USER_LOGIN                              = "User/login.php";			// IN > username=<value>&username=<value>	OUT > username":"","password":"","finalresponse":"Login Effettuato","logged":true,"userID":"3","deckID":"2"}
        public const string     URL_USER_REGISTER                           = "v2/register.php";		// IN > username=<value>&username=<value>&password=<value>&regCode=<value>	OUT > username":"<username>"}
        public const string     URL_USER_CHOOSE_STARTER_DECK                = "v2/chooseStarter.php";   // IN > username=<value>&username=<value>&starterId=<value>
        public const string     URL_USER_GAME_LIST                          = "v2/getGames.php";		// IN > password=1111&userID=1				OUT > {"result":0,"error":null,"data":{"gameCount":0}}
		public const string     URL_USER_FRIENDS_LIST                       = "v2/getFriendList.php";	// IN > password=1111&userID=1

        // Cards URLs
        public const string     URL_CARDS_LOAD_DEFINITIONS                  = "Cards/getCardList.php";
        
        // Quests URLs
        public const string     URL_QUEST_LOAD_DEFINITIONS                  = "Quests/init.php";

        // Game URLs
        public const string     URL_GAME_HEARTBEAT                          = "Game/heartbeat.php";
        public const string     URL_GAME_REQUEST                            = "Game/request.php";


        // Default values
        private const string    DEFAULT_CALL_URL                            = "";
        private const uint      DEFAULT_TIMEOUT                             = 15; // seconds

        // Execution error codes
        private const int       ERROR_CODE_TIMEOUT                          = 9000;
        private const int       ERROR_CODE_NETWORK_ERROR                    = 9001;
        private const int       ERROR_CODE_DATA_FORMAT                      = 9002;
        private const int       ERROR_CODE_CLIENT_EXCEPTION                 = 9003;

        // Logging debug key (used to enable or disable logging in the Logger class)
        public static string    DEBUG_KEY                                   = "sos.network.http.HTTPCall";

        #endregion CONSTANTS

        #region FIELDS

        private bool _executed;
        private string _url;
        private uint _timeout = 20; // secondi
        private Dictionary<string, string> _parameters;
        private CallStatus _status = CallStatus.READY;
        private CallResult<string> _result;

        #endregion FIELDS

        #region CONSTRUCTORS

        private HTTPCall()
        {
            _url        = DEFAULT_CALL_URL;
            _timeout    = DEFAULT_TIMEOUT;
            _parameters = new Dictionary<string, string>();
            _status     = CallStatus.READY;
            _result     = null;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        /// <summary>
        /// Base url: the url provided as Excute method without any GET parameter attached
        /// </summary>
        public string BaseUrl
        {
            get { return _url; }
        }

        /// <summary>
        /// Real url used to make the SoS server HTTP call. Builded using the parameters
        /// givent to Execute method
        /// </summary>
        public string Url
        {
            get { return BuildUrl(); }
        }

        /// <summary>
        /// 
        /// </summary>
        public CallResult<string> Result
        {
            get { return _result; }
        }

        /// <summary>
        /// 
        /// </summary>
        public CallStatus Status
        {
            get { return _status; }
        }

        /// <summary>
        /// True if the call has completed the async execution
        /// </summary>
        public bool IsExecuted
        {
            get { return _executed; }
        }

        /// <summary>
        /// True if the call has been executed succesfully or with an handled error
        /// </summary>
        public bool IsSuccesfull
        {
            get
            {
                return IsExecuted && (_status == CallStatus.SUCCEEDED ||  _status == CallStatus.FAILED);
            }
        }

        /// <summary>
        /// True if the call has been executed but any kind of internal errore has occoured
        /// and the response cannot be used. Check Status property to knowk the error reason and
        /// Result.ErrorMessage and Result.ErrorMessage for the details
        /// </summary>
        public bool IsInError
        {
            get
            {
                return  IsExecuted &&  _status != CallStatus.SUCCEEDED && _status != CallStatus.FAILED;
            }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        /// <summary>
        /// Builds the URL to use to perform the HTTP call to SoS server, using the given url and all
        /// the parameters provided
        /// </summary>
        /// <returns></returns>
        private string BuildUrl()
        {
            // TODO: Build URL with parameters if provided
            string urlParams = string.Empty;
            // Loop over parameters
            foreach (KeyValuePair<string, string> param in _parameters)
            {
                if (!string.IsNullOrEmpty(urlParams)) urlParams += "&";
                urlParams += string.Format("{0}={1}", param.Key, WWW.EscapeURL(param.Value));
            }
            // Add a timestamp to avoid any type of caching
            urlParams += !string.IsNullOrEmpty(urlParams) ? "&" : "";
            urlParams += string.Format("nocache={0:yyyyMMddHHmmssfff}", DateTime.Now);
            // Compose the full URL
            return string.Format("{0}?{1}", _url, urlParams);
        }

        /// <summary>
        /// Parses the default JSON protocol object used by the server to handler the base layer communication with the client.
        /// 
        /// JSON standard obejct:
        /// 
        /// Success response
        /// 
        /// {
        ///     "result": result_value,
        ///     "data": json_data,
        ///     "error": null
        /// }
        /// 
        /// Failure response
        /// 
        /// {
        ///     "result": result_value,
        ///     "data": null,
        ///     "error": {
        ///         "code": error_code
        ///         "message": error_message
        ///     }
        /// }
        /// </summary>
        /// <param name="www">Unity WWW object used to perform the HTTP request to the server</param>
        /// <param name="result">CallResult object </param>
        /// <returns>True if the response</returns>
        private bool ParseDefaultResponse(WWW www, out CallResult<string> result)
        {
            // Locals
            bool    success             = false;
            int     raw_result          = 0;
            string  raw_data            = string.Empty;
            int     raw_error_code      = Defaults.INVALID_CALL_ERROR_CODE;
            string  raw_error_message   = Defaults.INVALID_CALL_ERROR_MESSAGE;
            // Safe execution   
            try
            {

                #region JSON parsing: JSON.Net

                JObject response = JObject.Parse(www.text);
                // Retrieve protocol properties: result and data
                raw_result  = response["result"].Value<int>();
                raw_data    = (response["data"].Type != JTokenType.Null) ? response["data"].ToString(Newtonsoft.Json.Formatting.None) : null;
                // Check if the response contains error data
                if (response["error"].Type != JTokenType.Null)
                {
                    raw_error_code      = (int)     response["error"]["code"];
                    raw_error_message   = (string)  response["error"]["message"];
                }
                #endregion JSON parsing: JSON.Net

                #region JSON parsing: SimpleJSON library
                //// Parse json
                //JSONNode root = JSON.Parse(www.text);
                //// Retrieve protocol properties: result and data
                //raw_result  = root["result"].AsInt;
                //raw_data    = root["data"].ToString();
                //// If data is null , resets the reference
                //raw_data    = raw_data != JsonUtils.SIMPLE_JSON_NULL_STRING ? raw_data : null;
                //// Check if the response contains error data
                //if (root["error"].ToString() != JsonUtils.SIMPLE_JSON_NULL_STRING)
                //{
                //    raw_error_code      = root["error"]["code"].AsInt;
                //    raw_error_message   = root["error"]["message"].Value;
                //}
                #endregion JSON parsing: SimpleJSON library

                #region Standard respons object handling

                // Check result value
                // if (Enum.IsDefined(typeof(CallResultValue), raw_result))
                if ( EnumUtils.Exists<CallResultValue>(raw_result) )
                {
                    // Cast the value into the enum
                    CallResultValue value = (CallResultValue) raw_result;
                    // Check server result
                    switch ( value )
                    {
                        // Call succeded
                        case CallResultValue.EXEC_WITH_SUCCESS:
                            // Creates a successfull result
                            result = new CallResult<string>(value, raw_data);
                            break;

                        // Call failed, retrieve error details
                        case CallResultValue.EXEC_WITH_ERROR:
                            // Creates a failure result
                            result = new CallResult<string>(value, raw_error_code, raw_error_message);
                            break;

                        // Call in error, retrieve error details
                        case CallResultValue.SERVER_ERROR:
                            // Creates a failure result
                            result = new CallResult<string>(value, raw_error_code, raw_error_message);
                            break;

                        // Unexpected result value
                        case CallResultValue.CLIENT_ERROR:
                            // ERROR: The server cannot return CLIENT_ERROR as result value 
                            throw new CallException(string.Format("The server cannot return CLIENT_ERROR as result value (value: {0})", raw_result));

                        // Unexpected result value (to avoid compiler error)
                        default:
                            // ERROR: Invalid server result value
                            throw new CallException(string.Format("Invalid server result value (value: {0})", raw_result));
                    }
                }
                else
                {
                    // ERROR: Invalid server result value!
                    throw new CallException(string.Format("Invalid server result value (value: {0})", raw_result));
                }              

                #endregion Standard respons object handling

                // Parsing done
                success = true;
            }
            catch (Exception ex)
            {
                // Parsing failed
                success = false;
                // Messaggio
                string errorMessage = string.Format("Unexpected exception parsing standard JSON response: {0}", ex.Message);
                // Build results
                result = new CallResult<string>(CallResultValue.CLIENT_ERROR, ERROR_CODE_DATA_FORMAT, errorMessage);
            }
            return success;
        }

        /// <summary>
        /// 
        /// </summary>
        private void SetStatus(CallStatus status)
        {
            if (_status != status)
            {
                // Debug
                if (sos.logging.Logger.CanDebug(DEBUG_KEY)) sos.logging.Logger.Debug("HTTPCall.SetStatus() > From {0} to {1}", Enum.GetName(typeof(CallStatus), _status), Enum.GetName(typeof(CallStatus), status));
                _status = status;
            }
        }

        /// <summary>
        /// Internal call execution
        /// </summary>
        /// <returns></returns>
        private IEnumerator InternalExecute()
        {
            // Debug
            if (sos.logging.Logger.CanDebug(DEBUG_KEY)) sos.logging.Logger.Debug("HTTPCall.InternalExecute() > BEGIN");
            // STATUS > EXECUTING
            SetStatus(CallStatus.EXECUTING);
            // Call result
            CallResult<string> result = null;
            // Created the HTTP URL to call
            string url = BuildUrl();
            // Duration in seconds
            float duration = 0f;
            // Creates a new Unity WWW object
            using (WWW www = new WWW(url))
            {
                // Wait for completition, or for an error or for a duration greather the timeout
                while (!www.isDone && www.error == null && duration < _timeout)
                {
                    // Recal execution duration
                    duration += Time.deltaTime;
                    // Wait...
                    yield return null;
                }

                // Checks for a timeout
                if (duration > _timeout)
                {
                    // Build results
                    result = new CallResult<string>(CallResultValue.CLIENT_ERROR, ERROR_CODE_TIMEOUT, "Call timeout!");
                    // STATUS > TIMEOUT
                    SetStatus(CallStatus.TIMEOUT);
                    // Debug
                    if (sos.logging.Logger.CanDebug(DEBUG_KEY)) sos.logging.Logger.Debug("HTTPCall.InternalExecute() > TIMEOUT!");
                }
                // Checks for an error
                else if (www.error != null)
                {
                    // Build results
                    result = new CallResult<string>(CallResultValue.CLIENT_ERROR, ERROR_CODE_NETWORK_ERROR, string.Format("Internal Unity WWW class error: {0}", www.error));
                    // STATUS > NETWORK_ERROR
                    SetStatus(CallStatus.NETWORK_ERROR);
                    // Debug
                    if (sos.logging.Logger.CanDebug(DEBUG_KEY)) sos.logging.Logger.Debug("HTTPCall.InternalExecute() > NETWORK ERROR!");
                }
                else
                {
                    // Safe execution
                    try
                    {
                        // Default server response parsing
                        if (ParseDefaultResponse(www, out result))
                        {
                            // STATUS > SUCCEEDED!
                            SetStatus(CallStatus.SUCCEEDED);
                            // Debug
                            if (sos.logging.Logger.CanDebug(DEBUG_KEY)) sos.logging.Logger.Debug("HTTPCall.InternalExecute() > SUCCESS!");
                        }
                        else
                        {
                            // STATUS > DATA_ERROR
                            SetStatus(CallStatus.DATA_ERROR);
                            // Debug
                            if (sos.logging.Logger.CanDebug(DEBUG_KEY)) sos.logging.Logger.Debug("HTTPCall.InternalExecute() > DATA ERROR!");
                        }
                    }
                    catch (Exception ex)
                    {
                        // Messaggio
                        string errorMessage = string.Format("Unexpected exception during call: {0} (url: {1})", ex.Message, url);
                        // Build results
                        result = new CallResult<string>(CallResultValue.CLIENT_ERROR, ERROR_CODE_CLIENT_EXCEPTION, errorMessage);
                        // STATUS > CLIENT_ERROR
                        SetStatus(CallStatus.CLIENT_ERROR);
                        // Debug
                        if (sos.logging.Logger.CanDebug(DEBUG_KEY)) sos.logging.Logger.Debug("HTTPCall.InternalExecute() > CLIENT ERROR!");
                    }
                }
            }
            // Assign the result
            _result = result;
            // Execution completed
            _executed = true;
            // DEBUG
            if (sos.logging.Logger.CanDebug(DEBUG_KEY)) sos.logging.Logger.Debug("HTTPCall.InternalExecute() > END");
        }

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        public void Execute(string url)
        {
            Execute(url, DEFAULT_TIMEOUT, new Dictionary<string, string>());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="timeout"></param>
        public void Execute(string url, uint timeout)
        {
            Execute(url, timeout, new Dictionary<string, string>());
        }

        /// <summary>
        /// Executes the HTTP call to the server
        /// </summary>
        public void Execute(string url, Dictionary<string, string> parameters)
        {
            Execute(url, DEFAULT_TIMEOUT, parameters);
        }

        /// <summary>
        /// Executes the HTTP call to the server
        /// </summary>
        public void Execute(string url, uint timeout, Dictionary<string, string> parameters)
        {
            // Setup attributes
            _url = url;
            _timeout = timeout;
            // setup parameters
            _parameters.Clear();
            foreach (KeyValuePair<string, string> param in parameters)
            {
                _parameters.Add(param.Key, param.Value);
            }
            // Execute the call async
            StartCoroutine(InternalExecute());
        }

        #endregion PUBLIC METHODS
    }
}
