﻿using System;

namespace sos.network
{
    /// <summary>
    /// 
    /// </summary>
    public class CallException : Exception
    {

        public CallException() : base() {}
        public CallException(string message) : base(message) {}
        public CallException(string message, Exception cause) : base(message, cause) {}

    }
}
