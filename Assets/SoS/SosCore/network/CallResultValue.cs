﻿namespace sos.network
{
    /// <summary>
    /// Enumerations of the possible results responsed by a standard call SoS server call
    /// </summary>
    public enum CallResultValue : int
    {
	    EXEC_WITH_SUCCESS   =  0,
	    EXEC_WITH_ERROR		= -1,
	    SERVER_ERROR        = -2,
        CLIENT_ERROR        = -3,
    }
}
