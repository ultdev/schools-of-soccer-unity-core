﻿using sos.core.shop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class ShopHelper
    {

        #region Utility

        private static uint _shopItemId = 0;

        private static uint GetShopItemId()
        {
            return ++_shopItemId;
        }

        #endregion Utility

        #region Shop Helper

        // Dummy ShopItem instances

        private static readonly ShopItem SHOP_ITEM_DUMMY_01 = new ShopItem() { Id = GetShopItemId(), Name = "DUMMY SHOP ITEM 01", Description = "DUMMY SHOP ITEM 01", Cost =  250,  Owned = 3,  Image = "ShopItemDummy01.png" };
        private static readonly ShopItem SHOP_ITEM_DUMMY_02 = new ShopItem() { Id = GetShopItemId(), Name = "DUMMY SHOP ITEM 02", Description = "DUMMY SHOP ITEM 02", Cost = 1000,  Owned = 7,  Image = "ShopItemDummy02.png" };
        private static readonly ShopItem SHOP_ITEM_DUMMY_03 = new ShopItem() { Id = GetShopItemId(), Name = "DUMMY SHOP ITEM 03", Description = "DUMMY SHOP ITEM 03", Cost = 3000,  Owned = 12, Image = "ShopItemDummy03.png" };
        private static readonly ShopItem SHOP_ITEM_DUMMY_04 = new ShopItem() { Id = GetShopItemId(), Name = "DUMMY SHOP ITEM 04", Description = "DUMMY SHOP ITEM 04", Cost = 9999,  Owned = 1,  Image = "ShopItemDummy04.png" };
        private static readonly ShopItem SHOP_ITEM_DUMMY_05 = new ShopItem() { Id = GetShopItemId(), Name = "DUMMY SHOP ITEM 05", Description = "DUMMY SHOP ITEM 05", Cost = 73500, Owned = 0,  Image = "ShopItemDummy05.png" };


        public static Shop GetShop()
        {
            Shop shop = new Shop();
            shop.AddItem(SHOP_ITEM_DUMMY_01);
            shop.AddItem(SHOP_ITEM_DUMMY_02);
            shop.AddItem(SHOP_ITEM_DUMMY_03);
            shop.AddItem(SHOP_ITEM_DUMMY_04);
            shop.AddItem(SHOP_ITEM_DUMMY_05);
            return shop;
        }

        #endregion Shop Helper


    }
}
