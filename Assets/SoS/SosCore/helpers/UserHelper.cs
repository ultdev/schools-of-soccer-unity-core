﻿using System;
using System.Collections.Generic;
using sos.core.game;
using sos.core.cards;

namespace sos.core.helpers
{
    public static class UserHelper
    {

        public static readonly Player   AI_UBINNA           = new Player() { Id = 999, Username = "AI Ubinna",      Email = "ai01@gmail.com", FirstName = "AI1000", LastName = "Skynet", Registration = DateTime.Now };


        public static readonly Player   LOUSTRONG           = new Player() { Id = 1, Username = "Loustrong",        Email = "fra@gmail.com",        FirstName = "Francesco",    LastName = "Forte",         Registration = DateTime.Now.AddYears(-3)   };
        public static readonly Player   JIMMYTHEPAGE        = new Player() { Id = 2, Username = "JimmyThePage",     Email = "jimmy@gmail.com",      FirstName = "Jimmy",        LastName = "ThePage",       Registration = DateTime.Now.AddDays(-1)    };
        public static readonly Player   CLOUDDANCER         = new Player() { Id = 3, Username = "CloudDancer",      Email = "ultima23@gmail.com",   FirstName = "Luca",         LastName = "Tavecchia",     Registration = DateTime.Now.AddDays(-2)    };

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<Player> GetFriendList()
        {
            return new List<Player>() { JIMMYTHEPAGE, LOUSTRONG };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<UserGame> GetGameList()
        {

            Collection c = CardHelper.GetCollection();

            List<UserGame> games = new List<UserGame>();
            games.Add(new UserGame()  { Id = 1, Opponent = LOUSTRONG,      OpponentDeckId = 100, OpponentDeckName = "So Forte!",       PlayerDeckId = c.Decks[0].Id, PlayerDeckName = c.Decks[0].Name, Side = GameSide.HOME });
            games.Add(new UserGame()  { Id = 2, Opponent = JIMMYTHEPAGE,   OpponentDeckId = 200, OpponentDeckName = "JimmyThePunk",    PlayerDeckId = c.Decks[0].Id, PlayerDeckName = c.Decks[0].Name, Side = GameSide.AWAY });
            games.Add(new UserGame()  { Id = 3, Opponent = AI_UBINNA,      OpponentDeckId = 900, OpponentDeckName = "Ubinna Hard",     PlayerDeckId = c.Decks[0].Id, PlayerDeckName = c.Decks[0].Name, Side = GameSide.AWAY });

            return games;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static User GetUser()
        {

            Collection c = CardHelper.GetCollection();

            User user = new User() { Id = 3, Username = "Clouddancer", FirstName = "Luca", LastName = "Tavecchia", Email = "ultima23@gmail.com", Registration = DateTime.Now.AddYears(-2) };
            // friends
            user.Friends.Add(LOUSTRONG);
            user.Friends.Add(JIMMYTHEPAGE);
            // games
            user.Games.Add(new UserGame()  { Id = 1, Opponent = user.Friends[0],   OpponentDeckId = 100, OpponentDeckName = "So Forte!",       PlayerDeckId = c.Decks[0].Id, PlayerDeckName = c.Decks[0].Name, Side = GameSide.HOME });
            user.Games.Add(new UserGame()  { Id = 2, Opponent = user.Friends[1],   OpponentDeckId = 200, OpponentDeckName = "JimmyThePunk",    PlayerDeckId = c.Decks[0].Id, PlayerDeckName = c.Decks[0].Name, Side = GameSide.AWAY });
            user.Games.Add(new UserGame()  { Id = 3, Opponent = AI_UBINNA,         OpponentDeckId = 900, OpponentDeckName = "Ubinna Hard",     PlayerDeckId = c.Decks[0].Id, PlayerDeckName = c.Decks[0].Name, Side = GameSide.AWAY });

            return user;
        }

    }
}
