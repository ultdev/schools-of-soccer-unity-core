﻿using System;
using System.Collections.Generic;
using sos.core.cards;

namespace sos.core.helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class CardHelper
    {

        #region Utility

        private static uint _instanceId = 0;

        private static uint GetInstanceId()
        {
            return ++_instanceId;
        }

        #endregion Utility

        #region PlayerCard Helper

        public static readonly PlayerCard        PLAYER_CARD_01         = new PlayerCard(CardKind.DEFINITION)       { Id = 52,  InstanceId = GetInstanceId(), Skill = 4,    Image = "voigts",       Name = "Conrad Voigts ",        Role = Role.GetBySign("df"),    Schools = new List<School>() { School.GENERIC, School.GetById(5) }, ActionText = "Rush",                                                    Limit = 1,  Stats = new Stats() { Dribbling = 5, Passing = 5, Scoring = 3, Defense = 8, Goalkeeping = 0, Stamina = 10   },  Rarity = Rarity.GetById(3)	 };
        public static readonly PlayerCard        PLAYER_CARD_02         = new PlayerCard(CardKind.DEFINITION)       { Id = 53,  InstanceId = GetInstanceId(), Skill = 3,	Image = "kim",			Name = "Seok Kim ",				Role = Role.GetBySign("df"),	Schools = new List<School>() { School.GENERIC, School.GetById(2) },	ActionText = "Rush",													Limit = 1,	Stats = new Stats() { Dribbling = 6, Passing = 6, Scoring = 4, Defense = 7, Goalkeeping = 0, Stamina = 9	},  Rarity = Rarity.GetById(3)	 };
        public static readonly PlayerCard        PLAYER_CARD_03         = new PlayerCard(CardKind.DEFINITION)       { Id = 54,  InstanceId = GetInstanceId(), Skill = 4,	Image = "dragic",		Name = "Sre\u0107ko Dragic ",	Role = Role.GetBySign("df"),	Schools = new List<School>() { School.GENERIC, School.GetById(5) },	ActionText = "+2 DEF until end of turn if STA < 3",						Limit = 1,	Stats = new Stats() { Dribbling = 7, Passing = 7, Scoring = 5, Defense = 7, Goalkeeping = 0, Stamina = 9	},  Rarity = Rarity.GetById(2)	 };
        public static readonly PlayerCard        PLAYER_CARD_04         = new PlayerCard(CardKind.DEFINITION)       { Id = 55,  InstanceId = GetInstanceId(), Skill = 4,	Image = "volkov",		Name = "Genya Volkov ",			Role = Role.GetBySign("mf"),	Schools = new List<School>() { School.GENERIC, School.GetById(3) },	ActionText = "+1 PAS/DEF on round lost",								Limit = 1,	Stats = new Stats() { Dribbling = 7, Passing = 5, Scoring = 5, Defense = 6, Goalkeeping = 0, Stamina = 10	},  Rarity = Rarity.GetById(3)	 };
        public static readonly PlayerCard        PLAYER_CARD_05         = new PlayerCard(CardKind.DEFINITION)       { Id = 56,  InstanceId = GetInstanceId(), Skill = 3,	Image = "matos",		Name = "Carlinhos Matos ",		Role = Role.GetBySign("mf"),	Schools = new List<School>() { School.GENERIC, School.GetById(5) },	ActionText = "+2 SCO after successfull dribble",						Limit = 1,	Stats = new Stats() { Dribbling = 6, Passing = 7, Scoring = 4, Defense = 6, Goalkeeping = 0, Stamina = 9	},  Rarity = Rarity.GetById(3)	 };
        public static readonly PlayerCard        PLAYER_CARD_06         = new PlayerCard(CardKind.DEFINITION)       { Id = 57,  InstanceId = GetInstanceId(), Skill = 5,	Image = "ubina",		Name = "Eduardo Ubinna ",		Role = Role.GetBySign("fw"),	Schools = new List<School>() { School.GENERIC, School.GetById(5) },	ActionText = "Rush, Area Thief. SCO +2 until end of turn on freekick",	Limit = 1,	Stats = new Stats() { Dribbling = 8, Passing = 7, Scoring = 8, Defense = 6, Goalkeeping = 0, Stamina = 12	},  Rarity = Rarity.GetById(1)	 };
        public static readonly PlayerCard        PLAYER_CARD_07         = new PlayerCard(CardKind.DEFINITION)       { Id = 58,  InstanceId = GetInstanceId(), Skill = 3,	Image = "metaxas",		Name = "Panayiotis Metaxas ",	Role = Role.GetBySign("fw"),	Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "At the end of turn, +1 DRI/PAS if STA is less than 3",	Limit = 1,	Stats = new Stats() { Dribbling = 6, Passing = 6, Scoring = 6, Defense = 5, Goalkeeping = 0, Stamina = 10	},  Rarity = Rarity.GetById(1)	 };
        public static readonly PlayerCard        PLAYER_CARD_08         = new PlayerCard(CardKind.DEFINITION)       { Id = 59,  InstanceId = GetInstanceId(), Skill = 4,	Image = "vinci", 		Name = "Gianluca Vinci ",		Role = Role.GetBySign("fw"),	Schools = new List<School>() { School.GENERIC, School.GetById(5) },	ActionText = "Fast",													Limit = 1,	Stats = new Stats() { Dribbling = 7, Passing = 7, Scoring = 7, Defense = 4, Goalkeeping = 0, Stamina = 9	},  Rarity = Rarity.GetById(3)	 };
        public static readonly PlayerCard        PLAYER_CARD_09         = new PlayerCard(CardKind.DEFINITION)       { Id = 183, InstanceId = GetInstanceId(), Skill = 3,	Image = "vlatko",		Name = "Hrvoje Vlatko",			Role = Role.GetBySign("fw"),	Schools = new List<School>() { School.GENERIC, School.GetById(2) },	ActionText = "Methodical, Counter",										Limit = 1,	Stats = new Stats() { Dribbling = 6, Passing = 5, Scoring = 6, Defense = 5, Goalkeeping = 0, Stamina = 8	},  Rarity = Rarity.GetById(3)	 };
        public static readonly PlayerCard        PLAYER_CARD_10         = new PlayerCard(CardKind.DEFINITION)       { Id = 207,	InstanceId = GetInstanceId(), Skill = 4,	Image = "blankAttack",	Name = "Sjaak Van Amstel",		Role = Role.GetBySign("df"),	Schools = new List<School>() { School.GENERIC, School.GetById(2) },	ActionText = "Fast, Shadowing",											Limit = 1,	Stats = new Stats() { Dribbling = 6, Passing = 7, Scoring = 5, Defense = 7, Goalkeeping = 0, Stamina = 11	},  Rarity = Rarity.GetById(3)	 };
        public static readonly PlayerCard        PLAYER_CARD_11         = new PlayerCard(CardKind.DEFINITION)       { Id = 211,	InstanceId = GetInstanceId(), Skill = 1,	Image = "blankAttack",	Name = "Adrian V\u00e3duva",	Role = Role.GetBySign("df"),	Schools = new List<School>() { School.GENERIC, School.GetById(5) },	ActionText = null,														Limit = 1,	Stats = new Stats() { Dribbling = 6, Passing = 5, Scoring = 3, Defense = 6, Goalkeeping = 0, Stamina = 7	},  Rarity = Rarity.GetById(3)	 };
        public static readonly PlayerCard        PLAYER_CARD_12         = new PlayerCard(CardKind.DEFINITION)       { Id = 212,	InstanceId = GetInstanceId(), Skill = 2,	Image = "blankAttack",	Name = "Bohum\u00edr Kohout",	Role = Role.GetBySign("df"),	Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Fast",													Limit = 1,	Stats = new Stats() { Dribbling = 5, Passing = 6, Scoring = 4, Defense = 6, Goalkeeping = 0, Stamina = 8	},  Rarity = Rarity.GetById(2)	 };
        public static readonly PlayerCard        PLAYER_CARD_13         = new PlayerCard(CardKind.DEFINITION)       { Id = 213,	InstanceId = GetInstanceId(), Skill = 3,	Image = "blankAttack",	Name = "Damien Allard",			Role = Role.GetBySign("df"),	Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "-1 DEF +1 DRI on ball recovery",							Limit = 1,	Stats = new Stats() { Dribbling = 6, Passing = 4, Scoring = 6, Defense = 7, Goalkeeping = 0, Stamina = 9	},  Rarity = Rarity.GetById(2)	 };
        public static readonly PlayerCard        PLAYER_CARD_14         = new PlayerCard(CardKind.DEFINITION)       { Id = 214,	InstanceId = GetInstanceId(), Skill = 1,	Image = "blankAttack",	Name = "Xiang Chao Fan",		Role = Role.GetBySign("mf"),	Schools = new List<School>() { School.GENERIC, School.GetById(3) },	ActionText = null,														Limit = 1,	Stats = new Stats() { Dribbling = 5, Passing = 6, Scoring = 3, Defense = 4, Goalkeeping = 0, Stamina = 8	},  Rarity = Rarity.GetById(3)	 };
        public static readonly PlayerCard        PLAYER_CARD_15         = new PlayerCard(CardKind.DEFINITION)       { Id = 215,	InstanceId = GetInstanceId(), Skill = 2,	Image = "blankAttack",	Name = "Corentin Gwena\u00ebl",	Role = Role.GetBySign("mf"),	Schools = new List<School>() { School.GENERIC, School.GetById(3) },	ActionText = "Rush",													Limit = 1,	Stats = new Stats() { Dribbling = 6, Passing = 5, Scoring = 5, Defense = 5, Goalkeeping = 0, Stamina = 9	},  Rarity = Rarity.GetById(3)	 };
        public static readonly PlayerCard        PLAYER_CARD_16         = new PlayerCard(CardKind.DEFINITION)       { Id = 216,	InstanceId = GetInstanceId(), Skill = 5,	Image = "blankAttack",	Name = "Jaime Rivero",			Role = Role.GetBySign("mf"),	Schools = new List<School>() { School.GENERIC, School.GetById(2) },	ActionText = "Draw a card on successfull pass",							Limit = 1,	Stats = new Stats() { Dribbling = 7, Passing = 8, Scoring = 6, Defense = 6, Goalkeeping = 0, Stamina = 12	},  Rarity = Rarity.GetById(3)	 };
        public static readonly PlayerCard        PLAYER_CARD_17         = new PlayerCard(CardKind.DEFINITION)       { Id = 217,	InstanceId = GetInstanceId(), Skill = 1,	Image = "blankAttack",	Name = "Aarne Laaksonen",		Role = Role.GetBySign("fw"),	Schools = new List<School>() { School.GENERIC, School.GetById(3) },	ActionText = null,														Limit = 1,	Stats = new Stats() { Dribbling = 6, Passing = 5, Scoring = 6, Defense = 4, Goalkeeping = 0, Stamina = 6	},  Rarity = Rarity.GetById(4)	 };
        public static readonly PlayerCard        PLAYER_CARD_18         = new PlayerCard(CardKind.DEFINITION)       { Id = 218,	InstanceId = GetInstanceId(), Skill = 2,	Image = "blankAttack",	Name = "Lukas Amundsen",		Role = Role.GetBySign("fw"),	Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Rush",													Limit = 1,	Stats = new Stats() { Dribbling = 6, Passing = 6, Scoring = 6, Defense = 5, Goalkeeping = 0, Stamina = 8	},  Rarity = Rarity.GetById(3)	 };
        public static readonly PlayerCard        PLAYER_CARD_19         = new PlayerCard(CardKind.DEFINITION)       { Id = 219,	InstanceId = GetInstanceId(), Skill = 3,	Image = "blankAttack",	Name = "Nelinho Ferreira",		Role = Role.GetBySign("fw"),	Schools = new List<School>() { School.GENERIC, School.GetById(2) },	ActionText = "Fast",													Limit = 1,	Stats = new Stats() { Dribbling = 7, Passing = 6, Scoring = 7, Defense = 4, Goalkeeping = 0, Stamina = 9	},  Rarity = Rarity.GetById(1)	 };
        public static readonly PlayerCard        PLAYER_CARD_20         = new PlayerCard(CardKind.DEFINITION)       { Id = 220,	InstanceId = GetInstanceId(), Skill = 4,	Image = "blankAttack",	Name = "Christie Halloran",		Role = Role.GetBySign("fw"),	Schools = new List<School>() { School.GENERIC, School.GetById(5) },	ActionText = "+3 DRI -1 PAS until end of turn if team is losing",		Limit = 1,	Stats = new Stats() { Dribbling = 5, Passing = 7, Scoring = 7, Defense = 6, Goalkeeping = 0, Stamina = 11	},  Rarity = Rarity.GetById(2)	 };
                                                                                                                
        public static readonly GoalkeeperCard    GOALKEEPER_CARD_01     = new GoalkeeperCard(CardKind.DEFINITION)   { Id = 60,  InstanceId = GetInstanceId(), Skill = 1,	Image = "hashimoto",	Name = "Kohaku Hashimoto ",		Role = Role.GetBySign("gk"),	Schools = new List<School>() { School.GENERIC, School.GetById(5) },	ActionText = "+1 GK if team is winning",					            Limit = 1,	Stats = new Stats() { Dribbling = 0, Passing = 0, Scoring = 0, Defense = 0, Goalkeeping = 6, Stamina = 1    },  Rarity = Rarity.GetById(1)	  };
        public static readonly GoalkeeperCard    GOALKEEPER_CARD_02     = new GoalkeeperCard(CardKind.DEFINITION)   { Id = 69,  InstanceId = GetInstanceId(), Skill = 1,	Image = "ljung",		Name = "Olli Ljung ",			Role = Role.GetBySign("gk"),	Schools = new List<School>() { School.GENERIC, School.GetById(2) },	ActionText = "First goal attempt is automatically saved",	            Limit = 1,	Stats = new Stats() { Dribbling = 0, Passing = 0, Scoring = 0, Defense = 0, Goalkeeping = 6, Stamina = 1    },  Rarity = Rarity.GetById(3)	  };
        public static readonly GoalkeeperCard    GOALKEEPER_CARD_03     = new GoalkeeperCard(CardKind.DEFINITION)   { Id = 78,  InstanceId = GetInstanceId(), Skill = 1,	Image = "freitas",		Name = "Alexandre Freitas ",	Role = Role.GetBySign("gk"),	Schools = new List<School>() { School.GENERIC, School.GetById(3) },	ActionText = "+1 GK on goal saved. -1 GK on goal taken",	            Limit = 1,	Stats = new Stats() { Dribbling = 0, Passing = 0, Scoring = 0, Defense = 0, Goalkeeping = 6, Stamina = 1    },  Rarity = Rarity.GetById(2)	  };
        public static readonly GoalkeeperCard    GOALKEEPER_CARD_04     = new GoalkeeperCard(CardKind.DEFINITION)   { Id = 87,  InstanceId = GetInstanceId(), Skill = 1,	Image = "robledo",		Name = "Ram\u00f3n Robledo ",	Role = Role.GetBySign("gk"),	Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Opponent discards 2 cards on goal saved",		            Limit = 1,	Stats = new Stats() { Dribbling = 0, Passing = 0, Scoring = 0, Defense = 0, Goalkeeping = 6, Stamina = 1    },  Rarity = Rarity.GetById(1)	  };

        public static PlayerCard[] GetAttackPlayersCards()
        {
            return (new List<PlayerCard>()   {   PLAYER_CARD_01
                                                ,PLAYER_CARD_02
                                                ,PLAYER_CARD_03
                                                ,PLAYER_CARD_04
                                                ,PLAYER_CARD_05
                                                ,PLAYER_CARD_06
                                                ,PLAYER_CARD_07
                                                ,PLAYER_CARD_08
                                                ,PLAYER_CARD_09
                                                ,PLAYER_CARD_10
                                                ,PLAYER_CARD_11
                                                ,PLAYER_CARD_12
                                                ,PLAYER_CARD_13
                                                ,PLAYER_CARD_14
                                                ,PLAYER_CARD_15
                                                ,PLAYER_CARD_16
                                                ,PLAYER_CARD_17
                                                ,PLAYER_CARD_18
                                                ,PLAYER_CARD_19
                                                ,PLAYER_CARD_20
                                                ,GOALKEEPER_CARD_01
                                                ,GOALKEEPER_CARD_02
                                                ,GOALKEEPER_CARD_03
                                                ,GOALKEEPER_CARD_04
                                            }).ToArray();
        }

        public static PlayerCard[] GetGoalkeeperCards()
        {
            return (new List<PlayerCard>()   {   GOALKEEPER_CARD_01
                                                ,GOALKEEPER_CARD_02
                                                ,GOALKEEPER_CARD_03
                                                ,GOALKEEPER_CARD_04
                                            }).ToArray();
        }

        public static PlayerCard[] GetPlayersCards()
        {
            List<PlayerCard> cards = new List<PlayerCard>();
            cards.AddRange(GetAttackPlayersCards());
            return cards.ToArray();
        }

        #endregion PlayerCard helper

        #region ActionCard Helper

        public static readonly ActionCard        ACTION_CARD_01         = new ActionCard(CardKind.DEFINITION)       { Id = 33,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Aero Shin Guard",			Subtype = CardSubtype.GetById(4), UniqueClass = "Shin Guard",		UniqueClassType =  UniqueClassType.GetById(2),	Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "STA +2 DRI +1",																			Cost = 0,	Limit = 3,	Duration = 30,	Stats = new Stats() { Dribbling = 1,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 2		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("active"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(3)	};
        public static readonly ActionCard        ACTION_CARD_02         = new ActionCard(CardKind.DEFINITION)       { Id = 200,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Captain Armband",			Subtype = CardSubtype.GetById(4), UniqueClass = "captainArmband",	UniqueClassType =  UniqueClassType.GetById(1),	Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "+2 STA/PAS. eot: if your STA > opp STA, opp discards 1",									Cost = 0,	Limit = 1,	Duration = 20,	Stats = new Stats() { Dribbling = 0,	Passing = 2,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 2		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("active"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(4)	};
        public static readonly ActionCard        ACTION_CARD_03         = new ActionCard(CardKind.DEFINITION)       { Id = 206,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Clear calls",				Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "STA +1. Remove target card in your graveyard from the game. ",							Cost = 0,	Limit = 3,	Duration = 30,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 1		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("active"),	/* "input":{"input":{Target = Target.GetByDescription({"type":"card","valid":"any"}}},		*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(1)	};
        public static readonly ActionCard        ACTION_CARD_04         = new ActionCard(CardKind.DEFINITION)       { Id = 145,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Prey on the weak",			Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "+1 STA, -1 opp STA",																		Cost = 0,	Limit = 3,	Duration = 30,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 1		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("active"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(1)	};
        public static readonly ActionCard        ACTION_CARD_05         = new ActionCard(CardKind.DEFINITION)       { Id = 116,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Rythm",						Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "+2 STA for each card played until end of turn",											Cost = 0,	Limit = 3,	Duration = 0,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("active"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(3)	};
        public static readonly ActionCard        ACTION_CARD_06         = new ActionCard(CardKind.DEFINITION)       { Id = 38,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Trainer Advice",			Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "+1 STA. Draw 1, reshuffle this card into your deck.",										Cost = 0,	Limit = 3,	Duration = 30,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 1		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("active"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(1)	};
        public static readonly ActionCard        ACTION_CARD_07         = new ActionCard(CardKind.DEFINITION)       { Id = 255,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Burden",					Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Opp gets -1 DEF for each gear he has.",													Cost = 1,	Limit = 3,	Duration = 30,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("opponent"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(1)	};
        public static readonly ActionCard        ACTION_CARD_08         = new ActionCard(CardKind.DEFINITION)       { Id = 159,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Combo: Barrier",			Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "SCO -1 to target until end of turn; additional -2 if you play another card",				Cost = 1,	Limit = 3,	Duration = 0,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = -1,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("opponent"),	/* "input":{"input":{Target = Target.GetByDescription({"type":"target","valid":"any"}}},	*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(2)	};
        public static readonly ActionCard        ACTION_CARD_09         = new ActionCard(CardKind.DEFINITION)       { Id = 152,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Combo: Marking",			Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Opp DRI -1 until end of turn; additional -2 if you play another card",					Cost = 1,	Limit = 3,	Duration = 0,	Stats = new Stats() { Dribbling = -1,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("opponent"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(1)	};
        public static readonly ActionCard        ACTION_CARD_10         = new ActionCard(CardKind.DEFINITION)       { Id = 254,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Combo: Pass Cut",			Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Opp PAS -1 until end of turn; additional -2 if you play another card",					Cost = 1,	Limit = 3,	Duration = 0,	Stats = new Stats() { Dribbling = 0,	Passing = -1,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("opponent"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(1)	};
        public static readonly ActionCard        ACTION_CARD_11         = new ActionCard(CardKind.DEFINITION)       { Id = 175,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Combo: Refill",				Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Draw 1; draw 1 more if you play another card",											Cost = 1,	Limit = 3,	Duration = 0,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("active"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(1)	};
        public static readonly ActionCard        ACTION_CARD_12         = new ActionCard(CardKind.DEFINITION)       { Id = 196,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Combo: Score",				Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Opp Goalkeeper gets GK -1 until end of turn; if you play another card, opp discards 1",	Cost = 1,	Limit = 3,	Duration = 0,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = -1,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("oppGk"),		/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(1)	};
        public static readonly ActionCard        ACTION_CARD_13         = new ActionCard(CardKind.DEFINITION)       { Id = 129,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Costly win",				Subtype = CardSubtype.GetById(2), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Opp gains: \"Discard 1 eot on round won\"",												Cost = 1,	Limit = 3,	Duration = 30,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("opponent"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(3)	};
        public static readonly ActionCard        ACTION_CARD_14         = new ActionCard(CardKind.DEFINITION)       { Id = 163,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Fatigue",					Subtype = CardSubtype.GetById(2), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Opposing player gains: \"-1 DEF for 5 turns at the end of turn\"",						Cost = 1,	Limit = 3,	Duration = 30,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("opponent"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(2)	};
        public static readonly ActionCard        ACTION_CARD_15         = new ActionCard(CardKind.DEFINITION)       { Id = 268,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Flooded Storage",			Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Put the first 2 gear cards from opp deck in their grave.",								Cost = 1,	Limit = 3,	Duration = 0,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("opponent"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(1)	};
        public static readonly ActionCard        ACTION_CARD_16         = new ActionCard(CardKind.DEFINITION)       { Id = 284,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Good Plans",				Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Draw a random Special card from your deck",												Cost = 1,	Limit = 3,	Duration = 0,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("active"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(1)	};
        public static readonly ActionCard        ACTION_CARD_17         = new ActionCard(CardKind.DEFINITION)       { Id = 120,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Good Training",				Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Shuffle 3 random cards from grave to deck",												Cost = 1,	Limit = 3,	Duration = 0,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("active"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(1)	};
        public static readonly ActionCard        ACTION_CARD_18         = new ActionCard(CardKind.DEFINITION)       { Id = 23,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Lingering pain",			Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Opp DRI -2 for 5 turns. Draw a card",														Cost = 1,	Limit = 3,	Duration = 4,	Stats = new Stats() { Dribbling = -2,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("opponent"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(2)	};
        public static readonly ActionCard        ACTION_CARD_19         = new ActionCard(CardKind.DEFINITION)       { Id = 154,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Mayhem",					Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "All players get DEF = 5",																	Cost = 1,	Limit = 3,	Duration = 30,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("all"),		/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(4)	};
        public static readonly ActionCard        ACTION_CARD_20         = new ActionCard(CardKind.DEFINITION)       { Id = 202,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Out trade",					Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Draw a card. If you play another card, opp discards 1",									Cost = 1,	Limit = 3,	Duration = 0,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("active"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(1)	};
        public static readonly ActionCard        ACTION_CARD_21         = new ActionCard(CardKind.DEFINITION)       { Id = 179,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Purge",						Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Destroy 1 random buff on opponent. Draw a card.",											Cost = 1,	Limit = 3,	Duration = 0,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("opponent"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(1)	};
        public static readonly ActionCard        ACTION_CARD_22         = new ActionCard(CardKind.DEFINITION)       { Id = 121,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Replay",					Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Unique. Return target card from your graveyard to your hand",								Cost = 1,	Limit = 1,	Duration = 0,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("active"),	/* "input":{"input":{Target = Target.GetByDescription({"type":"card","valid":"any"}}},		*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(4)	};
        public static readonly ActionCard        ACTION_CARD_23         = new ActionCard(CardKind.DEFINITION)       { Id = 122,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Routine",					Subtype = CardSubtype.GetById(3), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "+1 PAS. shuffle a card from your grave to deck on successfull pass",						Cost = 1,	Limit = 3,	Duration = 30,	Stats = new Stats() { Dribbling = 0,	Passing = 1,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("active"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(2)	};
        public static readonly ActionCard        ACTION_CARD_24         = new ActionCard(CardKind.DEFINITION)       { Id = 278,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Spirit Crush",				Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "If you score a goal this turn, Opp GK gets Goalkeeping -2",								Cost = 1,	Limit = 3,	Duration = 0,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("active"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(2)	};
        public static readonly ActionCard        ACTION_CARD_25         = new ActionCard(CardKind.DEFINITION)       { Id = 203,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Suit Up",					Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Draw a random Gear card from your deck",													Cost = 1,	Limit = 3,	Duration = 0,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("active"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(1)	};
        public static readonly ActionCard        ACTION_CARD_26         = new ActionCard(CardKind.DEFINITION)       { Id = 253,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Bash",						Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Opponent discards a card. If opp has less than 6 STA, draw two cards.",					Cost = 2,	Limit = 3,	Duration = 0,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("opponent"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(2)	};
        public static readonly ActionCard        ACTION_CARD_27         = new ActionCard(CardKind.DEFINITION)       { Id = 195,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Body check",				Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "opp DEF = opp STA",																		Cost = 2,	Limit = 3,	Duration = 0,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("opponent"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(3)	};
        public static readonly ActionCard        ACTION_CARD_28         = new ActionCard(CardKind.DEFINITION)       { Id = 279,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Cripple",					Subtype = CardSubtype.GetById(2), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Attack is stopped, attacker keeps the ball, -2 DRI/STA to opponent. Discard a card.",		Cost = 2,	Limit = 3,	Duration = 30,	Stats = new Stats() { Dribbling = -2,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = -2	},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(2),	Target = Target.GetBySign("opponent"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(3)	};
        public static readonly ActionCard        ACTION_CARD_29         = new ActionCard(CardKind.DEFINITION)       { Id = 197,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Hard spikes",				Subtype = CardSubtype.GetById(4), UniqueClass = "Spikes",			UniqueClassType =  UniqueClassType.GetById(2),	Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "DEF +1. Opp STA -1 eot when defending",													Cost = 2,	Limit = 3,	Duration = 20,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 1,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("active"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(3)	};
        public static readonly ActionCard        ACTION_CARD_30         = new ActionCard(CardKind.DEFINITION)       { Id = 186,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Hinder",					Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Opp DRI/DEF -1",																			Cost = 2,	Limit = 3,	Duration = 30,	Stats = new Stats() { Dribbling = -1,	Passing = 0,	Scoring = 0,	Defense = -1,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("opponent"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(3)	};
        public static readonly ActionCard        ACTION_CARD_31         = new ActionCard(CardKind.DEFINITION)       { Id = 140,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Lower Pass",				Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Opp PAS -1. Reshuffle this card in your deck.",											Cost = 2,	Limit = 3,	Duration = 30,	Stats = new Stats() { Dribbling = 0,	Passing = -1,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("opponent"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(2)	};
        public static readonly ActionCard        ACTION_CARD_32         = new ActionCard(CardKind.DEFINITION)       { Id = 143,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Lower Scoring",				Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Target Opp SCO -1. Reshuffle this card in your deck.",									Cost = 2,	Limit = 3,	Duration = 30,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = -1,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("opponent"),	/* "input":{"input":{Target = Target.GetByDescription({"type":"target","valid":"any"}}},	*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(2)	};
        public static readonly ActionCard        ACTION_CARD_33         = new ActionCard(CardKind.DEFINITION)       { Id = 117,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "No quarter",				Subtype = CardSubtype.GetById(2), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "opp gains: \"-1 STA eot when attacking\"",												Cost = 2,	Limit = 3,	Duration = 30,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("opponent"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(1)	};
        public static readonly ActionCard        ACTION_CARD_34         = new ActionCard(CardKind.DEFINITION)       { Id = 125,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Postpone",					Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Select a card in your graveyard: shuffle that card back in your deck, then draw 2.",		Cost = 2,	Limit = 3,	Duration = 0,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("active"),	/* "input":{"input":{Target = Target.GetByDescription({"type":"card","valid":"any"}}},		*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(2)	};
        public static readonly ActionCard        ACTION_CARD_35         = new ActionCard(CardKind.DEFINITION)       { Id = 131,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Sabotage",					Subtype = CardSubtype.GetById(2), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "-3 DRI/DEF to opp for 4 turns if he has at least one gear card",							Cost = 2,	Limit = 3,	Duration = 0,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("opponent"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(2)	};
        public static readonly ActionCard        ACTION_CARD_36         = new ActionCard(CardKind.DEFINITION)       { Id = 104,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Sponsor won't like it...",	Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Destroy all gear on opponent",															Cost = 2,	Limit = 3,	Duration = 0,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("opponent"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(3)	};
        public static readonly ActionCard        ACTION_CARD_37         = new ActionCard(CardKind.DEFINITION)       { Id = 113,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Whittle Down",				Subtype = CardSubtype.GetById(2), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "-1 opp STA each turn for 4 turns",														Cost = 2,	Limit = 3,	Duration = 3,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("opponent"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(3)	};
        public static readonly ActionCard        ACTION_CARD_38         = new ActionCard(CardKind.DEFINITION)       { Id = 105,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Combo: Breach",				Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Opp DEF -1; additional -2 until end of turn if you play another card",					Cost = 3,	Limit = 3,	Duration = 30,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = -1,	Goalkeeping = 0,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("opponent"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(2)	};
        public static readonly ActionCard        ACTION_CARD_39         = new ActionCard(CardKind.DEFINITION)       { Id = 146,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Off Balance",				Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "-1 opp STA/DRI, opp discards a card",														Cost = 3,	Limit = 3,	Duration = 30,	Stats = new Stats() { Dribbling = -1,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = 0,	Stamina = -1	},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("opponent"),	/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(2)	};
        public static readonly ActionCard        ACTION_CARD_40         = new ActionCard(CardKind.DEFINITION)       { Id = 192,	InstanceId = GetInstanceId(), Skill = 1, Image = "acPunk",	Name = "Blind",						Subtype = CardSubtype.GetById(1), UniqueClass = "",					UniqueClassType =  UniqueClassType.NONE,		Schools = new List<School>() { School.GENERIC, School.GetById(4) },	ActionText = "Opp GK gets Goalkeeping -3 until end of turn.",											Cost = 4,	Limit = 3,	Duration = 0,	Stats = new Stats() { Dribbling = 0,	Passing = 0,	Scoring = 0,	Defense = 0,	Goalkeeping = -3,	Stamina = 0		},	TargetPositions = new List<Position>() { Position.ANY },	BallCondition = CardBallCondition.GetById(0),	Target = Target.GetBySign("oppGk"),		/* "input":"",																				*/	Special = false,	Flash = false,	Rarity = Rarity.GetById(3)	};


        public static ActionCard[] GetPunkActionCards()
        {
            return (new List<ActionCard>()   {   ACTION_CARD_01
                                                ,ACTION_CARD_02
                                                ,ACTION_CARD_03
                                                ,ACTION_CARD_04
                                                ,ACTION_CARD_05
                                                ,ACTION_CARD_06
                                                ,ACTION_CARD_07
                                                ,ACTION_CARD_08
                                                ,ACTION_CARD_09
                                                ,ACTION_CARD_10
                                                ,ACTION_CARD_11
                                                ,ACTION_CARD_12
                                                ,ACTION_CARD_13
                                                ,ACTION_CARD_14
                                                ,ACTION_CARD_15
                                                ,ACTION_CARD_16
                                                ,ACTION_CARD_17
                                                ,ACTION_CARD_18
                                                ,ACTION_CARD_19
                                                ,ACTION_CARD_20
                                                ,ACTION_CARD_21
                                                ,ACTION_CARD_22
                                                ,ACTION_CARD_23
                                                ,ACTION_CARD_24
                                                ,ACTION_CARD_25
                                                ,ACTION_CARD_26
                                                ,ACTION_CARD_27
                                                ,ACTION_CARD_28
                                                ,ACTION_CARD_29
                                                ,ACTION_CARD_30
                                                ,ACTION_CARD_31
                                                ,ACTION_CARD_32
                                                ,ACTION_CARD_33
                                                ,ACTION_CARD_34
                                                ,ACTION_CARD_35
                                                ,ACTION_CARD_36
                                                ,ACTION_CARD_37
                                                ,ACTION_CARD_38
                                                ,ACTION_CARD_39
                                                ,ACTION_CARD_40
                                            }).ToArray();
        }

        public static ActionCard[] GetActionCards()
        {
            List<ActionCard> cards = new List<ActionCard>();
            cards.AddRange(GetPunkActionCards());
            return cards.ToArray();
        }

        #endregion ActionCard Helper

        #region Collection and Deck Helper

        public static Card[] GetCards()
        {
            List<Card> cards = new List<Card>();
            cards.AddRange(GetPlayersCards());
            cards.AddRange(GetActionCards());
            return cards.ToArray();
        }

        public static Deck GetDeck(Collection collection)
        {
            /* TAV 2015-12-05 Collection reference and controls removed
            Deck deck = collection.CreateEmptyDeck(1, "HELPER-DECK-01");
            */
            Deck deck = new Deck(1, "HELPER-DECK-01");
            // Formation
            deck.SetPlayerCardByPosition(Position.GOALKEEPER,   GOALKEEPER_CARD_01);
            deck.SetPlayerCardByPosition(Position.DEFENDER_01,  PLAYER_CARD_01); // voigts
            deck.SetPlayerCardByPosition(Position.DEFENDER_02,  PLAYER_CARD_02); // kim
            deck.SetPlayerCardByPosition(Position.MIDFIELD,     PLAYER_CARD_16); // Jaime Rivero
            deck.SetPlayerCardByPosition(Position.FORWARD_01,   PLAYER_CARD_06); // ubinna
            deck.SetPlayerCardByPosition(Position.FORWARD_01,   PLAYER_CARD_08); // Vinci
            deck.SetPlayerCardByPosition(Position.SUB_DEFENCE,  PLAYER_CARD_03); // dragic
            deck.SetPlayerCardByPosition(Position.SUB_MIDFIELD, PLAYER_CARD_05); // matos
            deck.SetPlayerCardByPosition(Position.SUB_FORWARD,  PLAYER_CARD_07); // metaxas
            // Action cards
            deck.AddActionCard(ACTION_CARD_01);
            deck.AddActionCard(ACTION_CARD_02);
            deck.AddActionCard(ACTION_CARD_03);
            deck.AddActionCard(ACTION_CARD_04);
            deck.AddActionCard(ACTION_CARD_05);
            deck.AddActionCard(ACTION_CARD_06);
            deck.AddActionCard(ACTION_CARD_07);
            deck.AddActionCard(ACTION_CARD_08);
            deck.AddActionCard(ACTION_CARD_09);
            deck.AddActionCard(ACTION_CARD_10);
            deck.AddActionCard(ACTION_CARD_11);
            deck.AddActionCard(ACTION_CARD_12);
            deck.AddActionCard(ACTION_CARD_13);
            deck.AddActionCard(ACTION_CARD_14);
            deck.AddActionCard(ACTION_CARD_15);
            deck.AddActionCard(ACTION_CARD_16);
            deck.AddActionCard(ACTION_CARD_17);
            deck.AddActionCard(ACTION_CARD_18);
            deck.AddActionCard(ACTION_CARD_19);
            deck.AddActionCard(ACTION_CARD_20);
            deck.AddActionCard(ACTION_CARD_21);
            deck.AddActionCard(ACTION_CARD_22);
            deck.AddActionCard(ACTION_CARD_23);
            deck.AddActionCard(ACTION_CARD_24);
            deck.AddActionCard(ACTION_CARD_25);
            deck.AddActionCard(ACTION_CARD_26);
            deck.AddActionCard(ACTION_CARD_27);
            deck.AddActionCard(ACTION_CARD_28);
            deck.AddActionCard(ACTION_CARD_29);
            deck.AddActionCard(ACTION_CARD_30);
            deck.AddActionCard(ACTION_CARD_31);
            deck.AddActionCard(ACTION_CARD_32);
            deck.AddActionCard(ACTION_CARD_33);
            deck.AddActionCard(ACTION_CARD_34);
            deck.AddActionCard(ACTION_CARD_35);
            deck.AddActionCard(ACTION_CARD_36);
            deck.AddActionCard(ACTION_CARD_37);
            deck.AddActionCard(ACTION_CARD_38);
            deck.AddActionCard(ACTION_CARD_39);
            deck.AddActionCard(ACTION_CARD_40);
            return deck;
        }

        public static Collection GetCollection()
        {
            Collection collection = new Collection();
            foreach (Card card in GetCards()) collection.AddCard(card);
            // Decks
            collection.AddDeck(GetDeck(collection));
            // collection.ActiveDeck = collection.Decks[0];
            return collection;
        }

        #endregion Collection and Deck Helper

    }
}
