﻿using System;
using SimpleJSON;
using Newtonsoft.Json.Linq;

namespace sos.utils
{

    /// <summary>
    /// Utility functions to handle JSON data
    /// </summary>
    public static class JsonUtils
    {

        #region JSON.net

        /// <summary>
        /// Check if a JSOn.Net JObject contains a property
        /// </summary>
        /// <param name="jsonObject">JSON Object to chck</param>
        /// <param name="propertyName">name of the property to check</param>
        /// <returns>True if the property exists, event if the property is valued "null"</returns>
        public static bool HasProperty(JObject jsonObject, string propertyName)
        {
            return jsonObject[propertyName] != null;
        }

        /// <summary>
        /// Check if a JSOn.Net JObject contains a property and is not valued null
        /// </summary>
        /// <param name="jsonObject">JSON Object to chck</param>
        /// <param name="propertyName">name of the property to check</param>
        /// <returns>True if the property exists and is not valued null</returns>
        public static bool HasNotNullProperty(JObject jsonObject, string propertyName)
        {
            return HasProperty(jsonObject, propertyName) && jsonObject[propertyName].Type != JTokenType.Null;
        }

        /// <summary>
        /// Checks if a JSOn.Net JObject contains a property of type "array"
        /// </summary>
        /// <param name="jsonObject"></param>
        /// <param name="propertyName"></param>
        /// <returns>Terue if the property is a valid JSON array</returns>
        public static bool IsArrayProperty(JObject jsonObject, string propertyName)
        {
            return HasProperty(jsonObject, propertyName) && jsonObject[propertyName].Type == JTokenType.Array;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="raw"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static uint ValueAsUInt(JObject raw, string propertyName)
        {
            try
            {
                return raw[propertyName].Value<uint>();
            }
            catch (Exception)
            {
                throw new JsonFormatException(propertyName, "uint", string.Format("Property {0} format is not [uint]", propertyName));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="raw"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static int ValueAsInt(JObject raw, string propertyName)
        {
            try
            {
                return raw[propertyName].Value<int>();
            }
            catch (Exception)
            {
                throw new JsonFormatException(propertyName, "int", string.Format("Property {0} format is not [int]", propertyName));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="raw"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static DateTime ValueAsDateTime(JObject raw, string propertyName)
        {
            try
            {
                return raw[propertyName].Value<DateTime>();
            }
            catch (Exception)
            {
                throw new JsonFormatException(propertyName, "datetime", string.Format("Property {0} format is not [Datetime]", propertyName));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="raw"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static bool ValueAsBool(JObject raw, string propertyName)
        {
            try
            {
                return raw[propertyName].Value<bool>();
            }
            catch (Exception)
            {
                throw new JsonFormatException(propertyName, "string", string.Format("Property {0} format is not [bool]", propertyName));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="raw"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static string ValueAsString(JObject raw, string propertyName)
        {
            try
            {
                return raw[propertyName].Value<string>();
            }
            catch (Exception)
            {
                throw new JsonFormatException(propertyName, "string", string.Format("Property {0} format is not [string]", propertyName));
            }
        }

        /// <summary>
        /// Returns a property of the given JObject as a JArray
        /// </summary>
        /// <param name="jsonObject">JSON Object to chck</param>
        /// <param name="propertyName">Name of the property to retrieve as array</param>
        /// <returns></returns>
        public static JArray PropertyAsJArray(JObject raw, string propertyName)
        {
            try
            {
                JToken token = raw[propertyName];
                if (token.Type != JTokenType.Array) throw new JsonFormatException(propertyName, "string", string.Format("Property {0} is not a JSON array", propertyName));
                return token as JArray;
            }
            catch (Exception)
            {
                throw new JsonFormatException(propertyName, "string", string.Format("Property {0} is not a JSON array", propertyName));
            }
        }

        /// <summary>
        /// Returns a property of the given JObject as a JObject
        /// </summary>
        /// <param name="jsonObject">JSON Object to chck</param>
        /// <param name="propertyName">Name of the property to retrieve as array</param>
        /// <returns></returns>
        public static JObject PropertyAsJObject(JObject raw, string propertyName)
        {
            try
            {
                JToken token = raw[propertyName];
                if (token.Type != JTokenType.Object) throw new JsonFormatException(propertyName, "string", string.Format("Property {0} is not a JSON object", propertyName));
                return token as JObject;
            }
            catch (Exception)
            {
                throw new JsonFormatException(propertyName, "string", string.Format("Property {0} is not a JSON array", propertyName));
            }
        }

        #endregion JSON.net

        #region Simple JSON

        /// <summary>
        /// SimpleJSON library atring representetion for null properties.
        /// The library doesn't handle correctly null properies in JSON string, assigning this string instead of a null reference
        /// </summary>
        public const string     SIMPLE_JSON_NULL_STRING         = "\"null\"";

        /// <summary>
        /// Decodes the given string from JSON encoding to a JSON object
        /// </summary>
        /// <param name="json">JSON string to decode</param>
        /// <returns>JSON object decoded from the given string</returns>
        public static JSONNode Decode(string json)
        {
            return JSON.Parse(json);
        }

        /// <summary>
        /// Checks if the given object is a valid JSON object
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool IsJSON(object obj)
        {
            return obj is JSONNode;
        }

        #endregion Simple JSON

    }

    /// <summary>
    /// Exception used to signal error decoding JSON data into native .Net datatypes. 
    /// </summary>
    public class JsonFormatException : Exception
    {

		#region FIELDS

        private string _property;
        private string _datatype;

        #endregion FIELDS

        #region CONSTRUCTORS

        public JsonFormatException(string property, string datatype) : base()
        {
            _property = property;
            _datatype = datatype;
        }

        public JsonFormatException(string property, string datatype, string message) : base(message)
        {
            _property = property;
            _datatype = datatype;
        }

        public JsonFormatException(string property, string datatype, string message, Exception cause) : base(message, cause)
        {
            _property = property;
            _datatype = datatype;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public string Property
        {
            get { return _property; }
        }

        public string Datatype
        {
            get { return _datatype; }
        }

        #endregion PROPERTIES

    }

}
