﻿using System;

namespace sos.utils
{
    /// <summary>
    /// SoS Game default values
    /// </summary>
    public static class Defaults
    {

        /// <summary>
        /// Standard invalid index, to use to signal and unknown indexes
        /// </summary>
		public const int        INVALID_INDEX								= -1;

        /// <summary>
        /// Standard invalid index, to use to signal and unknown indexes
        /// </summary>
		public const uint       INVALID_ID								    = 0;
		        
        /// <summary>
        /// Standard invalid session token, used to track unknown and invalid session token variables
        /// </summary>
		public const string     INVALID_SESSION_TOKEN					    = "{invalid_session}";
		
        /// <summary>
        /// Standard invalid token, used to track unknown and invalid game token variables
        /// </summary>
		public const string     INVALID_GAME_TOKEN						    = "{invalid_game_token}";

        /// <summary>
        /// Standard invalid call error code, used to track not set error codes in CallResult.ErrorCode
        /// </summary>
		public const int        INVALID_CALL_ERROR_CODE					= 0;

        /// <summary>
        /// Standard invalid call error message, used to track not set error codes in CallResult.ErrorMessage
        /// </summary>
		public const string     INVALID_CALL_ERROR_MESSAGE					= "";

        /// <summary>
        /// Standard invalid date, to use to signal an unknown date 
        /// </summary>
		public static DateTime  INVALID_DATE    						= new DateTime(1900, 0, 0, 0, 0, 0, 0);

    }
}
