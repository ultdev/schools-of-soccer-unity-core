﻿using Newtonsoft.Json.Linq;
using sos.core.quest;
using sos.data.DTO;
using sos.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.data.parsers
{
    /// <summary>
    /// 
    /// </summary>
    public class QuestParser : DataParser
    {

        #region CONSTANTS

        // Login properties
        protected const string      JSON_PROPERTY_LOGIN_USER_QUESTS					= "userQuestList";

        // QuestDTO properties
        protected const	string	    JSON_PROPERTY_QUEST_DTO_ID                      = "id";
        protected const	string	    JSON_PROPERTY_QUEST_DTO_NAME                    = "name";
        protected const	string	    JSON_PROPERTY_QUEST_DTO_IMAGE                   = "image";
        protected const	string	    JSON_PROPERTY_QUEST_DTO_DESCRIPTION             = "description";
        protected const	string	    JSON_PROPERTY_QUEST_DTO_REWARD_TEXT             = "reward_text";
        protected const	string	    JSON_PROPERTY_QUEST_DTO_DAILY                   = "daily";

        // UserQuestDTO properties
        protected const	string	    JSON_PROPERTY_USER_QUEST_DTO_ID                 = "id";
        protected const	string	    JSON_PROPERTY_USER_QUEST_DTO_INSTANCE_ID        = "questInstanceId";
        protected const	string	    JSON_PROPERTY_USER_QUEST_DTO_STATUS             = "status";
        protected const	string	    JSON_PROPERTY_USER_QUEST_DTO_ITEM_COUNT         = "itemCount";

        #endregion CONSTANTS

        #region FIELDS

        #endregion FIELDS

        #region CONSTRUCTORS

        #endregion CONSTRUCTORS

        #region PROPERTIES

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #region Validation

        protected void ValidateQuestDTO(JObject raw)
        {
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_QUEST_DTO_ID))                ThrowMissingPropertyException(GetType().Name, "QuestDTO", JSON_PROPERTY_QUEST_DTO_ID);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_QUEST_DTO_NAME))              ThrowMissingPropertyException(GetType().Name, "QuestDTO", JSON_PROPERTY_QUEST_DTO_NAME);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_QUEST_DTO_IMAGE))             ThrowMissingPropertyException(GetType().Name, "QuestDTO", JSON_PROPERTY_QUEST_DTO_IMAGE);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_QUEST_DTO_DESCRIPTION))       ThrowMissingPropertyException(GetType().Name, "QuestDTO", JSON_PROPERTY_QUEST_DTO_DESCRIPTION);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_QUEST_DTO_REWARD_TEXT))       ThrowMissingPropertyException(GetType().Name, "QuestDTO", JSON_PROPERTY_QUEST_DTO_REWARD_TEXT);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_QUEST_DTO_DAILY))             ThrowMissingPropertyException(GetType().Name, "QuestDTO", JSON_PROPERTY_QUEST_DTO_DAILY);
        }

        protected void ValidateUserQuestDTO(JObject raw)
        {
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_USER_QUEST_DTO_ID))           ThrowMissingPropertyException(GetType().Name, "UserQuestDTO", JSON_PROPERTY_USER_QUEST_DTO_ID);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_USER_QUEST_DTO_INSTANCE_ID))  ThrowMissingPropertyException(GetType().Name, "UserQuestDTO", JSON_PROPERTY_USER_QUEST_DTO_INSTANCE_ID);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_USER_QUEST_DTO_STATUS))       ThrowMissingPropertyException(GetType().Name, "UserQuestDTO", JSON_PROPERTY_USER_QUEST_DTO_STATUS);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_USER_QUEST_DTO_ITEM_COUNT))   ThrowMissingPropertyException(GetType().Name, "UserQuestDTO", JSON_PROPERTY_USER_QUEST_DTO_ITEM_COUNT);
        }

        #endregion Validation

        #region Data filling

        /// <summary>
        /// Sets QuestDTO properties and contents from a raw JSON object
        /// </summary>
        /// <param name="raw">Raw JSON object to read</param>
        /// <param name="deck">QuestDTO object to fill</param>
        protected void FillQuestDTO(JObject raw, QuestDTO quest)
        {
            try
            {
                // Data retrieve
		        var id          = JsonUtils.ValueAsUInt(raw, JSON_PROPERTY_QUEST_DTO_ID);
		        var name        = JsonUtils.ValueAsString(raw, JSON_PROPERTY_QUEST_DTO_NAME);
		        var description = JsonUtils.ValueAsString(raw, JSON_PROPERTY_QUEST_DTO_DESCRIPTION);
		        var rewardText  = JsonUtils.ValueAsString(raw, JSON_PROPERTY_QUEST_DTO_REWARD_TEXT);
		        var daily       = JsonUtils.ValueAsUInt(raw, JSON_PROPERTY_QUEST_DTO_DAILY);
		        var image       = JsonUtils.ValueAsString(raw, JSON_PROPERTY_QUEST_DTO_IMAGE);
                // Filling
                quest.Id            = id;
                quest.Name          = name;
                quest.Description   = description;
                quest.RewardText    = rewardText;
                quest.Daily         = daily;
                quest.Image         = image;
            }
            catch (JsonFormatException ex)
            {
                ThrowWrongDatatypeException(GetType().Name, "QuestDTO", ex.Property, ex.Datatype);
            }
            catch (DataParserException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                ThrowUnexpectedErrorException(GetType().Name, "FillQuestDTO", "QuestDTO", ex);
            }
        }

        /// <summary>
        /// Sets UserQuestDTO properties and contents from a raw JSON object
        /// </summary>
        /// <param name="raw">Raw JSON object to read</param>
        /// <param name="deck">UserQuestDTO object to fill</param>
        protected void FillUserQuestDTO(JObject raw, UserQuestDTO userQuest)
        {
            try
            {
                // Data retrieve
		        var id          = JsonUtils.ValueAsUInt(raw, JSON_PROPERTY_USER_QUEST_DTO_ID);
		        var instanceId  = JsonUtils.ValueAsUInt(raw, JSON_PROPERTY_USER_QUEST_DTO_INSTANCE_ID);
		        var status      = JsonUtils.ValueAsInt(raw, JSON_PROPERTY_USER_QUEST_DTO_STATUS);
		        var itemCount   = JsonUtils.ValueAsUInt(raw, JSON_PROPERTY_USER_QUEST_DTO_ITEM_COUNT);
                // Filling
                userQuest.Id            = id;
                userQuest.InstanceId    = instanceId;
                userQuest.Status        = status;
                userQuest.ItemCount     = itemCount;
            }
            catch (JsonFormatException ex)
            {
                ThrowWrongDatatypeException(GetType().Name, "UserQuestDTO", ex.Property, ex.Datatype);
            }
            catch (DataParserException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                ThrowUnexpectedErrorException(GetType().Name, "FillQuestDTO", "UserQuestDTO", ex);
            }
        }

        #endregion Data filling

        #region Conversion

        private QuestDTO FromJObjectToQuestDTO(JObject raw)
        {
            // Data validation
            ValidateQuestDTO(raw);
            // Instance creation
            QuestDTO quest = new QuestDTO();
            // Data loading
            FillQuestDTO(raw, quest);

            return quest;
        }

        private UserQuestDTO FromJObjectToUserQuestDTO(JObject raw)
        {
            // Data validation
            ValidateUserQuestDTO(raw);
            // Instance creation
            UserQuestDTO userQuest = new UserQuestDTO();
            // Data loading
            FillUserQuestDTO(raw, userQuest);

            return userQuest;
        }

        #endregion Conversion

        #region Parsing

        protected UserQuestDTO[] ParseUserQuestsFromJarray(JArray raw_quests)
        {
            // Card list
            List<UserQuestDTO> quests = new List<UserQuestDTO>();
            // Safe exec
            try
            {
                // Loop over json array
                foreach (var raw_quest in raw_quests.Children<JObject>())
                {
                    quests.Add(FromJObjectToUserQuestDTO(raw_quest));
                }
            }
            catch (DataParserException ex)
            {
                // Rethrow
                throw ex;
            }
            catch (Exception ex)
            {
                // ThrowMissingPropertyException unexpected error
                ThrowUnexpectedErrorException(GetType().Name, "ParseQuests", "UserQuestDTO[]", raw_quests.ToString(Newtonsoft.Json.Formatting.None), ex);
            }
            return quests.ToArray();
        }

        #endregion Parsing

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        public QuestDTO[] ParseQuests(string json)
        {
            // Card list
            List<QuestDTO> quests = new List<QuestDTO>();
            // Safe exec
            try
            {
                // JSON-Net Objects
                var raw =  JArray.Parse(json);
                // Loop over json array
                foreach (var raw_quest in raw.Children<JObject>())
                {
                    quests.Add(FromJObjectToQuestDTO(raw_quest));
                }
            }
            catch (DataParserException ex)
            {
                // Rethrow
                throw ex;
            }
            catch (Exception ex)
            {
                // ThrowMissingPropertyException unexpected error
                ThrowUnexpectedErrorException(GetType().Name, "ParseQuests", "QuestDTO[]", json, ex);
            }
            return quests.ToArray();
        }

        public UserQuestDTO[] ParseUserQuests(string json)
        {
            // Card list
            List<UserQuestDTO> quests = new List<UserQuestDTO>();
            // Safe exec
            try
            {
                // JSON-Net Objects
                var raw = JArray.Parse(json);
                // return
                return ParseUserQuestsFromJarray(raw);
            }
            catch (Exception ex)
            {
                // ThrowMissingPropertyException unexpected error
                ThrowUnexpectedErrorException(GetType().Name, "ParseQuests", "UserQuestDTO[]", json, ex);
            }
            return quests.ToArray();
        }

        public UserQuestDTO[] ParseUserQuestsFromLoginData(string json)
        {
            // Quests DTO list
            UserQuestDTO[] quests = null;
            // Safe exec
            try
            {
                // JSON.Net Objects
                JObject raw = JObject.Parse(json);
                // Check for decks properties
                if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_LOGIN_USER_QUESTS)) ThrowMissingPropertyException(GetType().Name, "UserQuestDTO[]", JSON_PROPERTY_LOGIN_USER_QUESTS, json);
                // Retrieve quests array
                var raw_quests = JsonUtils.PropertyAsJArray(raw, JSON_PROPERTY_LOGIN_USER_QUESTS);
                // Quests parsing
                quests = ParseUserQuestsFromJarray(raw_quests);
            }
            catch (DataParserException ex)
            {
                // Rethrow
                throw ex;
            }
            catch (Exception ex)
            {
                // ThrowMissingPropertyException unexpected error
                ThrowUnexpectedErrorException(GetType().Name, "ParseDecksFromLoginData", "DeckDTO[]", json, ex);
            }
            return quests;
        }

		#endregion PUBLIC METHODS

    }
}
