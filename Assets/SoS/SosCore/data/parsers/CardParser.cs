﻿using Newtonsoft.Json.Linq;
using sos.core;
using sos.core.cards;
using sos.data.DTO;
using sos.data.providers.card;
using sos.logging;
using sos.utils;
using System;
using System.Collections.Generic;

namespace sos.data.parsers.card
{
    /// <summary>
    /// Parser used to convert from JSON to a sos.core.cards classes
    /// </summary>
    public class CardParser : DataParser
    {

		#region CONSTANTS


        // JSON PROPERTIES

        // CardDefinition properties
        protected const string      JSON_PROPERTY_CARD_DEFINITIONS_CARD_LIST            = "cardList";

        // Login properties
        protected const string      JSON_PROPERTY_LOGIN_COLLECTION					    = "collection";
        protected const string      JSON_PROPERTY_LOGIN_DECKS					        = "decks";
        protected const string      JSON_PROPERTY_LOGIN_DECKS_LIST					    = "list";
        protected const string      JSON_PROPERTY_LOGIN_DECKS_DECK_COUNT			    = "deckCount";

        // Collection properties
        protected const string      JSON_PROPERTY_COLLECTION_CARD_DTO_ID                = "id";
        protected const string      JSON_PROPERTY_COLLECTION_CARD_DTO_INSTANCE_LIST     = "instanceList";        

        // Deck properties
        protected const string      JSON_PROPERTY_DECK_DTO_ID					        = "id";
        protected const string      JSON_PROPERTY_DECK_DTO_NAME					        = "name";
        protected const string      JSON_PROPERTY_DECK_DTO_ACTIVE 				        = "active";
        protected const string      JSON_PROPERTY_DECK_DTO_SINGLE_SCHOOL			    = "singleSchool";
        protected const string      JSON_PROPERTY_DECK_DTO_ACTIONCARDS			        = "actionCards";
		protected const string      JSON_PROPERTY_DECK_DTO_FORMATION				    = "formation";
        protected const string      JSON_PROPERTY_DECK_DTO_FORMATION_GK_PLAYER          = "gk";
        protected const string      JSON_PROPERTY_DECK_DTO_FORMATION_D1_PLAYER          = "d1"; 
        protected const string      JSON_PROPERTY_DECK_DTO_FORMATION_D2_PLAYER          = "d2"; 
        protected const string      JSON_PROPERTY_DECK_DTO_FORMATION_MF_PLAYER          = "mf"; 
        protected const string      JSON_PROPERTY_DECK_DTO_FORMATION_F1_PLAYER          = "f1"; 
        protected const string      JSON_PROPERTY_DECK_DTO_FORMATION_F2_PLAYER          = "f2"; 
        protected const string      JSON_PROPERTY_DECK_DTO_FORMATION_SUB_DF_PLAYER      = "sub_df"; 
        protected const string      JSON_PROPERTY_DECK_DTO_FORMATION_SUB_MF_PLAYER      = "sub_mf"; 
        protected const string      JSON_PROPERTY_DECK_DTO_FORMATION_SUB_FW_PLAYER      = "sub_fw"; 

		// Card properties
		protected const string      JSON_PROPERTY_CARD_ID			                    = "cardId";
		protected const string	    JSON_PROPERTY_CARD_INSTANCE_ID		                = "instanceId";
		protected const string	    JSON_PROPERTY_CARD_NAME				                = "name";
		protected const string	    JSON_PROPERTY_CARD_CARDTYPE			                = "type";
		protected const string	    JSON_PROPERTY_CARD_CARDSUBTYPE		                = "subtype";
		protected const string	    JSON_PROPERTY_CARD_CARDSET			                = "";
		protected const string	    JSON_PROPERTY_CARD_IMAGE				            = "image";
		protected const string	    JSON_PROPERTY_CARD_LIMIT				            = "deckLimit";
		protected const string	    JSON_PROPERTY_CARD_ROLE				                = "role";
		protected const string	    JSON_PROPERTY_CARD_SCHOOL			                = "schoolId";
		protected const string	    JSON_PROPERTY_CARD_DURATION			                = "duration";
		protected const string	    JSON_PROPERTY_CARD_COST				                = "cost";
		protected const string	    JSON_PROPERTY_CARD_ACTIONTEXT		                = "actionText";
		protected const string	    JSON_PROPERTY_CARD_SKILL				            = "skill";
		protected const	string	    JSON_PROPERTY_CARD_RARITY			                = "rarity";

        // ActionCard properties
		protected const	string	    JSON_PROPERTY_ACTIONCARD_TARGET_POSITIONS		    = "fieldZone";
		protected const	string	    JSON_PROPERTY_ACTIONCARD_BALL_CONDITION			    = "ballStatus";
		protected const	string	    JSON_PROPERTY_ACTIONCARD_TARGET					    = "target";
		protected const	string	    JSON_PROPERTY_ACTIONCARD_INPUT					    = "input";
		protected const	string	    JSON_PROPERTY_ACTIONCARD_INPUT_TARGET			    = "target";
		protected const	string	    JSON_PROPERTY_ACTIONCARD_INPUT_TARGET_TYPE		    = "type";
		protected const	string	    JSON_PROPERTY_ACTIONCARD_UNIQUE_CLASS			    = "uniqueClass";
		protected const	string	    JSON_PROPERTY_ACTIONCARD_UNIQUE_CLASS_TYPE		    = "uniqueClassType";
		protected const	string	    JSON_PROPERTY_ACTIONCARD_SPECIAL				    = "special";
		protected const	string	    JSON_PROPERTY_ACTIONCARD_FLASH					    = "flash";

		// ActionCard CardTargetInput properties
		protected const	string	    JSON_PROPERTY_CARDTARGETINPUT_VALID			        = "valid";  // riferimento specifico al target in base alla tipologia di inpuc

        // Stats properties
		protected const	string	    JSON_PROPERTY_STATS_STAMINA		                    = "stamina";
		protected const	string	    JSON_PROPERTY_STATS_DRIBLING		                = "dribbling";
		protected const	string	    JSON_PROPERTY_STATS_PASSING		                    = "passing";
		protected const	string	    JSON_PROPERTY_STATS_SCORING		                    = "scoring";
		protected const	string	    JSON_PROPERTY_STATS_DEFENCE		                    = "defense";
		protected const	string	    JSON_PROPERTY_STATS_GOALKEEPING	                    = "goalkeeping";

        #endregion CONSTANTS

        #region FIELDS

        #endregion FIELDS

        #region CONSTRUCTORS

        #endregion CONSTRUCTORS

        #region PROPERTIES

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #region Validation

        protected void ValidateCardData(JObject raw)
        {
            // Validation
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_CARD_ID))             ThrowMissingPropertyException(GetType().Name, "Card", JSON_PROPERTY_CARD_ID);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_CARD_LIMIT))          ThrowMissingPropertyException(GetType().Name, "Card", JSON_PROPERTY_CARD_LIMIT);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_CARD_NAME))           ThrowMissingPropertyException(GetType().Name, "Card", JSON_PROPERTY_CARD_NAME);
        }

        protected void ValidatePlayerCardData(JObject raw)
        {
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_CARD_ROLE))           ThrowMissingPropertyException(GetType().Name, "PlayerCard", JSON_PROPERTY_CARD_ROLE);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_CARD_SCHOOL))         ThrowMissingPropertyException(GetType().Name, "PlayerCard", JSON_PROPERTY_CARD_SCHOOL);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_CARD_ACTIONTEXT))     ThrowMissingPropertyException(GetType().Name, "PlayerCard", JSON_PROPERTY_CARD_ACTIONTEXT);
        }

        protected void ValidateActionCardData(JObject raw)
        {
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_CARD_ROLE))           ThrowMissingPropertyException(GetType().Name, "ActionCard", JSON_PROPERTY_CARD_ROLE);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_CARD_SCHOOL))         ThrowMissingPropertyException(GetType().Name, "ActionCard", JSON_PROPERTY_CARD_SCHOOL);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_CARD_ACTIONTEXT))     ThrowMissingPropertyException(GetType().Name, "ActionCard", JSON_PROPERTY_CARD_ACTIONTEXT);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_CARD_DURATION))       ThrowMissingPropertyException(GetType().Name, "ActionCard", JSON_PROPERTY_CARD_DURATION);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_CARD_COST))           ThrowMissingPropertyException(GetType().Name, "ActionCard", JSON_PROPERTY_CARD_COST);
        }

        protected void ValidateStatsData(JObject raw)
        {
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_STATS_STAMINA))       ThrowMissingPropertyException(GetType().Name, "Stats", JSON_PROPERTY_STATS_STAMINA);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_STATS_DRIBLING))      ThrowMissingPropertyException(GetType().Name, "Stats", JSON_PROPERTY_STATS_DRIBLING);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_STATS_PASSING))       ThrowMissingPropertyException(GetType().Name, "Stats", JSON_PROPERTY_STATS_PASSING);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_STATS_SCORING))       ThrowMissingPropertyException(GetType().Name, "Stats", JSON_PROPERTY_STATS_SCORING);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_STATS_DEFENCE))       ThrowMissingPropertyException(GetType().Name, "Stats", JSON_PROPERTY_STATS_DEFENCE);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_STATS_GOALKEEPING))   ThrowMissingPropertyException(GetType().Name, "Stats", JSON_PROPERTY_STATS_GOALKEEPING);
        }

        protected void ValidateCollectionCardDTO(JObject raw)
        {
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_COLLECTION_CARD_DTO_ID))              ThrowMissingPropertyException(GetType().Name, "CollectionCardDTO", JSON_PROPERTY_COLLECTION_CARD_DTO_ID);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_COLLECTION_CARD_DTO_INSTANCE_LIST))   ThrowMissingPropertyException(GetType().Name, "CollectionCardDTO", JSON_PROPERTY_COLLECTION_CARD_DTO_INSTANCE_LIST);
        }

        protected void ValidateDeckDTO(JObject raw)
        {
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_DECK_DTO_ID))                     ThrowMissingPropertyException(GetType().Name, "DeckDTO", JSON_PROPERTY_DECK_DTO_ID);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_DECK_DTO_NAME))                   ThrowMissingPropertyException(GetType().Name, "DeckDTO", JSON_PROPERTY_DECK_DTO_NAME);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_DECK_DTO_ACTIVE))                 ThrowMissingPropertyException(GetType().Name, "DeckDTO", JSON_PROPERTY_DECK_DTO_ACTIVE);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_DECK_DTO_SINGLE_SCHOOL))          ThrowMissingPropertyException(GetType().Name, "DeckDTO", JSON_PROPERTY_DECK_DTO_SINGLE_SCHOOL);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_DECK_DTO_ACTIONCARDS))            ThrowMissingPropertyException(GetType().Name, "DeckDTO", JSON_PROPERTY_DECK_DTO_ACTIONCARDS);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_DECK_DTO_FORMATION))              ThrowMissingPropertyException(GetType().Name, "DeckDTO", JSON_PROPERTY_DECK_DTO_FORMATION);
            // Validate formation
            ValidateDeckDTOFormation(JsonUtils.PropertyAsJObject(raw, JSON_PROPERTY_DECK_DTO_FORMATION));
        }

        protected void ValidateDeckDTOFormation(JObject raw)
        {
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_DECK_DTO_FORMATION_GK_PLAYER))        ThrowMissingPropertyException(GetType().Name, "DeckDTO", JSON_PROPERTY_DECK_DTO_FORMATION_GK_PLAYER);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_DECK_DTO_FORMATION_D1_PLAYER))        ThrowMissingPropertyException(GetType().Name, "DeckDTO", JSON_PROPERTY_DECK_DTO_FORMATION_D1_PLAYER);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_DECK_DTO_FORMATION_D2_PLAYER))        ThrowMissingPropertyException(GetType().Name, "DeckDTO", JSON_PROPERTY_DECK_DTO_FORMATION_D2_PLAYER);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_DECK_DTO_FORMATION_MF_PLAYER))        ThrowMissingPropertyException(GetType().Name, "DeckDTO", JSON_PROPERTY_DECK_DTO_FORMATION_MF_PLAYER);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_DECK_DTO_FORMATION_F1_PLAYER))        ThrowMissingPropertyException(GetType().Name, "DeckDTO", JSON_PROPERTY_DECK_DTO_FORMATION_F1_PLAYER);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_DECK_DTO_FORMATION_F2_PLAYER))        ThrowMissingPropertyException(GetType().Name, "DeckDTO", JSON_PROPERTY_DECK_DTO_FORMATION_F2_PLAYER);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_DECK_DTO_FORMATION_SUB_DF_PLAYER))    ThrowMissingPropertyException(GetType().Name, "DeckDTO", JSON_PROPERTY_DECK_DTO_FORMATION_SUB_DF_PLAYER);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_DECK_DTO_FORMATION_SUB_MF_PLAYER))    ThrowMissingPropertyException(GetType().Name, "DeckDTO", JSON_PROPERTY_DECK_DTO_FORMATION_SUB_MF_PLAYER);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_DECK_DTO_FORMATION_SUB_FW_PLAYER))    ThrowMissingPropertyException(GetType().Name, "DeckDTO", JSON_PROPERTY_DECK_DTO_FORMATION_SUB_FW_PLAYER);
        }

        #endregion Validation

        #region Data filling

        /// <summary>
        /// Sets the base common attributes contained in every card
        /// </summary>
        /// <param name="raw">Raw JSON object to read</param>
        /// <param name="card">Card object to fill</param>
        protected void FillCardData(JObject raw, Card card)
        {
            try
            {
                // Data retrieve
                uint id         = JsonUtils.ValueAsUInt(raw, JSON_PROPERTY_CARD_ID);
                string name     = JsonUtils.ValueAsString(raw, JSON_PROPERTY_CARD_NAME);
                uint limit      = JsonUtils.ValueAsUInt(raw, JSON_PROPERTY_CARD_LIMIT);
                string image    = JsonUtils.HasProperty(raw, JSON_PROPERTY_CARD_IMAGE)          ? JsonUtils.ValueAsString(raw, JSON_PROPERTY_CARD_IMAGE) : string.Empty;
                int subtype     = JsonUtils.HasProperty(raw, JSON_PROPERTY_CARD_CARDSUBTYPE)    ? JsonUtils.ValueAsInt(raw, JSON_PROPERTY_CARD_CARDSUBTYPE) : Defaults.INVALID_INDEX;
                uint skill      = JsonUtils.HasProperty(raw, JSON_PROPERTY_CARD_SKILL)          ? JsonUtils.ValueAsUInt(raw, JSON_PROPERTY_CARD_SKILL) : card.Skill;
                int rarity      = JsonUtils.HasProperty(raw, JSON_PROPERTY_CARD_RARITY)         ? JsonUtils.ValueAsInt(raw, JSON_PROPERTY_CARD_RARITY) : Defaults.INVALID_INDEX;
                // Filling
                card.Id         = id;
                card.Name       = name;
                card.Limit      = limit;
                card.Image      = image;
                card.Subtype    = subtype != Defaults.INVALID_INDEX ? CardSubtype.GetById(subtype) : card.Subtype;
                card.Skill      = skill;
                card.Rarity     = rarity != Defaults.INVALID_INDEX ? Rarity.GetById(rarity) : card.Rarity;
            }
            catch (JsonFormatException ex)
            {
                ThrowWrongDatatypeException(GetType().Name, "FillCardData", ex.Property, ex.Datatype);
            }
            catch (DataParserException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                ThrowUnexpectedErrorException(GetType().Name, "FillCardData", "Card", ex);
            }
        }

        /// <summary>
        /// Sets PlayerCards properties from a raw JSON object
        /// </summary>
        /// <param name="raw">Raw JSON object to read</param>
        /// <param name="card">PlayerCard object to fill</param>
        protected void FillActionCardData(JObject raw, ActionCard card)
        {
            try
            {
                // Data retrieve
                var role            = JsonUtils.HasNotNullProperty(raw, JSON_PROPERTY_CARD_ROLE)    ? JsonUtils.ValueAsString(raw, JSON_PROPERTY_CARD_ROLE) 
                                                                                                    : Role.NONE.Sign;
                var actionText      = JsonUtils.ValueAsString(raw, JSON_PROPERTY_CARD_ACTIONTEXT);
                var cost            = JsonUtils.ValueAsInt(raw, JSON_PROPERTY_CARD_COST);
                var duration        = JsonUtils.ValueAsInt(raw, JSON_PROPERTY_CARD_DURATION);
                var special         = JsonUtils.ValueAsInt(raw, JSON_PROPERTY_ACTIONCARD_SPECIAL) == 1;
                var flash           = JsonUtils.ValueAsInt(raw, JSON_PROPERTY_ACTIONCARD_FLASH) == 1;
                var uniqueClass     = JsonUtils.HasNotNullProperty(raw, JSON_PROPERTY_ACTIONCARD_UNIQUE_CLASS)      ? JsonUtils.ValueAsString(raw, JSON_PROPERTY_ACTIONCARD_UNIQUE_CLASS) 
                                                                                                                    : string.Empty;
                var uniqueClassType = JsonUtils.HasNotNullProperty(raw, JSON_PROPERTY_ACTIONCARD_UNIQUE_CLASS_TYPE) ? JsonUtils.ValueAsInt(raw, JSON_PROPERTY_ACTIONCARD_UNIQUE_CLASS_TYPE) 
                                                                                                                    : UniqueClassType.NONE.Id;
                var ballCondition   = JsonUtils.HasProperty(raw, JSON_PROPERTY_ACTIONCARD_BALL_CONDITION)           ? JsonUtils.ValueAsInt(raw, JSON_PROPERTY_ACTIONCARD_BALL_CONDITION) 
                                                                                                                    : CardBallCondition.ALWAYS.Id;
                var target          = JsonUtils.HasProperty(raw, JSON_PROPERTY_ACTIONCARD_TARGET)   ? JsonUtils.ValueAsString(raw, JSON_PROPERTY_ACTIONCARD_TARGET) 
                                                                                                    : Target.NONE.Sign;
                var targetInput     = JsonUtils.HasProperty(raw, JSON_PROPERTY_ACTIONCARD_INPUT)    ? raw[JSON_PROPERTY_ACTIONCARD_INPUT] 
                                                                                                    : null;
                // School parsing
                List<School> schools    = new List<School>();
                if (JsonUtils.IsArrayProperty(raw, JSON_PROPERTY_CARD_SCHOOL))
                {
                    // Loop over schools id
                    foreach (var item in raw[JSON_PROPERTY_CARD_SCHOOL].Children())
                    {
                        var shooolId = item.Value<int>();
                        schools.Add(School.GetById(shooolId));
                    }
                }
                else
                {
                    // Adds a single school to the list
                    int shooolId = JsonUtils.ValueAsInt(raw, JSON_PROPERTY_CARD_SCHOOL);
                    schools.Add(School.GetById(shooolId));
                }
                // Target positions parsing
                List<Position> targetPositions = new List<Position>();
                if (JsonUtils.HasProperty(raw, JSON_PROPERTY_ACTIONCARD_TARGET_POSITIONS))
                {
                    if (JsonUtils.IsArrayProperty(raw, JSON_PROPERTY_ACTIONCARD_TARGET_POSITIONS))
                    {
                        // Loop over positions
                        foreach (var item in raw[JSON_PROPERTY_ACTIONCARD_TARGET_POSITIONS].Children())
                        {
                            var positionSign = item.Value<string>();
                            targetPositions.Add(Position.GetBySign(positionSign));
                        }
                    }
                    else
                    {
                        // retrieve the value
                        string positionSign = JsonUtils.ValueAsString(raw, JSON_PROPERTY_ACTIONCARD_TARGET_POSITIONS);
                        // Check if the "any" gisn is provided as position target
                        if (positionSign == Position.ANY.Sign)
                        {
                            targetPositions.Add(Position.ANY);
                        }
                        else ThrowInvalidValueException(GetType().Name, "ActionCard", JSON_PROPERTY_ACTIONCARD_TARGET_POSITIONS, positionSign);
                    }
                }
                // Stats parsing
                Stats stats = FromJObjectToStats(raw);
                // TargetInput parsing
                CardTargetInput cardTargetInput = targetInput is JObject ? FromJObjectToCardTargetInput(targetInput as JObject) : CardTargetInput.NONE;
                // Filling
                card.ActionText         = actionText;
                card.Cost               = cost;
                card.Duration           = duration;
                card.Special            = special;
                card.Flash              = flash;
                card.UniqueClass        = uniqueClass;
                card.UniqueClassType    = UniqueClassType.GetById(uniqueClassType);
                card.BallCondition      = CardBallCondition.GetById(ballCondition);
                card.Target             = Target.GetBySign(target);
                card.Role               = (role != Role.NONE.Sign) ? Role.GetBySign(role) : Role.NONE;
                card.TargetInput        = cardTargetInput;
                card.TargetPositions.Clear();
                card.TargetPositions.AddRange(targetPositions);
                card.UpdateSchools(schools);
                card.Stats.Copy(stats);
            }
            catch (JsonFormatException ex)
            {
                ThrowWrongDatatypeException(GetType().Name, "ActionCard", ex.Property, ex.Datatype, raw.ToString());
            }
            catch (DataParserException ex)
            {
                // Debug
                if (Logger.CanDebug(DEBUG_KEY)) Logger.Debug("CardParser > Parsing error: {0} - json dump : {1}", ex.Message, raw.ToString());
                throw ex;
            }
            catch (Exception ex)
            {
                ThrowUnexpectedErrorException(GetType().Name, "FillActionCardData", "ActionCard", raw.ToString(), ex);
            }
        }

        /// <summary>
        /// Sets PlayerCards properties from a raw JSON object
        /// </summary>
        /// <param name="raw">Raw JSON object to read</param>
        /// <param name="card">PlayerCard object to fill</param>
        protected void FillPlayerCardData(JObject raw, PlayerCard card)
        {
            try
            {
                // Data retrieve
                var role        = JsonUtils.ValueAsString(raw, JSON_PROPERTY_CARD_ROLE);
                var actionText  = JsonUtils.ValueAsString(raw, JSON_PROPERTY_CARD_ACTIONTEXT);
                List<School> schools = new List<School>();
                if (JsonUtils.IsArrayProperty(raw, JSON_PROPERTY_CARD_SCHOOL))
                {
                    // Loop over schools id
                    foreach (var item in raw[JSON_PROPERTY_CARD_SCHOOL].Children())
                    {
                        int shooolId = item.Value<int>();
                        schools.Add(School.GetById(shooolId));
                    }
                }
                else
                {
                    // Adds a single school to the list
                    int shooolId = JsonUtils.ValueAsInt(raw, JSON_PROPERTY_CARD_SCHOOL);
                    schools.Add(School.GetById(shooolId));
                }
                // Stats parsing
                Stats stats = FromJObjectToStats(raw);
                // Filling
                card.ActionText = actionText;
                card.Role = Role.GetBySign(role);
                card.UpdateSchools(schools);
                card.Stats.Copy(stats);
            }
            catch (JsonFormatException ex)
            {
                ThrowWrongDatatypeException(GetType().Name, "PlayerCard", ex.Property, ex.Datatype);
            }
            catch (DataParserException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                ThrowUnexpectedErrorException(GetType().Name, "FillPlayerCardData", "PlayerCard", ex);
            }
        }

        /// <summary>
        /// Sets Stats properties from a raw JSON object
        /// </summary>
        /// <param name="raw">Raw JSON object to read</param>
        /// <param name="stats">Stats object to set</param>
        protected void FillStatsData(JObject raw, Stats stats)
        {
            try
            {
                // Data retrieve
                int stamina     = JsonUtils.ValueAsInt(raw, JSON_PROPERTY_STATS_STAMINA);
                int dribiling   = JsonUtils.ValueAsInt(raw, JSON_PROPERTY_STATS_DRIBLING);
                int passing     = JsonUtils.ValueAsInt(raw, JSON_PROPERTY_STATS_PASSING);
                int scoring     = JsonUtils.ValueAsInt(raw, JSON_PROPERTY_STATS_SCORING);
                int defense     = JsonUtils.ValueAsInt(raw, JSON_PROPERTY_STATS_DEFENCE);
                int goalkeeping = JsonUtils.ValueAsInt(raw, JSON_PROPERTY_STATS_GOALKEEPING);
                // Filling
                stats.Stamina       = stamina;
                stats.Dribbling     = dribiling;
                stats.Passing       = passing;
                stats.Scoring       = scoring;
                stats.Defense       = defense;
                stats.Goalkeeping   = goalkeeping;
            }
            catch (JsonFormatException ex)
            {
                ThrowWrongDatatypeException(GetType().Name, "Stats", ex.Property, ex.Datatype);
            }
            catch (DataParserException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                ThrowUnexpectedErrorException(GetType().Name, "FillStatsData", "Stats", ex);
            }
        }

        /// <summary>
        /// Sets Deck properties and contents from a raw JSON object
        /// </summary>
        /// <param name="raw">Raw JSON object to read</param>
        /// <param name="deck">Deck object to set</param>
        protected void FillDeckDTO(JObject raw, DeckDTO deck)
        {
            try
            {
                // Data retrieve
                var id              = JsonUtils.ValueAsUInt(raw, JSON_PROPERTY_DECK_DTO_ID);
                var name            = JsonUtils.ValueAsString(raw, JSON_PROPERTY_DECK_DTO_NAME);
                var active          = JsonUtils.ValueAsInt(raw, JSON_PROPERTY_DECK_DTO_ACTIVE) == 1;
                var actionCards     = JsonUtils.PropertyAsJArray(raw, JSON_PROPERTY_DECK_DTO_ACTIONCARDS);
                var formation       = JsonUtils.PropertyAsJObject(raw, JSON_PROPERTY_DECK_DTO_FORMATION);
                // Formation player cards data retrieve
                var gk_player       = JsonUtils.ValueAsUInt(formation, JSON_PROPERTY_DECK_DTO_FORMATION_GK_PLAYER);
                var d1_player       = JsonUtils.ValueAsUInt(formation, JSON_PROPERTY_DECK_DTO_FORMATION_D1_PLAYER); 
                var d2_player       = JsonUtils.ValueAsUInt(formation, JSON_PROPERTY_DECK_DTO_FORMATION_D2_PLAYER); 
                var md_player       = JsonUtils.ValueAsUInt(formation, JSON_PROPERTY_DECK_DTO_FORMATION_MF_PLAYER); 
                var f1_player       = JsonUtils.ValueAsUInt(formation, JSON_PROPERTY_DECK_DTO_FORMATION_F1_PLAYER); 
                var f2_player       = JsonUtils.ValueAsUInt(formation, JSON_PROPERTY_DECK_DTO_FORMATION_F2_PLAYER); 
                var sub_df_player   = JsonUtils.ValueAsUInt(formation, JSON_PROPERTY_DECK_DTO_FORMATION_SUB_DF_PLAYER);
                var sub_md_player   = JsonUtils.ValueAsUInt(formation, JSON_PROPERTY_DECK_DTO_FORMATION_SUB_MF_PLAYER);
                var sub_fw_player   = JsonUtils.ValueAsUInt(formation, JSON_PROPERTY_DECK_DTO_FORMATION_SUB_FW_PLAYER);
                // Filling
                deck.Id             = id;
                deck.Name           = name;
                deck.Active         = active;
                deck.Goalkeeper     = gk_player;
                deck.Defence01      = d1_player;
                deck.Defence02      = d2_player;
                deck.Midfield       = md_player;
                deck.Forward01      = f1_player;
                deck.Forward02      = f2_player;
                deck.SubDefence     = sub_df_player;
                deck.SubMidfield    = sub_md_player;
                deck.SubForward     = sub_fw_player;
                foreach (var actionCardId in actionCards)
                {
                    deck.ActionCards.Add( actionCardId.Value<uint>() );
                }
            }
            catch (JsonFormatException ex)
            {
                ThrowWrongDatatypeException(GetType().Name, "DeckDTO", ex.Property, ex.Datatype);
            }
            catch (DataParserException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                ThrowUnexpectedErrorException(GetType().Name, "FillDeckDTO", "DeckDTO", ex);
            }
        }

        #endregion Data filling

        #region Conversion

        protected Stats FromJObjectToStats(JObject raw)
        {
            // Data validation
            ValidateStatsData(raw);
            // Instance creation
            Stats stats = new Stats();
            // Data loading
            FillStatsData(raw, stats);

            return stats;
        }

        protected CardTargetInput FromJObjectToCardTargetInput(JObject raw)
        {
            // Ref
            CardTargetInput cardTargetInput = null;
            // The property "type" is required for decoding a card
            if (JsonUtils.HasProperty(raw, JSON_PROPERTY_ACTIONCARD_INPUT))
            {
                // Retrieve internal CardTargetInput object
                var raw_target = raw[JSON_PROPERTY_ACTIONCARD_INPUT][JSON_PROPERTY_ACTIONCARD_INPUT_TARGET] as JObject;
                // The property "type" is required for decoding a card
                if (JsonUtils.HasProperty(raw_target, JSON_PROPERTY_ACTIONCARD_INPUT_TARGET_TYPE))
                {
                    // Retrieved the target type
                    string type = JsonUtils.ValueAsString(raw_target, JSON_PROPERTY_ACTIONCARD_INPUT_TARGET_TYPE);
                    // Card persing by card type
                    TargetType targetType = TargetType.GetBySign(type);
                    if (targetType == TargetType.ROLE)
                    {
                        cardTargetInput = FromJObjectToCardTargetInputRole(raw_target);
                    }
                    else if (targetType == TargetType.EFFECT)
                    {
                        cardTargetInput = FromJObjectToCardTargetInputEffect(raw_target);
                    }
                    else if (targetType == TargetType.GRAVEYARD)
                    {
                        cardTargetInput = FromJObjectToCardTargetInputGraveyard(raw_target);
                    }
                    else ThrowInvalidValueException(GetType().Name, "CardTargetInput", JSON_PROPERTY_ACTIONCARD_INPUT_TARGET_TYPE, type, raw_target.ToString());
                }
                // Cart type property not found, raise an error
                else ThrowMissingPropertyException(GetType().Name, "CardTargetInput", JSON_PROPERTY_ACTIONCARD_INPUT_TARGET_TYPE, raw_target.ToString());
            }
            // Internal "intput" property not found, raise an error 
            else ThrowMissingPropertyException(GetType().Name, "CardTargetInput", JSON_PROPERTY_ACTIONCARD_INPUT, raw.ToString());

            return cardTargetInput;
        }

        protected CardTargetInputRole FromJObjectToCardTargetInputRole(JObject raw)
        {
            CardTargetInputRole cardtargetInput = new CardTargetInputRole();
            // The property "valid" is required for decoding a card target type and contains the secification to perform the targeting
            if (JsonUtils.HasProperty(raw, JSON_PROPERTY_CARDTARGETINPUT_VALID))
            {
                // Data retrieve
                var valid = JsonUtils.ValueAsString(raw, JSON_PROPERTY_CARDTARGETINPUT_VALID);
                CardTargetInputRoleKind kind = CardTargetInputRoleKind.GetBySign(valid);
                // Filling
                cardtargetInput.Kind = kind;
            }
            // Internal "valid" property not found, raise an error 
            else ThrowMissingPropertyException(GetType().Name, "CardTargetInputRole", JSON_PROPERTY_CARDTARGETINPUT_VALID, raw.ToString());

            return cardtargetInput;
        }

        protected CardTargetInputEffect FromJObjectToCardTargetInputEffect(JObject raw)
        {
            CardTargetInputEffect cardtargetInput = new CardTargetInputEffect();

            // LT_TODO: Currently not implemented, not used anymore

            return cardtargetInput;
        }

        protected CardTargetInputGraveyard FromJObjectToCardTargetInputGraveyard(JObject raw)
        {
            CardTargetInputGraveyard cardtargetInput = new CardTargetInputGraveyard();
            // The property "valid" is required for decoding a card target type and contains the secification to perform the targeting
            if (JsonUtils.HasProperty(raw, JSON_PROPERTY_CARDTARGETINPUT_VALID))
            {
                // Data retrieve
                var valid = JsonUtils.ValueAsString(raw, JSON_PROPERTY_CARDTARGETINPUT_VALID);
                CardTargetInputGraveyardKind kind = CardTargetInputGraveyardKind.GetBySign(valid);
                // Filling
                cardtargetInput.Kind = kind;
            }
            // Internal "valid" property not found, raise an error 
            else ThrowMissingPropertyException(GetType().Name, "CardTargetInputGraveyard", JSON_PROPERTY_CARDTARGETINPUT_VALID, raw.ToString());

            return cardtargetInput;
        }

        protected ActionCard FromJObjectToActionCard(JObject raw)
        {
            // Data validation
            ValidateCardData(raw);
            ValidateActionCardData(raw);
            // Instance creation
            ActionCard card = new ActionCard(CardKind.DEFINITION);
            // Data loading
            FillCardData(raw, card);
            FillActionCardData(raw, card);

            return card;
        }

        protected PlayerCard FromJObjectToPlayerCard(JObject raw)
        {
            // Data validation
            ValidateCardData(raw);
            ValidatePlayerCardData(raw);
            // Instance creation
            PlayerCard card = new PlayerCard(CardKind.DEFINITION);
            // Data loading
            FillCardData(raw, card);
            FillPlayerCardData(raw, card);

            return card;
        }

        protected GoalkeeperCard FromJObjectToGoalkeeperCard(JObject raw)
        {
            // Data validation
            ValidateCardData(raw);
            ValidatePlayerCardData(raw);
            // Instance creation
            GoalkeeperCard card = new GoalkeeperCard(CardKind.DEFINITION);
            // Data loading
            FillCardData(raw, card);
            FillPlayerCardData(raw, card);
            // Goalkeeper role forcing
            card.Role = Role.GOALKEEPER;

            return card;
        }

        protected CollectionCardDTO FromJObjectToCollectionCardDTO(JObject raw)
        {
            // List of card instance ids to create
            CollectionCardDTO collectionCardDTO = new CollectionCardDTO();
            // Data validation
            ValidateCollectionCardDTO(raw);
            // Parsing
            try
            {
                // Retrieve the card id
                var cardId = JsonUtils.ValueAsUInt(raw, JSON_PROPERTY_COLLECTION_CARD_DTO_ID);
                // Retrieve instance ids array
                var instanceList = JsonUtils.PropertyAsJArray(raw, JSON_PROPERTY_COLLECTION_CARD_DTO_INSTANCE_LIST);
                // Card id
                collectionCardDTO.Id = cardId;
                // loop over ids
                foreach (var raw_id in instanceList)
                {
                    collectionCardDTO.InstanceList.Add( raw_id.Value<uint>() );
                }
            }
            catch (JsonFormatException ex)
            {
                ThrowWrongDatatypeException(GetType().Name, "CollectionCardDTO", ex.Property, ex.Datatype);
            }
            catch (DataParserException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                ThrowUnexpectedErrorException(GetType().Name, "FromJObjectToCollectionCard", "CollectionCardDTO", ex);
            }
            return collectionCardDTO;
        }

        protected DeckDTO FromJObjectToDeckDTO(JObject raw)
        {
            // Data validation
            ValidateDeckDTO(raw);
            // Instance creation
            DeckDTO deck = new DeckDTO();
            // Data loading
            FillDeckDTO(raw, deck);

            return deck;
        }

        /// <summary>
        /// Converts a JObject (json) to a Card
        /// </summary>
        /// <param name="raw"></param>
        /// <returns></returns>
        protected Card FromJObjectToCard(JObject raw)
        {
            // Card reference
            Card card = null;
            // The property "type" is required for decoding a card
            if (JsonUtils.HasProperty(raw, JSON_PROPERTY_CARD_CARDTYPE))
            {
                // Retrieved the card type
                int type = JsonUtils.ValueAsInt(raw, JSON_PROPERTY_CARD_CARDTYPE);
                // Card persing by card type
                CardType cardType = CardType.GetById(type);
                if (cardType == CardType.PLAYER)
                {
                    card = FromJObjectToPlayerCard(raw);
                }
                else if (cardType == CardType.GOALKEEPER)
                {
                    card = FromJObjectToGoalkeeperCard(raw);
                }
                else if (cardType == CardType.ACTION)
                {
                    card = FromJObjectToActionCard(raw);
                }
                else if (cardType == CardType.TRAINING)
                {
                    throw new NotImplementedException("CardType.TRAINING parsing is not yet implemented");
                }
            }
            // Cart type property not found, raise an error
            else ThrowMissingPropertyException(GetType().Name, "Card[] > Card", JSON_PROPERTY_CARD_CARDTYPE);
            // Returns card object
            return card;
        }

        #endregion Conversion

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public Card[] ParseCards(string json)
        {
            // Card list
            List<Card> cards = new List<Card>();
            // Safe exec
            try
            {
                // JSON-Net Objects
                JObject raw = JObject.Parse(json);
                JArray raw_card_array = null;
                // Check if CARD_DEFINITIONS_CARD_LIST exists
                if (JsonUtils.HasProperty(raw, JSON_PROPERTY_CARD_DEFINITIONS_CARD_LIST))
                {
                    // Cast the result as an array
                    raw_card_array = raw[JSON_PROPERTY_CARD_DEFINITIONS_CARD_LIST] as JArray;
                    // Loop over json array
                    foreach (var raw_card in raw_card_array.Children<JObject>())
                    {
                        cards.Add(FromJObjectToCard(raw_card));
                    }
                }
                else
                {
                    ThrowMissingPropertyException(GetType().Name, "Card[]", JSON_PROPERTY_CARD_DEFINITIONS_CARD_LIST, json);
                }
            }
            catch (DataParserException ex)
            {
                // Rethrow
                throw ex;
            }
            catch (Exception ex)
            {
                // ThrowMissingPropertyException unexpected error
                ThrowUnexpectedErrorException(GetType().Name, "ParseCards", "Cards[]", json, ex);
            }
            return cards.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public CollectionCardDTO[] ParseCollectionFromLoginData(string json)
        {
            // Card list
            List<CollectionCardDTO> cards = new List<CollectionCardDTO>();
            // Safe exec
            try
            {
                // JSON.Net Objects
                JObject raw = JObject.Parse(json);
                // Check if CARD_DEFINITIONS_CARD_LIST exists
                if (JsonUtils.HasProperty(raw, JSON_PROPERTY_LOGIN_COLLECTION))
                {
                    // Cast the result as an array
                    var raw_items = JsonUtils.PropertyAsJArray(raw, JSON_PROPERTY_LOGIN_COLLECTION);
                    // Loop over json array
                    foreach (var raw_item in raw_items)
                    {
                        cards.Add( FromJObjectToCollectionCardDTO(raw_item as JObject) );
                    }
                }
                else
                {
                    ThrowMissingPropertyException(GetType().Name, "CollectionCardDTO[]", JSON_PROPERTY_LOGIN_COLLECTION, json);
                }
            }
            catch (DataParserException ex)
            {
                // Rethrow
                throw ex;
            }
            catch (Exception ex)
            {
                // ThrowMissingPropertyException unexpected error
                ThrowUnexpectedErrorException(GetType().Name, "ParseCollectionFromLoginData", "CollectionCardDTO[]", json, ex);
            }
            //
            return cards.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public DeckDTO[] ParseDecksFromLoginData(string json)
        {
            List<DeckDTO> decks = new List<DeckDTO>();
            // Safe exec
            try
            {
                // JSON.Net Objects
                JObject raw = JObject.Parse(json);

                // Check for decks properties
                if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_LOGIN_DECKS)) ThrowMissingPropertyException(GetType().Name, "DeckDTO[]", JSON_PROPERTY_LOGIN_DECKS, json);
                // Retrieve decks object
                var raw_decks   = JsonUtils.PropertyAsJObject(raw, JSON_PROPERTY_LOGIN_DECKS);
                // Check for deck list properties
                if (!JsonUtils.HasProperty(raw_decks, JSON_PROPERTY_LOGIN_DECKS_LIST)) ThrowMissingPropertyException(GetType().Name, "DeckDTO[]", JSON_PROPERTY_LOGIN_DECKS_LIST, json);
                // Retrieve deck list
                var raw_deck_list   = JsonUtils.PropertyAsJArray(raw_decks, JSON_PROPERTY_LOGIN_DECKS_LIST);
                // Loop over all raw decks data
                foreach (var raw_deck in raw_deck_list)
                {
                    // Parsing deck attivo
                    decks.Add( FromJObjectToDeckDTO(raw_deck as JObject) );
                }
            }
            catch (DataParserException ex)
            {
                // Rethrow
                throw ex;
            }
            catch (Exception ex)
            {
                // ThrowMissingPropertyException unexpected error
                ThrowUnexpectedErrorException(GetType().Name, "ParseDecksFromLoginData", "DeckDTO[]", json, ex);
            }
            // Returns all decks
            return decks.ToArray();
        }

		#endregion PUBLIC METHODS

    }
}
