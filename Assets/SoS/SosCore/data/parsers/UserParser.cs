﻿using Newtonsoft.Json.Linq;
using sos.core;
using sos.core.game;
using sos.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.data.parsers
{
    /// <summary>
    /// Parser used to convert from JSON to a sos.core User classes
    /// </summary>
    public class UserParser : DataParser
    {

        #region CONSTANTS

        // JSON PROPERTIES 

        // Login properties
        protected const string      JSON_PROPERTY_LOGIN_USER					    = "user";

        // Player properties
		protected const string      JSON_PROPERTY_PLAYER_ID					        = "id";
		protected const string      JSON_PROPERTY_PLAYER_USER_ID			        = "userID";
		protected const string      JSON_PROPERTY_PLAYER_USERNAME			        = "username";
		protected const string      JSON_PROPERTY_PLAYER_PASSWORD			        = "password";
		protected const string      JSON_PROPERTY_PLAYER_FIRST_NAME			        = "firstName";
		protected const string      JSON_PROPERTY_PLAYER_LAST_NAME			        = "lastName";
		protected const string      JSON_PROPERTY_PLAYER_REGISTRATION_DATE	        = "regDate";
		protected const string      JSON_PROPERTY_PLAYER_EMAIL				        = "email";

        // User properties
		protected const string      JSON_PROPERTY_USER_SESSION_TOKEN		        = "sessionToken";
        protected const string      JSON_PROPERTY_USER_CREDITS				        = "balance";
		// protected const string      JSON_PROPERTY_USER_DECK_SIZE			        = "deckSize"; not used anymore!
		protected const string      JSON_PROPERTY_USER_FRIEND_LIST			        = "friendList";
		protected const string      JSON_PROPERTY_USER_GAME_LIST			        = "gameList";

		// UserGame properties
		protected const string      JSON_PROPERTY_USER_GAME_ID				        = "id";
		protected const string      JSON_PROPERTY_USER_GAME_PLAYER_SIDE			    = "playerSide";
		protected const string      JSON_PROPERTY_USER_GAME_OPPONENT_ID			    = "opponentID";
		protected const string      JSON_PROPERTY_USER_GAME_OPPONENT_NAME			= "opponentName";
		protected const string      JSON_PROPERTY_USER_GAME_PLAYER_DECK_ID			= "deckID";
		protected const string      JSON_PROPERTY_USER_GAME_PLAYER_DECK_NAME		= "deckName";
		protected const string      JSON_PROPERTY_USER_GAME_OPPONENT_DECK_ID		= "opponentDeckID";
		protected const string      JSON_PROPERTY_USER_GAME_OPPONENT_DECK_NAME		= "opponentDeckName";

        // UserGameList properties
        protected const string      JSON_PROPERTY_USER_GAME_LIST_LIST		        = "list";

        #endregion CONSTANTS

        #region FIELDS

        #endregion FIELDS

        #region CONSTRUCTORS

        #endregion CONSTRUCTORS

        #region PROPERTIES

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #region Validation

        protected void ValidatePlayerData(JObject raw)
        {

            if (   !JsonUtils.HasProperty(raw, JSON_PROPERTY_PLAYER_ID) 
                && !JsonUtils.HasProperty(raw, JSON_PROPERTY_PLAYER_USER_ID))       ThrowMissingPropertyException(GetType().Name, "Player", JSON_PROPERTY_PLAYER_ID);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_PLAYER_USERNAME))         ThrowMissingPropertyException(GetType().Name, "Player", JSON_PROPERTY_PLAYER_USERNAME);
        }

        protected void ValidateUserData(JObject raw)
        {
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_PLAYER_FIRST_NAME))       ThrowMissingPropertyException(GetType().Name, "User", JSON_PROPERTY_PLAYER_FIRST_NAME);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_PLAYER_LAST_NAME))        ThrowMissingPropertyException(GetType().Name, "User", JSON_PROPERTY_PLAYER_LAST_NAME);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_PLAYER_EMAIL))            ThrowMissingPropertyException(GetType().Name, "User", JSON_PROPERTY_PLAYER_EMAIL);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_USER_SESSION_TOKEN))      ThrowMissingPropertyException(GetType().Name, "User", JSON_PROPERTY_USER_SESSION_TOKEN);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_USER_CREDITS))            ThrowMissingPropertyException(GetType().Name, "User", JSON_PROPERTY_USER_CREDITS);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_USER_FRIEND_LIST))        ThrowMissingPropertyException(GetType().Name, "User", JSON_PROPERTY_USER_FRIEND_LIST);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_USER_GAME_LIST))          ThrowMissingPropertyException(GetType().Name, "User", JSON_PROPERTY_USER_GAME_LIST);
        }

        protected void ValidateUserGameData(JObject raw)
        {
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_USER_GAME_ID))                    ThrowMissingPropertyException(GetType().Name, "UserGame", JSON_PROPERTY_USER_GAME_ID);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_USER_GAME_PLAYER_SIDE))           ThrowMissingPropertyException(GetType().Name, "UserGame", JSON_PROPERTY_USER_GAME_PLAYER_SIDE);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_USER_GAME_OPPONENT_ID))           ThrowMissingPropertyException(GetType().Name, "UserGame", JSON_PROPERTY_USER_GAME_OPPONENT_ID);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_USER_GAME_OPPONENT_NAME))         ThrowMissingPropertyException(GetType().Name, "UserGame", JSON_PROPERTY_USER_GAME_OPPONENT_NAME);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_USER_GAME_PLAYER_DECK_ID))        ThrowMissingPropertyException(GetType().Name, "UserGame", JSON_PROPERTY_USER_GAME_PLAYER_DECK_ID);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_USER_GAME_PLAYER_DECK_NAME))      ThrowMissingPropertyException(GetType().Name, "UserGame", JSON_PROPERTY_USER_GAME_PLAYER_DECK_NAME);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_USER_GAME_OPPONENT_DECK_ID))      ThrowMissingPropertyException(GetType().Name, "UserGame", JSON_PROPERTY_USER_GAME_OPPONENT_DECK_ID);
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_USER_GAME_OPPONENT_DECK_NAME))    ThrowMissingPropertyException(GetType().Name, "UserGame", JSON_PROPERTY_USER_GAME_OPPONENT_DECK_NAME);
        }

        protected void ValidateUserGameListData(JObject raw)
        {
            if (!JsonUtils.HasProperty(raw, JSON_PROPERTY_USER_GAME_LIST_LIST))             ThrowMissingPropertyException(GetType().Name, "UserGameList", JSON_PROPERTY_USER_GAME_LIST_LIST);
        }

        #endregion Validation

        #region Data filling

        protected void FillPlayerData(JObject raw, Player player)
        {
            try
            {
                // Data retrieve
		        var id                  = JsonUtils.HasProperty(raw, JSON_PROPERTY_PLAYER_USER_ID)  ? JsonUtils.ValueAsUInt(raw, JSON_PROPERTY_PLAYER_USER_ID)
                                                                                                    : JsonUtils.ValueAsUInt(raw, JSON_PROPERTY_PLAYER_ID);
                var username            = JsonUtils.ValueAsString(raw, JSON_PROPERTY_PLAYER_USERNAME);
		        var firstName           = JsonUtils.HasProperty(raw, JSON_PROPERTY_PLAYER_FIRST_NAME) ? JsonUtils.ValueAsString(raw, JSON_PROPERTY_PLAYER_FIRST_NAME) : string.Empty;
		        var lastName            = JsonUtils.HasProperty(raw, JSON_PROPERTY_PLAYER_LAST_NAME)  ? JsonUtils.ValueAsString(raw, JSON_PROPERTY_PLAYER_LAST_NAME) : string.Empty;
		        var email               = JsonUtils.HasProperty(raw, JSON_PROPERTY_PLAYER_EMAIL) ? JsonUtils.ValueAsString(raw, JSON_PROPERTY_PLAYER_EMAIL) : string.Empty;
                // Filling
                player.Id           = id;
                player.Username     = username;
                player.FirstName    = firstName;
                player.LastName     = lastName;
                player.Email        = email;
            }
            catch (JsonFormatException ex)
            {
                ThrowWrongDatatypeException(GetType().Name, "Player", ex.Property, ex.Datatype);
            }
            catch (DataParserException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                ThrowUnexpectedErrorException(GetType().Name, "FillPlayerData", "Player", ex);
            }
        }

        protected void FillUserData(JObject raw, User user)
        {
            try
            {
                // Data retrieve
                var sessionToken    = JsonUtils.ValueAsString(raw, JSON_PROPERTY_USER_SESSION_TOKEN);
                var credits         = JsonUtils.ValueAsUInt(raw, JSON_PROPERTY_USER_CREDITS);
                var user_list       = JsonUtils.PropertyAsJArray(raw, JSON_PROPERTY_USER_FRIEND_LIST);
                var game_list       = JsonUtils.PropertyAsJObject(raw, JSON_PROPERTY_USER_GAME_LIST);
                // JSON_PROPERTY_USER_FRIEND_LIST list parsing
                PlayerList friends      = FromJArrayToPlayerList(user_list);
                // Game list parsing
                UserGameList games = FromJObjectToUserGameList(game_list);
                // Data filling
                user.SessionToken   = sessionToken;
                user.Credits        = credits;
                user.UpdateFriends(friends);
                user.UpdateGames(games);
            }
            catch (JsonFormatException ex)
            {
                ThrowWrongDatatypeException(GetType().Name, "User", ex.Property, ex.Datatype);
            }
            catch (DataParserException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                ThrowUnexpectedErrorException(GetType().Name, "FillUserData", "User", ex);
            }
        }

        protected void FillUserGameData(JObject raw, UserGame game)
        {
            try
            {
                // Data retrieve
                var id                  = JsonUtils.ValueAsUInt(raw, JSON_PROPERTY_USER_GAME_ID);
                var side                = JsonUtils.ValueAsString(raw, JSON_PROPERTY_USER_GAME_PLAYER_SIDE);
                var playerDeckId        = JsonUtils.ValueAsUInt(raw, JSON_PROPERTY_USER_GAME_PLAYER_DECK_ID);
                var playerDeckName      = JsonUtils.ValueAsString(raw, JSON_PROPERTY_USER_GAME_PLAYER_DECK_NAME);
		        var opponentId          = JsonUtils.ValueAsUInt(raw, JSON_PROPERTY_USER_GAME_OPPONENT_ID);
                var opponentName        = JsonUtils.ValueAsString(raw, JSON_PROPERTY_USER_GAME_OPPONENT_NAME);
                var opponentDeckId      = JsonUtils.ValueAsUInt(raw, JSON_PROPERTY_USER_GAME_OPPONENT_DECK_ID);
                var opponentDeckName    = JsonUtils.ValueAsString(raw, JSON_PROPERTY_USER_GAME_OPPONENT_DECK_NAME);
                // Player parsing
                var opponent = new Player();
                opponent.Id = opponentId;
                opponent.Username = opponentName;
                // Data Filling
                game.Id                 = id;
                game.Side               = GameSide.GetByName(side);
                game.PlayerDeckId       = playerDeckId;
                game.PlayerDeckName     = playerDeckName;
                game.Opponent           = opponent;
                game.OpponentDeckId     = opponentDeckId;
                game.OpponentDeckName   = opponentDeckName;
            }
            catch (JsonFormatException ex)
            {
                ThrowWrongDatatypeException(GetType().Name, "UserGame", ex.Property, ex.Datatype);
            }
            catch (DataParserException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                ThrowUnexpectedErrorException(GetType().Name, "FillUserGameData", "UserGame", ex);
            }

        }

        protected void FillUserGameListData(JObject raw, UserGameList gameList)
        {
            try
            {
                // Data retrieve
                var list = JsonUtils.PropertyAsJArray(raw, JSON_PROPERTY_USER_GAME_LIST_LIST);
                // Data filling
                foreach (var item in list)
                {
                    // Game parsing
                    var game = FromJObjectToUserGame(item as JObject);
                    gameList.Add(game);
                }
            }
            catch (JsonFormatException ex)
            {
                ThrowWrongDatatypeException(GetType().Name, "UserGameList", ex.Property, ex.Datatype);
            }
            catch (DataParserException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                ThrowUnexpectedErrorException(GetType().Name, "FillUserGameListData", "UserGameList", ex);
            }

        }

        #endregion Data filling

        #region Converters

        protected Player FromJObjectToPlayer(JObject raw)
        {
            // Data validation
            ValidatePlayerData(raw);
            // Instance creation
            Player player = new Player();
            // Data loading
            FillPlayerData(raw, player);

            return player;
        }

        protected User FromJObjectToUser(JObject raw)
        {
            // Data validation
            ValidatePlayerData(raw);
            ValidateUserData(raw);
            // Instance creation
            User user = new User();
            // Data loading
            FillPlayerData(raw, user);
            FillUserData(raw, user);

            return user;
        }

        protected PlayerList FromJArrayToPlayerList(JArray raw)
        {
            PlayerList playerList = new PlayerList();
            foreach (var item in raw)
            {
                playerList.Add(FromJObjectToPlayer(item as JObject));
            }
            return playerList;
        }

        protected UserGame FromJObjectToUserGame(JObject raw)
        {
            // Data validation
            ValidateUserGameData(raw);
            // Instance creation
            UserGame game = new UserGame();
            // Data loading
            FillUserGameData(raw, game);

            return game;
        }

        protected UserGameList FromJObjectToUserGameList(JObject raw)
        {
            // Data validation
            ValidateUserGameListData(raw);
            // Instance creation
            UserGameList gamelist = new UserGameList();
            // Data loading
            FillUserGameListData(raw, gamelist);

            return gamelist;
        }

        #endregion Converters

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public Player ParsePlayer(string json)
        {
            Player player = new Player();
            // Safe exec
            try
            {
                // JSON-Net Objects
                JObject raw = JObject.Parse(json);
                // Parse user data
                player = FromJObjectToPlayer(raw);
            }
            catch (DataParserException ex)
            {
                // Rethrow
                throw ex;
            }
            catch (Exception ex)
            {
                // ThrowMissingPropertyException unexpected error
                ThrowUnexpectedErrorException(GetType().Name, "ParsePlayer", "Player", json, ex);
            }
            return player;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public User ParseUser(string json)
        {
            User user = new User();
            // Safe exec
            try
            {
                // JSON-Net Objects
                JObject raw = JObject.Parse(json);
                // Parse user data
                user = FromJObjectToUser(raw);
            }
            catch (DataParserException ex)
            {
                // Rethrow
                throw ex;
            }
            catch (Exception ex)
            {
                // ThrowMissingPropertyException unexpected error
                ThrowUnexpectedErrorException(GetType().Name, "ParseUser", "User", json, ex);
            }
            // 
            return user;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public User ParseUserFromLoginData(string json)
        {
            User user = new User();
            // Safe exec
            try
            {
                // JSON-Net Objects
                JObject raw = JObject.Parse(json);
                // Check "user" object into login data
                if (JsonUtils.HasProperty(raw, JSON_PROPERTY_LOGIN_USER))
                {
                    // Parse user data
                    user = FromJObjectToUser(raw[JSON_PROPERTY_LOGIN_USER] as JObject);
                }
                else
                {
                    ThrowMissingPropertyException(GetType().Name, "User (from Login())", JSON_PROPERTY_LOGIN_USER, json);
                }
            }
            catch (DataParserException ex)
            {
                // Rethrow
                throw ex;
            }
            catch (Exception ex)
            {
                // ThrowMissingPropertyException unexpected error
                ThrowUnexpectedErrorException(GetType().Name, "ParseUserFromLoginData", "User", json, ex);
            }
            // 
            return user;
        }

        #endregion PUBLIC METHODS

    }
}
