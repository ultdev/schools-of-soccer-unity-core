﻿using System;

namespace sos.data.parsers
{
    
    /// <summary>
    /// 
    /// </summary>
    public enum DataParserExceptionType
    {
        MISSING_PROPERTY    = 1,
        WRONG_DATATYPE      = 2,
        INVALUED_VALUE      = 2,
        UNEXPECTED_ERROR    = 9,
    }

    /// <summary>
    /// Exception thrown the execution of a DataParser
    /// </summary>
    public class DataParserException : Exception
    {

		#region FIELDS

        private DataParserExceptionType _type;

        #endregion FIELDS

        #region CONSTRUCTORS

        public DataParserException(DataParserExceptionType type) : base()
        {
            _type = type;
        }

        public DataParserException(DataParserExceptionType type, string message) : base(message)
        {
            _type = type;
        }

        public DataParserException(DataParserExceptionType type, string message, Exception cause) : base(message, cause)
        {
            _type = type;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public DataParserExceptionType Type
        {
            get { return _type; }
        }

        #endregion PROPERTIES

    }
}
