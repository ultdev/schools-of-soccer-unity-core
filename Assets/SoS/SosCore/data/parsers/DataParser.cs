﻿using sos.logging;
using System;

namespace sos.data.parsers
{
    /// <summary>
    /// Base class to implement DataParsers
    /// </summary>
    public abstract class DataParser
    {
        #region CONSTANTS

        protected const bool    JSON_DUMP_ENABLED       = true;

        /// <summary>
        /// Debug key
        /// </summary>
        public const string  DEBUG_KEY               = "sos.data.parsers.DataParser";
        
        #endregion CONSTANTS

        #region FIELDS

        #endregion FIELDS

        #region CONSTRUCTORS

        #endregion CONSTRUCTORS

        #region PROPERTIES

        #endregion PROPERTIES

        #region PRIVATE METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parserName"></param>
        /// <param name="objectToParse"></param>
        /// <param name="wrongPropertyName"></param>
        /// <param name="expectedDatatype"></param>
        protected void ThrowInvalidValueException(string parserName, string objectToParse, string propertyName, object invalidValue)
        {
            ThrowInvalidValueException(parserName, objectToParse, propertyName, invalidValue, string.Empty);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="parserName"></param>
        /// <param name="objectToParse"></param>
        /// <param name="wrongPropertyName"></param>
        /// <param name="expectedDatatype"></param>
        protected void ThrowInvalidValueException(string parserName, string objectToParse, string propertyName, object invalidValue, string jsonDump)
        {
            // Builds error message
            string message = JSON_DUMP_ENABLED && !string.IsNullOrEmpty(jsonDump) 
                                ? string.Format("Unable to parse {0} from JSON with {1}: property '{2}' value is not valid (value: {3}) - json dump: {4}", objectToParse, parserName, propertyName, invalidValue, jsonDump)
                                : string.Format("Unable to parse {0} from JSON with {1}: property '{2}' value is not valid (value: {3})",                  objectToParse, parserName, propertyName, invalidValue);
            // Debug
            if (Logger.CanDebug(DEBUG_KEY)) Logger.Debug("DataParser > " + message);
            // throws the exception
            throw new DataParserException(DataParserExceptionType.INVALUED_VALUE, message);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parserName"></param>
        /// <param name="objectToParse"></param>
        /// <param name="propertyName"></param>
        /// <param name="expectedDatatype"></param>
        protected void ThrowWrongDatatypeException(string parserName, string objectToParse, string propertyName, string expectedDatatype)
        {
            ThrowWrongDatatypeException(parserName, objectToParse, propertyName, expectedDatatype, string.Empty);
        }
    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parserName"></param>
        /// <param name="objectToParse"></param>
        /// <param name="propertyName"></param>
        /// <param name="expectedDatatype"></param>
        protected void ThrowWrongDatatypeException(string parserName, string objectToParse, string propertyName, string expectedDatatype, string jsonDump)
        {
            // Builds error message
            string message = JSON_DUMP_ENABLED && !string.IsNullOrEmpty(jsonDump) 
                                ? string.Format("Unable to parse {0} from JSON with {1}: property '{2}' datatype is wrong (expected: {3}) - json dump: {4}", objectToParse, parserName, propertyName, expectedDatatype, jsonDump)
                                : string.Format("Unable to parse {0} from JSON with {1}: property '{2}' datatype is wrong (expected: {3})",                  objectToParse, parserName, propertyName, expectedDatatype);
            // Debug
            if (Logger.CanDebug(DEBUG_KEY)) Logger.Debug("DataParser > " + message);
            // throws the exception
            throw new DataParserException(DataParserExceptionType.WRONG_DATATYPE, message);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parserName"></param>
        /// <param name="objectToParse"></param>
        /// <param name="missingPropertyName"></param>
        protected void ThrowMissingPropertyException(string parserName, string objectToParse, string missingPropertyName)
        {
            ThrowMissingPropertyException(parserName, objectToParse, missingPropertyName, string.Empty);
        }

        protected void ThrowMissingPropertyException(string parserName, string objectToParse, string missingPropertyName, string jsonDump)
        {
            // Builds error message
            string message = JSON_DUMP_ENABLED && !string.IsNullOrEmpty(jsonDump) 
                                ? string.Format("Unable to parse {0} from JSON with {1}: missing property '{2}' - json dump: {3}", objectToParse, parserName, missingPropertyName, jsonDump)
                                : string.Format("Unable to parse {0} from JSON with {1}: missing property '{2}'",                  objectToParse, parserName, missingPropertyName);
            // Debug
            if (Logger.CanDebug(DEBUG_KEY)) Logger.Debug("DataParser > " + message);
            // throws the exception
            throw new DataParserException(DataParserExceptionType.MISSING_PROPERTY, message);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parserName"></param>
        /// <param name="objectToParse"></param>
        protected void ThrowUnexpectedErrorException(string parserName, string parserMethod, string objectToParse, Exception error)
        {
            ThrowUnexpectedErrorException(parserName, parserMethod, objectToParse, string.Empty, error);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parserName"></param>
        /// <param name="objectToParse"></param>
        protected void ThrowUnexpectedErrorException(string parserName, string parserMethod, string objectToParse, string jsonDump, Exception error)
        {
            // Builds error message
            string message = JSON_DUMP_ENABLED && !string.IsNullOrEmpty(jsonDump) 
                                ? string.Format("{0}.{1} unexpected error parsing {2} object: {3} - json dump: {4}", parserName, parserMethod, objectToParse, error.Message, jsonDump) 
                                : string.Format("{0}.{1} unexpected error parsing {2} object: {3}",                  parserName, parserMethod, objectToParse, error.Message);
            // Debug
            if (Logger.CanDebug(DEBUG_KEY)) Logger.Debug(message);
            // throws the exception
            throw new DataParserException(DataParserExceptionType.UNEXPECTED_ERROR, message);
        }

		#endregion PRIVATE METHODS

		#region PUBLIC METHODS

		#endregion PUBLIC METHODS
    }
}
