﻿namespace sos.data
{
    /// <summary>
    /// Generic object that holds data that can be loaded using external data, by a IDataAdapter using a IDataDecoder.
    /// </summary>
    public interface IDataObject
    {

        #region METHODS

        /// <summary>
        /// Clears all data contained in the data object and restores his state to the default
        /// </summary>
        void Clear();

        #endregion METHODS

    }
}
