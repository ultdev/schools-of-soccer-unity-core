﻿using System;

namespace sos.data
{
    public interface IDataDecoder
    {

        #region PROPERTIES

        /// <summary>
        /// DataForma handled by the decoder
        /// </summary>
        DataFormat Format { get; }

        /// <summary>
        /// True if the decoder contains the raw data
        /// </summary>
        bool HasData { get; }

        #endregion PROPERTIES

        #region METHODS

        /// <summary>
        /// Loads raw data into the decoder and converts it into the data format handled by the decoder itself.
        /// Call clear data before this method to remove any previous data
        /// If the conversion fails, the decoder must throw a DecoderFormatError
        /// </summary>
        /// <param name="data">Generic format data to be loaded into the decoder and converted into the expected format</param>
        void Load(object data);

        /// <summary>
        /// Check if a field exists into the data holded by the decoder
        /// </summary>
        /// <param name="field">Field name to check the existence</param>
        /// <returns>True if the field exists in the decoder</returns>
        bool Contains(string field);

        /// <summary>
        /// Retrieve the required field from the decoded data
        /// </summary>
        /// <typeparam name="T">Type of the value returned</typeparam>
        /// <param name="field">Field to retrieve the value</param>
        /// <returns>Value of the decoded field from the raw data</returns>
		T Decode<T>(string field);

        /// <summary>
        /// Clears raw and decoded data, must be caled before load() method
        /// </summary>
		void Clear();

        #endregion METHODS

    }
}
