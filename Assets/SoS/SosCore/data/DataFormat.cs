﻿namespace sos.data
{

    /// <summary>
    /// Defines the IDs available to define a DataFormat
    /// </summary>
    public enum DataFormatId : int
    {
		JSON            = 10,
		JSON_MINIFIED   = 11,
        BSON            = 20,
		XML             = 30
    }

    /// <summary>
    /// Defines the formats sopported that can be decode by the implementations of IDataDecoder
    /// </summary>
    public class DataFormat : Enumerator
    {

        #region CONSTANTS

        public static readonly DataFormat   JSON                        = new DataFormat((int) DataFormatId.JSON,             "JSON",           "JSON");
		public static readonly DataFormat   JSON_MINIFIED               = new DataFormat((int) DataFormatId.JSON_MINIFIED,    "JSON_MINIFIED",  "JSON Minified");
        public static readonly DataFormat   BSON                        = new DataFormat((int) DataFormatId.BSON,             "BSON",           "Binary JSON");
        public static readonly DataFormat   XML			                = new DataFormat((int) DataFormatId.XML,              "XML",            "Xml");

        #endregion CONSTANTS

        #region CONSTRUCTORS
        
        protected DataFormat(int id, string name, string sign) : base(id, name, sign) {}

        #endregion CONSTRUCTORS

        #region STATIC METHODS

        public static string[] Names()
        {
            return Enumerators.Names(typeof(DataFormat));
        }

        public static DataFormat[] Values()
        {
            return Enumerators.Values<DataFormat>(typeof(DataFormat));
        }

        public static DataFormat GetById(int id)
        {
            return Enumerators.GetById<DataFormat>(typeof(DataFormat), id);
        }

        public static DataFormat GetBySign(string sign)
        {
            return Enumerators.GetBySign<DataFormat>(typeof(DataFormat), sign);
        }

        #endregion STATIC METHODS

    }    
}
