﻿using System.Collections.Generic;

namespace sos.data.DTO
{

    /// <summary>
    /// Contains raw data from
    /// </summary>
    public class CollectionCardDTO
    {

		#region CONSTANTS

		#endregion CONSTANTS

		#region FIELDS

        private uint _id;
        private List<uint> _instanceList = new List<uint>();

        #endregion FIELDS

        #region CONSTRUCTORS

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public uint Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        public List<uint> InstanceList
        {
            get
            {
                return _instanceList;
            }

            set
            {
                _instanceList = value;
            }
        }

        #endregion PROPERTIES

        #region PUBLIC METHODS
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} (card id: {1}, instances: {2})",
                                 GetType().FullName,
                                 _id,
                                 _instanceList.Count);
        }

        #endregion PUBLIC METHODS

    }
}
