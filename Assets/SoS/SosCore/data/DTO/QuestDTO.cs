﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.data.DTO
{

    /// <summary>
    /// Data Transfer Object for Quest class
    /// </summary>
    public class QuestDTO
    {

		#region CONSTANTS

		#endregion CONSTANTS

		#region FIELDS

		private uint _id;
		private string _name;
		private string _description;
		private string _rewardText;
		private uint _daily;
		private string _image;

		#endregion FIELDS

		#region CONSTRUCTORS

		#endregion CONSTRUCTORS

		#region PROPERTIES

        public uint Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }

            set
            {
                _description = value;
            }
        }

        public string RewardText
        {
            get
            {
                return _rewardText;
            }

            set
            {
                _rewardText = value;
            }
        }

        public uint Daily
        {
            get
            {
                return _daily;
            }

            set
            {
                _daily = value;
            }
        }

        public string Image
        {
            get
            {
                return _image;
            }

            set
            {
                _image = value;
            }
        }

		#endregion PROPERTIES

		#region PUBLIC METHODS

        public override string ToString()
        {
            return string.Format("{0} (id: {1}, name: {2})",
                                 GetType().FullName,
                                 _id,
                                 _name);
        }

		#endregion PUBLIC METHODS

    }
}
