﻿using System.Collections.Generic;

namespace sos.data.DTO
{
    /// <summary>
    /// Data Transfer Object of Deck class
    /// </summary>
    public class DeckDTO
    {

		#region CONSTANTS

		#endregion CONSTANTS

		#region FIELDS

        private uint _id;
        private string _name;
        private bool _active;
        private bool _singleShool;
        private List<uint> _actionCards = new List<uint>();
        private uint _goalkeeper;
        private uint _defence01;
        private uint _defence02;
        private uint _midfield;
        private uint _forward01;
        private uint _forward02;
        private uint _subDefence;
        private uint _subMidfield;
        private uint _subForward;

        #endregion FIELDS

        #region CONSTRUCTORS

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public uint Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        public bool Active
        {
            get
            {
                return _active;
            }

            set
            {
                _active = value;
            }
        }

        public bool SingleShool
        {
            get
            {
                return _singleShool;
            }

            set
            {
                _singleShool = value;
            }
        }

        public List<uint> ActionCards
        {
            get
            {
                return _actionCards;
            }

            set
            {
                _actionCards = value;
            }
        }

        public uint Goalkeeper
        {
            get
            {
                return _goalkeeper;
            }

            set
            {
                _goalkeeper = value;
            }
        }

        public uint Defence01
        {
            get
            {
                return _defence01;
            }

            set
            {
                _defence01 = value;
            }
        }

        public uint Defence02
        {
            get
            {
                return _defence02;
            }

            set
            {
                _defence02 = value;
            }
        }

        public uint Midfield
        {
            get
            {
                return _midfield;
            }

            set
            {
                _midfield = value;
            }
        }

        public uint Forward01
        {
            get
            {
                return _forward01;
            }

            set
            {
                _forward01 = value;
            }
        }

        public uint Forward02
        {
            get
            {
                return _forward02;
            }

            set
            {
                _forward02 = value;
            }
        }

        public uint SubDefence
        {
            get
            {
                return _subDefence;
            }

            set
            {
                _subDefence = value;
            }
        }

        public uint SubMidfield
        {
            get
            {
                return _subMidfield;
            }

            set
            {
                _subMidfield = value;
            }
        }

        public uint SubForward
        {
            get
            {
                return _subForward;
            }

            set
            {
                _subForward = value;
            }
        }

        #endregion PROPERTIES

        #region PUBLIC METHODS
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} (id: {1}, name: {2})",
                                 GetType().FullName,
                                 _id,
                                 _name);
        }

        #endregion PUBLIC METHODS

    }
}
