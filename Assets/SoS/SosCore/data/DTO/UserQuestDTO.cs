﻿namespace sos.data.DTO
{

    /// <summary>
    /// Data Transfer Object for UserQuest class
    /// </summary>
    public class UserQuestDTO
    {

		#region CONSTANTS

		#endregion CONSTANTS

		#region FIELDS

        private uint _id;
		private uint _instanceId;
		private uint _itemCount;
		private int _status;

        #endregion FIELDS

        #region CONSTRUCTORS

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public uint Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        public uint InstanceId
        {
            get
            {
                return _instanceId;
            }

            set
            {
                _instanceId = value;
            }
        }

        public uint ItemCount
        {
            get
            {
                return _itemCount;
            }

            set
            {
                _itemCount = value;
            }
        }

        public int Status
        {
            get
            {
                return _status;
            }

            set
            {
                _status = value;
            }
        }

        #endregion PROPERTIES

        #region PUBLIC METHODS

        public override string ToString()
        {
            return string.Format("{0} (quest id: {1}, instance id: {2})",
                                 GetType().FullName,
                                 _id,
                                 _instanceId);
        }

        #endregion PUBLIC METHODS


    }
}
