﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.data
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDataAdapter 
    {
		
        #region METHODS

        /// <summary>
        /// Base method to retrieve a data object from the given raw data, using the data decoder internally owned by adapter
        /// </summary>
        /// <param name="data">Raw data to adapt to a IDataObject interface instance using a IDataDecoder</param>
        /// <returns>A new IDataObject interface instance</returns>
		IDataObject GetDataObject(object data, DataParameters parameters);

        #endregion METHODS

    }
}
