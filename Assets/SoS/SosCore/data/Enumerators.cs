﻿using System;
using System.Collections.Generic;

namespace sos.data
{
    /// <summary>
    /// Global Enumerator repository: contains all the descendants of Enumerator and provides static utilities to retrieve enumeration metadata
    /// </summary>
    public static class Enumerators
    {

        #region FIELDS

        // Enumerators repository
        private static Dictionary<string, EnumeratorMetadata> _definitions = new Dictionary<string,EnumeratorMetadata>();

        #endregion FIELDS

        #region PRIVATE METHODS

        /// <summary>
        /// Retrieves the enumerator metatata using the ginve enumerator type
        /// </summary>
        /// <param name="enumClass">Type of the enumerator to retrieve the metadata</param>
        /// <returns>enumerator metadata, null if no metadata is contained in the repository for the given type</returns>
        internal static EnumeratorMetadata GetMetadata(Type enumClass)
        {
            return (_definitions.ContainsKey(enumClass.FullName)) ? _definitions[enumClass.FullName] : null;
        }

        /// <summary>
        /// Initializes a new enumerator metadata and stores it in the global repository
        /// </summary>
        /// <param name="enumClass">Type of the enumerator to init metadata</param>
        /// <returns>Enumeration metadata</returns>
        internal static EnumeratorMetadata Init(Type enumClass)
        {
            EnumeratorMetadata metadata = new EnumeratorMetadata(enumClass.FullName);
            _definitions.Add(metadata.UID, metadata);
            return metadata;
        }

        /// <summary>
        /// Registers a new enumerator in the global repository
        /// </summary>
        /// <param name="value">Enumerator instance to register</param>
        internal static void Register(Enumerator value)
        {
            // retrieve the enum type
            Type enumDatatype = value.GetType();
            // retrieve or creates metadata
            EnumeratorMetadata metadata = GetMetadata(enumDatatype);
            if (metadata == null)
            {
                // Initializes metadata for the enumerator
                metadata = Init(enumDatatype);
            }
            // Add the enum value to the metadata
            metadata.Add(value);
        }

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// Retrieve all the enumeration names for a enumerator using the enumeration type
        /// </summary>
        /// <param name="enumDatatype"></param>
        /// <returns></returns>
        public static string[] Names(Type enumDatatype)
        {
            // Retrieve metadata
            EnumeratorMetadata metadata = GetMetadata(enumDatatype);
            if (metadata == null) throw new ArgumentException(string.Format("Enum {0} is not registered", enumDatatype.FullName));
            return metadata.Names;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumDatatype"></param>
        /// <returns></returns>
        public static T[] Values<T>(Type enumDatatype) where T : Enumerator
        {
            // Retrieve metadata
            EnumeratorMetadata metadata = GetMetadata(enumDatatype);
            if (metadata == null) throw new ArgumentException(string.Format("Enum {0} is not registered", enumDatatype.FullName));
            // Values cast
            List<T> values = new List<T>();
            foreach (Enumerator e in metadata.Values)
            {
                values.Add(e as T);
            }
            return values.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumDatatype"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static T GetById<T>(Type enumDatatype, int id) where T : Enumerator
        {
            // Retrieve metadata
            EnumeratorMetadata metadata = GetMetadata(enumDatatype);
            if (metadata == null) throw new ArgumentException(string.Format("Enum {0} is not registered", enumDatatype.FullName));
            // Retrieve the value
            T value = metadata.GetById(id) as T;
            if (value == null) throw new ArgumentException(string.Format("Enum {0} does not contains a definition for id '{1}'", enumDatatype.FullName, id));
            // Returns the value
            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumDatatype"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static T GetByName<T>(Type enumDatatype, string name) where T : Enumerator
        {
            // Retrieve metadata
            EnumeratorMetadata metadata = GetMetadata(enumDatatype);
            if (metadata == null) throw new ArgumentException(string.Format("Enum {0} is not registered", enumDatatype.FullName));
            // Retrieve the value
            T value = metadata.GetByName(name) as T;
            if (value == null) throw new ArgumentException(string.Format("Enum {0} does not contains a definition for name '{1}'", enumDatatype.FullName, name));
            // Returns the value
            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumDatatype"></param>
        /// <param name="sign"></param>
        /// <returns></returns>
        public static T GetBySign<T>(Type enumDatatype, string sign) where T : Enumerator
        {
            // Retrieve metadata
            EnumeratorMetadata metadata = GetMetadata(enumDatatype);
            if (metadata == null) throw new ArgumentException(string.Format("Enum {0} is not registered", enumDatatype.FullName));
            // Retrieve the value
            T value = metadata.GetBySign(sign) as T;
            if (value == null) throw new ArgumentException(string.Format("Enum {0} does not contains a definition for sign '{1}'", enumDatatype.FullName, sign));
            // Returns the value
            return value;
        }

        /// <summary>
        /// Retrieves a enumerator value by the given class for a custom property using the given value
        /// </summary>
        /// <typeparam name="T">Type of the enumerator to retrieve</typeparam>
        /// <param name="enumDatatype">Type</param>
        /// <param name="propertyName">Property name to use to search the enumeration value</param>
        /// <param name="propertyValue"></param>
        /// <returns>Enumeration value for the given class type, </returns>
        public static T GetByProperty<T>(Type enumDatatype, string propertyName, object propertyValue) where T : Enumerator
        {
            // Retrieve metadata
            EnumeratorMetadata metadata = GetMetadata(enumDatatype);
            if (metadata == null) throw new ArgumentException(string.Format("Enum {0} is not registered", enumDatatype.FullName));
            // Retrieve the value
            T value = metadata.GetByProperty(propertyName, propertyValue) as T;
            if (value == null) throw new ArgumentException(string.Format("Enum {0} does not contains a definition for property '{1}' with value '{2}'", enumDatatype.FullName, propertyName, propertyValue));
            // Returns the value
            return value;
        }

        #endregion PUBLIC METHODS

    }
}
