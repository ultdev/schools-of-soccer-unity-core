﻿using System;

namespace sos.data.adapters
{
    /// <summary>
    /// 
    /// </summary>
    public class DataAdapterFactoryOptions : IDataObject
    {

        #region CONSTANTS

		// Default values
		private static readonly bool                        DEFAULT_USE_SINGLETONS				    = true;
		private static readonly DataFormat                  DEFAULT_DATA_FORMAT				        = DataFormat.JSON;

        /// <summary>
        /// Default DataFactoryOptions
        /// </summary>
        private static readonly DataAdapterFactoryOptions   DEFAULT                                 = new DataAdapterFactoryOptions();

        #endregion CONSTANTS

        #region FIELDS

		// True if the factory creates only one instance for adapter, and seves it as singletons
        private bool _useSingletons;
		// Default DataFormat to use to create adapters
        private DataFormat _dataFormat;

        #endregion FIELDS

        #region CONSTRUCTORS

        public DataAdapterFactoryOptions() 
        {
            Clear();
        }

        public DataAdapterFactoryOptions(bool useSingletons, DataFormat dataFormat) 
        {
            _useSingletons  = useSingletons;
            _dataFormat = dataFormat;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        /// <summary>
        /// True if the factory creates only one instance for adapter, and seves it as singletons
        /// </summary>
        public bool UseSingletons
        {
            get { return _useSingletons; }
            set { _useSingletons = value; }
        }

        /// <summary>
        /// Default DataFormat to use to create adapters
        /// </summary>
        public DataFormat DataFormat
        {
            get { return _dataFormat; }
            set { _dataFormat = value; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="options"></param>
        public void Copy(DataAdapterFactoryOptions options)
        {
			_useSingletons	= options.UseSingletons;
			_dataFormat		= options.DataFormat;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataAdapterFactoryOptions Clone()
        {
            DataAdapterFactoryOptions options = new DataAdapterFactoryOptions();
            options.Copy(this);
            return options;
        }

        /// <summary>
        /// 
        /// </summary>
		public void Clear()
		{
			_useSingletons	= DEFAULT_USE_SINGLETONS;
			_dataFormat		= DEFAULT_DATA_FORMAT;
		}

        #endregion PUBLIC METHODS

    }
}
