﻿using System;
using System.Collections.Generic;
using sos.utils;
using sos.logging;

namespace sos.data.adapters
{
    /// <summary>
    /// Factory used to create and retrieve the adapter used to build and load 
    /// </summary>
    public class DataAdapterFactory
    {

        #region CONSTANTS

        // LT_TODO: Namespace di adapters e decoders variato, nel momento in cui verrà implementata la funzionalità
        // sarà necessario rivedere il meccanismo di composizione dei namespace\class name

        protected const	string      NAMESPACE_SEPARATOR					    = ".";
		protected const	string      ADAPTERS_SUFFIX					        = "Adapter";
		protected const	string      ADAPTERS_PACKAGE					    = "sos.data.adapters";
		protected const	string      DECODERS_SUFFIX					        = "Decoder";
		protected const	string      DECODERS_PACKAGE					    = "sos.data.decoders";

        #endregion CONSTANTS

        #region FIELDS

		// Initialization flag
		private static bool _init;
		// Default data format
		private static DataAdapterFactoryOptions _options;
		// Created adapters repository
		private static Dictionary<string, Dictionary<DataFormat, IDataAdapter>> _adapters;
		// Created decoders repository
		private static Dictionary<string, IDataDecoder> _decoders;

        #endregion FIELDS

        #region CONSTRUCTORS

        #endregion CONSTRUCTORS

        #region PROPERTIES

        /// <summary>
        /// True if the factory is initialized
        /// </summary>
		public static bool IsInit
		{
			get { return _init; }
		}

        #endregion PROPERTIES

        #region PRIVATE METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="className"></param>
        /// <returns></returns>
        private static bool IsValidDataAdapterClassName(string dataObjectClassName)
        {
            return dataObjectClassName.EndsWith(ADAPTERS_SUFFIX);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="className"></param>
        /// <returns></returns>
        private static bool IsValidDataDecoderClassName(string dataObjectClassName)
        {
            return dataObjectClassName.EndsWith(ADAPTERS_SUFFIX);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataObjectClassName"></param>
        /// <returns></returns>
        private static bool IsFullNameDataObject(string dataObjectClassName)
        {
            return dataObjectClassName.IndexOf(NAMESPACE_SEPARATOR) != Defaults.INVALID_INDEX;
        }

        /// <summary>
        /// Retrieve the namespace of the given IDataObject class name, without namespace
        /// </summary>
        /// <param name="classFullName"></param>
        /// <returns></returns>
        private static string GetDataObjectClassName(string dataObjectPath)
        {
            int idx = dataObjectPath.LastIndexOf(NAMESPACE_SEPARATOR);
            return dataObjectPath.Substring(idx + 1);
        }

        /// <summary>
        /// Retrieve the namespace of the given IDataObject class complete path
        /// </summary>
        /// <param name="dataObjectPath">IDataObject class complete path</param>
        /// <returns>The namespace to the given IDataObject class complete path</returns>
        private static string GetDataObjectNamespace(string dataObjectPath)
        {
            int idx = dataObjectPath.LastIndexOf(NAMESPACE_SEPARATOR);
            return dataObjectPath.Substring(0, idx);
        }

        /// <summary>
        /// Creates the IDataDecoder class name from the given IDataObject class name. If data object class
        /// contains the full class path, the IDataDecoder class name generated supposes that the decoder 
        /// is located in the same package/namespace ot the IDataObject, otherwise it builds the path
        /// using the defaults pagkage
        /// </summary>
        /// <param name="dataObjectName">IDataObject class name</param>
        /// <returns>The full name of the class that represents the IDataDecoder for the given IDataObject class name</returns>
		private static string BuildDataDecoderClassName(string dataObjectName, DataFormat dataFormat)
		{
            // Class full name tokens
            string package  = string.Empty;
            string clazz    = string.Empty;
            // Check if the given name contains the full namespace path, in that case retrieve the namespace
            if (IsFullNameDataObject(dataObjectName))
            {
                package = GetDataObjectNamespace(dataObjectName);
                clazz   = GetDataObjectClassName(dataObjectName);
            }
            else
            {
                package  = DECODERS_PACKAGE;
                clazz    = dataObjectName;
            }
            // Compose the decoder class full name (with package/namespace)
			return  package + "." + dataObjectName + dataFormat.Sign + DECODERS_SUFFIX;
		}
		
        /// <summary>
        /// Creates the IDataAdapter class name from the given IDataObject class name. If data object class
        /// contains the full class path, the IDataAdapter class name generated supposes that the adapter 
        /// is located in the same package/namespace ot the IDataObject, otherwise it builds the path
        /// using the defaults pagkage
        /// </summary>
        /// <param name="dataObjectName">IDataObject class name</param>
        /// <returns>The full name of the class that represents the IDataAdapter for the given IDataObject class name</returns>
		private static string BuildDataAdapterClassName(string dataObjectName)
		{
            // Class full name tokens
            string package  = string.Empty;
            string clazz    = string.Empty;
            // Check if the given name contains the full namespace path, in that case retrieve the namespace
            if (IsFullNameDataObject(dataObjectName))
            {
                package = GetDataObjectNamespace(dataObjectName);
                clazz   = GetDataObjectClassName(dataObjectName);
            }
            else
            {
                package  = ADAPTERS_PACKAGE;
                clazz    = dataObjectName;
            }
            // Compose the decoder class full name (with package/namespace)
			return  package + "." + dataObjectName + ADAPTERS_SUFFIX;
		}

        /// <summary>
        /// Creates or retrieves the decoder instance of the given IDataObject class for the given DataFormat
        /// </summary>
        /// <param name="dataObjectName">Name of the class of the IDataObject to create the decoder for</param>
        /// <param name="dataFormat">Data format that the decoder class must handler</param>
        /// <returns></returns>
		private static IDataDecoder GetDecoder(string dataObjectName, DataFormat dataFormat)
		{
			// Decoder reference
			IDataDecoder decoder = null;
			// Safe exec
			try 
			{
				// decoder class name
				string decoderClassName = BuildDataDecoderClassName(dataObjectName, dataFormat);
				// Check if the decoder is already available
				if (_options.UseSingletons == false || _decoders.ContainsKey(decoderClassName) == false)
				{
					try
					{
                        // Retrieve the decoder class type
                        Type decoderType = Type.GetType(decoderClassName);
						// Initilized the decoder
						decoder = Activator.CreateInstance(decoderType) as IDataDecoder;
						// Adds the decoder to the decoders collection
						if (_options.UseSingletons) _decoders.Add(decoderClassName, decoder);
					}
					catch (Exception ex)
					{
						throw new DataAdapterFactoryException("AdapterFactory fails to create the new IDataDecoder [" + decoderClassName + "] for the given data object [" + dataObjectName + "] : " + ex.Message, ex);
					}
				}
				else
				{
					// Retrieve the decoder
					decoder = _decoders[decoderClassName] as IDataDecoder;
				}
			}
			catch (DataAdapterFactoryException ex)
			{
				throw ex;
			}
			catch (Exception ex)
			{
				// DEBUG > Log error
				Logger.Error(ex, "DataAdapterFactory.GetDecoder() unexpected error");
				// ERROR > Unexpeted error
				throw new DataAdapterFactoryException("DataAdapterFactory.GetDecoder() unexpected error : " + ex.Message, ex);
			}
			return decoder;
		}

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

		/// <summary>
		/// Initializes the factory, cannot be called twice
		/// </summary>
		/// <param name="options"></param>
		public static void Init(DataAdapterFactoryOptions options)
		{
			// Initialization check
			if (_init) throw new DataAdapterFactoryException("DataAdapterFactory is already initialized!");
			// LT_TODO caricare configurazione
			_options = options.Clone();
			// Object initialization
			_adapters = new Dictionary<string, Dictionary<DataFormat, IDataAdapter>>();
			_decoders = new Dictionary<string, IDataDecoder>();
			// Initialization done
			_init = true;
		}

        /// <summary>
        /// Builds the adapter class name for the given IDataObject class using the dafault DataFormat (options.DataFormat)
        /// </summary>
        /// <param name="dataObjectClassName">Name of the class of the IDataObject to create the adapter for</param>
        /// <returns>Instance of the IDataAdapter class to handle the decoding process of the given IDataObject class for the default DataFormat</returns>
        public static IDataAdapter GetAdapter(string dataObjectClassName)
        {
            return GetAdapterByDataFormat(dataObjectClassName, _options.DataFormat);
        }

        /// <summary>
        /// Creates or retrieves the decoder instance of the given data object class for the given data format
        /// </summary>
        /// <param name="dataObjectClassName">Name of the class of the IDataObject to create the adapter for</param>
        /// <param name="dataFormat">Data format that the decoder class must handle</param>
        /// <returns>Instance of the IDataAdapter class to handle the decoding process of the given IDataObject class for the given DataFormat</returns>
		public static IDataAdapter GetAdapterByDataFormat(string dataObjectClassName, DataFormat dataFormat)
		{
			// Initialization check
			if (!_init) throw new DataAdapterFactoryException("DataAdapterFactory is not initialized!");
			// Adapter reference
			IDataAdapter adapter = null;
			// Safe exec
			try 
			{
				// Force the dataformat if an invalid format is provided
				dataFormat = dataFormat != null ? dataFormat : _options.DataFormat;
				// Adapter class name
				string adapterClassName = BuildDataAdapterClassName(dataObjectClassName);
				// Check if the adapter is already available
				if (_options.UseSingletons == false || _adapters.ContainsKey(adapterClassName) == false || _adapters[adapterClassName].ContainsKey(dataFormat) == false)
				{
					try
					{
						// Retrieves the decoder the adapter must use
						IDataDecoder decoder = GetDecoder(dataObjectClassName, dataFormat);
                        // Retrieve the decoder class type
                        Type decoderType = Type.GetType(adapterClassName);
						// Initilized the decoder
						adapter = Activator.CreateInstance(decoderType, decoder) as IDataAdapter;
						// Stores the adapter into the internal adapters list3
						if (_options.UseSingletons)
                        {
                            // Creates the main adapter entry
                            if (!_adapters.ContainsKey(adapterClassName)) _adapters.Add(adapterClassName, new Dictionary<DataFormat,IDataAdapter>());
                            // Adds the adapter for the given data format
                            _adapters[adapterClassName].Add(dataFormat, adapter);
                        }
					}
					catch (DataAdapterFactoryException ex)
					{
						throw ex;
					}
					catch (Exception ex)
					{
						throw new DataAdapterFactoryException("DataAdapterFactory fails to create the new IDataAdapter [" + adapterClassName + "] for the given data object [" + dataObjectClassName + "], unexpected error: " + ex.Message, ex);
					}
				}
				else
				{
					// Retrieve the adapter
					adapter = _adapters[adapterClassName][dataFormat];
				}
			}
			catch (DataAdapterFactoryException ex)
			{
				throw ex;
			}
			catch (Exception ex)
			{
				// DEBUG > Log error
				Logger.Error(ex, "DataAdapterFactory.GetAdapterByDataFormat() unexpected error");
				// ERROR > Unexpeted error
				throw new DataAdapterFactoryException("DataAdapterFactory.getAdapterByDataFormat() unexpected error : " + ex.Message, ex);
			}
			return adapter;
		}

        #endregion PUBLIC METHODS

    }
}
