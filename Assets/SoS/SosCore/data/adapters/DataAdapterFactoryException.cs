﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.data.adapters
{
    public class DataAdapterFactoryException : Exception
    {

        #region CONSTRUCTORS

        public DataAdapterFactoryException() : base() {}
        public DataAdapterFactoryException(string message) : base(message) {}
        public DataAdapterFactoryException(string message, Exception cause) : base(message, cause) {}

        #endregion CONSTRUCTORS

    }
}
