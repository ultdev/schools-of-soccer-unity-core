﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.data.adapters
{
    public class DataAdapterException : Exception
    {

        #region CONSTRUCTORS

        public DataAdapterException() : base() {}
        public DataAdapterException(string message) : base(message) {}
        public DataAdapterException(string message, Exception cause) : base(message, cause) {}

        #endregion CONSTRUCTORS

    }
}
