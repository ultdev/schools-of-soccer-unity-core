﻿using System;
using sos.logging;
using sos.data.decoders;

namespace sos.data.adapters
{
    /// <summary>
    /// Abstract data adapters implementations, all the specific adatpters of ant IDataObejec must implement this interface 
    /// </summary>
    public abstract class DataAdapter<D> : IDataAdapter where D : class, IDataObject
    {

        #region CONSTANTS

        #endregion CONSTANTS

        #region FIELDS

        // Decoded used to convert raw data into the IDataObject 
        protected IDataDecoder _decoder;

        #endregion FIELDS

        #region CONSTRUCTORS

		public DataAdapter(IDataDecoder decoder)
		{
			// Initialization check
			if (decoder == null) throw new ArgumentNullException("decoder");
			_decoder = decoder;
		}

        #endregion CONSTRUCTORS

        #region PROPERTIES

        /// <summary>
        /// Decoded used to convert raw data into the IDataObject 
        /// </summary>
		protected IDataDecoder Decoder
		{
			get { return _decoder; }
		}

        #endregion PROPERTIES

        #region PRIVATE METHODS

        /// <summary>
        /// Abstract reference to custome implemetation of the adapting defined in the descendants
        /// </summary>
        /// <returns></returns>
        protected abstract D Adapt(DataParameters parameters);

        /// <summary>
        /// Retrieves a parameter values and casts it to the desired type
        /// </summary>
        /// <typeparam name="V">Datatype of the value of the parameter to retrieve</typeparam>
        /// <param name="parameters">Parameters collection to use in the adaptiong process</param>
        /// <param name="parameterName">name of the parameter to retrieve</param>
        /// <returns>Value of the parameter for the given name</returns>
        protected V GetDataParameterValue<V>(DataParameters parameters, string parameterName) where V : class
        {
            V value = parameters[parameterName] as V;
            if (value == null) throw new DataAdapterException(string.Format("{0} data parameter [{1}] retrieving failed: the value is not of type [{2}] or is null", GetType().FullName, parameterName, typeof(V).FullName));
            return value;
        }

        /// <summary>
        /// Throws an exception to notify that a null property value for a required property of the object data used to fill the DataObject
        /// </summary>
		protected void ThrowNullFieldException()
		{
			throw new DataAdapterException(GetType().FullName + " data adapt failed: encoded data  provided should not be null!");
		}
		
        /// <summary>
        /// Throws an error to sign a missing property in the generic object used to fill the DataObject
        /// </summary>
        /// <param name="field">Name of the missing field</param>
		protected void ThrowMissingPropertyException(string field)
		{
			throw new DataAdapterException(GetType().FullName + " data adapt failed: encoded data provided doesn't contains '" + field + "' field!");
		}

        /// <summary>
        /// Throws an error to sign a missing property in the generic object used to fill the DataObject
        /// </summary>
        /// <param name="field">Name of the missing field</param>
		protected void ThrowMissingDataParameterException(string argument)
		{
			throw new DataAdapterException(GetType().FullName + " data adapt failed: arguments provided to the adapter doesn't contains '" + argument + "' argument!");
		}

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

		/**
		 * Retrieve the data object from the given generica raw data provided
		 * @param data Raw data to adapt into the fial IDataObject
		 * @return IDataObject instance
		 */		
		public IDataObject GetDataObject(object data, DataParameters parameters)
		{
			// Data object
			D dataObject = null;
			// Safe exec
			try 
			{
				// Clears decoder
				_decoder.Clear();
				// Parse the raw data to decode
				_decoder.Load(data);
				// Converts data from the decoder into the final data object, using the external given parameters
				dataObject = Adapt(parameters);
				// Clears decoder data
				_decoder.Clear();
			}
			catch (DataAdapterFactoryException ex)
			{
				// ERROR > Internal data factory error
				throw new DataAdapterException(GetType().FullName + " internal/nested data factory error : " + ex.Message, ex);
			}
			catch (DataDecoderException ex)
			{
				// ERROR > Decoder errror
				throw new DataAdapterException(GetType().FullName + " raw data decoding/parsing error : " + ex.Message, ex);
			}
			catch (DataAdapterException ex)
			{
				throw ex;
			}
			catch (Exception ex)
			{
				// DEBUG > Log unexpected error
				Logger.Error(ex, GetType().FullName + ".getDataObject() unexpected error");
				// ERROR > Unexpected error
				throw new DataAdapterException(GetType().FullName + ".getDataObject() unexpected error : " + ex.Message, ex);
			}
			// return
			return dataObject;
		}

        #endregion PUBLIC METHODS

    }
}
