﻿using System.Collections.Generic;

namespace sos.data
{
    /// <summary>
    /// 
    /// </summary>
    public class DataParameters : Dictionary<string, object>
    {

        /// <summary>
        /// Checks if a parameter is provided and has a valid value
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="parameterName"></param>
        /// <returns></returns>
        public bool Contains(string parameterName)
        {
            return ContainsKey(parameterName) && this[parameterName] != null;
        }

    }
}
