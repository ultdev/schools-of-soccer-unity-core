﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sos.data
{
    /// <summary>
    /// Base class to use to create complex class based enumerators
    /// </summary>
    public class Enumerator
    {

        #region FIELDS

        protected int _id;
        protected string _sign;
        protected string _name;

        #endregion FIELDS

        #region CONSTRUCTORS

        /// <summary>
        /// Creates a new enumerator defining only id and name
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        protected Enumerator(int id, string name) : this (id, name, null) {}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="sign"></param>
        protected Enumerator(int id, string name, string sign) : this(id, name, sign, true) {}

        /// <summary>
        /// Creates a new enumerator
        /// </summary>
        /// <param name="id">Enumerator unique id</param>
        /// <param name="name">Enumerator name</param>
        /// <param name="sign">Enumerator sign (short name/code)</param>
        /// <param name="register">If true the enum is registered into the enumerator metadata, othewise the instance is not stored inside the enumerator list</param>
        protected Enumerator(int id, string name, string sign, bool register)
        {
            _id = id;
		    _name = name;
            _sign = sign;
            // Register the enumeration value in the global enumerator repository
            if (register) Enumerators.Register(this);
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public int Id
        {
            get { return _id; }
        }

        public string Sign
        {
            get { return _sign; }
        }

        public string Name
        {
            get { return _name; }
        }

        #endregion PROPERTIES

        #region PUBLIC METHOD

        public override string ToString()
        {
            return string.Format("{0}::{1}", GetType().FullName, _name.ToUpper().Replace(" ", "_"));
        }

        #endregion PUBLIC METHOD

    }
}
