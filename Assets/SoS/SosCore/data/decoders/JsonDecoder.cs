﻿using System;
using sos.utils;
using sos.logging;

namespace sos.data.decoders
{
    /// <summary>
    /// Generic decoder implementation to handle JSON data format
    /// </summary>
    public abstract class JsonDecoder : DataDecoder
    {

        #region CONSTANTS

        #endregion CONSTANTS

        #region FIELDS

		// Flag to allow the decoder to use data alreay parsed or only JSON String data to 
		// be parsed inside load() method
		protected bool _allowParsedData;

        #endregion FIELDS

        #region CONSTRUCTORS

		public JsonDecoder(bool allowParsedData) : base(DataFormat.JSON)
		{
			// Init
			_allowParsedData = allowParsedData;
		}

        #endregion CONSTRUCTORS

        #region PROPERTIES

        /// <summary>
        /// If true the decoder can accept data already parsed before, as an Objct, otherwise data provided to
        /// Load() method must be given as JSON String
        /// </summary>
		public bool AllowParsedData
		{
			get { return _allowParsedData; }
		}

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// IDataDecoder.Load implementation
        /// </summary>
        /// <param name="data">Generic format data to be loaded into the decoder and converted into the expected format</param>
        public override void Load(object data)
        {
			// Datatype check
            bool dataIsString = data is String;
			bool dataIsJSON = JsonUtils.IsJSON(data);
			// JSON Decoder must parse and load String or JSON objects
			if (!_allowParsedData && !dataIsString) throw new DataDecoderException("JsonDecoder.load() error: Only JSON String is accepted as input raw data");
			if (!dataIsString && !dataIsJSON) throw new DataDecoderException("JsonDecoder.load() error: Only JSON String or raw Object is accepted as input raw data");
			// Try to parse and load data
			try 
			{
				// Check if the data must be parsed
				if (dataIsString)
				{
					// Converts the data into a string
					string jsonString = data as String;
					// Decodes the JSON and Saves internal decode data
					_rawdata = JsonUtils.Decode(jsonString);
				}
				else
				{
					// Saves givend data AS IS
					_rawdata = data;
				}
			}
			catch (Exception ex)
			{
				// DEBUG > Unexpected error
				Logger.Error("JsonDecoder.load() unexpected error", ex);
				// Rethrow the error
				throw new DataDecoderException("JsonDecoder.load() unexpected error: " + ex.Message);
			}
        }

        #endregion PUBLIC METHODS

    }
}
