﻿namespace sos.data.decoders
{
    public abstract class DataDecoder : IDataDecoder
    {

        #region CONSTANTS

		// Logging
		private const string LOG_MARKER						        = "DECODER > ";

        #endregion CONSTANTS

        #region FIELDS

        protected object _rawdata;
        protected DataFormat _format;

        #endregion FIELDS

        #region CONSTRUCTORS
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
		public DataDecoder(DataFormat format) 
		{
			// Init
			_format = format;
			_rawdata = null;
		}

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public object RawData
        {
            get { return _rawdata; }
        }

		public bool HasData
		{
			get { return _rawdata != null; }
		}

        public DataFormat Format
        {
            get { return _format; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        public abstract void Load(object data);

        public abstract bool Contains(string field);

        public abstract T Decode<T>(string field);

        /// <summary>
        /// Clears raw and decoded data, must be caled before load() method
        /// </summary>
        public virtual void Clear()
        {
            _rawdata = null;
        }

        #endregion PUBLIC METHODS

    }
}
