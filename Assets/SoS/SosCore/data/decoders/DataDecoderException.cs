﻿using System;

namespace sos.data.decoders
{
    /// <summary>
    /// 
    /// </summary>
    public class DataDecoderException : Exception
    {

        #region CONSTRUCTORS

        public DataDecoderException() : base() {}
        public DataDecoderException(string message) : base(message) {}
        public DataDecoderException(string message, Exception cause) : base(message, cause) {}

        #endregion CONSTRUCTORS

    }
}
