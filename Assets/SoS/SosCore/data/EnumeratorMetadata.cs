﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace sos.data
{
    public class EnumeratorMetadata
    {

        private string _uid;
        private List<int>    _ids;
        private List<string> _names;
        private List<Enumerator>   _values;

        public EnumeratorMetadata(string uid)
        {
            _uid = uid;
            _ids = new List<int>();
            _names = new List<string>();
            _values = new List<Enumerator>();
        }

        public string UID
        {
            get { return _uid; }
        }

        public int[] Ids
        {
            get { return _ids.ToArray(); }
        }

        public string[] Names
        {
            get { return _names.ToArray(); }
        }

        public Enumerator[] Values
        {
            get { return _values.ToArray(); }
        }

        public bool Contains(Enumerator value)
        {
            foreach (Enumerator e in _values)
            {
                if (e.Id == value.Id) return true;
            }
            return false;
        }

        public Enumerator GetById(int id)
        {
            foreach (Enumerator e in _values)
            {
                if (e.Id == id) return e;
            }
            return null;
        }

        public Enumerator GetByName(string name)
        {
            foreach (Enumerator e in _values)
            {
                if (e.Name == name) return e;
            }
            return null;
        }

        public Enumerator GetBySign(string sign)
        {
            foreach (Enumerator e in _values)
            {
                if (e.Sign == sign) return e;
            }
            return null;
        }

        public Enumerator GetByProperty(string property, object value)
        {
            // Enum class type
            Type enumType = _values[0].GetType();
            // retrieve the property info
            PropertyInfo propertyInfo = enumType.GetProperty(property);
            // Check if the current enum has the property
            if (propertyInfo == null) throw new ArgumentException(string.Format("Enum {0} doesn't contains {1} property", enumType.FullName, property), "property");
            // Searchs the enum by property
            foreach (Enumerator e in _values)
            {
                if (propertyInfo.GetValue(e, null) == value) return e;
            }
            return null;
        }

        public void Add(Enumerator value)
        {
            // Check for unique enum id
            if (Contains(value)) throw new ArgumentException(string.Format("Enum {0} already contains a definition with id {1}", value.GetType().FullName, value.Id), "value");
            // Registers enum data
            _ids.Add(value.Id);
            _names.Add(value.Name);
            _values.Add(value);
        }


    }
}
