﻿namespace sos.data.providers.quest
{
    /// <summary>
    /// 
    /// </summary>
    public class QuestProviderEventArgs : DataProviderEventArgs<QuestProviderEvents>
    {

        #region CONSTRUCTORS

        public QuestProviderEventArgs(QuestProvider provider, QuestProviderEvents evnt) : base(provider, evnt)
        {
        }

        #endregion CONSTRUCTORS

    }
}
