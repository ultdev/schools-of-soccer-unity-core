﻿namespace sos.data.providers.quest
{
    /// <summary>
    /// Arguments for and event that represents an error occourred during a call made by a QuestProvider instance
    /// </summary>
    public class QuestProviderErrorEventArgs : DataProviderErrorEventArgs<QuestProviderEvents>
    {

        #region CONSTRUCTORS

        public QuestProviderErrorEventArgs(QuestProvider provider, QuestProviderEvents evnt, string errorMessage) : base(provider, evnt,errorMessage)
        {
        }

        #endregion CONSTRUCTORS

    }
}
