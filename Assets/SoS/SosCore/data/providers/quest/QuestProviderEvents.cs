﻿namespace sos.data.providers.quest
{
    /// <summary>
    /// List of available events raised by the CardProvider
    /// </summary>
    public enum QuestProviderEvents
    {

        LOAD_DEFINITIONS_START          = 10,
        LOAD_DEFINITIONS_DONE           = 11,
        LOAD_DEFINITIONS_FAILED         = 12,

    }
}
