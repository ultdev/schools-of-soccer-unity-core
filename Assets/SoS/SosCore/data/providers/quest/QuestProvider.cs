﻿using sos.core.quest;
using sos.data.DTO;
using sos.data.parsers;
using sos.network;
using sos.network.http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace sos.data.providers.quest
{
    /// <summary>
    /// 
    /// </summary>
    public class QuestProvider : DataProvider
    {

		#region CONSTANTS

		#endregion CONSTANTS

        #region LoadDefinitions() method

        public delegate void LoadDefinitionsStartDelegate(QuestProviderEventArgs args);
        public delegate void LoadDefinitionsDoneDelegate(QuestProviderEventArgs args);
        public delegate void LoadDefinitionsFailedDelegate(QuestProviderErrorEventArgs args);
        
        public event LoadDefinitionsStartDelegate   LoadDefinitionsStart;
        public event LoadDefinitionsDoneDelegate    LoadDefinitionsDone;
		public event LoadDefinitionsFailedDelegate  LoadDefinitionsFailed;

        #endregion LoadDefinitions() method

		#region FIELDS

        // Singleton for QuestDefinitions
        private static QuestDefinitions _definitions;

		#endregion FIELDS

		#region CONSTRUCTORS

        public QuestProvider()
        {
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #region DTO conversion

        public Quest CreateQuest(QuestDTO questDTO)
        {
            var quest = new Quest();
            quest.Id            = questDTO.Id;
            quest.Name          = questDTO.Name;
            quest.Description   = questDTO.Description;
            quest.RewardText    = questDTO.RewardText;
            quest.Daily         = questDTO.Daily;
            quest.Image         = questDTO.Image;
            return quest;
        }

        public List<Quest> CreateQuesList(QuestDTO[] questDTOs)
        {
            List<Quest> quests = new List<Quest>();
            Array.ForEach<QuestDTO>(questDTOs, q => quests.Add( CreateQuest(q) ) );
            return quests;
        }

        #endregion DTO conversion

        #region LoadDefinitions() handlers

        private void LoadDefinitionsSuccessHandler(CallResult<string> result)
        {
            try
            {
                // Quest definition parsing
                QuestParser parser = new QuestParser();
                QuestDTO[] questDTOs = parser.ParseQuests(result.Data);
                // DTO conversion
                Quest[] quests = CreateQuesList(questDTOs).ToArray();
                // Creates a new quest definition (to check duplicates)
                QuestDefinitions definitions = new QuestDefinitions( quests );
                // Check definition existence: if exists clears an reload otherwise store the reference
                if (_definitions != null)
                {
                    _definitions.Refresh( quests );
                }
                else
                {
                    _definitions = definitions;
                }
                // Raise LOAD_DEFINITIONS_DONE event
                LoadDefinitionsDone(new QuestProviderEventArgs(this, QuestProviderEvents.LOAD_DEFINITIONS_DONE));
            }
            catch (Exception ex)
            {
                // Raise LOAD_DEFINITIONS_FAILED event
                LoadDefinitionsFailed(new QuestProviderErrorEventArgs(this, QuestProviderEvents.LOAD_DEFINITIONS_FAILED, string.Format("LoadDefinitionsSuccessHandler unexpected error: {0}", ex.Message)));
            }

        }

        private void LoadDefinitionsFailedHandler(CallResult<string> result)
        {
            try
            {
                // builds the error message
                string msg = string.Format("LoadDefinitions failed: {0}", result.ErrorMessage);
                // Raise LOAD_DEFINITIONS_FAILED event
                LoadDefinitionsFailed(new QuestProviderErrorEventArgs(this, QuestProviderEvents.LOAD_DEFINITIONS_FAILED, msg));
            }
            catch (Exception ex)
            {
                // Raise LOAD_DEFINITIONS_FAILED event
                LoadDefinitionsFailed(new QuestProviderErrorEventArgs(this, QuestProviderEvents.LOAD_DEFINITIONS_FAILED, string.Format("LoadDefinitionsFailedHandler unexpected error: {0}", ex.Message)));
            }
        }

        private void LoadDefinitionsErrorHandler(CallResult<string> result, string message)
        {
            try
            {
                // Raise LOAD_DEFINITIONS_FAILED event
                LoadDefinitionsFailed(new QuestProviderErrorEventArgs(this, QuestProviderEvents.LOAD_DEFINITIONS_FAILED, message));
            }
            catch (Exception ex)
            {
                // Raise LOAD_DEFINITIONS_FAILED event
                LoadDefinitionsFailed(new QuestProviderErrorEventArgs(this, QuestProviderEvents.LOAD_DEFINITIONS_FAILED, string.Format("LoadDefinitionsErrorHandler unexpected error: {0}", ex.Message)));
            }
        }

        #endregion LoadDefinitions() handlers

		#endregion PRIVATE METHODS

		#region PUBLIC METHODS

        /// <summary>
        /// Loads Quest definitions from SoS server
        /// 
        /// Check QuestProvider.AreDefinitionsLoaded to check if the definitions are already loaded
        /// Get QuestDefinition singleton use QuestProvider.GetDefinitions
        /// 
        /// </summary>
        public void LoadDefinitions()
        {
            // Raise LOAD_DEFINITIONS_START event
            LoadDefinitionsStart(new QuestProviderEventArgs(this, QuestProviderEvents.LOAD_DEFINITIONS_START));
            // Async execution
            StartCoroutine(ExecuteCall(GetType().Name, MethodBase.GetCurrentMethod().Name, HTTPCall.URL_QUEST_LOAD_DEFINITIONS, LoadDefinitionsSuccessHandler, LoadDefinitionsFailedHandler, LoadDefinitionsErrorHandler));
        }

		#endregion PUBLIC METHODS

        #region STATIC

        /// <summary>
        /// True if the QuestDefinition has been already loaded
        /// </summary>
        public static bool AreDefinitionsLoaded
        {
            get{ return _definitions != null; }
        }

        /// <summary>
        /// Retrieve QuestDefinitions singleton
        /// </summary>
        /// <returns></returns>
        public static QuestDefinitions GetDefinitions()
        {
            if (_definitions == null) throw new Exception("QuestDefinitions has never been loaded!");
            return _definitions;
        }

        #endregion STATIC

    }
}
