﻿namespace sos.data.providers
{
    public class DataProviderErrorEventArgs<TEventType> : DataProviderEventArgs<TEventType>
    {

        #region FIELDS

        private string _errorMessage;

        #endregion FIELDS

        #region CONSTRUCTORS

        public DataProviderErrorEventArgs(IDataProvider provider, TEventType eventType, string errorMessage) : base(provider, eventType)
        {
            _errorMessage = errorMessage;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        /// <summary>
        /// Error message related to the event 
        /// </summary>
        public string ErrorMessage
        {
            get { return _errorMessage; }
        }

        #endregion PROPERTIES

    }
}
