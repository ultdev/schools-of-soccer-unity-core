﻿using System;

namespace sos.data.providers
{
    
    /// <summary>
    /// 
    /// </summary>
    public enum DataProviderExceptionType
    {
        DECODING_ERROR      = 1,
        UNEXPECTED_ERROR    = 9,
    }

    /// <summary>
    /// Exception thrown the execution of a DataProvider
    /// </summary>
    public class DataProviderException : Exception
    {

		#region FIELDS

        private DataProviderExceptionType _type;

        #endregion FIELDS

        #region CONSTRUCTORS

        public DataProviderException(DataProviderExceptionType type) : base()
        {
            _type = type;
        }

        public DataProviderException(DataProviderExceptionType type, string message) : base(message)
        {
            _type = type;
        }

        public DataProviderException(DataProviderExceptionType type, string message, Exception cause) : base(message, cause)
        {
            _type = type;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public DataProviderExceptionType Type
        {
            get { return _type; }
        }

        #endregion PROPERTIES

    }
}
