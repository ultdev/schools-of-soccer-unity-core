﻿namespace sos.data.providers
{
    /// <summary>
    /// List of available events raised by the GameProvider
    /// </summary>
    public enum GameProviderEvents
    {

        HEARTBEAT_START                 = 10,
        HEARTBEAT_DONE                  = 11,
        HEARTBEAT_FAILED                = 12,

        REQUEST_START                   = 20,
        REQUEST_DONE                    = 21,
        REQUEST_FAILED                  = 22,

    }
}
