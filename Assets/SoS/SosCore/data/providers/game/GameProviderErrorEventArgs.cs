﻿using sos.core;

namespace sos.data.providers.game
{
    /// <summary>
    /// Arguments for and event that represents an error occourred during a call made by a GameProvider instance
    /// </summary>
    public class GameProviderErrorEventArgs : DataProviderErrorEventArgs<GameProviderEvents>
    {

        #region FIELDS
        
        #endregion FIELDS

        #region CONSTRUCTORS

        public GameProviderErrorEventArgs(GameProvider provider, GameProviderEvents evnt, string errorMessage) : base(provider, evnt,errorMessage)
        {
        }

        #endregion CONSTRUCTORS

    }
}
