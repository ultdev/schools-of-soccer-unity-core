﻿using sos.core.game;
using sos.core.game.requests;
using sos.network;
using sos.network.http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace sos.data.providers.game
{
    public class GameProvider : DataProvider
    {

		#region CONSTANTS

        // Heartbeat call parameters
        private const string    PARAM_HEARTBEAT_USERNAME                    = "username";
        private const string    PARAM_HEARTBEAT_PASSWORD                    = "password";
        private const string    PARAM_HEARTBEAT_SESSION_TOKEN               = "sessionToken";
        private const string    PARAM_HEARTBEAT_LAST_COUNT                  = "last";


		// JSON properties names
		private const string    PARAM_REQUEST_PHASE             	        = "request"; 		// GamePhase.id
		private const string    PARAM_REQUEST_TURN                          = "roundOrder";		// Game.turn
		private const string    PARAM_REQUEST_GAME_ID             	        = "gameID";
		private const string    PARAM_REQUEST_USER_ID             	        = "userID";
		private const string    PARAM_REQUEST_USER_PASSWORD            	    = "password";
		private const string    PARAM_REQUEST_ACTION_DATA			        = "actionJson";
		private const string    PARAM_REQUEST_SESSION_TOKEN                 = "sessionToken";
		private const string    PARAM_REQUEST_GAME_TOKEN                    = "gameToken";

		#endregion CONSTANTS

		#region FIELDS

		#endregion FIELDS

        #region EVENTS AND DELEGATES

        #region Heartbeat() method

        public delegate void HeartbeatStartDelegate(GameProviderEventArgs args);
        public delegate void HeartbeatDoneDelegate(GameProviderEventArgs args);
        public delegate void HeartbeatFailedDelegate(GameProviderErrorEventArgs args);
        
        public event HeartbeatStartDelegate     HeartbeatStart;
        public event HeartbeatDoneDelegate      HeartbeatDone;
		public event HeartbeatFailedDelegate    HeartbeatFailed;

        #endregion Heartbeat() method

        #region Request() method

        public delegate void RequestStartDelegate(GameProviderEventArgs args);
        public delegate void RequestDoneDelegate(GameProviderRequestEventArgs args);
        public delegate void RequestFailedDelegate(GameProviderErrorEventArgs args);
        
        public event RequestStartDelegate     RequestStart;
        public event RequestDoneDelegate      RequestDone;
		public event RequestFailedDelegate    RequestFailed;

        #endregion Request() method

        #endregion EVENTS AND DELEGATES

		#region CONSTRUCTORS

        public GameProvider()
        {
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #region Heartbeat() call event handlers

        private void HeartbeatSuccessHandler(CallResult<string> result)
        {
            try
            {
                // Raise HEARTBEAT_DONE event
                if (HeartbeatDone != null) HeartbeatDone(new GameProviderEventArgs(this, GameProviderEvents.HEARTBEAT_DONE));
            }
            catch (Exception ex)
            {
                // Raise HEARTBEAT_FAILED event
                if (HeartbeatFailed != null) HeartbeatFailed(new GameProviderErrorEventArgs(this, GameProviderEvents.HEARTBEAT_FAILED, string.Format("HeartbeatSuccessHandler unexpected error: {0}", ex.Message)));
            }

        }

        private void HeartbeatFailedHandler(CallResult<string> result)
        {
            try
            {
                // Raise HEARTBEAT_FAILED event
                if (HeartbeatFailed != null) HeartbeatFailed(new GameProviderErrorEventArgs(this, GameProviderEvents.HEARTBEAT_FAILED, "Heartbeat call failed: " + result.ErrorMessage));
            }
            catch (Exception ex)
            {
                // Raise HEARTBEAT_FAILED event
                if (HeartbeatFailed != null) HeartbeatFailed(new GameProviderErrorEventArgs(this, GameProviderEvents.HEARTBEAT_FAILED, "HeartbeatFailedHandler unexpected error: " + ex.Message));
            }
        }

        private void HeartbeatErrorHandler(CallResult<string> result, string message)
        {
            try
            {
                // Raise HEARTBEAT_FAILED event
                if (HeartbeatFailed != null) HeartbeatFailed(new GameProviderErrorEventArgs(this, GameProviderEvents.HEARTBEAT_FAILED, "Heartbeat call error: " + message));
            }
            catch (Exception ex)
            {
                // Raise HEARTBEAT_FAILED event
                if (HeartbeatFailed != null) HeartbeatFailed(new GameProviderErrorEventArgs(this, GameProviderEvents.HEARTBEAT_FAILED, "HeartbeatErrorHandler unexpected error: " + ex.Message));
            }
        }

        #endregion Heartbeat() call event handlers

        #region Request() call event handlers

        private void RequestSuccessHandler(CallResult<string> result)
        {
            try
            {
                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                // LT_TODO: Request result parsing
                GameRequestResult fakeRequestResult = new GameRequestResult();
                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                // Raise REQUEST_DONE event
                if (RequestDone != null) RequestDone(new GameProviderRequestEventArgs(this, GameProviderEvents.REQUEST_DONE, fakeRequestResult));
            }
            catch (Exception ex)
            {
                // Raise REQUEST_FAILED event
                if (RequestFailed != null) RequestFailed(new GameProviderErrorEventArgs(this, GameProviderEvents.REQUEST_FAILED, string.Format("RequestSuccessHandler unexpected error: {0}", ex.Message)));
            }

        }

        private void RequestFailedHandler(CallResult<string> result)
        {
            try
            {
                // Raise REQUEST_FAILED event
                if (RequestFailed != null) RequestFailed(new GameProviderErrorEventArgs(this, GameProviderEvents.REQUEST_FAILED, "Request call failed: " + result.ErrorMessage));
            }
            catch (Exception ex)
            {
                // Raise REQUEST_FAILED event
                if (RequestFailed != null) RequestFailed(new GameProviderErrorEventArgs(this, GameProviderEvents.REQUEST_FAILED, "RequestFailedHandler unexpected error: " + ex.Message));
            }
        }

        private void RequestErrorHandler(CallResult<string> result, string message)
        {
            try
            {
                // Raise REQUEST_FAILED event
                if (RequestFailed != null) RequestFailed(new GameProviderErrorEventArgs(this, GameProviderEvents.REQUEST_FAILED, "Request call error: " + message));
            }
            catch (Exception ex)
            {
                // Raise REQUEST_FAILED event
                if (RequestFailed != null) RequestFailed(new GameProviderErrorEventArgs(this, GameProviderEvents.REQUEST_FAILED, "RequestErrorHandler unexpected error: " + ex.Message));
            }
        }

        #endregion Request() call event handlers

        #region Misc

        /// <summary>
        /// Builds the ActionData parameter for each GameRequest phase: each phase
        /// has his own parameters to pass to the SoS server 
        /// 
        /// Currently ActionData is JSON encoded
        /// </summary>
        /// <param name="pars">Request parameters used to build the action data</param>
        /// <returns></returns>
        public string GetActionDataByRequestParamsPhase(GameRequestParams pars)
        {
            // custom action data by request phase
            var actionData = string.Empty;

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            // LT_TODO: Implementare parametri custom da vecchi metodi virtuali 
            // GameRequestParams.createActionData()
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if (pars.Phase == GamePhase.SUBSTITUTE)
            {
            }
            else if (pars.Phase == GamePhase.RESOLVE_SUBSTITUTE)
            {
            }
            else if (pars.Phase == GamePhase.DISCARD)
            {
            }
            else if (pars.Phase == GamePhase.RESOLVE_DISCARD)
            {
            }
            else if (pars.Phase == GamePhase.PLAY)
            {
            }
            else if (pars.Phase == GamePhase.RESOLVE_PLAY)
            {
            }
            else if (pars.Phase == GamePhase.RESYNC)
            {
            }
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            return actionData;
        }

        #endregion Misc

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /*
        public void Heartbeat(string userID, string password, int lastHeartbeatCount)
        {
            // Creates call parameters
            Dictionary<string, string> prms = new Dictionary<string, string>();
            prms.Add(PARAM_HEARTBEAT_USERNAME,      userID);
            prms.Add(PARAM_HEARTBEAT_PASSWORD,      password);
            prms.Add(PARAM_HEARTBEAT_LAST_COUNT,    lastHeartbeatCount.ToString());
            // Executes the call
            StartCoroutine(ExecuteCall(GetType().Name, 
                                       MethodBase.GetCurrentMethod().Name,
                                       HTTPCall.URL_GAME_HEARTBEAT,
                                       HeartbeatSuccessHandler,
                                       HeartbeatFailedHandler,
                                       HeartbeatErrorHandler));
        }
        */

        /// <summary>
        /// Performs the heartbeat call to SoS server, used to establish and maitein the server
        /// client Game connection
        /// </summary>
        /// <param name="sessionToken"></param>
        /// <param name="lastHeartbeatCount"></param>
        public void Heartbeat(string sessionToken, int lastHeartbeatCount)
        {
            // Creates call parameters
            Dictionary<string, string> prms = new Dictionary<string, string>();
            prms.Add(PARAM_HEARTBEAT_SESSION_TOKEN, sessionToken);
            prms.Add(PARAM_HEARTBEAT_LAST_COUNT,    lastHeartbeatCount.ToString());
            // Executes the call
            StartCoroutine(ExecuteCall(GetType().Name, 
                                       MethodBase.GetCurrentMethod().Name,
                                       HTTPCall.URL_GAME_HEARTBEAT,
                                       prms,
                                       HeartbeatSuccessHandler,
                                       HeartbeatFailedHandler,
                                       HeartbeatErrorHandler));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        public void Request(GameRequest request)
        {
            // Creates call parameters
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            // Base request call params
            parameters.Add(PARAM_REQUEST_PHASE,           request.Params.Phase.Id.ToString());
            parameters.Add(PARAM_REQUEST_TURN,            request.Params.Turn.ToString());
            parameters.Add(PARAM_REQUEST_GAME_ID,         request.Params.GameId.ToString());
            parameters.Add(PARAM_REQUEST_USER_ID,         request.Params.UserId.ToString());
            parameters.Add(PARAM_REQUEST_USER_PASSWORD,   request.Params.Password);
            parameters.Add(PARAM_REQUEST_SESSION_TOKEN,   request.Params.SessionToken);
            parameters.Add(PARAM_REQUEST_GAME_TOKEN,      request.Params.GameToken);
            parameters.Add(PARAM_REQUEST_ACTION_DATA,     GetActionDataByRequestParamsPhase(request.Params));
            // Executes the call
            StartCoroutine(ExecuteCall(GetType().Name, 
                                       MethodBase.GetCurrentMethod().Name,
                                       HTTPCall.URL_GAME_REQUEST,
                                       parameters,
                                       RequestSuccessHandler,
                                       RequestFailedHandler,
                                       RequestErrorHandler));
        }

		#endregion PUBLIC METHODS

    }
}
