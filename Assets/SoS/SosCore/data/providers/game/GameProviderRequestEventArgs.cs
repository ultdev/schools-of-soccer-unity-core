﻿using sos.core;
using sos.core.game.requests;

namespace sos.data.providers.game
{
    /// <summary>
    /// 
    /// </summary>
    public class GameProviderRequestEventArgs : DataProviderEventArgs<GameProviderEvents>
    {

        #region FIELDS
        
        private GameRequestResult _result;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameProviderRequestEventArgs(GameProvider provider, GameProviderEvents evnt, GameRequestResult result) : base(provider, evnt)
        {
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public GameRequestResult Result
        {
            get { return _result; }
        }

        #endregion PROPERTIES

    }
}
