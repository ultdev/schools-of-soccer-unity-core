﻿using sos.core;

namespace sos.data.providers.game
{
    /// <summary>
    /// 
    /// </summary>
    public class GameProviderEventArgs : DataProviderEventArgs<GameProviderEvents>
    {

        #region FIELDS
        
        #endregion FIELDS

        #region CONSTRUCTORS

        public GameProviderEventArgs(GameProvider provider, GameProviderEvents evnt) : base(provider, evnt)
        {
        }

        #endregion CONSTRUCTORS

    }
}
