﻿using sos.logging;
using sos.network;
using sos.network.http;
using sos.utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sos.data.providers
{
    public class DataProvider : MonoBehaviour, IDataProvider
    {

        #region CONSTANTS

        // Logging debug key (used to enable or disable logging in the Logger class)
        public static string    DEBUG_KEY                   = "sos.data.providers.DataProvider";

        #endregion CONSTANTS

        #region EVENTS AND DELEGATES

        protected delegate void CallSuccededCallback(CallResult<string> result);
        protected delegate void CallFailedCallback(CallResult<string> result);
        protected delegate void CallErrorCallback(CallResult<string> result, string message);

        #endregion EVENTS AND DELEGATES

        #region PRIVATE METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="relativeUrl"></param>
        /// <returns></returns>
        protected string GetAbsoluteUrl(string relativeUrl)
        {
            return HTTPCall.URL_BASE + relativeUrl;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TDataFormat"></typeparam>
        /// <param name="call"></param>
        /// <param name="messageToFormat"></param>
        /// <returns></returns>
        protected string GetCallErrorMessage<TDataFormat>(ICall<TDataFormat> call, string messageToFormat) where TDataFormat : class
        {
            return string.Format(messageToFormat, call.Result.ErrorMessage, EnumUtils.GetReadableName<CallStatus>(call.Status));
        }

        /// <summary>
        /// Unified handling for all async SoS server call, without passing any parameter
        /// </summary>
        /// <param name="providerName"></param>
        /// <param name="providerMethod"></param>
        /// <param name="relativeUrl"></param>
        /// <param name="successCallback"></param>
        /// <param name="failCallback"></param>
        /// <param name="errorCallback"></param>
        /// <returns></returns>
        protected IEnumerator ExecuteCall(string providerName, string providerMethod, string relativeUrl, CallSuccededCallback successCallback, CallFailedCallback failCallback, CallErrorCallback errorCallback)
        {
            return ExecuteCall(providerName, providerMethod, relativeUrl, new Dictionary<string, string>(), successCallback, failCallback, errorCallback);
        }

        /// <summary>
        /// Unified handling for all async SoS server call
        /// </summary>
        /// <param name="providerName"></param>
        /// <param name="providerMethod"></param>
        /// <param name="relativeUrl"></param>
        /// <param name="parameters"></param>
        /// <param name="successCallback"></param>
        /// <param name="failCallback"></param>
        /// <param name="errorCallback"></param>
        /// <returns></returns>
        protected IEnumerator ExecuteCall(string providerName, string providerMethod, string relativeUrl, Dictionary<string, string> parameters, CallSuccededCallback successCallback, CallFailedCallback failCallback, CallErrorCallback errorCallback)
        {
            // Locals
            string gameObjectName = string.Format("{0}_{1}_Call_{2:yyyyMMdd_HHmmssfff}", providerName, providerMethod, System.DateTime.Now);
            string logMethodTrace = string.Format("{0}.{1}()", providerName, providerMethod);
            // LOG
            if (sos.logging.Logger.CanDebug(DEBUG_KEY)) sos.logging.Logger.Debug("{0} > BEGIN", logMethodTrace);
            // Creates a GameObject to create a MonoBehavior
            GameObject obj = new GameObject(gameObjectName);
            GameObject.DontDestroyOnLoad(obj);
            // Creates the call
            ICall<string> call = obj.AddComponent<HTTPCall>();
            call.Execute(GetAbsoluteUrl(relativeUrl), parameters);
            // Loops untile the call completes
            while (!call.IsExecuted)
            {
                // LOG
                if (sos.logging.Logger.CanDebug(DEBUG_KEY)) sos.logging.Logger.Debug("{0} > Waiting for call to be executed", logMethodTrace);
                yield return null;
            }
            // Creates a copy of the call results
            CallResult<string> result = call.Result.Clone();
            // Check if the execution caused an unexpected error
            if (!call.IsInError)
            {
                // Check if the SoS server response is successful or contains an handler error
                if (call.Result.IsSuccesfull)
                {
                    // LOG
                    if (sos.logging.Logger.CanDebug(DEBUG_KEY)) sos.logging.Logger.Debug("{0} > CALL SUCCEDED!", logMethodTrace);
                    // Execute CALL SUCCESS callback delegate
                    successCallback(result);
                }
                else
                {
                    // LOG
                    if (sos.logging.Logger.CanDebug(DEBUG_KEY)) sos.logging.Logger.Debug("{0} > CALL FAILED!", logMethodTrace);
                    // Execute CALL FAILED callback delegate
                    failCallback(result);
                }
            }
            else
            {
                // LOG
                if (sos.logging.Logger.CanDebug(DEBUG_KEY)) sos.logging.Logger.Debug("{0} > CALL ERROR!", logMethodTrace);
                // Execute CALL ERROR callback delegate
                errorCallback(result, GetCallErrorMessage<string>(call, "Login in error: {0} (internal error: {1})"));
            }
            // Destroy the GameObject
            GameObject.DestroyImmediate(obj);
            // LOG
            if (sos.logging.Logger.CanDebug(DEBUG_KEY)) sos.logging.Logger.Debug("{0} > END", logMethodTrace);
        }

        #endregion PRIVATE METHODS

    }
}
