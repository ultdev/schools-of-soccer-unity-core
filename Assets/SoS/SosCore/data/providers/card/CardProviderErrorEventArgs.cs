﻿namespace sos.data.providers.card
{
    /// <summary>
    /// Arguments for and event that represents an error occourred during a call made by a CardProvider instance
    /// </summary>
    public class CardProviderErrorEventArgs : DataProviderErrorEventArgs<CardProviderEvents>
    {

        #region CONSTRUCTORS

        public CardProviderErrorEventArgs(CardProvider provider, CardProviderEvents evnt, string errorMessage) : base(provider, evnt,errorMessage)
        {
        }

        #endregion CONSTRUCTORS

    }
}
