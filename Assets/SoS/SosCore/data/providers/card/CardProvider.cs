﻿using sos.core.cards;
using sos.data.parsers.card;
using sos.network;
using sos.network.http;
using System;
using System.Linq;
using System.Reflection;

namespace sos.data.providers.card
{
    /// <summary>
    /// 
    /// </summary>
    public class CardProvider : DataProvider
    {

		#region CONSTANTS

		#endregion CONSTANTS

		#region FIELDS

        // Singleton for CardDefinitions
        private static CardDefinitions _definitions;

		#endregion FIELDS

        #region EVENTS AND DELEGATES

        #region LoadDefinitions() method

        public delegate void LoadDefinitionsStartDelegate(CardProviderEventArgs args);
        public delegate void LoadDefinitionsDoneDelegate(CardProviderEventArgs args);
        public delegate void LoadDefinitionsFailedDelegate(CardProviderErrorEventArgs args);
        
        public event LoadDefinitionsStartDelegate   LoadDefinitionsStart;
        public event LoadDefinitionsDoneDelegate    LoadDefinitionsDone;
		public event LoadDefinitionsFailedDelegate  LoadDefinitionsFailed;

        #endregion LoadDefinitions() method

        #endregion EVENTS AND DELEGATES

        #region CONSTRUCTORS

        public CardProvider()
        {
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #region LoadDefinitions() handlers

        private void LoadDefinitionsSuccessHandler(CallResult<string> result)
        {
            try
            {
                // Card definition parsing
                CardParser parser = new CardParser();
                Card[] cards = parser.ParseCards(result.Data);
                // Creates a new cards definition (to check duplicates)
                CardDefinitions definitions = new CardDefinitions(cards);
                // Check definition existence: if exists clears an reload otherwise store the reference
                if (_definitions != null)
                {
                    _definitions.Refresh(cards.ToArray());
                }
                else
                {
                    _definitions = definitions;
                }
                // Raise LOAD_DEFINITIONS_DONE event
                LoadDefinitionsDone(new CardProviderEventArgs(this, CardProviderEvents.LOAD_DEFINITIONS_DONE));
            }
            catch (Exception ex)
            {
                // Raise LOAD_DEFINITIONS_FAILED event
                LoadDefinitionsFailed(new CardProviderErrorEventArgs(this, CardProviderEvents.LOAD_DEFINITIONS_FAILED, string.Format("LoadDefinitionsSuccessHandler unexpected error: {0}", ex.Message)));
            }

        }

        private void LoadDefinitionsFailedHandler(CallResult<string> result)
        {
            try
            {
                // builds the error message
                string msg = string.Format("LoadDefinitions failed: {0}", result.ErrorMessage);
                // Raise LOAD_DEFINITIONS_FAILED event
                LoadDefinitionsFailed(new CardProviderErrorEventArgs(this, CardProviderEvents.LOAD_DEFINITIONS_FAILED, msg));
            }
            catch (Exception ex)
            {
                // Raise LOAD_DEFINITIONS_FAILED event
                LoadDefinitionsFailed(new CardProviderErrorEventArgs(this, CardProviderEvents.LOAD_DEFINITIONS_FAILED, string.Format("LoadDefinitionsFailedHandler unexpected error: {0}", ex.Message)));
            }
        }

        private void LoadDefinitionsErrorHandler(CallResult<string> result, string message)
        {
            try
            {
                // Raise LOAD_DEFINITIONS_FAILED event
                LoadDefinitionsFailed(new CardProviderErrorEventArgs(this, CardProviderEvents.LOAD_DEFINITIONS_FAILED, message));
            }
            catch (Exception ex)
            {
                // Raise LOAD_DEFINITIONS_FAILED event
                LoadDefinitionsFailed(new CardProviderErrorEventArgs(this, CardProviderEvents.LOAD_DEFINITIONS_FAILED, string.Format("LoadDefinitionsErrorHandler unexpected error: {0}", ex.Message)));
            }
        }

        #endregion LoadDefinitions() handlers

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// Loads Cards definitions from SoS server
        /// 
        /// Check CardProvider.AreDefinitionsLoaded to check if the definitions are already loaded
        /// Get CardDefinition singleton use CardProvider.GetDefinitions
        /// 
        /// </summary>
        public void LoadDefinitions()
        {
            // Raise LOAD_DEFINITIONS_START event
            LoadDefinitionsStart(new CardProviderEventArgs(this, CardProviderEvents.LOAD_DEFINITIONS_START));
            // Async execution
            StartCoroutine(ExecuteCall(GetType().Name, MethodBase.GetCurrentMethod().Name, HTTPCall.URL_CARDS_LOAD_DEFINITIONS, LoadDefinitionsSuccessHandler, LoadDefinitionsFailedHandler, LoadDefinitionsErrorHandler));
        }

		#endregion PUBLIC METHODS

        #region STATIC

        /// <summary>
        /// True if the QuestDefinition has been already loaded
        /// </summary>
        public static bool AreDefinitionsLoaded
        {
            get{ return _definitions != null; }
        }

        /// <summary>
        /// Retrieve CardDefinitions singleton
        /// </summary>
        /// <returns></returns>
        public static CardDefinitions GetDefinitions()
        {
            if (_definitions == null) throw new Exception("CardDefinitions has never been loaded!");
            return _definitions;
        }

        #endregion #region STATIC
        
    }
}
