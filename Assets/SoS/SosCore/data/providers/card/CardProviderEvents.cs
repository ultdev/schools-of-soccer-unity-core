﻿namespace sos.data.providers.card
{
    /// <summary>
    /// List of available events raised by the CardProvider
    /// </summary>
    public enum CardProviderEvents
    {

        LOAD_DEFINITIONS_START          = 10,
        LOAD_DEFINITIONS_DONE           = 11,
        LOAD_DEFINITIONS_FAILED         = 12,

    }
}
