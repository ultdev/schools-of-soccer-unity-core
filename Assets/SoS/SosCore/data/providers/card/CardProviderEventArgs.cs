﻿namespace sos.data.providers.card
{
    /// <summary>
    /// 
    /// </summary>
    public class CardProviderEventArgs : DataProviderEventArgs<CardProviderEvents>
    {

        #region CONSTRUCTORS

        public CardProviderEventArgs(CardProvider provider, CardProviderEvents evnt) : base(provider, evnt)
        {
        }

        #endregion CONSTRUCTORS

    }
}
