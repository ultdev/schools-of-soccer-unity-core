﻿using System;

namespace sos.data.providers
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class DataProviderEventArgs<TEventType> : EventArgs, IDataProviderEventArgs<TEventType>
    {

        #region FIELDS

        private TEventType _eventType;
        private IDataProvider _provider;

        #endregion FIELDS

        #region CONSTRUCTORS

        public DataProviderEventArgs(IDataProvider provider, TEventType eventType) : base()
        {
            _eventType = eventType;
            _provider = provider;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        /// <summary>
        /// <see cref="IDataProviderEventArgs{TEventType}"/>
        /// </summary>
        public TEventType EventType
        {
            get
            {
                return _eventType;
            }
        }

        /// <summary>
        /// <see cref="IDataProviderEventArgs{TEventType}"/>
        /// </summary>
        public IDataProvider Provider
        {
            get { return _provider; }
            protected set{ _provider = value; }
        }

        #endregion

    }
}
