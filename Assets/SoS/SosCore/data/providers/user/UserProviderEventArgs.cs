﻿using sos.core;

namespace sos.data.providers.user
{
    /// <summary>
    /// 
    /// </summary>
    public class UserProviderEventArgs : DataProviderEventArgs<UserProviderEvents>
    {

        #region FIELDS

        private User _user;
        
        #endregion FIELDS

        #region CONSTRUCTORS

        public UserProviderEventArgs(UserProvider provider, UserProviderEvents evnt) : this(provider, evnt, null)
        {
        }

        public UserProviderEventArgs(UserProvider provider, UserProviderEvents evnt, User user) : base(provider, evnt)
        {
            _user = user;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public User User
        {
            get { return _user; }
        }

        #endregion PROPERTIES

    }
}
