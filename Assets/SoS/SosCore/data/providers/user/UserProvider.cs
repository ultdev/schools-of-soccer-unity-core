﻿using sos.core;
using sos.core.cards;
using sos.core.helpers;
using sos.core.quest;
using sos.data.DTO;
using sos.data.parsers;
using sos.data.parsers.card;
using sos.data.providers.card;
using sos.data.providers.quest;
using sos.logging;
using sos.network;
using sos.network.http;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace sos.data.providers.user
{

    /// <summary>
    /// 
    /// </summary>
    public class UserProvider : DataProvider
    {

        #region CONSTANTS

        // options
        private const bool      DO_FAKE_LOGIN               = false;

        // Login parameters names
        private const string    PARAM_LOGIN_USERNAME        = "username";
        private const string    PARAM_LOGIN_PASSWORD        = "password";

        #endregion CONSTANTS

        #region EVENTS AND DELEGATES

        #region Login() method

        public delegate void LoginStartDelegate(UserProviderEventArgs args);
        public delegate void LoginDoneDelegate(UserProviderEventArgs args);
        public delegate void LoginFailedDelegate(UserProviderErrorEventArgs args);
        
        public event LoginStartDelegate LoginStart;
        public event LoginDoneDelegate LoginDone;
		public event LoginFailedDelegate LoginFailed;

        #endregion Login() method

        #region Register() method

        public delegate void RegisterStartDelegate(UserProviderEventArgs args);
        public delegate void RegisterDoneDelegate(UserProviderEventArgs args);
        public delegate void RegisterFailedDelegate(UserProviderErrorEventArgs args);
        
        public event RegisterStartDelegate     RegisterStart;
        public event RegisterDoneDelegate      RegisterDone;
		public event RegisterFailedDelegate    RegisterFailed;

        #endregion Register() method

        #region ChooseStarterDeck() method

        public delegate void ChooseStarterDeckStartDelegate(UserProviderEventArgs args);
        public delegate void ChooseStarterDeckDoneDelegate(UserProviderEventArgs args);
        public delegate void ChooseStarterDeckFailedDelegate(UserProviderErrorEventArgs args);
        
        public event ChooseStarterDeckStartDelegate     ChooseStarterDeckStart;
        public event ChooseStarterDeckDoneDelegate      ChooseStarterDeckDone;
		public event ChooseStarterDeckFailedDelegate    ChooseStarterDeckFailed;

        #endregion ChooseStarterDeck() method

        #region LoadFriendList() method

        public delegate void LoadFriendsStartDelegate(UserProviderEventArgs args);
        public delegate void LoadFriendsDoneDelegate(UserProviderLoadFriendsEventArgs args);
        public delegate void LoadFriendsFailedDelegate(UserProviderErrorEventArgs args);
        
        public event LoadFriendsStartDelegate     LoadFriendsStart;
        public event LoadFriendsDoneDelegate      LoadFriendsDone;
		public event LoadFriendsFailedDelegate    LoadFriendsFailed;

        #endregion LoadFriendList() method
		
		#endregion EVENTS AND DELEGATES

        #region CONSTRUCTORS

        public UserProvider()
        {
        }

        #endregion CONSTRUCTORS

        #region PRIVATE METHODS

        #region DTO conversion

        private Deck CreateDeck(Collection collection, DeckDTO deckDTO)
        {
            // New deck
            Deck deck;
            // Safe exec
            try
            {
                // Init
                deck = new Deck();
                deck.Id = deckDTO.Id;
                deck.Name = deckDTO.Name;
                deck.Active = deckDTO.Active;
                // Adds all action cards instances
                foreach (var actionCard in deckDTO.ActionCards)
                {
                    deck.AddActionCard( collection.GetActionCardByInstanceId(actionCard) );
                }
                // Adds all player cards instances to the formation
                deck.SetPlayerCardByPosition(Position.GOALKEEPER,   collection.GetPlayerCardByInstanceId(deckDTO.Goalkeeper) );
                deck.SetPlayerCardByPosition(Position.DEFENDER_01,  collection.GetPlayerCardByInstanceId(deckDTO.Defence01) );
                deck.SetPlayerCardByPosition(Position.DEFENDER_02,  collection.GetPlayerCardByInstanceId(deckDTO.Defence02) );
                deck.SetPlayerCardByPosition(Position.MIDFIELD,     collection.GetPlayerCardByInstanceId(deckDTO.Midfield) );
                deck.SetPlayerCardByPosition(Position.FORWARD_01,   collection.GetPlayerCardByInstanceId(deckDTO.Forward01) );
                deck.SetPlayerCardByPosition(Position.FORWARD_02,   collection.GetPlayerCardByInstanceId(deckDTO.Forward02) );
                deck.SetPlayerCardByPosition(Position.SUB_DEFENCE,  collection.GetPlayerCardByInstanceId(deckDTO.SubDefence) );
                deck.SetPlayerCardByPosition(Position.SUB_MIDFIELD, collection.GetPlayerCardByInstanceId(deckDTO.SubMidfield) );
                deck.SetPlayerCardByPosition(Position.SUB_FORWARD,  collection.GetPlayerCardByInstanceId(deckDTO.SubForward) );
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Deck creation failed, unexpected error: {0}", ex.Message), ex);
            }
            return deck;
        }

        private Collection CreateCollection(CardDefinitions definitions, CollectionCardDTO[] cardsDTO, DeckDTO[] decksDTO)
        {
            // New collection
            Collection collection;
            // Safe exec
            try
            {
                // Init
                collection = new Collection();
                // Creates the card instances
                foreach (var cardDTO in cardsDTO)
                {
                    // Retrieve the card definition
                    Card card = definitions[cardDTO.Id];
                    // Creates each instance from the card definition
                    foreach (var instanceId in cardDTO.InstanceList)
                    {
                        collection.AddCard( card.CreateInstance( instanceId ) );
                    }
                }
                // Creates decks instances and adds to the collection
                foreach (var deckDTO in decksDTO)
                {
                    collection.AddDeck( CreateDeck(collection , deckDTO) );
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Collection creation failed, unexpected error: {0}", ex.Message), ex);
            }            
            return collection;
        }

        private UserQuestStatus CreateUserQuestStatus(int userQuestStatus)
        {
            try
            {
                return UserQuestStatus.GetById(userQuestStatus);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("UserQuestStatus creation failed, unexpected error: {0}", ex.Message), ex);
            }
        }

        private UserQuest CreateUserQuest(QuestDefinitions definitions, UserQuestDTO questDTO)
        {
            // New collection
            UserQuest userQuest;
            // Safe exec
            try
            {

                // Retrieve quest definition
                Quest definition = definitions[questDTO.Id];
                // Retrieve the quest status
                UserQuestStatus status = CreateUserQuestStatus(questDTO.Status);
                // Creates the user quest instance
                userQuest = new UserQuest(definition, questDTO.InstanceId, questDTO.ItemCount, status);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("UserQuest creation failed, unexpected error: {0}", ex.Message), ex);
            }            
            return userQuest;
        }

        private UserQuestList CreateUserQuestList(QuestDefinitions definitions, UserQuestDTO[] questsDTO)
        {
            // New collection
            UserQuestList userQuestList = new UserQuestList();
            // Safe exec
            try
            {
                foreach (UserQuestDTO questDTO in questsDTO)
                {
                    userQuestList.Add( CreateUserQuest(definitions, questDTO) );
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("UserQuestList creation failed, unexpected error: {0}", ex.Message), ex);
            }            
            return userQuestList;
        }

        #endregion DTO conversion

        #region Login() call event handlers

        private void LoginSuccessHandler(CallResult<string> result)
        {
            try
            {
                User user = null;
                // Fake user retrieve
                if (!DO_FAKE_LOGIN)
                {

                    // Retrieve the cards definitions
                    CardDefinitions cardDefinitions = CardProvider.GetDefinitions();
                    QuestDefinitions questDefinitions = QuestProvider.GetDefinitions();
                    // Parsers creation
                    UserParser parser = new UserParser();
                    CardParser cardParser = new CardParser();
                    QuestParser questParser = new QuestParser(); 
                    // User parsing
                    user = parser.ParseUserFromLoginData(result.Data);
                    // Card parsing
                    CollectionCardDTO[] collection = cardParser.ParseCollectionFromLoginData(result.Data);
                    DeckDTO[] decks = cardParser.ParseDecksFromLoginData(result.Data);
                    // Quest parsing
                    UserQuestDTO[] quests = questParser.ParseUserQuestsFromLoginData(result.Data);
                    // User data filling: collections, deck, quests and shop items
                    user.UpdateCollection( CreateCollection( cardDefinitions, collection, decks ) );
                    user.UpdateQuests( CreateUserQuestList( questDefinitions, quests ) );
                }
                else
                {
                    user = UserHelper.GetUser();
                }
                // Raise LOGIN_DONE event
                LoginDone(new UserProviderEventArgs(this, UserProviderEvents.LOGIN_DONE, user));
            }
            catch (Exception ex)
            {
                // Raise LOGIN_FAILED event
                LoginFailed(new UserProviderErrorEventArgs(this, UserProviderEvents.LOGIN_FAILED, string.Format("LoginSuccessHandler unexpected error: {0}", ex.Message)));
            }

        }

        private void LoginFailedHandler(CallResult<string> result)
        {
            try
            {
                // builds the error message
                string msg = string.Format("Login failed: {0}", result.ErrorMessage);
                // Raise LOGIN_FAILED event
                LoginFailed(new UserProviderErrorEventArgs(this, UserProviderEvents.LOGIN_FAILED, msg));
            }
            catch (Exception ex)
            {
                // Raise LOGIN_FAILED event
                LoginFailed(new UserProviderErrorEventArgs(this, UserProviderEvents.LOGIN_FAILED, string.Format("LoginFailedHandler unexpected error: {0}", ex.Message)));
            }
        }

        private void LoginErrorHandler(CallResult<string> result, string message)
        {
            try
            {
                // Raise LOGIN_FAILED event
                LoginFailed(new UserProviderErrorEventArgs(this, UserProviderEvents.LOGIN_FAILED, message));
            }
            catch (Exception ex)
            {
                // Raise LOGIN_FAILED event
                LoginFailed(new UserProviderErrorEventArgs(this, UserProviderEvents.LOGIN_FAILED, string.Format("LoginFailedHandler unexpected error: {0}", ex.Message)));
            }
        }

        #endregion Login() call event handlers

        #region OLD COROUTINES

        private IEnumerator RegisterCoroutine(UserRegisterData userData)
        {
            // DEBUG
            sos.logging.Logger.Debug("ShopDataProvider.RegisterCoroutine() > BEGIN");
            // Raise LOGIN_START event
            RegisterStart(new UserProviderEventArgs(this, UserProviderEvents.REGISTER_START));

            #region Fake handling
            // Fake user retrieve
            User user = UserHelper.GetUser();
            user.Username = userData.Username;
            user.Email = userData.Email;
            // Fake HTTP call
            WWW www = new WWW (HTTPCall.URL_FAKE_DOWNLOAD);
            yield return www;
            #endregion Fake handling

            // Raise LOGIN_DONE event
            RegisterDone(new UserProviderEventArgs(this, UserProviderEvents.REGISTER_DONE, user));
            // DEBUG
            sos.logging.Logger.Debug("ShopDataProvider.RegisterCoroutine() > END");
        }

        private IEnumerator ChooseStarterDeckCoroutine(User user, School starterDeckShool)
        {
            // DEBUG
            sos.logging.Logger.Debug("ShopDataProvider.RegisterCoroutine() > BEGIN");
            // Raise LOGIN_START event
            RegisterStart(new UserProviderEventArgs(this, UserProviderEvents.CHOOSE_STARTER_DECK_START, user));

            #region Fake handling
            // Fake user retrieve

            // Attualmente nel client ActionScript viene rifatta la login per caricare tutti i dati dell'utente

            // Fake HTTP call
            WWW www = new WWW (HTTPCall.URL_FAKE_DOWNLOAD);
            yield return www;
            #endregion Fake handling

            // Raise LOGIN_DONE event
            RegisterDone(new UserProviderEventArgs(this, UserProviderEvents.CHOOSE_STARTER_DECK_DONE, user));
            // DEBUG
            sos.logging.Logger.Debug("ShopDataProvider.RegisterCoroutine() > END");
        }

        private IEnumerator LoadFriendsCoroutine(User user)
        {
            // DEBUG
            sos.logging.Logger.Debug("ShopDataProvider.LoadFriendsCoroutine() > BEGIN");
            // Raise LOGIN_START event
            RegisterStart(new UserProviderEventArgs(this, UserProviderEvents.LOAD_FRIENDS_START, user));

            #region Fake handling
            // Fake user freind list retrieve
            PlayerList friends = new PlayerList(UserHelper.GetFriendList());
            // Fake HTTP call
            WWW www = new WWW (HTTPCall.URL_FAKE_DOWNLOAD);
            yield return www;
            #endregion Fake handling

            // Raise LOGIN_DONE event
            RegisterDone(new UserProviderEventArgs(this, UserProviderEvents.LOAD_FRIENDS_DONE, user));
            // DEBUG
            sos.logging.Logger.Debug("ShopDataProvider.LoadFriendsCoroutine() > END");
        }

        #endregion OLD COROUTINES

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// Perform the user login into SoS system
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public void Login(string username, string password)
        {
            // Build parameters dictionary
            Dictionary<string, string> prms = new Dictionary<string, string>();
            prms.Add(PARAM_LOGIN_USERNAME, username);
            prms.Add(PARAM_LOGIN_PASSWORD, password);
            // Raise LOGIN_START event
            LoginStart(new UserProviderEventArgs(this, UserProviderEvents.LOGIN_START));
            // Async execution
            StartCoroutine(ExecuteCall(GetType().Name, MethodBase.GetCurrentMethod().Name, HTTPCall.URL_USER_LOGIN, prms, LoginSuccessHandler, LoginFailedHandler, LoginErrorHandler));
        }

        /// <summary>
        /// Perform creation of a new SoS account
        /// </summary>
        /// <param name="userData"></param>
        public void Register(UserRegisterData userData)
        {
            // Async execution
            StartCoroutine(RegisterCoroutine(userData));
        }

        /// <summary>
        /// Starter deck selection after first register
        /// </summary>
        /// <param name="user"></param>
        /// <param name="starterDeckShool"></param>
        public void ChooseStarterDeck(User user, School starterDeckShool)
        {
            // Async exec
            StartCoroutine(ChooseStarterDeckCoroutine(user, starterDeckShool));
        }

        /// <summary>
        /// Load the firends list for the given User
        /// </summary>
        /// <param name="user">User to load the friend list</param>
        public void LoadFriends(User user)
        {
            // Async exec
            StartCoroutine(LoadFriendsCoroutine(user));            
        }

        #endregion PUBLIC METHODS

    }
}