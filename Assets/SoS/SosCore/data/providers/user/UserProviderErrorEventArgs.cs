﻿using sos.core;

namespace sos.data.providers.user
{
    /// <summary>
    /// Arguments for and event that represents an error occourred during a call made by a UserDataProvider instance
    /// </summary>
    public class UserProviderErrorEventArgs : DataProviderErrorEventArgs<UserProviderEvents>
    {

        #region FIELDS

        private User _user;
        
        #endregion FIELDS

        #region CONSTRUCTORS

        public UserProviderErrorEventArgs(UserProvider provider, UserProviderEvents evnt, string errorMessage) : base(provider, evnt,errorMessage)
        {
        }

        public UserProviderErrorEventArgs(UserProvider provider, UserProviderEvents evnt, User user, string errorMessage) : base(provider, evnt,errorMessage)
        {
            _user = user;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public User User
        {
            get { return _user; }
        }

        #endregion PROPERTIES

    }
}
