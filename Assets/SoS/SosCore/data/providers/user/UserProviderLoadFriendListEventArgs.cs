﻿using sos.core;

namespace sos.data.providers.user
{
    /// <summary>
    /// 
    /// </summary>
    public class UserProviderLoadFriendsEventArgs : UserProviderEventArgs
    {

        #region FIELDS

        private PlayerList _friends;
        
        #endregion FIELDS

        #region CONSTRUCTORS

        public UserProviderLoadFriendsEventArgs(UserProvider provider, UserProviderEvents evnt, User user, PlayerList friends) : base(provider, evnt, user)
        {
            _friends = friends;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public PlayerList Friends
        {
            get
            {
                return _friends;
            }
        }

        #endregion PROPERTIES

    }
}
