﻿namespace sos.data.providers
{
    /// <summary>
    /// List of available events raised by the UserDataProvider
    /// </summary>
    public enum UserProviderEvents
    {

        LOGIN_START                     = 10,
        LOGIN_DONE                      = 11,
        LOGIN_FAILED                    = 12,

        REGISTER_START                  = 20,
        REGISTER_DONE                   = 21,
        REGISTER_FAILED                 = 22,

        CHOOSE_STARTER_DECK_START       = 30,
        CHOOSE_STARTER_DECK_DONE        = 31,
        CHOOSE_STARTER_DECK_FAILED      = 32,

        LOAD_FRIENDS_START              = 40,
        LOAD_FRIENDS_DONE               = 41,
        LOAD_FRIENDS_FAILED             = 42,

    }
}
