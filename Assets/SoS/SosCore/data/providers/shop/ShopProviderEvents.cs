﻿namespace sos.data.providers.shop
{
    /// <summary>
    /// List of available events raised by the ShopDataProvider
    /// </summary>
    public enum ShopProviderEvents
    {
        LOAD_SHOP_START         = 10,
        LOAD_SHOP_DONE          = 11,
        LOAD_SHOP_FAILED        = 12,

        BUY_SHOP_ITEM_START     = 20,
        BUY_SHOP_ITEM_DONE      = 21,
        BUY_SHOP_ITEM_FAILED    = 22,
    }
}