﻿using sos.core;
using sos.core.shop;

namespace sos.data.providers.shop
{
    /// <summary>
    /// 
    /// </summary>
    public class ShopProviderEventArgs : DataProviderEventArgs<ShopProviderEvents>
    {

        #region CONSTRUCTORS

        public ShopProviderEventArgs(ShopProvider provider, ShopProviderEvents evnt) : base(provider, evnt)
        {
        }

        #endregion CONSTRUCTORS

    }
}
