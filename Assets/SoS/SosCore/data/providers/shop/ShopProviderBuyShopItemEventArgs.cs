﻿using sos.core;
using sos.core.shop;

namespace sos.data.providers.shop
{
    /// <summary>
    /// 
    /// </summary>
    public class ShopProviderBuyShopItemEventArgs : ShopProviderEventArgs
    {

        #region FIELDS

        
        private User _user;
        private ShopItem _shopItem;
        private int _itemCount;
        
        #endregion FIELDS

        #region CONSTRUCTORS

        public ShopProviderBuyShopItemEventArgs(ShopProvider provider, User user, ShopItem shopItem, int itemCount) : base(provider, ShopProviderEvents.BUY_SHOP_ITEM_DONE)
        {
            _user = user;
            _shopItem = shopItem;
            _itemCount = itemCount;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public ShopItem Shop
        {
            get { return _shopItem; }
        }

        public User User
        {
            get { return _user; }
        }

        public int ItemCount
        {
            get { return _itemCount; }
        }

        #endregion PROPERTIES

    }
}
