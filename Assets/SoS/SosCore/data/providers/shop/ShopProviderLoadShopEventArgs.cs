﻿using sos.core.shop;

namespace sos.data.providers.shop
{
    /// <summary>
    /// 
    /// </summary>
    public class ShopProviderLoadShopEventArgs : ShopProviderEventArgs
    {


        #region FIELDS

        private Shop _shop;
        
        #endregion FIELDS

        #region CONSTRUCTORS

        public ShopProviderLoadShopEventArgs(ShopProvider provider, Shop shop) : base(provider, ShopProviderEvents.LOAD_SHOP_DONE)
        {
            _shop = shop;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public Shop Shop
        {
            get { return _shop; }
        }

        #endregion PROPERTIES

    }
}
