﻿namespace sos.data.providers.shop
{
    /// <summary>
    /// Arguments for and event that represents an error occourred during a call made by a ShopDataProvider instance
    /// </summary>
    public class ShopProviderErrorEventArgs : DataProviderErrorEventArgs<ShopProviderEvents>
    {

        #region CONSTRUCTORS

        public ShopProviderErrorEventArgs(ShopProvider provider, ShopProviderEvents evnt, string errorMessage) : base(provider, evnt,errorMessage)
        {
        }

        #endregion CONSTRUCTORS

    }
}
