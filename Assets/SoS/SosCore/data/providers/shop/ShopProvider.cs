﻿using sos.core;
using sos.core.helpers;
using sos.core.shop;
using sos.logging;
using sos.network.http;
using System.Collections;
using UnityEngine;

namespace sos.data.providers.shop
{
    /// <summary>
    /// 
    /// </summary>
    public class ShopProvider : DataProvider
    {

        #region CONSTANTS

        #endregion CONSTANTS

        #region EVENTS AND DELEGATES

        #region LoadShop call

        public delegate void LoadShopStartDelegate(ShopProviderEventArgs args);
        public delegate void LoadShopDoneDelegate(ShopProviderEventArgs args);
        public delegate void LoadShopFailedDelegate(ShopProviderErrorEventArgs args);

        public event LoadShopStartDelegate          LoadShopStart;
        public event LoadShopDoneDelegate           LoadShopDone;
        public event LoadShopFailedDelegate         LoadShopFailed;

        #endregion LoadShop call

        #region BuyShopitem call

        public delegate void BuyShopItemStartDelegate(ShopProviderEventArgs args);
        public delegate void BuyShopItemDoneDelegate(ShopProviderEventArgs args);
        public delegate void BuyShopItemFailedDelegate(ShopProviderErrorEventArgs args);

        public event BuyShopItemStartDelegate       BuyShopItemStart;
        public event BuyShopItemDoneDelegate        BuyShopItemDone;
        public event BuyShopItemFailedDelegate      BuyShopItemFailed;

        #endregion BuyShopitem call

        #endregion EVENTS AND DELEGATES

        #region FIELDS

        #endregion FIELDS

        #region CONSTRUCTORS

        public ShopProvider()
        {
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        #endregion PROPERTIES

        #region PRIVATE METHODS

        private IEnumerator LoadShopCoroutine()
        {
            // DEBUG
            sos.logging.Logger.Debug("ShopDataProvider.LoadShopCoroutine() > BEGIN");
            // Raise LOGIN_START event
            LoadShopStart(new ShopProviderEventArgs(this, ShopProviderEvents.LOAD_SHOP_START));

            #region Fake handling
            // Fake shop retrieve
            Shop shop = ShopHelper.GetShop();
            // Fake HTTP call
            WWW www = new WWW (HTTPCall.URL_FAKE_DOWNLOAD);
            yield return www;
            #endregion Fake handling

            // Raise LOGIN_DONE event
            LoadShopDone(new ShopProviderLoadShopEventArgs(this, shop));
            // DEBUG
            sos.logging.Logger.Debug("ShopDataProvider.LoadShopCoroutine() > END");
        }

        private IEnumerator BuyShopIemCoroutine(User user, ShopItem item, int itemCount)
        {
            // DEBUG
            sos.logging.Logger.Debug("ShopDataProvider.BuyShopIemCoroutine() > BEGIN");
            // Raise LOGIN_START event
            BuyShopItemStart(new ShopProviderEventArgs(this, ShopProviderEvents.BUY_SHOP_ITEM_START));

            #region Fake handling
            // Fake HTTP call
            WWW www = new WWW (HTTPCall.URL_FAKE_DOWNLOAD);
            yield return www;
            // Fake credites decrease
            user.Credits -= (uint) itemCount;
            #endregion Fake handling

            // Raise LOGIN_DONE event
            BuyShopItemDone(new ShopProviderBuyShopItemEventArgs(this, user, item, itemCount));
            // DEBUG
            sos.logging.Logger.Debug("ShopDataProvider.BuyShopIemCoroutine() > END");
        }


        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// Loads the shop data from the SoS server
        /// </summary>
        public void LoadShop()
        {
            // Async execution
            StartCoroutine(LoadShopCoroutine());
        }

        /// <summary>
        /// Buy an item from the shop for the given user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="item"></param>
        /// <param name="itemCount"></param>
        public void BuyShopIem(User user, ShopItem item, int itemCount)
        {
            // Async execution
            StartCoroutine(BuyShopIemCoroutine(user, item, itemCount));
        }

		#endregion PUBLIC METHODS

    }
}
