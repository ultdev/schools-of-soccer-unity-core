﻿using System;

namespace sos.data
{
    /// <summary>
    /// Generic arguments passed to an event raised by a IDataProvider implementation
    /// </summary>
    public interface IDataProviderEventArgs<TEventType>
    {

        /// <summary>
        /// 
        /// </summary>
        TEventType EventType
        {
            get;
        }

        /// <summary>
        /// IDataProvider who rises the event
        /// </summary>
        IDataProvider Provider
        {
            get;
        }

    }
}
