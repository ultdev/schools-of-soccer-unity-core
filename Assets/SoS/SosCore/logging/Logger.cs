﻿using sos.core.game.client;
using sos.data.parsers;
using sos.data.providers;
using sos.network.http;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace sos.logging
{
    /// <summary>
    /// 
    /// </summary>
    public static class Logger
    {

        #region FIELDS

        /// <summary>
        /// List of debug keys 
        /// </summary>
        private static List<string> _enabledDebugKeys = new List<string>() {     DataProvider.DEBUG_KEY
                                                                                ,DataParser.DEBUG_KEY
                                                                                ,HTTPCall.DEBUG_KEY
                                                                                ,GameClient.DEBUG_KEY_WORKER
                                                                                ,GameClient.DEBUG_KEY_HEARTBEAT
                                                                                ,GameClient.DEBUG_KEY_CONNECTION
                                                                            };

        #endregion FIELDS


        #region PUBLIC METHODS

        /// <summary>
        ///  Check if the given key is containted
        /// </summary>
        /// <param name="debugKey"></param>
        /// <returns></returns>
        public static bool CanDebug(string debugKey)
		{
			return _enabledDebugKeys.Contains(debugKey);
		}
		
		/**
		 * Signal using a WARN log that a method is not implemented! 
		 */		
		// LT_TODO > ORIGINAL public static void NotImplemented(Type klazz, string method)
        public static void NotImplemented()
		{
            StackFrame[] stack = (new StackTrace()).GetFrames();
			Warn("{0}.{1} is not yet implemented!", stack[1].GetMethod().ReflectedType.FullName, stack[1].GetMethod().Name);
		}
		
		public static void Info(string message)
		{
            // LT_TODO > Implementare gestione LogWriter
            UnityEngine.Debug.Log("[INFO] - " + message);
		}

		public static void Info(string message, params object[] args)
		{
            // LT_TODO > Implementare gestione LogWriter
            UnityEngine.Debug.LogFormat("[INFO] - " + message, args);
		}

		public static void Info(Exception ex, string message, params object[] args)
		{
            // LT_TODO > Implementare gestione LogWriter
            UnityEngine.Debug.LogFormat("[INFO] - " + message + "\r\n - Exception: \r\n" + ex.ToString(), args);
		}

        public static void Debug(string message)
        {
            // LT_TODO > Implementare gestione LogWriter
            UnityEngine.Debug.Log("[DEBUG] - " + message);
        }

		public static void Debug(string message, params object[] args)
		{
            // LT_TODO > Implementare gestione LogWriter
            UnityEngine.Debug.LogFormat("[DEBUG] - " + message, args);
		}

		public static void Debug(Exception ex, string message, params object[] args)
		{
            // LT_TODO > Implementare gestione LogWriter
            UnityEngine.Debug.LogFormat("[DEBUG] - " + message + "\r\n - Exception: \r\n" + ex.ToString(), args);
		}

        public static void Warn(string message)
        {
            // LT_TODO > Implementare gestione LogWriter
            UnityEngine.Debug.LogWarning(message);
        }

		public static void Warn(string message, params object[] args)
		{
            // LT_TODO > Implementare gestione LogWriter
            UnityEngine.Debug.LogWarningFormat(message, args);
		}

		public static void Warn(Exception ex, string message, params object[] args)
		{
            // LT_TODO > Implementare gestione LogWriter
            UnityEngine.Debug.LogWarningFormat(message + "\r\n - Exception: \r\n" + ex.ToString(), args);
		}

        public static void Error(string message)
        {
            // LT_TODO > Implementare gestione LogWriter
            UnityEngine.Debug.LogError(message);
        }

		public static void Error(string message, params object[] args)
		{
            // LT_TODO > Implementare gestione LogWriter
            UnityEngine.Debug.LogErrorFormat(message, args);
		}

		public static void Error(Exception ex, string message)
		{
            // LT_TODO > Implementare gestione LogWriter
            UnityEngine.Debug.LogError(message + "\r\n - Exception: \r\n" + ex.ToString());
		}

		public static void Error(Exception ex, string message, params object[] args)
		{
            // LT_TODO > Implementare gestione LogWriter
            UnityEngine.Debug.LogErrorFormat(message + "\r\n - Exception: \r\n" + ex.ToString(), args);
		}
		
        public static void Fatal(string message)
        {
            // LT_TODO > Implementare gestione LogWriter
            UnityEngine.Debug.LogError("[FATAL] " + message);
        }

		public static void Fatal(string message, params object[] args)
		{
            // LT_TODO > Implementare gestione LogWriter
            UnityEngine.Debug.LogErrorFormat("[FATAL] " + message, args);
		}

		public static void Fatal(Exception ex, string message)
		{
            // LT_TODO > Implementare gestione LogWriter
            UnityEngine.Debug.LogError("[FATAL] " + message + "\r\n - Exception: \r\n" + ex.ToString());
		}

		public static void Fatal(Exception ex, string message, params object[] args)
		{
            // LT_TODO > Implementare gestione LogWriter
            UnityEngine.Debug.LogErrorFormat("[FATAL] " + message + "\r\n - Exception: \r\n" + ex.ToString(), args);
		}
		
        #endregion PUBLIC METHODS

    }
}
