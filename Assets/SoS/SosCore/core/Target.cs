﻿using System;
using sos.data;

namespace sos.core
{
    /// <summary>
    /// Card action target
    /// </summary>
    public class Target : Enumerator
    {

		#region CONSTANTS

		public static readonly Target   NONE					= new Target(1, "NONE",                 "none");
		public static readonly Target   ACTIVE					= new Target(2, "ACTIVE",               "active");
		public static readonly Target   OPPONENT				= new Target(3, "OPPONENT",             "opponent");
		public static readonly Target   ALL						= new Target(4, "ALL",                  "all");
		public static readonly Target   ALL_PLAYER				= new Target(5, "ALL_PLAYER",           "allOwn");
		public static readonly Target   ALL_OPPONENT			= new Target(6, "ALL_OPPONENT",         "allOpponents");
		public static readonly Target   GRAVEYARD				= new Target(7, "GRAVEYARD",            "card");
        public static readonly Target   GOALKEEPER				= new Target(8, "GOALKEEPER",           "gk");
        public static readonly Target   OPPONENT_GOALKEEPER     = new Target(9, "OPPONENT_GOALKEEPER",  "oppGk");

        #endregion CONSTANTS

        #region CONSTRUCTORS

        protected Target(int id, string name, string sign) : base(id, name, sign, true)  {}

        #endregion CONSTRUCTORS

        #region STATIC METHODS

        public static string[] Names()
        {
            return Enumerators.Names(typeof(Target));
        }

        public static Target[] Values()
        {
            return Enumerators.Values<Target>(typeof(Target));
        }

        public static Target GetById(int id)
        {
            return Enumerators.GetById<Target>(typeof(Target), id);
        }

        public static Target GetByName(string name)
        {
            return Enumerators.GetByName<Target>(typeof(Target), name);
        }

        public static Target GetBySign(string sign)
        {
            return Enumerators.GetBySign<Target>(typeof(Target), sign);
        }

        #endregion STATIC METHODS

    }
}
