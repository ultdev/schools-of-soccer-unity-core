﻿using sos.data;

namespace sos.core
{
    public class Role : Enumerator
    {

        #region CONSTANTS

        public static readonly Role    GOALKEEPER               = new Role(1, "Goalkeeper",  "gk");
		public static readonly Role    DEFENSE			        = new Role(2, "Defence",     "df");
		public static readonly Role    MIDFIELD			        = new Role(3, "Midfield",    "mf");
		public static readonly Role    FORWARD			        = new Role(4, "Forward",     "fw");
        
        public static readonly Role    NONE				        = new Role(5, "None",        "none", false);    // not registered into enum metadata
		public static readonly Role    ALL				        = new Role(0, "All",         "any",  false);    // not registered into enum metadata

        #endregion CONSTANTS

        #region CONSTRUCTORS
        
        protected Role(int id, string name, string sign) : base(id, name, sign) {}

        protected Role(int id, string name, string sign, bool register) : base(id, name, sign, register) {}

        #endregion CONSTRUCTORS

        #region STATIC METHODS

        public static string[] Names()
        {
            return Enumerators.Names(typeof(Role));
        }

        public static Role[] Values()
        {
            return Enumerators.Values<Role>(typeof(Role));
        }

        public static Role GetById(int id)
        {
            return Enumerators.GetById<Role>(typeof(Role), id);
        }

        public static Role GetBySign(string sign)
        {
            return Enumerators.GetBySign<Role>(typeof(Role), sign);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
		public static Role GetByPosition(Position position)
		{
			Role role = Role.NONE;
            // Get the role
                 if (position == Position.GOALKEEPER)   role = Role.GOALKEEPER;
			else if (position == Position.DEFENDER_01) 	role = Role.DEFENSE;
			else if (position == Position.DEFENDER_02) 	role = Role.DEFENSE;
			else if (position == Position.MIDFIELD) 	role = Role.MIDFIELD;
			else if (position == Position.FORWARD_01) 	role = Role.FORWARD;
			else if (position == Position.FORWARD_02) 	role = Role.FORWARD;
			else if (position == Position.SUB_DEFENCE) 	role = Role.DEFENSE;
			else if (position == Position.SUB_MIDFIELD) role = Role.MIDFIELD;
			else if (position == Position.SUB_FORWARD) 	role = Role.FORWARD;
			return role;
		}

        #endregion STATIC METHODS

    }
}
