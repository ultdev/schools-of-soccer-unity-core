﻿using sos.core.cards;
using sos.core.quest;
using sos.utils;

namespace sos.core
{
    /// <summary>
    /// 
    /// </summary>
    public class User : Player
    {

        #region CONSTANTS

        protected const uint    DEFAULT_CREDITS         = 9999;

        #endregion CONSTANTS

        #region FIELDS

        private string _sessionToken;
        private uint _credits;
        private PlayerList _friends = new PlayerList();
        private UserGameList _games = new UserGameList();
        private UserQuestList _quests = new UserQuestList();
        private Collection _collection = new Collection();

        #endregion FIELDS

        #region CONSTRUCTORS

        public User() : base()
        {
            Clear();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public string SessionToken
        {
            get { return _sessionToken; }
            set { _sessionToken = value; }
        }

        public PlayerList Friends
        {
            get { return _friends; }
        }

        public UserGameList Games
        {
            get { return _games; }
        }

        public Collection Collection
        {
            get
            {
                return _collection;
            }
        }

        public UserQuestList Quests
        {
            get
            {
                return _quests;
            }
        }

        public uint Credits
        {
            get { return _credits; }
            set { _credits = value; }
        }

        public bool IsLoggedIn
        {
            get { return _sessionToken != Defaults.INVALID_SESSION_TOKEN; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="friends"></param>
        public void UpdateFriends(PlayerList friends)
		{
			_friends.Clear();
			_friends.Copy(friends);
		}
		
        /// <summary>
        /// 
        /// </summary>
        /// <param name="games"></param>
		public void UpdateGames(UserGameList games)
		{
			_games.Clear();
			_games.Copy(games);
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quests"></param>
		public void UpdateQuests(UserQuestList quests)
		{
			_quests.Clear();
			_quests.Copy(quests);
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="collection"></param>
        public void UpdateCollection(Collection collection)
        {
            _collection.Clear();
            _collection.Copy(collection);
        }
		
        /// <summary>
        /// 
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
		public override bool Equals(Player player)
		{
			if (player is User && base.Equals(player))
			{
				// User cast
				User user = player as User;
				// Check User properties
				return 	_id				== user.Id			&&
						_sessionToken	== user.SessionToken;
			}
			return false;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="player"></param>
        public override void Copy(Player player)
		{	
			// Ancestor copy
			base.Copy(player);
			// User cast
			User user = player as User;
			if (user != null)
			{
				// Copy valus
				_sessionToken	= user.SessionToken;
				// Copy objects
				_friends.Copy(user.Friends);
				_games.Copy(user.Games);
                _quests.Copy(user.Quests);
			}
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override Player Clone()
		{
			User user = new User();
			user.Copy(this);
			return user;
		}

        /// <summary>
        /// 
        /// </summary>
        public override void Clear()
		{
			// Ancestor
			base.Clear();
			// Clear data
			_sessionToken	= Defaults.INVALID_SESSION_TOKEN;
            _credits        = DEFAULT_CREDITS;
			// Clear obejcts
			_friends.Clear();
			_games.Clear();
            _quests.Clear();
            _collection.Clear();
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
		{
			return string.Format("{0} {1} (id: {2}, session token: {3})", GetType().FullName, _username, _id, _sessionToken);
		}

        #endregion PUBLIC METHODS

    }
}
