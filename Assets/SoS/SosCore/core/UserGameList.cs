﻿using System;
using System.Collections.Generic;
using sos.utils;

namespace sos.core
{
    public class UserGameList
    {

        #region CONSTANTS

        #endregion CONSTANTS

        #region FIELDS

        private List<UserGame> _list;

        #endregion FIELDS

        #region CONSTRUCTORS

        public UserGameList()
        {
            _list = new List<UserGame>();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public int Count
        {
            get { return _list.Count; }
        }

        public UserGame this[int index]
        {
            get { return _list[index]; }
        }

        public UserGame this[uint id]
        {
            get
            {
                int index = IndexOf(id);
                if (index == Defaults.INVALID_INDEX) throw new ArgumentException(string.Format("GameReference with id {0} not found", id));
                return _list[index];
            }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        /// <summary>
        /// Retrieve a GameReference list index by given ID
        /// </summary>
        /// <param name="id">ID of the GameReference to retrieve</param>
        /// <returns>GameReference instance contained into the list with the given id, returns Defaults.INVALID_INDEX if no GameReferences is found for the given id</returns>
        private int IndexOf(uint id)
        {
			for (var i=0; i<_list.Count; i++) if (_list[i].Id == id) return i;
			return Defaults.INVALID_INDEX;
        }

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public void Add(UserGame item)
        {
            _list.Add(item);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public UserGame Get(int index)
        {
            return _list[index];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UserGame GetById(uint id)
        {
            int index = IndexOf(id);
            UserGame GameReference = null;
            if (index != Defaults.INVALID_INDEX) GameReference = _list[index];
            return GameReference;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameReferenceList"></param>
        public void Copy(UserGameList gameReferenceList)
        {
            // Value parsing and casting
            List<UserGame> list = new List<UserGame>();
            // Objects parsing
            for (int i = 0; i < gameReferenceList.Count; i++)
            {
                list.Add(gameReferenceList[i].Clone());
            }
            // Updating GameReference list
            _list.Clear();
            _list.AddRange(list);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public UserGameList Clone()
        {
            UserGameList GameReferenceList = new UserGameList();
            GameReferenceList.Copy(this);
            return GameReferenceList;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Clear()
        {
            _list.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} (game count: {1})", GetType().FullName, _list.Count);
        }

        #endregion PUBLIC METHODS

    }

}
