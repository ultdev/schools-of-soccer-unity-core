﻿using sos.data;

namespace sos.core
{
    /// <summary>
    /// Tipogia di classe/modello di carta azione
    /// </summary>
    public class UniqueClassType : Enumerator
    {
        #region CONSTANTS

        public static readonly UniqueClassType  NONE			        = new UniqueClassType(0, "None", 	"");
		public static readonly UniqueClassType  SELF			        = new UniqueClassType(1, "Self", 	"self");
		public static readonly UniqueClassType  SHARED                  = new UniqueClassType(2, "Shared",  "shared");

        #endregion CONSTANTS

        #region CONSTRUCTORS

        protected UniqueClassType(int id, string name, string sign) : base(id, name, sign, true)  {}

        #endregion CONSTRUCTORS

        #region STATIC METHODS

        public static string[] Names()
        {
            return Enumerators.Names(typeof(UniqueClassType));
        }

        public static UniqueClassType[] Values()
        {
            return Enumerators.Values<UniqueClassType>(typeof(UniqueClassType));
        }

        public static UniqueClassType GetById(int id)
        {
            return Enumerators.GetById<UniqueClassType>(typeof(UniqueClassType), id);
        }

        public static UniqueClassType GetByName(string name)
        {
            return Enumerators.GetByName<UniqueClassType>(typeof(UniqueClassType), name);
        }

        #endregion STATIC METHODS
    }
}
