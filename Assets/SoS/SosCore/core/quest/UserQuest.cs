﻿using sos.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.quest
{
    /// <summary>
    /// 
    /// </summary>
    public class UserQuest : Quest
    {

		#region CONSTANTS

		#endregion CONSTANTS

		#region FIELDS

		private uint _instanceId;
		private uint _itemCount;
		private UserQuestStatus _status;

        #endregion FIELDS

        #region CONSTRUCTORS

        /// <summary>
        /// Creates an invalid user Quest instance
        /// </summary>
        public UserQuest()
        {
            Clear();
        }

        /// <summary>
        /// Creates a valid quest instance
        /// </summary>
        /// <param name="quest"></param>
        /// <param name="instanceId"></param>
        /// <param name="itemCount"></param>
        /// <param name="status"></param>
        public UserQuest(Quest quest, uint instanceId, uint itemCount, UserQuestStatus status)
        {
            // Copy base data
            base.Copy(quest);
            // Init UserQuest data
            _instanceId = instanceId;
            _itemCount = itemCount;
            _status = status;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public uint InstanceId
        {
            get
            {
                return _instanceId;
            }

            set
            {
                _instanceId = value;
            }
        }

        public uint ItemCount
        {
            get
            {
                return _itemCount;
            }

            set
            {
                _itemCount = value;
            }
        }

        public UserQuestStatus Status
        {
            get
            {
                return _status;
            }

            set
            {
                _status = value;
            }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        public override bool Equals(Quest quest)
        {
            if (quest is UserQuest && base.Equals(quest))
            {
                // User cast
                UserQuest userQuest = quest as UserQuest;
                // Check User properties
                return  _instanceId == userQuest.InstanceId &&
                        _itemCount == userQuest.ItemCount &&
                        _status == userQuest.Status;
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Quest"></param>
        public override void Copy(Quest quest)
		{	
			// Ancestor copy
			base.Copy(quest);
			// User cast
			UserQuest userQuest = quest as UserQuest;
			if (userQuest != null)
			{
                _instanceId = userQuest.InstanceId;
                _itemCount = userQuest.ItemCount;
                _status = userQuest.Status;
			}
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override Quest Clone()
		{
			UserQuest user = new UserQuest();
			user.Copy(this);
			return user;
		}

        /// <summary>
        /// 
        /// </summary>
        public override void Clear()
		{
			// Ancestor
			base.Clear();
			// Clear data
            _instanceId = Defaults.INVALID_ID;
            _itemCount  = 0;
            _status     = UserQuestStatus.INVALID;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} {1} (instance id: {2}, id: {3}, status: {4})"
                                ,GetType().FullName
                                ,Name
                                ,InstanceId
                                ,Id
                                ,_status.ToString());
        }

        #endregion PUBLIC METHODS

    }
}
