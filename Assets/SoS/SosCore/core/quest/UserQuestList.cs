﻿using sos.utils;
using System;
using System.Collections.Generic;

namespace sos.core.quest
{

    /// <summary>
    /// 
    /// </summary>
    public class UserQuestList
    {

        #region FIELDS

        private List<UserQuest> _list = new List<UserQuest>();

        #endregion FIELDS

        #region CONSTRUCTORS

        public UserQuestList()
        {
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public int Count
        {
            get { return _list.Count; }
        }

        public UserQuest this[int index]
        {
            get { return _list[index]; }
        }

        public UserQuest this[uint instanceId]
        {
            get 
            { 
                int index = IndexOf(instanceId);
                if (index == Defaults.INVALID_INDEX) throw new ArgumentException(string.Format("UserQuest with InstanceId {0} not found", instanceId));
                return _list[index]; 
            }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        /// <summary>
        /// Returns a Quert instance by given ID
        /// </summary>
        /// <param name="id">ID of the UserQuest to retrieve</param>
        /// <returns>UserQuest instance contained into the list with the given id, returns Defaults.INVALID_INDEX if no UserQuest is found for the given id</returns>
		private int IndexOf(uint instanceId)
		{
			int index = Defaults.INVALID_INDEX;
			for (int i = 0; i < _list.Count; i++)
			{
				if (_list[i].InstanceId == instanceId) break;
			}
			return index;
		}

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userQuest"></param>
        /// <returns></returns>
        public bool Contains(UserQuest userQuest)
        {
            foreach (var u in _list) { if (u.Equals(userQuest)) { return true; }; }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
		public void Add(UserQuest quest)
		{
            if (Contains(quest)) throw new ArgumentException(string.Format("Quest {0} (instanceId: {1}, id: {2}) already contained into the UserQuestList!", quest.Name, quest.InstanceId, quest.Id));
			_list.Add(quest);
		}
		
        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserQuestList"></param>
		public void Copy(UserQuestList userQuestList)
		{
			// Value parsing and casting
			List<UserQuest> list = new List<UserQuest>();
			// Objects parsing
			for (int i = 0; i < userQuestList.Count; i++)
			{
				list.Add(userQuestList[i].Clone() as UserQuest);
			}
			// Updating UserQuest list
			_list.Clear();
            _list.AddRange(list);
		}
		
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
		public UserQuestList Clone()
		{
			UserQuestList userQuestList = new UserQuestList();
			userQuestList.Copy(this);
			return userQuestList;
		}

        /// <summary>
        /// 
        /// </summary>
        public void Clear()
		{
            _list.Clear();
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} (quest count: {1})", GetType().FullName, _list.Count);
        }

        #endregion PUBLIC METHODS

    }
}
