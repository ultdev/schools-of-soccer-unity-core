﻿using sos.utils;
using System;
using System.Collections.Generic;

namespace sos.core.quest
{

    /// <summary>
    /// 
    /// </summary>
    public class QuestDefinitions
    {

        #region FIELDS

        private List<Quest> _list = new List<Quest>();

        #endregion FIELDS

        #region CONSTRUCTORS

        public QuestDefinitions(Quest[] quests)
        {
            Add(quests);
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public int Count
        {
            get { return _list.Count; }
        }

        public Quest this[int index]
        {
            get { return _list[index]; }
        }

        public Quest this[uint id]
        {
            get 
            { 
                int index = IndexOf(id);
                if (index == Defaults.INVALID_INDEX) throw new ArgumentException(string.Format("Quest with id {0} not found", id));
                return _list[index]; 
            }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        /// <summary>
        /// Returns a Quert instance by given ID
        /// </summary>
        /// <param name="questId">ID of the Quest to retrieve</param>
        /// <returns>Quest instance contained into the list with the given id, returns Defaults.INVALID_INDEX if no Quest is found for the given id</returns>
		private int IndexOf(uint questId)
		{
			for (var i=0; i<_list.Count; i++) if (_list[i].Id == questId) return i;
			return Defaults.INVALID_INDEX;
		}

        /// <summary>
        /// Adds a quest to the quests definition
        /// </summary>
        /// <param name="quest"></param>
        private void Add(Quest quest)
        {
            if (Contains(quest.Id)) throw new ArgumentException("Quest with id {0} already contained into QuestDefinition");
            _list.Add(quest);
        }

        /// <summary>
        /// Adds all the given card to the card definitions
        /// </summary>
        /// <param name="quests"></param>
        private void Add(Quest[] quests)
        {
            Array.ForEach<Quest>(quests, q => Add(q) );
        }

        /// <summary>
        /// Clears all the definitions
        /// </summary>
        private void Clear()
        {
            _list.Clear();
        }

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// Check if a card for the given Card.Id exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
		public bool Contains(uint id)
		{
			return IndexOf(id) != Defaults.INVALID_INDEX;
		}
		
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
		public Quest Get(int index)
		{
			return _list[index];
		}
		
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
		public Quest GetById(uint id)
		{
			int index = IndexOf(id);
			Quest Quest = null;
			if (index != Defaults.INVALID_INDEX) Quest = _list[index];
			return Quest;
		}

        /// <summary>
        /// Refresh definitions clearing definition list and reloading it using the given list
        /// </summary>
        public void Refresh(Quest[] quests)
        {
            Clear();
            Add(quests);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} (card definitions count: {1})",
                                 GetType().FullName,
                                 Count);
        }

        #endregion PUBLIC METHODS

    }
}
