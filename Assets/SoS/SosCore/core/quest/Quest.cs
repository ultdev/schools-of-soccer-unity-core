﻿using sos.utils;

namespace sos.core.quest
{
    /// <summary>
    /// 
    /// </summary>
    public class Quest
    {

		#region CONSTANTS

        public const string  DEFAULT_NAME 						= "SAMPLE_QUEST";

		#endregion CONSTANTS

		#region FIELDS

		private uint _id;
		private string _name;
		private string _description;
		private string _rewardText;
		private uint _daily;
		private string _image;
		private QuestBlueprint _blueprint;

        #endregion FIELDS

        #region CONSTRUCTORS

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public uint Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }

            set
            {
                _description = value;
            }
        }

        public string RewardText
        {
            get
            {
                return _rewardText;
            }

            set
            {
                _rewardText = value;
            }
        }

        public uint Daily
        {
            get
            {
                return _daily;
            }

            set
            {
                _daily = value;
            }
        }

        public string Image
        {
            get
            {
                return _image;
            }

            set
            {
                _image = value;
            }
        }

        public QuestBlueprint Blueprint
        {
            get
            {
                return _blueprint;
            }

            set
            {
                _blueprint = value;
            }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quest"></param>
        /// <returns></returns>
		public virtual bool Equals(Quest quest)
		{
			return _id == quest.Id;
		}

        /// <summary>
        /// 
        /// </summary>
        public virtual void Copy(Quest quest)
		{
            _id             = quest.Id;
            _name           = quest.Name;
            _description    = quest.Description;
            _rewardText     = quest.RewardText;
            _daily          = quest.Daily;
            _image          = quest.Image;
            _blueprint      = quest.Blueprint; // verificare se andrà effettuata un clone o va bene una shallowcopy
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual Quest Clone()
		{
			Quest quest = new Quest();
			quest.Copy(this);
			return quest;
		}

        /// <summary>
        /// 
        /// </summary>
        public virtual void Clear()
		{
            _id             = Defaults.INVALID_ID;
            _name           = DEFAULT_NAME;
            _description    = string.Empty;
            _rewardText     = string.Empty;
            _daily          = 0;
            _image          = string.Empty;
            _blueprint      = null;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} {1} (id: {2})"
                                ,GetType().FullName
                                ,Name
                                ,Id);
        }

        #endregion PUBLIC METHODS

    }
}
