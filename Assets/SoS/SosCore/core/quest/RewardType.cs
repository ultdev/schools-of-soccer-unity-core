﻿using sos.data;

namespace sos.core.quest
{
    public class RewardType : Enumerator
    {

        #region CONSTANTS

        public static readonly RewardType    CREDITS                = new RewardType(1, "credits");
		public static readonly RewardType    PACK			        = new RewardType(2, "pack");
		public static readonly RewardType    CARD			        = new RewardType(3, "card");

        #endregion CONSTANTS

        #region CONSTRUCTORS
        
        protected RewardType(int id, string name) : base(id, name) {}

        protected RewardType(int id, string name, bool register) : base(id, name, string.Empty, register) {}

        #endregion CONSTRUCTORS

        #region STATIC METHODS

        public static string[] Names()
        {
            return Enumerators.Names(typeof(RewardType));
        }

        public static RewardType[] Values()
        {
            return Enumerators.Values<RewardType>(typeof(RewardType));
        }

        public static RewardType GetById(int id)
        {
            return Enumerators.GetById<RewardType>(typeof(RewardType), id);
        }

        #endregion STATIC METHODS

    }
}
