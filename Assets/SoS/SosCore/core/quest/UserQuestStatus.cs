﻿using sos.data;

namespace sos.core.quest
{
    public class UserQuestStatus : Enumerator
    {

        #region CONSTANTS

        public static readonly UserQuestStatus    INVALID               = new UserQuestStatus(-1,   "Unknown");
        public static readonly UserQuestStatus    ACTIVE                = new UserQuestStatus(0,    "Active");
		public static readonly UserQuestStatus    COMPLETE			    = new UserQuestStatus(1,    "Complete");
		public static readonly UserQuestStatus    REDEEMED			    = new UserQuestStatus(2,    "Redeemed");

        #endregion CONSTANTS

        #region CONSTRUCTORS
        
        protected UserQuestStatus(int id, string name) : base(id, name) {}

        protected UserQuestStatus(int id, string name, bool register) : base(id, name, string.Empty, register) {}

        #endregion CONSTRUCTORS

        #region STATIC METHODS

        public static string[] Names()
        {
            return Enumerators.Names(typeof(UserQuestStatus));
        }

        public static UserQuestStatus[] Values()
        {
            return Enumerators.Values<UserQuestStatus>(typeof(UserQuestStatus));
        }

        public static UserQuestStatus GetById(int id)
        {
            return Enumerators.GetById<UserQuestStatus>(typeof(UserQuestStatus), id);
        }

        #endregion STATIC METHODS

    }
}
