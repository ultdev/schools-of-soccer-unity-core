﻿using sos.utils;

namespace sos.core.quest
{
    /// <summary>
    /// Quest completed reward
    /// </summary>
    public class Reward
    {

		#region CONSTANTS

		#endregion CONSTANTS

		#region FIELDS

		private RewardType _type;
		private uint _id;
		private uint _amount;
		private bool _visible;
		private uint _detailId; // id del riferimento del premio rappresentato (carta, pack, altra Reward etc etc) valorizzato in base al RewardType

        #endregion FIELDS

        #region CONSTRUCTORS

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public RewardType Type
        {
            get
            {
                return _type;
            }

            set
            {
                _type = value;
            }
        }

        public uint Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        public uint Amount
        {
            get
            {
                return _amount;
            }

            set
            {
                _amount = value;
            }
        }

        public bool Visible
        {
            get
            {
                return _visible;
            }

            set
            {
                _visible = value;
            }
        }

        public uint DetailId
        {
            get
            {
                return _detailId;
            }

            set
            {
                _detailId = value;
            }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS
            
        #region PUBLIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reward"></param>
        /// <returns></returns>
        public bool Equals(Reward reward)
        {
            return _id == reward.Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reward"></param>
        public void Copy(Reward reward)
		{	
            _id         = reward.Id;
		    _type       = reward.Type;		    
		    _amount     = reward.Amount;
		    _visible    = reward.Visible;
		    _detailId   = reward.DetailId;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Reward Clone()
		{
			Reward reward = new Reward();
			reward.Copy(this);
			return reward;
		}

        /// <summary>
        /// 
        /// </summary>
        public void Clear()
		{
			// Clear data
            _id         = Defaults.INVALID_ID;
		    _type       = RewardType.CREDITS;		    
		    _amount     = 0;
		    _visible    = true;
		    _detailId   = Defaults.INVALID_ID;
		}

        #endregion PUBLIC METHODS

    }
}
