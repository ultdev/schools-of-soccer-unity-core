﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core
{
    public class Stats
    {

        #region CONSTANTS

		// JSON data properties names
		public const string     STAT_NAME_STAMINA		                = "stamina";
		public const string     STAT_NAME_DRIBLING		                = "dribbling";
		public const string     STAT_NAME_PASSING		                = "passing";
		public const string     STAT_NAME_SCORING		                = "scoring";
		public const string     STAT_NAME_DEFENCE		                = "defense";
		public const string     STAT_NAME_GOALKEEPING	                = "goalkeeping";

		// Default values
		public const int        DEFAULT_STAMINA					        = 6;
		public const int        DEFAULT_DRIBLING				        = 6;
		public const int        DEFAULT_PASSING					        = 6;
		public const int        DEFAULT_SCORING					        = 6;
		public const int        DEFAULT_DEFENCE					        = 6;
		public const int        DEFAULT_GOALKEEPING				        = 0;

        #endregion CONSTANTS

        #region FIELDS

        protected int _stamina;
        protected int _dribiling;
        protected int _passing;
        protected int _scoring;
        protected int _defense;
        protected int _goalkeeping;

        #endregion FIELDS

        #region CONSTRUCTORS

        public Stats()
        {
			Clear();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public int Stamina
        {
            get { return _stamina; }
            set { _stamina = value; }
        }

        public int Dribbling
        {
            get { return _dribiling; }
            set { _dribiling = value; }
        }

        public int Passing
        {
            get { return _passing; }
            set { _passing = value; }
        }

        public int Scoring
        {
            get { return _scoring; }
            set { _scoring = value; }
        }

        public int Defense
        {
            get { return _defense; }
            set { _defense = value; }
        }

        public int Goalkeeping
        {
            get { return _goalkeeping; }
            set { _goalkeeping = value; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

	    /// <summary>
        /// Updates the current status, merging the values of the given object (sum)
	    /// </summary>
        /// <param name="stats">Stats opbject to update the currents stats with</param>
        public virtual void Update(Stats stats)
		{
			// Merges the current stats with the provided one
			Stats merge = Merge(stats);
			// Copies data from the merged object
			Copy(merge);
		}

        /// <summary>
        /// Creates a new Stats object as the result of the merge (sum) of the values of each Stat value
        /// </summary>
        /// <param name="stats">Stats to merge</param>
        /// <returns>New Stats object containing the stats obtained merging the two objects</returns>
		public virtual Stats Merge(Stats stats)
		{
			// Stats merge
			int stam 	= _stamina		+ stats.Stamina;
			int drib 	= _dribiling	+ stats.Dribbling;
			int pass 	= _passing		+ stats.Passing;
			int scor 	= _scoring		+ stats.Scoring;
			int def 	= _defense		+ stats.Defense;
			int goal    = _goalkeeping	+ stats.Goalkeeping;
			// Stats object
			Stats delta = new Stats();
			delta.Stamina 		= stam;
			delta.Dribbling 	= drib;
			delta.Passing 		= pass;
			delta.Scoring 		= scor;
			delta.Defense 		= def;
			delta.Goalkeeping	= goal;
			// Returns the Stats
			return delta;
		}
		
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stats"></param>
        /// <returns></returns>
        public virtual bool Equals(Stats stats)
		{
			return	_stamina		== stats.Stamina		&&
					_dribiling		== stats.Dribbling		&&
					_passing		== stats.Passing		&&
					_scoring		== stats.Scoring		&&
					_defense		== stats.Defense		&&
					_goalkeeping	== stats.Goalkeeping;
		}
		
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stats"></param>
        public virtual void Copy(Stats stats)
		{
			// Check differences
			if (!Equals(stats))
			{
				// Updates the stats
				_stamina		= stats.Stamina;
				_dribiling		= stats.Dribbling;
				_passing		= stats.Passing;
				_scoring		= stats.Scoring;
				_defense		= stats.Defense;
				_goalkeeping	= stats.Goalkeeping;
			}
		}
		
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual Stats Clone()
		{
			Stats stats = new Stats();
			stats.Copy(this);
			return stats;
		}

        /// <summary>
        /// 
        /// </summary>
        public virtual void Clear()
		{
			// Restore defailt values
			_stamina 		= Stats.DEFAULT_STAMINA;
			_dribiling		= Stats.DEFAULT_DRIBLING;
			_passing		= Stats.DEFAULT_PASSING;
			_scoring		= Stats.DEFAULT_SCORING;
			_defense		= Stats.DEFAULT_DEFENCE;
			_goalkeeping	= Stats.DEFAULT_GOALKEEPING;
		}

        #endregion PUBLIC METHODS

        #region STATIC METHODS

        /// <summary>
        /// Check if the given pro name string is a valid stat contained by a Stat object 
        /// </summary>
        /// <param name="statName">Stat property name to check</param>
        /// <returns>True is the stat name is a valid stat property</returns>
		public static bool IsBaseStatName(string statName)
		{
			return	statName == STAT_NAME_DEFENCE	    ||
					statName == STAT_NAME_STAMINA	    ||
					statName == STAT_NAME_DRIBLING	    ||
					statName == STAT_NAME_PASSING	    ||
					statName == STAT_NAME_SCORING	    ||
					statName == STAT_NAME_DEFENCE	    ||
					statName == STAT_NAME_GOALKEEPING;
        }

        #endregion STATIC METHODS

    }
}
