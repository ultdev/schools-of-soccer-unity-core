﻿using System;
using sos.data;

namespace sos.core.cards
{
    /// <summary>
    /// 
    /// </summary>
    public class CardSubtype : Enumerator
    {

        #region CONSTANTS

        public static readonly  CardSubtype NONE							= new CardSubtype(0, "No Subtype", false);
		public static readonly  CardSubtype GENERAL							= new CardSubtype(1, "Action", false);
		public static readonly  CardSubtype DEBUFF							= new CardSubtype(2, "Debuff", true);
		public static readonly  CardSubtype BUFF							= new CardSubtype(3, "Buff", true);
		public static readonly  CardSubtype GEAR							= new CardSubtype(4, "Gear", true);
		public static readonly  CardSubtype FREEKICK						= new CardSubtype(5, "Freekick", false);
		public static readonly  CardSubtype SWAP							= new CardSubtype(6, "Action", false);
		public static readonly  CardSubtype CHILD_EFFECT					= new CardSubtype(7, "Child Effect", false);
		public static readonly  CardSubtype FOUL							= new CardSubtype(8, "Foul", false);
		public static readonly  CardSubtype YELLOW_CARD						= new CardSubtype(9, "Yellow Card", false);
		public static readonly  CardSubtype RED_CARD						= new CardSubtype(10, "Red Card", false);
		public static readonly  CardSubtype SCHOOL							= new CardSubtype(11, "School", false);
		public static readonly  CardSubtype STAMINA_REGEN					= new CardSubtype(12, "Stamina Regen (second half)", false);
		public static readonly  CardSubtype CARD_COST						= new CardSubtype(13, "cardCost", false);
		public static readonly  CardSubtype CHILD_DEBUFF					= new CardSubtype(14, "Child Debuff", true);
		public static readonly  CardSubtype CHILD_BUFF						= new CardSubtype(15, "Child Buff", true);
		public static readonly  CardSubtype ANALYSIS_MARKER					= new CardSubtype(16, "Analysis Marker", true);
		public static readonly  CardSubtype INVISIBLE_MARKER				= new CardSubtype(17, "Invisible Marker", false);

        #endregion CONSTANTS

        #region FIELDS

        private bool _visible;

        #endregion FIELDS

        #region CONSTRUCTORS

        public CardSubtype(int id, string name, bool visible) : base(id, name)
        {
            _visible = visible;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public bool Visible
        {
            get { return _visible; }
        }

        #endregion PROPERTIES

        #region STATIC METHODS

        public static string[] Names()
        {
            return Enumerators.Names(typeof(CardSubtype));
        }

        public static CardSubtype[] Values()
        {
            return Enumerators.Values<CardSubtype>(typeof(CardSubtype));
        }

        public static CardSubtype GetById(int id)
        {
            return Enumerators.GetById<CardSubtype>(typeof(CardSubtype), id);
        }

        public static CardSubtype GetByName(string name)
        {
            return Enumerators.GetByName<CardSubtype>(typeof(CardSubtype), name);
        }

        #endregion STATIC METHODS

    }
}
