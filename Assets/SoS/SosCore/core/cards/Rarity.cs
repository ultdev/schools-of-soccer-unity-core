﻿using sos.data;

namespace sos.core.cards
{
    /// <summary>
    /// Card rarity enumerator
    /// </summary>
    public class Rarity : Enumerator
    {

        #region CONSTANTS

		public static readonly Rarity    COMMON                 = new Rarity(1, "Common");
		public static readonly Rarity    UNCOMMON               = new Rarity(2, "Uncommon");
		public static readonly Rarity    RARE                   = new Rarity(3, "Rare");
		public static readonly Rarity    WORLD_CLASS            = new Rarity(4, "World Class");

        #endregion CONSTANTS

        #region CONSTRUCTORS
        
        protected Rarity(int id, string name) : base(id, name) {}

        protected Rarity(int id, string name, bool register) : base(id, name, string.Empty, register) {}

        #endregion CONSTRUCTORS

        #region STATIC METHODS

        public static string[] Names()
        {
            return Enumerators.Names(typeof(Rarity));
        }

        public static Rarity[] Values()
        {
            return Enumerators.Values<Rarity>(typeof(Rarity));
        }

        public static Rarity GetById(int id)
        {
            return Enumerators.GetById<Rarity>(typeof(Rarity), id);
        }

        #endregion STATIC METHODS

    }
}
