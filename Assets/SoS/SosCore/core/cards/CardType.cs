﻿using System;
using sos.data;

namespace sos.core.cards
{
    /// <summary>
    /// 
    /// </summary>
    public class CardType : Enumerator
    {

        #region CONSTANTS

		public static readonly  CardType    PLAYER							= new CardType(1, "PLAYER");
		public static readonly  CardType    ACTION							= new CardType(2, "ACTION");
		public static readonly  CardType    TRAINING						= new CardType(3, "TRAINING");
		public static readonly  CardType    GOALKEEPER						= new CardType(4, "GOALKEEPER");

        #endregion CONSTANTS

        #region CONSTRUCTORS

        public CardType(int id, string name) : base(id, name) {}

        #endregion CONSTRUCTORS

        #region STATIC METHODS

        public static string[] Names()
        {
            return Enumerators.Names(typeof(CardType));
        }

        public static CardType[] Values()
        {
            return Enumerators.Values<CardType>(typeof(CardType));
        }

        public static CardType GetById(int id)
        {
            return Enumerators.GetById<CardType>(typeof(CardType), id);
        }

        public static CardType GetByName(string name)
        {
            return Enumerators.GetByName<CardType>(typeof(CardType), name);
        }

        #endregion STATIC METHODS

    }
}
