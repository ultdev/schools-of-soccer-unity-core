﻿using System;

namespace sos.core.cards
{
    public class CardTargetInputEffect : CardTargetInput
    {

        #region FIELDS

        private uint _effectId;

        #endregion FIELDS

        #region CONSTRUCTORS

        public CardTargetInputEffect() : base(TargetType.EFFECT)
        {
            Clear();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

		public uint EffectId
		{
			get { return _effectId; }
            set { _effectId  = value; }
		}

        #endregion PROPERTIES

    }
}
