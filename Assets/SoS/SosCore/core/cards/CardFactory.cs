﻿using System;
using System.Globalization;
using sos.logging;
using System.IO;

namespace sos.core.cards
{
    /// <summary>
    /// 
    /// </summary>
    public static class CardFactory
    {

        #region CONSTANTS

        private const string    CARDS_CLASS_NAMESPACE                       = "sos.core.cards";
        private const string    CARDS_CLASS_POSTFIX                         = "Card";

        #endregion CONSTANTS

        #region PRIVATE METHODS

        /// <summary>
        /// Creates the full name of a car class
        /// </summary>
        /// <param name="type">CardType to retrieve the full class name</param>
        /// <returns>Full name of a class (namespace + name) of a card type</returns>
		private static string GetClassName(CardType type)
		{
			return string.Format("{0}.{1}{2}", CARDS_CLASS_NAMESPACE, (new CultureInfo("en-US", false)).TextInfo.ToTitleCase(type.Name.ToLower()), CARDS_CLASS_POSTFIX);
		}

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// Create an instance of the card for the given CardType using the default CardKind.INSTANCE
        /// </summary>
        /// <param name="type">CardType of the card to create</param>
        /// <returns>An instance of a Card for the given CardType an the given CardKind</returns>
        public static Card Create(CardType type)
        {
            return Create(type, null);
        }

        /// <summary>
        /// Create an instance of the card for the given CardType and CardKind
        /// </summary>
        /// <param name="type">CardType of the card to create</param>
        /// <param name="kind">CardKind of the card to create</param>
        /// <returns>An instance of a Card for the given CardType an the given CardKind</returns>
		public static Card Create(CardType type, CardKind kind)
		{
            // Class name
            string cardClass = string.Empty;
            // Safe exec
            try
            {
			    // Checks card kind
			    kind = (kind != null) ? kind : CardKind.INSTANCE;
			    // Retrieve the class name
			    cardClass = GetClassName(type);
                // retrieve the class type
                Type cardType = Type.GetType(cardClass);
			    // Dynamic class creation
			    return Activator.CreateInstance(cardType, kind) as Card;
            }
            catch (FileNotFoundException ex)
            {
                Logger.Error(ex, "Creation of a Card of type {0} failed, CardType dependecies cannot be loaded [class:{1}]", type, cardClass);
                throw new Exception("CardFactory.Create() failed, CardType dependecies cannot be loaded");
            }
            catch (FileLoadException ex)
            {
                Logger.Error(ex, "Creation of a Card of type {0} failed, CardType dependecies not found [class:{1}]", type, cardClass);
                throw new Exception("CardFactory.Create() failed, CardType dependecies cannot be found");
            }
            catch (BadImageFormatException ex)
            {
                Logger.Error(ex, "Creation of a Card of type {0} failed, CardType assembly not valid [class:{1}]", type, cardClass);
                throw new Exception("CardFactory.Create() failed, CardType assembly not valid ");
            }
            catch (TypeLoadException ex)
            {
                Logger.Error(ex, "Creation of a Card of type {0} failed, CardType class name not valid or wrong [class:{1}]", type, cardClass);
                throw new Exception("CardFactory.Create() failed, CardType class name not valid or wrong");
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Creation of a Card of type {0} failed, unexpected error occoured: {1}", type, ex.Message);
                throw new Exception(string.Format("CardFactory.Create() failed, unexpected error occoured: {0}", ex.Message), ex);
            }
		}

        /// <summary>
        /// Create an instance of the card for the given CardType and fullfills all the card data
        /// using the given object
        /// </summary>
        /// <param name="type">CardType of the card to create</param>
        /// <param name="kind">CardKind of the card to create</param>
        /// <param name="data">Generic data object used to load the created Card</param>
        /// <returns>An instance of a Card for the given CardType an the given CardKind</returns>
		public static Card CreateAndLoad(CardType type, CardKind kind, object data)
		{
            Card card = Create(type, kind);
            // LOG > Caricamento dati carta non implementato
            Logger.Warn("Card data loading not implemented!");
			// Dynamic class creation
			return card;
		}

        #endregion PUBLIC METHODS

    }
}
