﻿using System;

namespace sos.core.cards
{
    public class PlayerCard : ExpCard
    {

        #region CONSTANTS

		protected static readonly Role              DEFAULT_ROLE							= Role.NONE;
		protected static readonly string            DEFAULT_ACTION_TEXT					    = "";

        #endregion CONSTANTS

        #region FIELDS

        protected Role _role;
        protected Stats _stats;
        protected string _actionText;

        #endregion FIELDS

        #region CONSTRUCTORS

		public PlayerCard(CardKind kind) : base(kind, CardType.PLAYER)
		{
			// Init
			_stats = new Stats();
			// Clear
			Clear();
		}

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public Role Role
        {
            get { return _role; }
            set { _role = value; }
        }

        public Stats Stats
        {
            get { return _stats; }
            set { _stats = value; }
        }

        public string ActionText
        {
            get { return _actionText; }
            set { _actionText = value; }
        }

		public override School SingleSchool
		{
			get { return _schools[1]; }
		}
		
		public override Boolean IsSingleSchool
		{
			get { return _schools.Count == 2; } // School.GENERIC is always present in PlayerCard 
		}
		
		public override bool IsMultiSchool
		{
			get { return _schools.Count > 2; }
		}

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

		public override void Copy(Card card)
		{
			base.Copy(card);
			// Cast the card
			PlayerCard player = card as PlayerCard;
			// Copy properties
			_role			= player.Role;
			_actionText		= player.ActionText;
			_stats			= player.Stats.Clone();
		}
		
		public override Card Clone()
		{
			PlayerCard clone = new PlayerCard(_kind);
			clone.Copy(this);
			return clone;
		}
		
		public override void Clear()
		{
			// Ancestor
			base.Clear();
			// Clears data
			_role			= DEFAULT_ROLE;
			_actionText		= DEFAULT_ACTION_TEXT;
			// Clears objects
			_stats.Clear();
            _schools.Add(School.GENERIC);
		}

        public override string ToString()
        {
            return string.Format("{0} (name: {1}, id: {2}, intanceid: {3}, role: {4}, school: {5})", 
                                 GetType().FullName,
                                 Name,
                                 Id,
                                 InstanceId,
                                 Role.Name,
                                 SchoolName);
        }

        #endregion PUBLIC METHODS

    }
}
