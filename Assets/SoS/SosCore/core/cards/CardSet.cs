﻿using System;
using sos.data;

namespace sos.core.cards
{
    /// <summary>
    /// 
    /// </summary>
    public class CardSet : Enumerator
    {

        #region CONSTANTS

        public static readonly CardSet  BASE		            = new CardSet(1, "Base");

        #endregion CONSTANTS

        #region CONSTRUCTORS

        public CardSet(int id, string name) : base(id, name) {}

        #endregion CONSTRUCTORS

        #region STATIC METHODS

        public static string[] Names()
        {
            return Enumerators.Names(typeof(CardSet));
        }

        public static CardSet[] Values()
        {
            return Enumerators.Values<CardSet>(typeof(CardSet));
        }

        public static CardSet GetById(int id)
        {
            return Enumerators.GetById<CardSet>(typeof(CardSet), id);
        }

        public static CardSet GetByName(string name)
        {
            return Enumerators.GetByName<CardSet>(typeof(CardSet), name);
        }

        #endregion STATIC METHODS

    }
}
