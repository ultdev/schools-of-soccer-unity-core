﻿using System;

namespace sos.core.cards
{
    public class GoalkeeperCard : PlayerCard
    {

        #region CONSTANTS

        #endregion CONSTANTS

        #region FIELDS

        #endregion FIELDS

        #region CONSTRUCTORS

        public GoalkeeperCard(CardKind kind) : base(kind)
        {
            _type = CardType.GOALKEEPER;
            _role = Role.GOALKEEPER;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        public override void Copy(Card card)
        {
            base.Copy(card);
            _role = Role.GOALKEEPER;
        }

        public override Card Clone()
        {
			GoalkeeperCard clone = new GoalkeeperCard(_kind);
			clone.Copy(this);
			return clone;
        }

        #endregion PUBLIC METHODS

    }
}
