﻿using System;
using System.Collections.Generic;
using sos.logging;
using sos.utils;

namespace sos.core.cards
{
    public class Deck
    {

        #region CONSTANTS

        public const string DEFAULT_NAME = "Empty Deck";

        #endregion CONSTANTS

        #region FIELDS

        private uint _id;
        private string _name;
        private bool _active;
        private Dictionary<Position, PlayerCard> _formation = new Dictionary<Position, PlayerCard>();
        private List<ActionCard> _actionCards = new List<ActionCard>();
        // TAV 2015-12-05 Collection reference and controls removed
        // private Collection _collection;

        #endregion FIELDS

        #region CONSTRUCTORS

        /// <summary>
        /// Create a new deck with an id and a name
        /// </summary>
        /// <param name="id">Deck unique id</param>
        /// <param name="name">Deck name</param>
        public Deck(uint id, string name) : this()
        {
            // Initialization
            _name = name;
            _id = id;
        }

        /// <summary>
        /// Creates an empty Deck
        /// </summary>
        public Deck()
        {
            Clear();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        //public Collection Collection
        //{
        //    get { return _collection; }
        //}

        public uint Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
            }
        }

        public bool Active
        {
            get { return _active; }
            set { _active = value;}
        }

        public Dictionary<Position, PlayerCard> Formation
        {
            get { return _formation; }
        }

        public List<ActionCard> ActionCards
        {
            get { return _actionCards; }
        }

        public int ActionCardsCount
        {
            get { return _actionCards.Count; }
        }

        public GoalkeeperCard Goalkeeper
        {
            get { return GetPlayerCardByPosition(Position.GOALKEEPER) as GoalkeeperCard; }
        }

        public bool HasGoalkeeper
        {
            get
            {
                return HasPlayerCardByPosition(Position.GOALKEEPER);
            }
        }

        public PlayerCard Defender01
        {
            get
            {
                return GetPlayerCardByPosition(Position.DEFENDER_01);
            }
        }

        public bool HasDefender01
        {
            get
            {
                return HasPlayerCardByPosition(Position.DEFENDER_01);
            }
        }

        public PlayerCard Defender02
        {
            get
            {
                return GetPlayerCardByPosition(Position.DEFENDER_02);
            }
        }

        public bool HasDefender02
        {
            get
            {
                return HasPlayerCardByPosition(Position.DEFENDER_02);
            }
        }

        public PlayerCard Midfield
        {
            get
            {
                return GetPlayerCardByPosition(Position.MIDFIELD);
            }
        }

        public bool HasMidfield
        {
            get
            {
                return HasPlayerCardByPosition(Position.MIDFIELD);
            }
        }

        public PlayerCard Forward01
        {
            get
            {
                return GetPlayerCardByPosition(Position.FORWARD_01);
            }
        }

        public bool HasForward01
        {
            get
            {
                return HasPlayerCardByPosition(Position.FORWARD_01);
            }
        }

        public PlayerCard Forward02
        {
            get
            {
                return GetPlayerCardByPosition(Position.FORWARD_02);
            }
        }

        public bool HasForward02
        {
            get
            {
                return HasPlayerCardByPosition(Position.FORWARD_02);
            }
        }

        public PlayerCard SubstitutionDefence
        {
            get
            {
                return GetPlayerCardByPosition(Position.SUB_DEFENCE);
            }
        }

        public bool HasSubstitutionDefence
        {
            get
            {
                return HasPlayerCardByPosition(Position.SUB_DEFENCE);
            }
        }

        public PlayerCard SubstitutionMidfield
        {
            get
            {
                return GetPlayerCardByPosition(Position.SUB_MIDFIELD);
            }
        }

        public bool HasSubstitutionMidfield
        {
            get
            {
                return HasPlayerCardByPosition(Position.SUB_MIDFIELD);
            }
        }

        public PlayerCard SubstitutionForward
        {
            get
            {
                return GetPlayerCardByPosition(Position.SUB_FORWARD);
            }
        }

        public bool HasSubstitutionForward
        {
            get
            {
                return HasPlayerCardByPosition(Position.SUB_FORWARD);
            }
        }

        public int PlayersCount
        {
            get
            {
                int count = 0;
                if (HasPlayerCardByPosition(Position.GOALKEEPER))
                    count++;
                if (HasPlayerCardByPosition(Position.DEFENDER_01))
                    count++;
                if (HasPlayerCardByPosition(Position.DEFENDER_02))
                    count++;
                if (HasPlayerCardByPosition(Position.MIDFIELD))
                    count++;
                if (HasPlayerCardByPosition(Position.FORWARD_01))
                    count++;
                if (HasPlayerCardByPosition(Position.FORWARD_02))
                    count++;
                if (HasPlayerCardByPosition(Position.SUB_DEFENCE))
                    count++;
                if (HasPlayerCardByPosition(Position.SUB_MIDFIELD))
                    count++;
                if (HasPlayerCardByPosition(Position.SUB_FORWARD))
                    count++;
                return count;
            }
        }

        public bool IsValid
        {
            get
            {
                return  _name != "" &&
                        IsFormationValid &&
                        IsActionListValid;
            }
        }

        public bool IsActivable
        {
            get { return true; }
            /* LT_TODO: verificare utilizzo e riabilitare controllo
            _name != "" &&
            isFormationValid && 
            isActionListValid;
            */
        }

        public bool IsFormationValid
        {
            get
            {
                return  HasPlayerCardByPosition(Position.GOALKEEPER)    &&
                        HasPlayerCardByPosition(Position.DEFENDER_01)   &&
                        HasPlayerCardByPosition(Position.DEFENDER_02)   &&
                        HasPlayerCardByPosition(Position.MIDFIELD)      &&
                        HasPlayerCardByPosition(Position.FORWARD_01)    &&
                        HasPlayerCardByPosition(Position.FORWARD_02);
            }
        }

        public bool IsActionListValid
        {
            get
            {
                return RequiredActionCards == ActionCardsCount;
            }

        }

        public int RequiredActionCards
        {
            get
            {
                throw new NotImplementedException("Deck.RequiredActionCards is not implemented!");
            }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        protected int GetActionCardIndex(uint instanceId)
        {
            for (int i = 0; i < _actionCards.Count; i++)
            {
                if (_actionCards[i].InstanceId == instanceId)
                    return i;
            }
            return Defaults.INVALID_INDEX;
        }

        protected int GetActionCardInstanceCount(ActionCard card)
        {
            int count = 0;
            // Count how many card instances are already present into the deck
            for (int i = 0; i < _actionCards.Count; i++)
            {
                // If the card has the same id, increment the count
                if (_actionCards[i].Id == card.Id)
                    count++;
            }
            return count;
        }

        protected void SetPlayerCard(Position position, PlayerCard card)
        {
            // TAV 2015-12-05 Collection reference and controls removed
            //if (!_collection.ContainsCard(card))
            //    throw new Exception("PlayerCard card is not contained by deck card collection!");
            // Apply new player
            _formation[position] = card;
        }

        /* TAV 2015-10-05 Data object do not dispatch events

		// Event dispatched methods
		
		protected void DispatchFormationChanged()
		{
            Logger.NotImplemented();
		}
		
		protected void DispatchActionListChanged()
		{
            Logger.NotImplemented();
		}
        */

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        public PlayerCard GetPlayerCardByPosition(Position position)
        {
            return _formation[position];
        }

        public void SetPlayerCardByPosition(Position position, PlayerCard card)
        {
            // Save the current card
            PlayerCard removedCard = _formation[position];
            // Apply the change
            SetPlayerCard(position, card);
            /* TAV 2015-10-05 Data object do not dispatch events
			// Dispatch formation changed event
			DispatchFormationChanged();
            */
        }

        public void ResetPlayerCardByPosition(Position position)
        {
            // Save the current card
            PlayerCard removedCard = _formation[position];
            // Resets player
            _formation[position] = null;
            /* TAV 2015-10-05 Data object do not dispatch events
			// dispatch formation changed event
			DispatchFormationChanged();
            */
        }

        public bool HasPlayerCardByPosition(Position position)
        {
            return _formation[position] != null;
        }

        public bool ContainsPlayerCard(PlayerCard playerCard)
        {
            return ContainsPlayerCardByInstanceId(playerCard.InstanceId);
        }

        public bool ContainsPlayerCardByInstanceId(uint instanceId)
        {
            // Loop over formation player instance id
            foreach (KeyValuePair<Position, PlayerCard> pair in _formation)
            {
                if (pair.Value != null && instanceId == pair.Value.InstanceId)
                    return true;
            }
            return false;
        }

        public void SwapPlayersByPosition(Position source, Position destination)
        {
            // Retrieve the players
            PlayerCard src = _formation[source];
            PlayerCard dst = _formation[destination];
            // Retrieve roles by position
            Role src_role = Role.GetByPosition(source);
            Role dst_role = Role.GetByPosition(destination);
            // Check role
            if (src_role != dst_role)
                throw new Exception("Players cards with different Role cannot be swapped!");
            // Swap
            _formation[source] = dst;
            _formation[destination] = src;
            /* TAV 2015-10-05 Data object do not dispatch events
			// Dispatch events to refresh
			DispatchFormationChanged();
            */
        }

        public ActionCard GetActionCard(int index)
        {
            return _actionCards[index];
        }

        public ActionCard GetActionCardByInstanceId(uint instanceId)
        {
            ActionCard card = null;
            int index = GetActionCardIndex(instanceId);
            if (index > Defaults.INVALID_INDEX)
                card = _actionCards[index];
            return card;
        }

        public uint[] GetActionCardsInstanceIdList()
        {
            List<uint> actionCardsId = new List<uint>();
            for (int i = 0; i < _actionCards.Count; i++)
            {
                ActionCard card = GetActionCard(i);
                actionCardsId.Add(card.InstanceId);
            }
            return actionCardsId.ToArray();
        }

        public bool ContainsActionCard(ActionCard card)
        {
            return ContainsActionCardByInstanceId(card.InstanceId);
        }

        public bool ContainsActionCardByInstanceId(uint instanceId)
        {
            return GetActionCardIndex(instanceId) != Defaults.INVALID_INDEX;
        }

        public void AddActionCard(ActionCard card)
        {
            // Card check
            if (ContainsActionCard(card))
                throw new Exception("Action card instance already present into deck!");
            // TAV 2015-12-05 Collection reference and controls removed
            // if (!_collection.ContainsCard(card as Card))
            //     throw new Exception("Action card is not contained by deck card collection!");
            if (GetActionCardInstanceCount(card) >= card.Limit)
                throw new Exception("Max number of Card already present into deck!");
            // Add the card
            _actionCards.Add(card);
            /* TAV 2015-10-05 Data object do not dispatch events
			// Dispatch events
			DispatchActionListChanged();
            */
        }

        public void RemoveActionCard(ActionCard card)
        {
            // Card check
            int index = GetActionCardIndex(card.InstanceId);
            if (index == Defaults.INVALID_INDEX)
                throw new Exception("Action card instance is NOT present into deck!");
            // Remove the card
            _actionCards.RemoveAt(index);
            /* TAV 2015-10-05 Data object do not dispatch events
			// Dispatch events
			DispatchActionListChanged();
            */
        }

        public bool EqualsActionCards(Deck deck)
        {
            if (_actionCards.Count != deck.ActionCardsCount)
                return false;
            for (int i = 0; i < _actionCards.Count; i++)
            {
                ActionCard c1 = GetActionCard(i);
                ActionCard c2 = deck.GetActionCard(i);
                if (!c1.Equals(c2))
                    return false;
            }
            return true;
        }

        public bool EqualsFormation(Deck deck)
        {
            if (PlayersCount != deck.PlayersCount)
                return false;
            return ((!HasGoalkeeper && !deck.HasGoalkeeper) || _formation[Position.GOALKEEPER].Equals(deck.Goalkeeper)) &&
                    ((!HasDefender01 && !deck.HasDefender01) || _formation[Position.DEFENDER_01].Equals(deck.Defender01)) &&
                    ((!HasDefender02 && !deck.HasDefender02) || _formation[Position.DEFENDER_02].Equals(deck.Defender02)) &&
                    ((!HasMidfield && !deck.HasMidfield) || _formation[Position.MIDFIELD].Equals(deck.Midfield)) &&
                    ((!HasForward01 && !deck.HasForward01) || _formation[Position.FORWARD_01].Equals(deck.Forward01)) &&
                    ((!HasForward02 && !deck.HasForward02) || _formation[Position.FORWARD_02].Equals(deck.Forward02)) &&
                    ((!HasSubstitutionDefence && !deck.HasSubstitutionDefence) || _formation[Position.SUB_DEFENCE].Equals(deck.SubstitutionDefence)) &&
                    ((!HasSubstitutionMidfield && !deck.HasSubstitutionMidfield) || _formation[Position.SUB_MIDFIELD].Equals(deck.SubstitutionMidfield)) &&
                    ((!HasSubstitutionForward && !deck.HasSubstitutionForward) || _formation[Position.SUB_FORWARD].Equals(deck.SubstitutionForward));
        }

        public bool Equals(Deck deck)
        {
            // Basic checks
            if (deck == null)
                return false;
            if (deck == this)
                return true;
            // Check action cards
            return _id == deck.Id &&
                    _name == deck.Name &&
                    EqualsFormation(deck) &&
                    EqualsActionCards(deck);
        }

        public void CopyActionCards(Deck deck)
        {
            // Temporary array
            List<ActionCard> actionCards = new List<ActionCard>();
            // Copy the action cards
            for (int i = 0; i < deck.ActionCardsCount; i++)
            {
                /* TAV 2015-12-05 Collection reference and controls removed
                // Retrieve source deck action card
                ActionCard card = deck.GetActionCard(i);
                // If the collection of the deck is different, verify the card existence into the current collection
                if (deck.Collection != _collection && !_collection.ContainsCard(card))
                   throw new Exception(string.Format("DeckNew copy failed, action card '{0}' doesn't exists in current collection", card.InstanceId));
                // Adds the card to the temporary list
                actionCards.Add(_collection.GetCardByInstanceId(card.InstanceId) as ActionCard);
                */
                
                // Retrieve source deck action card
                ActionCard card = deck.GetActionCard(i);
                // Adds the card to the temporary list
                actionCards.Add(GetActionCardByInstanceId(card.InstanceId));
            }
            // Copy action cards
            _actionCards.Clear();
            _actionCards.AddRange(actionCards);
            /* TAV 2015-10-05 Data object do not dispatch events
			// Dispatch change events
			if (dispatchEvent) DispatchActionListChanged();
            */
        }

        public void CopyFormation(Deck deck)
        {
            /* TAV 2015-12-05 Collection reference and controls removed
            // Check if the collection is the same
            if (deck.Collection == _collection)
            {
                // Check fromation cards
                if (deck.HasGoalkeeper && !_collection.ContainsCard(deck.Goalkeeper))
                    throw new Exception(string.Format("Deck copy failed, deck {0} card doesn't exists in current collection", Position.GOALKEEPER.Name));
                if (deck.HasDefender01 && !_collection.ContainsCard(deck.Defender01))
                    throw new Exception(string.Format("Deck copy failed, deck {0} card doesn't exists in current collection", Position.DEFENDER_01.Name));
                if (deck.HasDefender02 && !_collection.ContainsCard(deck.Defender02))
                    throw new Exception(string.Format("Deck copy failed, deck {0} card doesn't exists in current collection", Position.DEFENDER_02.Name));
                if (deck.HasMidfield && !_collection.ContainsCard(deck.Midfield))
                    throw new Exception(string.Format("Deck copy failed, deck {0} card doesn't exists in current collection", Position.MIDFIELD.Name));
                if (deck.HasForward01 && !_collection.ContainsCard(deck.Forward01))
                    throw new Exception(string.Format("Deck copy failed, deck {0} card doesn't exists in current collection", Position.FORWARD_01.Name));
                if (deck.HasForward02 && !_collection.ContainsCard(deck.Forward02))
                    throw new Exception(string.Format("Deck copy failed, deck {0} card doesn't exists in current collection", Position.FORWARD_01.Name));
                if (deck.HasSubstitutionDefence && !_collection.ContainsCard(deck.SubstitutionDefence))
                    throw new Exception(string.Format("Deck copy failed, deck {0} card doesn't exists in current collection", Position.SUB_DEFENCE.Name));
                if (deck.HasSubstitutionMidfield && !_collection.ContainsCard(deck.SubstitutionMidfield))
                    throw new Exception(string.Format("Deck copy failed, deck {0} card doesn't exists in current collection", Position.SUB_MIDFIELD.Name));
                if (deck.HasSubstitutionForward && !_collection.ContainsCard(deck.SubstitutionForward))
                    throw new Exception(string.Format("Deck copy failed, deck {0} card doesn't exists in current collection", Position.SUB_FORWARD.Name));
            }            
            // Copy all formation references
            _formation[Position.GOALKEEPER]     = deck.HasGoalkeeper ? _collection.GetCardByInstanceId(deck.Goalkeeper.InstanceId) as PlayerCard : null;
            _formation[Position.DEFENDER_01]    = deck.HasDefender01 ? _collection.GetCardByInstanceId(deck.Defender01.InstanceId) as PlayerCard : null;
            _formation[Position.DEFENDER_02]    = deck.HasDefender02 ? _collection.GetCardByInstanceId(deck.Defender02.InstanceId) as PlayerCard : null;
            _formation[Position.MIDFIELD]       = deck.HasMidfield ? _collection.GetCardByInstanceId(deck.Midfield.InstanceId) as PlayerCard : null;
            _formation[Position.FORWARD_01]     = deck.HasForward01 ? _collection.GetCardByInstanceId(deck.Forward01.InstanceId) as PlayerCard : null;
            _formation[Position.FORWARD_02]     = deck.HasForward02 ? _collection.GetCardByInstanceId(deck.Forward02.InstanceId) as PlayerCard : null;
            _formation[Position.SUB_DEFENCE]    = deck.HasSubstitutionDefence ? _collection.GetCardByInstanceId(deck.SubstitutionDefence.InstanceId) as PlayerCard : null;
            _formation[Position.SUB_MIDFIELD]   = deck.HasSubstitutionMidfield ? _collection.GetCardByInstanceId(deck.SubstitutionMidfield.InstanceId) as PlayerCard : null;
            _formation[Position.SUB_FORWARD]    = deck.HasSubstitutionForward ? _collection.GetCardByInstanceId(deck.SubstitutionForward.InstanceId) as PlayerCard : null;
            */

            
            // Copy all formation references
            _formation[Position.GOALKEEPER]     = deck.HasGoalkeeper            ? deck.GetPlayerCardByPosition(Position.GOALKEEPER)     : null;
            _formation[Position.DEFENDER_01]    = deck.HasDefender01            ? deck.GetPlayerCardByPosition(Position.DEFENDER_01)    : null;
            _formation[Position.DEFENDER_02]    = deck.HasDefender02            ? deck.GetPlayerCardByPosition(Position.DEFENDER_02)    : null;
            _formation[Position.MIDFIELD]       = deck.HasMidfield              ? deck.GetPlayerCardByPosition(Position.MIDFIELD)       : null;
            _formation[Position.FORWARD_01]     = deck.HasForward01             ? deck.GetPlayerCardByPosition(Position.FORWARD_01)     : null;
            _formation[Position.FORWARD_02]     = deck.HasForward02             ? deck.GetPlayerCardByPosition(Position.FORWARD_02)     : null;
            _formation[Position.SUB_DEFENCE]    = deck.HasSubstitutionDefence   ? deck.GetPlayerCardByPosition(Position.SUB_DEFENCE)    : null;
            _formation[Position.SUB_MIDFIELD]   = deck.HasSubstitutionMidfield  ? deck.GetPlayerCardByPosition(Position.SUB_MIDFIELD)   : null;
            _formation[Position.SUB_FORWARD]    = deck.HasSubstitutionForward   ? deck.GetPlayerCardByPosition(Position.SUB_FORWARD)    : null;

            /* TAV 2015-10-05 Data object do not dispatch events
			// Dispatch change events
			if (dispatchEvent) DispatchFormationChanged();
            */
        }

        public void Copy(Deck deck)
        {
            /* TAV 2015-12-05 Collection reference and controls removed
            // Temporary array
            List<ActionCard> actionCards = new List<ActionCard>();
            // Check if the collection is the same
            if (deck.Collection != _collection)
            {
                // Check formation cards
                if (deck.HasGoalkeeper && !_collection.ContainsCard(deck.Goalkeeper))
                    throw new Exception(string.Format("Deck copy failed, deck {0} card doesn't exists in current collection", Position.GOALKEEPER.Name));
                if (deck.HasDefender01 && !_collection.ContainsCard(deck.Defender01))
                    throw new Exception(string.Format("Deck copy failed, deck {0} card doesn't exists in current collection", Position.DEFENDER_01.Name));
                if (deck.HasDefender02 && !_collection.ContainsCard(deck.Defender02))
                    throw new Exception(string.Format("Deck copy failed, deck {0} card doesn't exists in current collection", Position.DEFENDER_02.Name));
                if (deck.HasMidfield && !_collection.ContainsCard(deck.Midfield))
                    throw new Exception(string.Format("Deck copy failed, deck {0} card doesn't exists in current collection", Position.MIDFIELD.Name));
                if (deck.HasForward01 && !_collection.ContainsCard(deck.Forward01))
                    throw new Exception(string.Format("Deck copy failed, deck {0} card doesn't exists in current collection", Position.FORWARD_01.Name));
                if (deck.HasForward02 && !_collection.ContainsCard(deck.Forward02))
                    throw new Exception(string.Format("Deck copy failed, deck {0} card doesn't exists in current collection", Position.FORWARD_01.Name));
                if (deck.HasSubstitutionDefence && !_collection.ContainsCard(deck.SubstitutionDefence))
                    throw new Exception(string.Format("Deck copy failed, deck {0} card doesn't exists in current collection", Position.SUB_DEFENCE.Name));
                if (deck.HasSubstitutionMidfield && !_collection.ContainsCard(deck.SubstitutionMidfield))
                    throw new Exception(string.Format("Deck copy failed, deck {0} card doesn't exists in current collection", Position.SUB_MIDFIELD.Name));
                if (deck.HasSubstitutionForward && !_collection.ContainsCard(deck.SubstitutionForward))
                    throw new Exception(string.Format("Deck copy failed, deck {0} card doesn't exists in current collection", Position.SUB_FORWARD.Name));
                // Check action cards
                for (int x = 0; x < deck.ActionCardsCount; x++)
                {
                    // Retrieve source deck action card
                    ActionCard c = deck.GetActionCard(x);
                    // Verify the card existence into the current collection
                    if (!_collection.ContainsCard(c))
                        throw new Exception(string.Format("DeckNew copy failed, action card '{0}' doesn't exists in current collection", c.InstanceId));
                }
            }
            // Retrieve action cards
            for (int i = 0; i < deck.ActionCardsCount; i++)
            {
                // Retrieve source deck action card
                ActionCard card = deck.GetActionCard(i);
                // Adds the card to the temporary list
                actionCards.Add(_collection.GetCardByInstanceId(card.InstanceId) as ActionCard);
            }
            // Retrieve formation
            PlayerCard goalkeeper = deck.HasGoalkeeper ? _collection.GetCardByInstanceId(deck.Goalkeeper.InstanceId) as PlayerCard : null;
            PlayerCard defender01 = deck.HasDefender01 ? _collection.GetCardByInstanceId(deck.Defender01.InstanceId) as PlayerCard : null;
            PlayerCard defender02 = deck.HasDefender02 ? _collection.GetCardByInstanceId(deck.Defender02.InstanceId) as PlayerCard : null;
            PlayerCard midfield = deck.HasMidfield ? _collection.GetCardByInstanceId(deck.Midfield.InstanceId) as PlayerCard : null;
            PlayerCard forward01 = deck.HasForward01 ? _collection.GetCardByInstanceId(deck.Forward01.InstanceId) as PlayerCard : null;
            PlayerCard forward02 = deck.HasForward02 ? _collection.GetCardByInstanceId(deck.Forward02.InstanceId) as PlayerCard : null;
            PlayerCard substitutionDefence = deck.HasSubstitutionDefence ? _collection.GetCardByInstanceId(deck.SubstitutionDefence.InstanceId) as PlayerCard : null;
            PlayerCard substitutionMidfield = deck.HasSubstitutionMidfield ? _collection.GetCardByInstanceId(deck.SubstitutionMidfield.InstanceId) as PlayerCard : null;
            PlayerCard substitutionForward = deck.HasSubstitutionForward ? _collection.GetCardByInstanceId(deck.SubstitutionForward.InstanceId) as PlayerCard : null;
            // Copy action cards
            _actionCards.Clear();
            _actionCards.AddRange(actionCards);
            // Copy formation
            _formation[Position.GOALKEEPER] = goalkeeper;
            _formation[Position.DEFENDER_01] = defender01;
            _formation[Position.DEFENDER_02] = defender02;
            _formation[Position.MIDFIELD] = midfield;
            _formation[Position.FORWARD_01] = forward01;
            _formation[Position.FORWARD_02] = forward02;
            _formation[Position.SUB_DEFENCE] = substitutionDefence;
            _formation[Position.SUB_MIDFIELD] = substitutionMidfield;
            _formation[Position.SUB_FORWARD] = substitutionForward;
            // Copy properties
            _id = deck.Id;
            _name = deck.Name;
            */

            // Retrieve action cards
            List<ActionCard> actionCards = new List<ActionCard>();
            for (int i = 0; i < deck.ActionCardsCount; i++)
            {
                // Retrieve source deck action card
                ActionCard card = deck.GetActionCard(i);
                // Adds the card to the temporary list
                actionCards.Add(GetActionCardByInstanceId(card.InstanceId));
            }
            // Retrieve formation
            PlayerCard goalkeeper           = deck.HasGoalkeeper            ? deck.GetPlayerCardByPosition(Position.GOALKEEPER)      : null;
            PlayerCard defender01           = deck.HasDefender01            ? deck.GetPlayerCardByPosition(Position.DEFENDER_01)     : null;
            PlayerCard defender02           = deck.HasDefender02            ? deck.GetPlayerCardByPosition(Position.DEFENDER_02)     : null;
            PlayerCard midfield             = deck.HasMidfield              ? deck.GetPlayerCardByPosition(Position.MIDFIELD)        : null;
            PlayerCard forward01            = deck.HasForward01             ? deck.GetPlayerCardByPosition(Position.FORWARD_01)      : null;
            PlayerCard forward02            = deck.HasForward02             ? deck.GetPlayerCardByPosition(Position.FORWARD_02)      : null;
            PlayerCard substitutionDefence  = deck.HasSubstitutionDefence   ? deck.GetPlayerCardByPosition(Position.SUB_DEFENCE)     : null;
            PlayerCard substitutionMidfield = deck.HasSubstitutionMidfield  ? deck.GetPlayerCardByPosition(Position.SUB_MIDFIELD)    : null;
            PlayerCard substitutionForward  = deck.HasSubstitutionForward   ? deck.GetPlayerCardByPosition(Position.SUB_FORWARD)     : null;
            // Copy action cards
            _actionCards.Clear();
            _actionCards.AddRange(actionCards);
            // Copy formation
            _formation[Position.GOALKEEPER]     = goalkeeper;
            _formation[Position.DEFENDER_01]    = defender01;
            _formation[Position.DEFENDER_02]    = defender02;
            _formation[Position.MIDFIELD]       = midfield;
            _formation[Position.FORWARD_01]     = forward01;
            _formation[Position.FORWARD_02]     = forward02;
            _formation[Position.SUB_DEFENCE]    = substitutionDefence;
            _formation[Position.SUB_MIDFIELD]   = substitutionMidfield;
            _formation[Position.SUB_FORWARD]    = substitutionForward;
            // Copy properties
            _id = deck.Id;
            _name = deck.Name;

            /* TAV 2015-10-05 Data object do not dispatch events
			// Dispatch change events
			DispatchActionListChanged();
			DispatchFormationChanged();
            */
        }

        public Deck Clone()
        {
            /* TAV 2015-12-05 Collection reference and controls removed
            Deck deck = new Deck(_collection, _id, _name);
            deck.Copy(this);
            return deck;
            */
            Deck deck = new Deck(_id, _name);
            deck.Copy(this);
            return deck;
        }

        /* TAV 2015-12-05 Collection reference and controls removed
        public Deck Duplicate(string newName)
        {
            // Create a new empty deck
            Deck newDeck = _collection.CreateNewDeck(Defaults.INVALID_ID, newName);
            // Disables event dispatch
            // LT_TODO > REMOVED newDeck.dispatchEnabled = false;
            // Cop formation and action cards
            newDeck.CopyFormation(this);
            newDeck.CopyActionCards(this);
            // TAV 2015-10-05 Data object do not dispatch events
			// // Ensables event dispatch
			// // LT_TODO > REMOVED newDeck.dispatchEnabled = true;
            return newDeck;
        }
        */

        public void ClearFormation()
        {
            // Clears the formation!
            _formation[Position.GOALKEEPER]     = null;
            _formation[Position.DEFENDER_01]    = null;
            _formation[Position.DEFENDER_02]    = null;
            _formation[Position.MIDFIELD]       = null;
            _formation[Position.FORWARD_01]     = null;
            _formation[Position.FORWARD_02]     = null;
            _formation[Position.FORWARD_02]     = null;
            _formation[Position.SUB_DEFENCE]    = null;
            _formation[Position.SUB_MIDFIELD]   = null;
            _formation[Position.SUB_FORWARD]    = null;
            /* TAV 2015-10-05 Data object do not dispatch events
			// Check if dispatch events
			if (dispatchEvent) DispatchFormationChanged();
            */
        }

        public void ClearActionCards()
        {
            // Clears the action card list 
            _actionCards.Clear();
            /* TAV 2015-10-05 Data object do not dispatch events
			// Check if dispatch events
			if (dispatchEvent) DispatchActionListChanged();
            */
        }

        public void Clear()
        {
            _id = Defaults.INVALID_ID;
            _name = Deck.DEFAULT_NAME;
            // Clears the formation
            ClearFormation();
            // Clears the action cards
            ClearActionCards();
            /* TAV 2015-10-05 Data object do not dispatch events
			// Dispatch events
			DispatchFormationChanged();
			DispatchActionListChanged();
            */
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} (id: {1}, name: {2})",
                                 GetType().FullName,
                                 _id,
                                 _name);
        }

        #endregion PUBLIC METHODS

    }
}
