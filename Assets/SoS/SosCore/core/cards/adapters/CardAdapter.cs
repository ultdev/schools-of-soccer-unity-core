﻿using sos.core.cards;
using sos.data.adapters;
using sos.data;

namespace sos.core.cards.adapters
{

    public class CardAdapter
    {

    }

    /* TAV Rimosso in attesa di implementazione correta tra librerie, probabile spostamento in unica libreria sos.core 
    public class CardAdapter<C> : DataAdapter<C> where C : Card
    {

        #region CONSTANTS

		// Fields definitions definition
		protected static readonly string        FIELD_CARD_ID			        = "cardId";
		protected static readonly string        FIELD_INSTANCE_ID		        = "instanceId";
		protected static readonly string        FIELD_CARD_NAME			        = "cardName";
		protected static readonly string        FIELD_NAME				        = "name";
		protected static readonly string        FIELD_CARD_TYPE			        = "type";
		protected static readonly string        FIELD_CARD_SUBTYPE		        = "subtype";
		protected static readonly string        FIELD_CARD_SET			        = "cardSet";
		protected static readonly string        FIELD_DESCRIPTION		        = "descrtiption";
		protected static readonly string        FIELD_IMAGE				        = "image";
		protected static readonly string        FIELD_LIMIT				        = "deckLimit";
		protected static readonly string        FIELD_SCHOOL			        = "schoolId";
		protected static readonly string        FIELD_SKILL				        = "skill";

        // Parameters definitions definition
        public static readonly string           PARAM_CARD_KIND				    = "cardKind";

        #endregion CONSTANTS

        #region FIELDS

        #endregion FIELDS

        #region CONSTRUCTORS

        public CardAdapter(IDataDecoder decoder) : base(decoder)
        {
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        #endregion PROPERTIES

        #region PRIVATE METHODS

        protected override C Adapt(DataParameters parameters)
        {
            // Base data validation
            if (!_decoder.Contains(FIELD_CARD_TYPE)) ThrowMissingPropertyException(FIELD_CARD_TYPE);
            if (!parameters.Contains(PARAM_CARD_KIND)) ThrowMissingDataParameterException(PARAM_CARD_KIND);
            // Retrieve the card kind
            CardKind cardKind = GetDataParameterValue<CardKind>(parameters, PARAM_CARD_KIND);
            // Retrieve the card type
            CardType cardType = CardType.GetById(_decoder.Decode<int>(FIELD_CARD_TYPE));
            // Card object creation
            Card c = CardFactory.Create(cardType, cardKind);
			// Validation by CardKind
            if (cardKind == CardKind.DEFINITION)
            {
                if (!_decoder.Contains(FIELD_CARD_ID))  ThrowMissingPropertyException(FIELD_CARD_ID);
                if (!_decoder.Contains(FIELD_LIMIT))    ThrowMissingPropertyException(FIELD_LIMIT);
            }
            else if (cardKind == CardKind.INSTANCE)
            {
                if (!_decoder.Contains(FIELD_CARD_ID))  ThrowMissingPropertyException(FIELD_CARD_ID);
                if (!_decoder.Contains(FIELD_LIMIT))    ThrowMissingPropertyException(FIELD_LIMIT);
            }
            else if (cardKind == CardKind.GAME_INSTANCE)
            {
            }
            // Generic validation
            if (!_decoder.Contains(FIELD_CARD_NAME) && !_decoder.Contains(FIELD_NAME))  ThrowMissingPropertyException(FIELD_NAME);
            // Adapting
			if (_decoder.Contains(FIELD_CARD_ID)) 	    c.CardId 	    = _decoder.Decode<uint>(FIELD_CARD_ID);
			if (_decoder.Contains(FIELD_INSTANCE_ID))   c.InstanceId    = _decoder.Decode<uint>(FIELD_INSTANCE_ID);
			if (_decoder.Contains(FIELD_CARD_NAME)) 	c.Name 		    = _decoder.Decode<string>(FIELD_CARD_NAME);
			if (_decoder.Contains(FIELD_NAME)) 		    c.Name 		    = _decoder.Decode<string>(FIELD_NAME);
			if (_decoder.Contains(FIELD_LIMIT)) 		c.Limit		    = _decoder.Decode<uint>(FIELD_LIMIT);
			if (_decoder.Contains(FIELD_IMAGE)) 		c.Image		    = _decoder.Decode<string>(FIELD_IMAGE);
			if (_decoder.Contains(FIELD_CARD_SUBTYPE))  c.Subtype	    = CardSubtype.GetById(_decoder.Decode<int>(FIELD_CARD_SUBTYPE));
			if (_decoder.Contains(FIELD_SKILL)) 		c.Skill		    = _decoder.Decode<uint>(FIELD_SKILL);
            // Returns the adapted object
            return c as C;
        }

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        #endregion PUBLIC METHODS

    }
    */
}
