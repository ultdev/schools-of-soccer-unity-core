﻿using System.Linq;
using sos.data;

namespace sos.core.cards
{

    class ActionCardAdapter
    {

    }

    /* TAV Rimosso in attesa di implementazione correta tra librerie, probabile spostamento in unica libreria sos.core 
    class ActionCardAdapter : CardAdapter<ActionCard>
    {

        #region CONSTANTS

		// Fields definitions definition
        protected static readonly string    FIELD_ROLE				            = "role";
		protected static readonly string    FIELD_SPECIAL				        = "special";
        protected static readonly string    FIELD_COST				            = "cost";
        protected static readonly string    FIELD_DURATION			            = "duration";
        protected static readonly string    FIELD_ACTIONTEXT		            = "actionText";
		protected static readonly string    FIELD_TARGET_POSITIONS              = "fieldZone";
		protected static readonly string    FIELD_BALL_CONDITION		        = "ballStatus";
		protected static readonly string    FIELD_TARGET				        = "target";
		protected static readonly string    FIELD_INPUT					        = "input";
		protected static readonly string    FIELD_INPUT_TARGET			        = "target";
		protected static readonly string    FIELD_INPUT_TARGET_TYPE		        = "type";
		protected static readonly string    FIELD_UNIQUE_CLASS			        = "uniqueClass";
		protected static readonly string    FIELD_UNIQUE_CLASS_TYPE		        = "uniqueClassType";


        #endregion CONSTANTS

        #region CONSTRUCTORS

        public ActionCardAdapter(IDataDecoder decoder) : base(decoder)
        {
        }

        #endregion CONSTRUCTORS

        #region PRIVATE METHODS

        protected override ActionCard Adapt(DataParameters parameters)
        {
            // base adapting
            ActionCard actionCard = base.Adapt(parameters);
			// Validating data
			if (!_decoder.Contains(FIELD_ROLE)) 		ThrowMissingPropertyException(FIELD_ROLE);
			if (!_decoder.Contains(FIELD_SCHOOL)) 	    ThrowMissingPropertyException(FIELD_SCHOOL);
			if (!_decoder.Contains(FIELD_ACTIONTEXT))   ThrowMissingPropertyException(FIELD_ACTIONTEXT);
			if (!_decoder.Contains(FIELD_DURATION)) 	ThrowMissingPropertyException(FIELD_DURATION);
            if (!_decoder.Contains(FIELD_COST)) ThrowMissingPropertyException(FIELD_COST);
            // Adapting
            actionCard.Role         = Role.GetBySign(_decoder.Decode<string>(FIELD_ROLE));
            actionCard.ActionText   = _decoder.Decode<string>(FIELD_ACTIONTEXT);
            actionCard.Cost         = _decoder.Decode<uint>(FIELD_COST);
            actionCard.Duration     = _decoder.Decode<uint>(FIELD_DURATION);
            actionCard.Special      = _decoder.Decode<int>(FIELD_SPECIAL) == 0;
            actionCard.UniqueClass  = _decoder.Decode<string>(FIELD_UNIQUE_CLASS);
            if (!string.IsNullOrEmpty(actionCard.UniqueClass))
            {
                actionCard.UniqueClassType  = UniqueClassType.GetById(_decoder.Decode<int>(FIELD_UNIQUE_CLASS_TYPE));
            }

            // TODO : Stats
            // TODO : BallCondition
            // TODO : Target
            // TODO : Target Input
            // TODO : Target positions


            return actionCard;
        }

        #endregion PRIVATE METHODS

    }
    */
}
