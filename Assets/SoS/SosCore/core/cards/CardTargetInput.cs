﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.cards
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class CardTargetInput
    {

        #region CONSTANTS

        /// <summary>
        /// Unique instance that reresents that a card has no input target
        /// </summary>
        public static readonly CardTargetInput NONE     = new CardTargetInputNone();

        #endregion CONSTANTS

        #region FIELDS

        protected TargetType _type;

        #endregion FIELDS

        #region CONSTRUCTORS

        public CardTargetInput(TargetType type)
        {
            _type = type;
            Clear();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public TargetType Type
        {
            get { return _type; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

		public virtual void Clear()
		{
		}

        #endregion PUBLIC METHODS

    }
}
