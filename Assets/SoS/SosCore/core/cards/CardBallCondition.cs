﻿using sos.data;

namespace sos.core.cards
{
    public class CardBallCondition : Enumerator
    {

        #region CONSTANTS

		public static readonly CardBallCondition  ALWAYS			                    = new CardBallCondition(0, "ALWAYS");
		public static readonly CardBallCondition  WITH_BALL			                    = new CardBallCondition(1, "WITH_BALL");
		public static readonly CardBallCondition  WITHOUT_BALL		                    = new CardBallCondition(2, "WITHOUT_BALL");

        #endregion CONSTANTS

        #region CONSTRUCTORS

        public CardBallCondition(int id, string name) : base(id, name) {}

        #endregion CONSTRUCTORS

        #region STATIC METHODS

        public static string[] Names()
        {
            return Enumerators.Names(typeof(CardBallCondition));
        }

        public static CardBallCondition[] Values()
        {
            return Enumerators.Values<CardBallCondition>(typeof(CardBallCondition));
        }

        public static CardBallCondition GetById(int id)
        {
            return Enumerators.GetById<CardBallCondition>(typeof(CardBallCondition), id);
        }

        public static CardBallCondition GetByName(string name)
        {
            return Enumerators.GetByName<CardBallCondition>(typeof(CardBallCondition), name);
        }

        #endregion STATIC METHODS

    }
}
