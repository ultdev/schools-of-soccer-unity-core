﻿using System;
using sos.data;

namespace sos.core.cards
{
    public class CardKind : Enumerator
    {

        #region CONSTANTS

		public static readonly CardKind  DEFINITION			                = new CardKind(1, "Definition");
		public static readonly CardKind  INSTANCE			                = new CardKind(2, "Instance");
		public static readonly CardKind  GAME_INSTANCE		                = new CardKind(3, "Game instance");

        #endregion CONSTANTS

        #region CONSTRUCTORS

        public CardKind(int id, string name) : base(id, name) {}

        #endregion CONSTRUCTORS

        #region STATIC METHODS

        public static string[] Names()
        {
            return Enumerators.Names(typeof(CardKind));
        }

        public static CardKind[] Values()
        {
            return Enumerators.Values<CardKind>(typeof(CardKind));
        }

        public static CardKind GetById(int id)
        {
            return Enumerators.GetById<CardKind>(typeof(CardKind), id);
        }

        public static CardKind GetByName(string name)
        {
            return Enumerators.GetByName<CardKind>(typeof(CardKind), name);
        }

        #endregion STATIC METHODS

    }
}
