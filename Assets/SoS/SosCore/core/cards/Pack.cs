﻿using sos.utils;
using System.Collections.Generic;

namespace sos.core.cards
{
    /// <summary>
    /// 
    /// </summary>
    public class Pack
    {

		#region CONSTANTS

        // Default values
        private const string    DEFAULT_NAME                        = "UNKNOWKN PACK";

		#endregion CONSTANTS

		#region FIELDS

		private uint _id;
		private string _name;
		private List<Card> _cards;

        // Definite in ActionScript ma non usate direttamente
        // private List<ActionCard> _actionCards:;
        // private List<ActionCard> _playerCards;

        #endregion FIELDS

        #region CONSTRUCTORS

        public Pack()
        {
            _cards = new List<Card>();
            Clear();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public uint Id
        {
            get
            {
                return _id;
            }

            protected set
            {
                _id = value;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        public List<Card> Cards
        {
            get
            {
                return _cards;
            }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        /// <summary>
        /// Check if the given pack contains all the same Cards instances of the current Pack
        /// </summary>
        /// <param name="pack">Pack to equals</param>
        /// <returns>True if the given Pack contains all the Card instances of the current Pack</returns>
        protected bool EqualsCards(Pack pack)
        {
            if (_cards.Count != pack.Cards.Count) return false;
            foreach (var card in _cards)
            {
                if (pack.Cards.Find(c => card.Equals(card)) == null) return false;
            }
            return true;
        }

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pack"></param>
        /// <returns></returns>
        public bool Equals(Pack pack)
        {
            return  _id == pack.Id && 
                    EqualsCards(pack);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pack"></param>
        public void Copy(Pack pack)
		{	
            _id         = pack.Id;
            _name       = pack.Name;
            _cards.Clear();
            _cards.AddRange(pack.Cards);
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Pack Clone()
		{
			Pack pack = new Pack();
			pack.Copy(this);
			return pack;
		}

        /// <summary>
        /// 
        /// </summary>
        public void Clear()
		{
			// Clear data
            _id         = Defaults.INVALID_ID;
            _name       = DEFAULT_NAME;
            _cards.Clear();
		}

        #endregion PUBLIC METHODS

    }
}
