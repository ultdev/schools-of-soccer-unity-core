﻿namespace sos.core.cards
{
    public abstract class ExpCard : Card
    {

        #region CONSTANTS

        #endregion CONSTANTS

        #region FIELDS

        protected uint _level;
        protected uint _maxLevel;
        protected uint _expToNextLevel;

        #endregion FIELDS

        #region CONSTRUCTORS

        public ExpCard(CardKind kind, CardType type) : base(kind, type) { }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public uint Level
        {
            get { return _level; }
            set { _level = value; }
        }

        public uint MaxLevel
        {
            get { return _maxLevel; }
            set { _maxLevel = value; }
        }

        public uint ExpToNextLevel
        {
            get { return _expToNextLevel; }
            set { _expToNextLevel = value; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

		public override void Copy(Card card)
		{
			base.Copy(card);
			// Cast the card
			ExpCard expcard = card as ExpCard;
			// Copy properties
			_level			= expcard.Level;
			_maxLevel		= expcard.MaxLevel;
			_expToNextLevel	= expcard.ExpToNextLevel;
		}

        #endregion PUBLIC METHODS

    }
}
