﻿using System;
using System.Collections.Generic;
using System.Text;
using sos.utils;
using sos.data;

namespace sos.core.cards
{
    /// <summary>
    /// Abstract representation of a card
    /// </summary>
    public abstract class Card : IDataObject
    {

        #region CONSTANTS

		protected static readonly uint          DEFAULT_INSTANCE_ID 				    = Defaults.INVALID_ID;
		protected static readonly uint          DEFAULT_CARD_ID 					    = Defaults.INVALID_ID;
		protected static readonly string        DEFAULT_NAME 					        = "";
		protected static readonly string        DEFAULT_DESCRIPTION 				    = "";
		protected static readonly string        DEFAULT_IMAGE 					        = "";
		protected static readonly uint          DEFAULT_LIMIT 						    = 1;
        protected static readonly uint          DEFAULT_SKILL 						    = 1;
		protected static readonly CardType      DEFAULT_TYPE 					        = CardType.ACTION;
		protected static readonly CardSet       DEFAULT_CARD_SET 				        = CardSet.BASE;
		protected static readonly CardKind      DEFAULT_CARD_KIND 				        = CardKind.INSTANCE;
        protected static readonly CardSubtype   DEFAULT_SUBTYPE 				        = CardSubtype.NONE;
        protected static readonly Rarity        DEFAULT_RARITY 				            = Rarity.COMMON;

        protected const string                  SCHOOLS_NAME_SEPARATOR                  = "-";

        #endregion CONSTANTS

        #region FIELDS

        protected CardKind _kind;
        protected CardType _type;
        protected CardSubtype _subtype;
        protected uint _cardId;
        protected uint _instanceId;
        protected string _name;
        protected string _description;
        protected string _image;
        protected uint _limit;
        protected uint _skill;
        protected List<School> _schools;
        protected CardSet _cardSet;
        protected Rarity _rarity;

        #endregion FIELDS

        #region CONSTRUCTORS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="kind"></param>
        /// <param name="type"></param>
        protected Card(CardKind kind, CardType type)
        {
            // Params check
            if (kind == null) throw new ArgumentNullException("kind");
            if (type == null) throw new ArgumentNullException("type");
            // Init values
            _kind = kind;
            _type = type;
            // Init objects
            _schools = new List<School>();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public CardKind Kind
        {
            get { return _kind; }
        }

        public CardType Type
        {
            get { return _type; }
        }

        public CardSubtype Subtype
        {
            get { return _subtype; }
            set { _subtype = value; }
        }

        public uint Id
        {
            get { return _cardId; }
            set { _cardId = value; }
        }

        public uint InstanceId
        {
            get { return _instanceId; }
            set { _instanceId = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public string Image
        {
            get { return _image; }
            set { _image = value; }
        }

        public uint Limit
        {
            get { return _limit; }
            set { _limit = value; }
        }

        public uint Skill
        {
            get { return _skill; }
            set { _skill = value; }
        }

        public List<School> Schools
        {
            get { return _schools; }
            set { _schools = value; }
        }

		public virtual School SingleSchool
		{
			get { return _schools[0]; }
		}

		public virtual bool IsGenericSchool
		{
			get { return HasSchool(School.GENERIC); }
		}
		
		public virtual bool IsSingleSchool
		{
			get { return _schools.Count == 1; }
		}
		
		public virtual bool IsMultiSchool
		{
			get { return _schools.Count > 1; }
		}
		
		public virtual string SchoolName
		{
            get
            {
			    StringBuilder name = new StringBuilder();
			
			    if (IsSingleSchool)
			    {
				    name.Append(SingleSchool.Name);
			    }
			    else
			    {
				    foreach (School school in _schools)
				    {
					    if (name.Length == 0) name.Append(SCHOOLS_NAME_SEPARATOR);
					    name.Append(school.Name);
				    }
			    }
			    return name.ToString();
            }
		}

        public CardSet CardSet
        {
            get { return _cardSet; }
            set { _cardSet = value; }
        }

        public Rarity Rarity
        {
            get { return _rarity; }
            set { _rarity = value; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        protected int GetSchoolIndex(School school)
		{
			for (int i = 0; i<_schools.Count; i++)
			{
				if (_schools[i].Id == school.Id) return i;
			}
			return Defaults.INVALID_INDEX;
		}

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        public bool HasSchool(School school)
		{
			return  GetSchoolIndex(school) != Defaults.INVALID_INDEX;
		}
		
		public bool MatchSchools(List<School> schools)
		{
			for (int i= 0; i < schools.Count; i++)
			{
				if (!HasSchool(schools[i])) return false;
			}
			return true;
		}

        public void AddSchool(School school)
        {
            if (!HasSchool(school))
            {
                _schools.Add(school);
            }
        }

        public void UpdateSchools(List<School> schools)
        {
            schools.ForEach(s => AddSchool(s));
        }

		public virtual bool Equals(Card card)
		{
			return 	_cardId		== card.Id && 
					_instanceId == card.InstanceId;
		}
		
		public virtual void Copy(Card card)
		{
			if (_type != card.Type) throw new ArgumentException(string.Format("Card copy failed, type '{0}' does not match with current card type", card.Type.Name));
			// Copy
			_cardId			= card.Id;
			_instanceId		= card.InstanceId;
			_name			= card.Name;
			_description	= card.Description;
			_image			= card.Image;
			_limit			= card.Limit;
			_cardSet		= card.CardSet;
			_skill			= card.Skill;
            _subtype        = card.Subtype;
            _rarity         = card.Rarity;
			// Copy schools
			_schools.Clear();
            _schools.AddRange(card.Schools);
		}
		
		public virtual Card Clone()
		{
            // LT_TODO > Aggiungere dopo implementazione CardFactory
            throw new NotImplementedException("Card.Clone is not yet implemented!");
			// var clone:Card = CardFactory.createEmptyCard(_type);
			// clone.copy(this);
			// return clone;
		}

        /// <summary>
        /// Creates a card istance of the current card: workd only for card of kind DEFINITION: used to popolate
        /// user card instances from the generic card definitions
        /// </summary>
        /// <param name="instanceId">Id of the instance of the card to create from the definition</param>
        /// <returns>A copy of the current card definition</returns>
        public virtual Card CreateInstance(uint instanceId)
        {
            // Check if the current card is a DEFINITION
            if (_kind != CardKind.DEFINITION) throw new Exception("Cannot create an instance of this card, current card is not a valid card definition (kind: DEFINITION)");
            // Clone and forcing CardKind
            Card instance = Clone();
            instance._kind = CardKind.INSTANCE;
            instance.InstanceId = instanceId;
            return instance;
        }
		
		public virtual void Clear()
		{
			_cardId 		= DEFAULT_CARD_ID;
			_instanceId		= DEFAULT_INSTANCE_ID;
			_name			= DEFAULT_NAME;
			_description	= DEFAULT_DESCRIPTION;
			_image			= DEFAULT_IMAGE;
			_limit			= DEFAULT_LIMIT;
            _skill			= DEFAULT_SKILL;
			_cardSet		= DEFAULT_CARD_SET;
            _subtype        = DEFAULT_SUBTYPE;
            _rarity         = DEFAULT_RARITY;
            _schools.Clear();
            /*
            CardSubtype _subtype;
            uint _cardId;
            uint _instanceId;
            string _name;
            string _description;
            string _image;
            uint _limit;
            uint _skill;
            List<School> _schools;
            CardSet _cardSet;
            */
		}

        #endregion PUBLIC METHODS

    }
}
