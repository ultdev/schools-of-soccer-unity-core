﻿namespace sos.core.cards
{
    public class CardTargetInputNone : CardTargetInput
    {

        #region FIELDS

        private uint _effectId;

        #endregion FIELDS

        #region CONSTRUCTORS

        public CardTargetInputNone() : base(TargetType.NONE)
        {
            Clear();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

		public uint EffectId
		{
			get { return _effectId; }
            set { _effectId  = value; }
		}

        #endregion PROPERTIES

    }
}
