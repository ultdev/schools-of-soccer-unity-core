﻿using System.Collections.Generic;

namespace sos.core.cards
{
    /// <summary>
    /// 
    /// </summary>
    public class ActionCard : Card
    {

        #region CONSTANTS

		// Default values
		protected static readonly Role              DEFAULT_ROLE							= Role.NONE;
		protected static readonly int               DEFAULT_DURATION						= 1;
		protected static readonly int               DEFAULT_COST							= 1;
		protected static readonly string            DEFAULT_ACTION_TEXT					    = "";
		protected static readonly CardBallCondition DEFAULT_BALL_CONDITION	                = CardBallCondition.ALWAYS;
        protected static readonly string            DEFAULT_UNIQUE_CLASS                    = string.Empty;
		protected static readonly UniqueClassType   DEFAULT_UNIQUE_CLASS_TYPE               = UniqueClassType.NONE;
		protected static readonly bool              DEFAULT_SPECIAL		                    = false;
        protected static readonly bool              DEFAULT_FLASH		                    = false;

        #endregion CONSTANTS

        #region FIELDS

        protected Role _role;
        protected int _duration;
        protected int _cost;
        protected string _actionText;
        protected Stats _stats;
        protected CardBallCondition _ballCondition;
        protected Target _target;
        protected CardTargetInput _targetInput;
        protected List<Position> _targetPositions;
        protected string _uniqueClass;
        protected UniqueClassType _uniqueClassType;
        protected bool _special;
        protected bool _flash;

        #endregion FIELDS

        #region CONSTRUCTORS

        public ActionCard(CardKind kind) : base(kind, CardType.ACTION)
        {
			// Init objets
			_stats = new Stats();
			_targetPositions = new List<Position>();
			// Clear
			Clear();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public Role Role
        {
            get { return _role; }
            set { _role = value; }
        }

        public int Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }

        public int Cost
        {
            get { return _cost; }
            set { _cost = value; }
        }

        public string ActionText
        {
            get { return _actionText; }
            set { _actionText = value; }
        }

        public Stats Stats
        {
            get { return _stats; }
            set { _stats = value; }
        }

        public CardBallCondition BallCondition
        {
            get { return _ballCondition; }
            set { _ballCondition = value; }
        }

        public Target Target
        {
            get { return _target; }
            set { _target = value; }
        }

        public CardTargetInput TargetInput
        {
            get { return _targetInput; }
            set { _targetInput = value; }
        }

		public bool HasTargetInput
		{
            get { return _targetInput != null && _targetInput != CardTargetInput.NONE; }
		}

        public List<Position> TargetPositions
        {
            get { return _targetPositions; }
            set { _targetPositions = value; }
        }

        public string UniqueClass
        {
            get { return _uniqueClass; }
            set { _uniqueClass = value; }
        }

        public UniqueClassType UniqueClassType
        {
            get { return _uniqueClassType; }
            set { _uniqueClassType = value; }
        }

        public bool Special
        {
            get { return _special; }
            set { _special = value; }
        }

        public bool Flash
        {
            get { return _flash; }
            set{ _flash = value; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        public override void Copy(Card card)
		{
			base.Copy(card);
			// Cast the card
			ActionCard action = card as ActionCard;
			// Copy properties
			_role				= action.Role;
			_duration			= action.Duration;
			_cost				= action.Cost;
			_actionText			= action.ActionText;
			_stats				= action.Stats.Clone();
			_target				= action.Target;            // LT_TODO: implement Clone()
			_targetPositions	= action.TargetPositions;   // array di enumeratori posso copiare la reference
			_ballCondition		= action.BallCondition;     // LT_TODO: implement Clone()
			_targetInput		= action.TargetInput;       // LT_TODO: implement Clone()
			_uniqueClass		= action.UniqueClass;
			_uniqueClassType	= action.UniqueClassType;   
			_special			= action.Special;
            _flash			    = action.Flash;
		}
		
		public override Card Clone()
		{
			ActionCard clone = new ActionCard(_kind);
			clone.Copy(this);
			return clone;
		}
		
		public override void Clear()
		{
			// Ancestor
			base.Clear();
			// Clears data
			_role		        = DEFAULT_ROLE;
			_duration	        = DEFAULT_DURATION;
			_cost		        = DEFAULT_COST;
			_actionText	        = DEFAULT_ACTION_TEXT;
			_ballCondition      = DEFAULT_BALL_CONDITION;
			_uniqueClass        = DEFAULT_UNIQUE_CLASS;
			_uniqueClassType    = DEFAULT_UNIQUE_CLASS_TYPE;
			_special		    = DEFAULT_SPECIAL;
            _flash		        = DEFAULT_FLASH;
			// Clears objects
			_stats.Clear();
			_targetPositions.Clear();
			_targetPositions.Add(Position.ANY);
		}

        public override string ToString()
        {
            return string.Format("{0} (name: {1}, id: {2}, intanceid: {3}, role: {4}, school: {5})", 
                                 GetType().FullName,
                                 Name,
                                 Id,
                                 InstanceId,
                                 Role.Name,
                                 SchoolName);
        }

        #endregion PUBLIC METHODS

    }
}
