﻿using sos.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.cards
{
    /// <summary>
    /// 
    /// </summary>
    public class CardDefinitions
    {
        
		#region CONSTANTS

		#endregion CONSTANTS
        
		#region FIELDS

        private List<Card> _list = new List<Card>();

        #endregion FIELDS

        #region CONSTRUCTORS

        public CardDefinitions(Card[] cards)
        {
            Add(cards);
        }

		#endregion CONSTRUCTORS

		#region PROPERTIES

        public Card this[int index]
        {
            get { return _list[index]; }
        }

        public Card this[uint cardId]
        {
            get 
            { 
                int index = IndexOf(cardId);
                if (index == Defaults.INVALID_INDEX) throw new ArgumentException(string.Format("Card with id {0} not found", cardId));
                return _list[index];
            }
        }

        public int Count
        {
            get { return _list.Count; }
        }

		#endregion PROPERTIES

		#region PRIVATE METHODS

        /// <summary>
        /// Retrieve the ordinal index in the list of the card with the given CardId. Defaults.
        /// Returns INVALID_INDEX if the ID is not found
        /// </summary>
        /// <param name="id">Card.Id to find in the definitions list</param>
        /// <returns>Ordinal index in the list of the card with the given CardId. Returns Defaults.INVALID_INDEX if the ID is not found</returns>
		private int IndexOf(uint id)
		{
			for (var i=0; i<_list.Count; i++) if (_list[i].Id == id) return i;
			return Defaults.INVALID_INDEX;
		}

        /// <summary>
        /// Adds a card to the cards definition
        /// </summary>
        /// <param name="card"></param>
        private void Add(Card card)
        {
            if (Contains(card.Id)) throw new ArgumentException("Card with id {0} already contained into CardDefinition");
            _list.Add(card);
        }

        /// <summary>
        /// Adds all the given card to the card definitions
        /// </summary>
        /// <param name="cards"></param>
        private void Add(List<Card> cards)
        {
            cards.ForEach(c => Add(c));
        }

        /// <summary>
        /// Adds all the given card to the card definitions
        /// </summary>
        /// <param name="cards"></param>
        private void Add(Card[] cards)
        {
            Array.ForEach<Card>(cards, c => Add(c) );
        }

        /// <summary>
        /// Clears all the definitions
        /// </summary>
        private void Clear()
        {
            _list.Clear();
        }

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// Check if a card for the given Card.Id exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
		public bool Contains(uint id)
		{
			return IndexOf(id) != Defaults.INVALID_INDEX;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="school"></param>
        /// <param name="includeGeneric"></param>
        /// <returns></returns>
		public List<ActionCard> SearchActionCards(School school, bool includeGeneric)
		{
			List<ActionCard> actionCards = new List<ActionCard>();
			for (var i = 0; i < _list.Count; i++)
			{
				Card card = _list[i];
				if (card.Type == CardType.ACTION)
				{
					if (card.HasSchool(school) || (includeGeneric == true && card.IsGenericSchool))
					{
						actionCards.Add(card as ActionCard);
					}
				}
			}
			
			return actionCards;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public List<PlayerCard> SearchPlayerCards(Role role)
        {
            return SearchPlayerCards(null, role);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="school"></param>
        /// <returns></returns>
        public List<PlayerCard> SearchPlayerCards(School school)
        {
            return SearchPlayerCards(school, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="school"></param>
        /// <param name="role"></param>
        /// <returns></returns>
		public List<PlayerCard> SearchPlayerCards(School school, Role role)
		{
			List<PlayerCard> playerCards = new List<PlayerCard>();
			for (var i = 0; i < _list.Count; i++)
			{
				Card card = _list[i];
				if (card.Type == CardType.PLAYER)
				{
                    // Check the school
					if (school == null || card.HasSchool(school))
					{
                        PlayerCard playerCard = card as PlayerCard;
                        // Check the role
                        if (role == null || playerCard.Role == role)
                        {
                            playerCards.Add(playerCard);
                        }
					}
				}
			}
			return playerCards;
		}

        /// <summary>
        /// Refresh definitions clearing definition list and reloading it using the given list
        /// </summary>
        public void Refresh(Card[] cards)
        {
            Clear();
            Add(cards);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Card[] ToArray()
        {
            return _list.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} (card definitions count: {1})",
                                 GetType().FullName,
                                 Count);
        }

		#endregion PUBLIC METHODS

    }
}
