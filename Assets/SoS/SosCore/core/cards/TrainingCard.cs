﻿using System;

namespace sos.core.cards
{
    public class TrainingCard : Card
    {

        #region CONSTRUCTORS

        public TrainingCard(CardKind kind) : base(kind, CardType.TRAINING)
        {
        }

        #endregion CONSTRUCTORS

    }
}
