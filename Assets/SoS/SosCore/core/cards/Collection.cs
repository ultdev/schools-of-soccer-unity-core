﻿using System;
using System.Collections.Generic;
using sos.utils;

namespace sos.core.cards
{
    public class Collection
    {

        #region CONSTANTS

        #endregion CONSTANTS

        #region FIELDS

        // private uint _activeDeckId;
		private List<Card> _cards;
        private List<ActionCard> _actionCards;
        private List<PlayerCard> _playerCards;
        private List<TrainingCard> _trainingCards;
        private List<Deck> _decks;

        #endregion FIELDS

        #region CONSTRUCTORS

		public Collection()
		{
			// _activeDeckId = Defaults.INVALID_ID;
			_cards = new List<Card>();
			_actionCards = new List<ActionCard>();
			_playerCards = new List<PlayerCard>();
			_trainingCards = new List<TrainingCard>();
			_decks = new List<Deck>();
		}

        #endregion CONSTRUCTORS

        #region PROPERTIES

        /*
        public Deck ActiveDeck
        {
            get { return HasActiveDeck ? GetDeckById(_activeDeckId) : null; }
            set 
            { 
			    // Check if the Deck is valid
			    if (!value.IsActivable) throw new Exception("Deck couldn't be set as active deck");
			    // check if the deck is contained into the collection
			    if (!ContainsDeck(value)) throw new Exception("Deck not contained into the collection! Coudn't be set as active deck");
			    // Sets the active deck id
                _activeDeckId = value.Id; 
            }
        }
        */

        /*
		public bool HasActiveDeck
		{
			get { return _activeDeckId != Defaults.INVALID_ID; }
		}
        */

		public int DeckCount
		{
			get { return _decks.Count; }
		}

		public Deck[] Decks
		{
			get { return _decks.ToArray(); }
		}
		
		public Card[] Cards
		{
			get { return _cards.ToArray(); }
		}

		public int CardCount
		{
			get { return _cards.Count; }
		}

        public ActionCard[] ActionCards
        {
            get { return _actionCards.ToArray(); }
        }

		public int ActionCardsCount
		{
			get { return _actionCards.Count; }
		}

        public PlayerCard[] PlayerCards
        {
            get { return _playerCards.ToArray(); }
        }

		public int PlayerCardsCount
		{
			get { return _playerCards.Count; }
		}

        public TrainingCard[] TrainingCards
        {
            get { return _trainingCards.ToArray(); }
        }

		public int TrainingCardsCount
		{
			get { return _trainingCards.Count; }
		}

        #endregion PROPERTIES

        #region PRIVATE METHODS

		protected int GetCardIndexByInstanceId(uint instanceId)
		{
			for (int i=0; i<_cards.Count; i++)
			{
				Card card = _cards[i] as Card;
				if (card.InstanceId == instanceId) return i;
			}
			return Defaults.INVALID_INDEX;
		}

        protected int GetDeckIndexById(uint id)
		{
			for (int i=0; i<_decks.Count; i++)
			{
				Deck deck = _decks[i];
				if (deck.Id == id) return i;
			}
			return Defaults.INVALID_INDEX;
		}

		protected int GetDeckIndexByName(string name)
		{
			for (int i=0; i<_decks.Count; i++)
			{
				Deck deck = _decks[i];
				if (deck.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase)) return i;
			}
			return Defaults.INVALID_INDEX;
		}

		protected int GetActionCardIndexByInstanceId(uint instanceId)
		{
			for (int i=0; i<_actionCards.Count; i++)
			{
				Card card = _actionCards[i];
				if (card.InstanceId == instanceId) return i;
			}
			return Defaults.INVALID_INDEX;
		}

		protected int GetPlayerCardIndexByInstanceId(uint instanceId)
		{
			for (int i=0; i<_playerCards.Count; i++)
			{
				Card card = _playerCards[i];
				if (card.InstanceId == instanceId) return i;
			}
			return Defaults.INVALID_INDEX;
		}

		protected int GetTrainingCardIndexByInstanceId(uint instanceId)
		{
			for (int i=0; i<_trainingCards.Count; i++)
			{
				Card card = _trainingCards[i];
				if (card.InstanceId == instanceId) return i;
			}
			return Defaults.INVALID_INDEX;
		}

		protected void AddAllCards(List<Card> cards)
		{
			for (int i=0; i<cards.Count; i++)
			{
				// Retrieve card
				Card card = cards[i];
				// Add the card to the full list
				_cards.Add(card);
				// Add the card to the typed list and dispatch custom card type events
                if (card.Type == CardType.ACTION)       _actionCards.Add(card as ActionCard);
                if (card.Type == CardType.PLAYER)       _playerCards.Add(card as PlayerCard);
                if (card.Type == CardType.GOALKEEPER)   _playerCards.Add(card  as PlayerCard);
                if (card.Type == CardType.TRAINING)     _trainingCards.Add(card  as TrainingCard);
			}
		}

		protected bool EqualsCards(Collection collection)
		{
			if (CardCount != collection.CardCount) return false;
			for (int i=0; i < CardCount; i++)
			{
				Card c1 = GetCard(i);
				Card c2 = collection.GetCard(i);
				if (!c1.Equals(c2)) return false;
			}
			return true;
		}

		protected bool EqualsDecks(Collection collection)
		{
			if (DeckCount != collection.DeckCount) return false;
			for (int i=0; i < DeckCount; i++)
			{
				Deck d1 = GetDeck(i);
				Deck d2 = collection.GetDeck(i);
				if (!d1.Equals(d2)) return false;
			}
			return true;
		}

        /* TAV 2015-10-05 Data object do not dispatch events

		// Event dispatcher
		
		protected void DispatchActiveDeckChanged()
		{
			// LT_TODO > dispatchEvent(new Event(EVENT_ACTIVE_DECK_CHANGED));
            Logger.NotImplemented();
		}
		
		protected void DispatchDeckListChanged()
		{
			// LT_TODO > dispatchEvent(new Event(EVENT_DECK_LIST_CHANGED));
            Logger.NotImplemented();
		}
		
		protected void DispatchCardListChanged()
		{
			// LT_TODO > dispatchEvent(new Event(EVENT_CARD_LIST_CHANGED));
            Logger.NotImplemented();
		}
		
		protected void DispatchActionCardListChanged()
		{
			// LT_TODO > dispatchEvent(new Event(EVENT_ACTION_CARD_LIST_CHANGED));
            Logger.NotImplemented();
		}
		
		protected void DispatchPlayerCardListChanged()
		{
			// LT_TODO > dispatchEvent(new Event(EVENT_PLAYER_CARD_LIST_CHANGED));
            Logger.NotImplemented();
		}
		
		protected void DispatchTrainingCardListChanged()
		{
			// LT_TODO > dispatchEvent(new Event(EVENT_TRAINING_CARD_LIST_CHANGED));
            Logger.NotImplemented();
		}
        */


        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /* TAV 2015-12-05 Collection reference and controls removed
        /// <summary>
        /// Creates a new deck for the collection and adds it to the collection
        /// </summary>
        /// <param name="id">Deck unique id</param>
        /// <param name="name">Deck name</param>
        /// <returns>New deck instance, already added to the collection deck list</returns>
		public Deck CreateNewDeck(uint id, string name)
		{
			Deck deck = CreateEmptyDeck(id, name);
			AddDeck(deck);
			return deck;
		}
        */

        /* TAV 2015-12-05 Collection reference and controls removed
		/// <summary>
		/// Creates an empty deck without any data inside and not added to the owner Collection
		/// </summary>
		/// <param name="id">Deck unique id</param>
		/// <param name="name">Deck name</param>
		/// <returns>An empty Deck linked to the current Collection but empty and not added to the collection </returns>
		public Deck CreateEmptyDeck(uint id, string name)
		{
			return new Deck(this, id, name);
		}
        */

        /// <summary>
        /// Retrieves a Card using his index in the list 
        /// </summary>
        /// <param name="index">Index of the Card to retrieve</param>
        /// <returns>Card for the given index in the collection</returns>
		public Card GetCard(int index)
		{
			return _cards[index] as Card;
		}

        /// <summary>
        /// Retrieves a Card using instance unique id
        /// </summary>
        /// <param name="instanceId">Card instance unique id</param>
        /// <returns>Card for the given instance unique id</returns>
		public Card GetCardByInstanceId(uint instanceId)
		{
            int index = GetCardIndexByInstanceId(instanceId);
            if (index == Defaults.INVALID_INDEX) throw new ArgumentException(string.Format("Card with instance id {0} is not contained by the collection", instanceId));
			return _cards[index];
		}

        /// <summary>
        /// Returns all the instance of a card present into the collection  
        /// </summary>
        /// <param name="cardId">Card id to search</param>
        /// <returns>List of Card having the given cardId</returns>
		public List<Card> GetCardsByCardId(uint cardId)
		{
			List<Card> instances = new List<Card>();
			// Loop over the cards
			for (int i=0; i<_cards.Count; i++)
			{
				Card card = GetCard(i);
				if (card.Id == cardId)
				{
					instances.Add(card);
				}
			}
			return instances;
		}

        /// <summary>
        /// Counts the number of instance for the same card by the card id 
        /// </summary>
        /// <param name="cardId">Card id</param>
        /// <returns>Number of the number of instances for the same card</returns>
		public int GetCardInstancesCountByCardId(uint cardId)
		{
			int count = 0;
			for (int i=0; i<_cards.Count; i++)
			{
				Card c = GetCard(i);
				if (c.Id == cardId) count++;
			}
			return count;
		}

        /// <summary>
        /// Counts the number of instance for the same card 
        /// </summary>
        /// <param name="card">Card id</param>
        /// <returns>Number of the number of instances for the same card</returns>
		public int GetCardInstancesCount(Card card)
		{
			int count = 0;
			for (int i=0; i<_cards.Count; i++)
			{
				if (_cards[i].Id == card.Id) count++;
			}
			return count;
		}

		/// <summary>
		/// Check if the Collection contains a card by his instance unique id
		/// </summary>
		/// <param name="instanceId">Card instance unique id</param>
		/// <returns>True if the Collection contains a Card for the given instance unique id, </returns>
		public bool ContainsCardByInstanceId(uint instanceId)
		{
			return (GetCardIndexByInstanceId(instanceId) > Defaults.INVALID_INDEX);
		}
    
        /// <summary>
        /// Check if the Collection contains the given card, using his instance unique id
        /// </summary>
        /// <param name="card">Card to check</param>
        /// <returns>True if the Collection contains the given Card </returns>
		public bool ContainsCard(Card card)
		{
			return (GetCardIndexByInstanceId(card.InstanceId) > Defaults.INVALID_INDEX) ;
		}
        
        /// <summary>
        /// Retrieves an ActionCard using his index in the action card list
        /// </summary>
        /// <param name="index">Index of the ActionCard to retrieve</param>
        /// <returns>ActionCard for the given index in the action card list</returns>
		public ActionCard GetActionCard(int index)
		{
			return _actionCards[index];
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        public ActionCard GetActionCardByInstanceId(uint instanceId)
        {
            var card = GetCardByInstanceId(instanceId);
            if (card.Type != CardType.ACTION) throw new Exception(string.Format("Card with instance id {0} is not an ActionCard", instanceId));
            return card as ActionCard;
        }

        /// <summary>
        /// Retrieves an PlayerCard using his index in the player card list
        /// </summary>
        /// <param name="index">Index of the PlayerCard to retrieve</param>
        /// <returns>PlayerCard for the given index in the player card list</returns>
		public PlayerCard GetPlayerCard(int index)
		{
			return _playerCards[index];
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instanceId"></param>
        /// <returns></returns>
        public PlayerCard GetPlayerCardByInstanceId(uint instanceId)
        {
            var card = GetCardByInstanceId(instanceId);
            if (card.Type != CardType.PLAYER && card.Type != CardType.GOALKEEPER) throw new Exception(string.Format("Card with instance id {0} is not a PlayerCard", instanceId));
            return card as PlayerCard;
        }
		
        /// <summary>
        /// Retrieves an TrainingCard using his index in the training card list 
        /// </summary>
        /// <param name="index">Index of the TrainingCard to retrieve</param>
        /// <returns>TrainingCard for the given index in the training card list</returns>
		public TrainingCard GetTrainingCard(int index)
		{
			return _trainingCards[index];
		}

        /// <summary>
        /// Adds a card to the collection
        /// </summary>
        /// <param name="card">Card to add</param>
		public void AddCard(Card card)
		{
			// Card duplication check
			if (ContainsCard(card)) throw new ArgumentException(string.Format("Card '{0}' instance already contained into the collection!", card.Name));
			// Add the card to the full list
			_cards.Add(card);
            /* TAV 2015-10-05 Data object do not dispatch events
			// Dispatch CARD_LIST_CHANGED event
			DispatchCardListChanged();
            */
			// Add the card to the typed list and dispatch custom card type events
            if (card.Type == CardType.ACTION)
            {
                _actionCards.Add(card as ActionCard);
                /* TAV 2015-10-05 Data object do not dispatch events
                DispatchActionCardListChanged();
                */
            }
            if ( (card.Type == CardType.GOALKEEPER) || (card.Type == CardType.PLAYER) )
            {
                _playerCards.Add(card  as PlayerCard);
                /* TAV 2015-10-05 Data object do not dispatch events
                DispatchPlayerCardListChanged();
                */
            }
            if (card.Type == CardType.TRAINING)
            {
                _trainingCards.Add(card  as TrainingCard);
                /* TAV 2015-10-05 Data object do not dispatch events
                DispatchTrainingCardListChanged();	
                */
            }
		}
 
        /// <summary>
        /// Remove a card from the collection 
        /// </summary>
        /// <param name="card">Card to remove</param>
		public void RemoveCard(Card card)
		{
			// Card duplication check
			if (!ContainsCard(card)) throw new ArgumentException(string.Format("Card '{0}' instance not contained into the collection!", card.Name));
			// Remove the card
			_cards.RemoveAt(GetCardIndexByInstanceId(card.InstanceId));
            /* TAV 2015-10-05 Data object do not dispatch events
			// Dispatch CARD_LIST_CHANGED event
			DispatchCardListChanged();
            */
			// Dispatch custom card type events
            if (card.Type == CardType.ACTION)
            {
                _actionCards.RemoveAt(GetActionCardIndexByInstanceId(card.InstanceId));
                /* TAV 2015-10-05 Data object do not dispatch events
                DispatchActionCardListChanged();
                */
            }
            if ( (card.Type == CardType.GOALKEEPER) || (card.Type == CardType.PLAYER) )
            {
                _playerCards.RemoveAt(GetPlayerCardIndexByInstanceId(card.InstanceId));
                /* TAV 2015-10-05 Data object do not dispatch events
                DispatchPlayerCardListChanged();
                */
            }
            if (card.Type == CardType.TRAINING)
            {
                _trainingCards.RemoveAt(GetTrainingCardIndexByInstanceId(card.InstanceId));
                /* TAV 2015-10-05 Data object do not dispatch events
                DispatchTrainingCardListChanged();	
                */
            }
		}

		/// <summary>
		/// Removes all the cards from the collection
		/// </summary>
		public void ClearCards()
		{
			_cards.Clear();
			_actionCards.Clear();
			_playerCards.Clear();
			_trainingCards.Clear();
            /* TAV 2015-10-05 Data object do not dispatch events
			// Dispatch CARD_LIST_CHANGED event
			DispatchCardListChanged();
			// Dispatch custom typed card list event
			DispatchActionCardListChanged();
			DispatchPlayerCardListChanged();
			DispatchTrainingCardListChanged();
            */
		}

        /// <summary>
        /// Verify if a Deck is already contained into the collection using is unique id
        /// </summary>
        /// <param name="id">Deck unique id to verify</param>
        /// <returns>True if the deck is already contained</returns>
		public bool ContainsDeckById(uint id)
		{
			return GetDeckIndexById(id) != Defaults.INVALID_INDEX;
		}

        /// <summary>
        /// Verify if a Deck is already contained into the collection using is name
        /// </summary>
        /// <param name="name">Deck name to verify</param>
        /// <returns>True if the deck is already contained</returns>
		public bool ContainsDeckByName(string name)
		{
			return GetDeckIndexByName(name) != Defaults.INVALID_INDEX;
		}

        /// <summary>
        /// Verify if a Deck is already contained into the collection
        /// </summary>
        /// <param name="deck">Deck to verify</param>
        /// <returns>True if the deck is already contained</returns>
		public bool ContainsDeck(Deck deck)
		{
			return GetDeckIndexById(deck.Id) != Defaults.INVALID_INDEX;
		}

		/// <summary>
		/// Retrieves a Deck using his index in the list
		/// </summary>
		/// <param name="index">Index of the Card to retrieve</param>
		/// <returns>Deck for the given index in the collection</returns>
		public Deck GetDeck(int index)
		{
			return _decks[index] as Deck;
		}

        /// <summary>
        /// Retrieves a Card using instance unique id
        /// </summary>
        /// <param name="id">Card instance unique id</param>
        /// <returns>Card for the given instance unique id</returns>
		public Deck GetDeckById(uint id)
		{
			Deck deck = null;
			int index = GetDeckIndexById(id);
			if (index != Defaults.INVALID_INDEX)
			{
				deck = _decks[index];
			}
			return deck;
		}

		/// <summary>
		/// Adds a deck to the collection
		/// </summary>
		/// <param name="deck">Deck to add</param>
		public void AddDeck(Deck deck)
		{
			if (deck.Id != Defaults.INVALID_ID && ContainsDeckById(deck.Id)) throw new ArgumentException("Deck with the same id already contained into the collection!");
			if (ContainsDeckByName(deck.Name)) throw new ArgumentException("Deck with the same name already contained into the collection!");
			_decks.Add(deck);
            /* TAV 2015-10-05 Data object do not dispatch events
			// Dispatch EVENT_DECK_LIST_CHANGED event
			DispatchDeckListChanged();
            */
		}

		/// <summary>
		/// Removes a deck from the collection 
		/// </summary>
		/// <param name="deck">Deck to remove</param>
		public void RemoveDeck(Deck deck)
		{
			int index = GetDeckIndexById(deck.Id);
			if (index > Defaults.INVALID_INDEX)
			{
				// Last deck check
				if (_decks.Count == 1) throw new Exception("The last deck cannot be removed from the Collection");
                /*
				// Check active deck
				if (deck.Id == _activeDeckId) throw new Exception("The active deck cannot be removed from the Collection");
                */
                // Remove the deck
				_decks.RemoveAt(index);
                /* TAV 2015-10-05 Data object do not dispatch events
				// Dispatch EVENT_DECK_LIST_CHANGED event
				DispatchDeckListChanged();
                */
			}
		}

		/// <summary>
		/// Clear all the collectionc decks 
		/// </summary>
		public void ClearDecks()
		{
			_decks.Clear();
            /* TAV 2015-10-05 Data object do not dispatch events
			// Dispatch EVENT_DECK_LIST_CHANGED event
			DispatchDeckListChanged();
            */
		}

		/// <summary>
		/// Checks if the collection is equals to the given
		/// </summary>
		/// <param name="collection">Collection to check the equality with</param>
		/// <returns>True if the collection is equal to the given one</returns>
		public bool Equals(Collection collection)
		{
			return	// _activeDeckId == collection.ActiveDeck.Id &&
				    EqualsCards(collection) &&
				    EqualsDecks(collection);
		}

		/// <summary>
		/// Copies all cards and decks from a collection
		/// </summary>
		/// <param name="collection">Collection to copy</param>
		public void Copy(Collection collection)
		{
			// Disables event dispatch
			// LT_TODO > REMOVED dispatchEnabled = false;
			// Temporaty array
			List<Card> cards = new List<Card>();
			List<Deck> decks = new List<Deck>();
			// Clone each card
			for (int i=0; i < collection.CardCount; i++)
			{
				cards.Add( collection.GetCard(i).Clone() );
			}
			// Clone each deck
			for (int j=0; j < collection.DeckCount; j++)
			{
                /* TAV 2015-12-05 Collection reference and controls removed
				// Original deck
				Deck deck = collection.GetDeck(j);
				// Creates a new deck from the original
				Deck deck_copy = CreateEmptyDeck(deck.Id, deck.Name);
				// Duplicates the deck using current collection cards but reading the source deck composition
				deck_copy.Copy(deck);
				// Add deck to temporary list
				decks.Add(deck_copy);
                */
				// Creates a new deck from the original
				Deck deck_clone = collection.GetDeck(j).Clone(); 
				// Add deck to temporary list
				decks.Add(deck_clone);
			}
			// Clears and assign the complete cards list
			ClearCards();
			AddAllCards(cards);
            // Clears and assign the complete deck list
            _decks.Clear();
            _decks.AddRange(decks);
			// Copy active deck
			// _activeDeckId = collection.ActiveDeck.Id;
            /* TAV 2015-10-05 Data object do not dispatch events
			// Disables event dispatch
			// LT_TODO > REMOVED dispatchEnabled = true;
			// Dispatch events
			DispatchDeckListChanged();
			DispatchCardListChanged();
			DispatchActionCardListChanged();
			DispatchPlayerCardListChanged();
			DispatchTrainingCardListChanged();
			DispatchActiveDeckChanged();
            */
		}

        /// <summary>
        /// Creates a clone of the collection with all the cards and decks
        /// </summary>
        /// <returns>A new Collection with all the cards and decks</returns>
		public Collection Clone()
		{
			Collection collection = new Collection();
			collection.Copy(this);
			return collection;
		}

        /// <summary>
        /// Clear all the collectionc cards and decks 
        /// </summary>
		public void Clear()
		{
            // _activeDeckId = Defaults.INVALID_ID;
			ClearCards();
			ClearDecks();
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} (card count: {1}, deck count: {2})",
                                 GetType().FullName,
                                 CardCount,
                                 DeckCount);
        }

        #endregion PUBLIC METHODS

    }
}
