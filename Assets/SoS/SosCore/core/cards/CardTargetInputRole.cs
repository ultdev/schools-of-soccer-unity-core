﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.cards
{
    public class CardTargetInputRole : CardTargetInput
    {

        #region FIELDS

        private CardTargetInputRoleKind _kind;

        #endregion FIELDS

        #region CONSTRUCTORS

        public CardTargetInputRole() : base(TargetType.ROLE)
        {
            Clear();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public CardTargetInputRoleKind Kind
        {
            get { return _kind; }
            set { _kind = value; }
        }

        #endregion PROPERTIES

    }
}
