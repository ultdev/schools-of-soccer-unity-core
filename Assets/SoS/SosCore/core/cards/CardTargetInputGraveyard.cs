﻿using System;

namespace sos.core.cards
{
    public class CardTargetInputGraveyard : CardTargetInput
    {

        #region FIELDS

        private CardTargetInputGraveyardKind _kind;

        #endregion FIELDS

        #region CONSTRUCTORS

        public CardTargetInputGraveyard() : base(TargetType.GRAVEYARD)
        {
            Clear();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public CardTargetInputGraveyardKind Kind
        {
            get { return _kind; }
            set { _kind = value; }
        }

        #endregion PROPERTIES

    }
}
