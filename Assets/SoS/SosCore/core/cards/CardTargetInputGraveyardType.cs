﻿using sos.data;

namespace sos.core.cards
{
    /// <summary>
    /// Represents the target possibilities for a card that have to select another card into the graveyard
    /// </summary>
    public class CardTargetInputGraveyardKind : Enumerator
    {

        #region CONSTANTS

		//public static readonly CardTargetInputGraveyardType NONE				    = new CardTargetInputGraveyardType(0, "NONE",        "none");
		//public static readonly CardTargetInputGraveyardType GOALKEEPER			= new CardTargetInputGraveyardType(1, "GOALKEEPER",  "gk");
		//public static readonly CardTargetInputGraveyardType DEFENSE				= new CardTargetInputGraveyardType(2, "DEFENCE",     "df");
		//public static readonly CardTargetInputGraveyardType MIDFIELD			    = new CardTargetInputGraveyardType(3, "MIDFIELD",    "mf");
		//public static readonly CardTargetInputGraveyardType FORWARD				= new CardTargetInputGraveyardType(4, "FORWARD",     "fw");
		//public static readonly CardTargetInputGraveyardType REACH				    = new CardTargetInputGraveyardType(5, "REACH",       "reach");
		public static readonly CardTargetInputGraveyardKind ANY					    = new CardTargetInputGraveyardKind(6, "ANY",         "any");

        #endregion CONSTANTS

        #region CONSTRUCTORS
        
        protected CardTargetInputGraveyardKind(int id, string name, string sign) : base(id, name, sign) {}

        protected CardTargetInputGraveyardKind(int id, string name, string sign, bool register) : base(id, name, sign, register) {}

        #endregion CONSTRUCTORS

        #region STATIC METHODS

        public static string[] Names()
        {
            return Enumerators.Names(typeof(CardTargetInputGraveyardKind));
        }

        public static CardTargetInputGraveyardKind[] Values()
        {
            return Enumerators.Values<CardTargetInputGraveyardKind>(typeof(CardTargetInputGraveyardKind));
        }

        public static CardTargetInputGraveyardKind GetById(int id)
        {
            return Enumerators.GetById<CardTargetInputGraveyardKind>(typeof(CardTargetInputGraveyardKind), id);
        }

        public static CardTargetInputGraveyardKind GetBySign(string sign)
        {
            return Enumerators.GetBySign<CardTargetInputGraveyardKind>(typeof(CardTargetInputGraveyardKind), sign);
        }

        #endregion STATIC METHODS

    }
}
