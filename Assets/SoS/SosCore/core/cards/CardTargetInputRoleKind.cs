﻿using sos.data;

namespace sos.core.cards
{
    /// <summary>
    /// Represents the target possibilities for a card that have to select another card to be played
    /// </summary>
    public class CardTargetInputRoleKind : Enumerator
    {

        #region CONSTANTS

		public static readonly CardTargetInputRoleKind NONE				        = new CardTargetInputRoleKind(0, "NONE",        "none");
		public static readonly CardTargetInputRoleKind GOALKEEPER			    = new CardTargetInputRoleKind(1, "GOALKEEPER",  "gk");
		public static readonly CardTargetInputRoleKind DEFENSE				    = new CardTargetInputRoleKind(2, "DEFENCE",     "df");
		public static readonly CardTargetInputRoleKind MIDFIELD			        = new CardTargetInputRoleKind(3, "MIDFIELD",    "mf");
		public static readonly CardTargetInputRoleKind FORWARD				    = new CardTargetInputRoleKind(4, "FORWARD",     "fw");
		public static readonly CardTargetInputRoleKind REACH				    = new CardTargetInputRoleKind(5, "REACH",       "reach");
		public static readonly CardTargetInputRoleKind ANY					    = new CardTargetInputRoleKind(6, "ANY",         "any");

        #endregion CONSTANTS

        #region CONSTRUCTORS
        
        protected CardTargetInputRoleKind(int id, string name, string sign) : base(id, name, sign) {}

        protected CardTargetInputRoleKind(int id, string name, string sign, bool register) : base(id, name, sign, register) {}

        #endregion CONSTRUCTORS

        #region STATIC METHODS

        public static string[] Names()
        {
            return Enumerators.Names(typeof(CardTargetInputRoleKind));
        }

        public static CardTargetInputRoleKind[] Values()
        {
            return Enumerators.Values<CardTargetInputRoleKind>(typeof(CardTargetInputRoleKind));
        }

        public static CardTargetInputRoleKind GetById(int id)
        {
            return Enumerators.GetById<CardTargetInputRoleKind>(typeof(CardTargetInputRoleKind), id);
        }

        public static CardTargetInputRoleKind GetBySign(string sign)
        {
            return Enumerators.GetBySign<CardTargetInputRoleKind>(typeof(CardTargetInputRoleKind), sign);
        }

        #endregion STATIC METHODS

    }
}
