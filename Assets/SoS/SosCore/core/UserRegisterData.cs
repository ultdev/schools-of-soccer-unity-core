﻿namespace sos.core
{
    /// <summary>
    /// Data needed to perform the creation of a new SoS user account
    /// </summary>
    public class UserRegisterData
    {

		#region FIELDS

        private string _username;
        private string _password;
        private string _email;
        private string _secretCode;

        #endregion FIELDS

        #region PROPERTIES

        public string Username
        {
            get
            {
                return _username;
            }

            set
            {
                _username = value;
            }
        }

        public string Password
        {
            get
            {
                return _password;
            }

            set
            {
                _password = value;
            }
        }

        public string Email
        {
            get
            {
                return _email;
            }

            set
            {
                _email = value;
            }
        }

        public string SecretCode
        {
            get
            {
                return _secretCode;
            }

            set
            {
                _secretCode = value;
            }
        }

        #endregion PROPERTIES

    }
}
