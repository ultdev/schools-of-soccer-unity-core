﻿using System;
using sos.data;

namespace sos.core
{
    public class School : Enumerator
    {

        #region CONSTANTS

		public static readonly School   GENERIC			            = new School(1,     "Generic",		0xFFFFFF, "Generic",    true);
		public static readonly School   DEFENCE			            = new School(2,     "Defense",		0xD0F5FF, "Kruger",     true);
		public static readonly School   PLAYMAKING		            = new School(3,     "Playmaking",	0x88D788, "Seigyo",     true);
		public static readonly School   PUNK			            = new School(4,     "Punk",			0xFFC6AA, "O'Connor",   true);
		public static readonly School   ATTACK			            = new School(5,     "Attack",		0xC6AAFF, "Ubinna",     true);
		
		public static readonly School   ALL				            = new School(0,     "All",			0xFFFFFF, "All",        false);
		public static readonly School   MULTISCHOOL		            = new School(99,    "Multi School",	0xF5D524, "Multi",      false);

        #endregion CONSTANTS

        #region FIELDS

        private uint _color;
        private string _lineage; // discendenza, famiglia, nome reale della scuola

        #endregion FIELDS

        #region CONSTRUCTORS

        protected School(int id, string name, uint color, string lineage) : this(id, name, color, lineage, true)  {}

        protected School(int id, string name, uint color, string lineage, bool register) : base(id, name, null, register) 
        {
            _color = color;
            _lineage = lineage;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        /// <summary>
        /// Color associated to the school
        /// </summary>
        public uint Color
        {
            get { return _color; }
        }

        /// <summary>
        /// School Lineage\Family name
        /// </summary>
        public string Lineage
        {
            get { return _lineage; }
        }

        #endregion PROPERTIES

        #region STATIC METHODS

        public static string[] Names()
        {
            return Enumerators.Names(typeof(School));
        }

        public static School[] Values()
        {
            return Enumerators.Values<School>(typeof(School));
        }

        public static School GetById(int id)
        {
            return Enumerators.GetById<School>(typeof(School), id);
        }

        public static School GetByName(string name)
        {
            return Enumerators.GetByName<School>(typeof(School), name);
        }

        public static bool IsPlayableSchool(School school)
        {
            return Array.Find<School>(Values(), s => s.Id == school.Id) != null;
        }

        #endregion STATIC METHODS

    }
}
