﻿using sos.utils;

namespace sos.core.shop
{
    /// <summary>
    /// 
    /// </summary>
    public class ShopItem
    {

        #region FIELDS

        private uint _id;
		private string _name;
		private string _description;
		private uint _cost;
		private uint _owned;
		private string _image;

        #endregion FIELD

        #region CONSTRUCTORS

        public ShopItem()
        {
            Clear();
        }

		#endregion CONSTRUCTORS

		#region PROPERTIES

        public uint Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;
            }
        }

        public string Description
        {
            get
            {
                return _description;
            }

            set
            {
                _description = value;
            }
        }

        public uint Cost
        {
            get
            {
                return _cost;
            }

            set
            {
                _cost = value;
            }
        }

        public uint Owned
        {
            get
            {
                return _owned;
            }

            set
            {
                _owned = value;
            }
        }

        public string Image
        {
            get
            {
                return _image;
            }

            set
            {
                _image = value;
            }
        }

		#endregion PROPERTIES

		#region PRIVATE METHODS

		#endregion PRIVATE METHODS

		#region PUBLIC METHODS

		public bool Equals(ShopItem shopItem)
		{
			return	_id             == shopItem.Id &&
		            _name           == shopItem.Name &&
		            _description    == shopItem.Description &&
		            _cost           == shopItem.Cost &&
		            _owned          == shopItem.Owned &&
		            _image          == shopItem.Image;
		}

        public void Copy(ShopItem shopItem)
		{
            _id             = shopItem.Id;
		    _name           = shopItem.Name;
		    _description    = shopItem.Description;
		    _cost           = shopItem.Cost;
		    _owned          = shopItem.Owned;
		    _image          = shopItem.Image;
		}

        public ShopItem Clone()
		{
			ShopItem shopitem = new ShopItem();
			shopitem.Copy(this);
			return shopitem;
		}
		
		public void Clear()
		{
            _id             = Defaults.INVALID_ID;
		    _name           = string.Empty;
		    _description    = string.Empty;
		    _cost           = 0;
		    _owned          = 0;
		    _image          = string.Empty;
		}

        public override string ToString()
        {
            return string.Format("{0} {1} - {2} (id: {3})", GetType().FullName, _name, _description,_id);
        }

		#endregion PUBLIC METHODS


    }
}
