﻿using System.Collections.Generic;

namespace sos.core.shop
{

    /// <summary>
    /// 
    /// </summary>
    public class Shop
    {

        #region CONSTANTS

        #endregion CONSTANTS

        #region FIELDS

        private List<ShopItem> _items = new List<ShopItem>();

        #endregion FIELDS

        #region CONSTRUCTORS

        public Shop()
        {
            Clear();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

		public void AddItem(ShopItem item)
		{
			_items.Add(item);
		}
		
		public uint AddToItemCount(uint itemId, uint count)
		{
			GetItemById(itemId).Owned += count;
			return GetItemById(itemId).Owned;
		}
		
		public ShopItem GetItemByIndex(int index)
		{
            return (_items.Count > 0 && index >= 0 && _items.Count > index) ? _items[index] : null;
		}
		
		public ShopItem GetItemById(uint itemId)
		{
			for (var i = 0; i < _items.Count; i++)
			{
				if (_items[i].Id == itemId)
					return _items[i];
			}
			return null;
		}
		
		public bool HasItem(uint itemId)
		{
			for (var i = 0; i < _items.Count; i++)
			{
				if (_items[i].Id == itemId)
					return true;
			}
			return false;
		}

        public void Clear()
        {
            _items.Clear();
        }

		#endregion PUBLIC METHODS

    }


}