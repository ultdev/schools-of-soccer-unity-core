﻿using System;
using sos.data;
using System.Collections.Generic;

namespace sos.core
{

    public class Position : Enumerator
    {

        #region CONSTANTS

		public static readonly Position GOALKEEPER			        = new Position(1, 	"Goalkeeper",       "gk");
		public static readonly Position DEFENDER_01		            = new Position(2, 	"Defender Top",     "d1");
		public static readonly Position DEFENDER_02		            = new Position(3, 	"Defender Bottom",  "d2");
		public static readonly Position MIDFIELD			        = new Position(4, 	"Midfield",         "mf");
		public static readonly Position FORWARD_01			        = new Position(5, 	"Forward Top",      "f1");
		public static readonly Position FORWARD_02			        = new Position(6, 	"Forward Bottom",   "f2");
		public static readonly Position SUB_DEFENCE		            = new Position(9, 	"Subst. Defence",   "sub_df");
		public static readonly Position SUB_MIDFIELD		        = new Position(10,	"Subst. Midfield",  "sub_mf");
		public static readonly Position SUB_FORWARD		            = new Position(11, 	"Subst. Forward",   "sub_fw");

        public static readonly Position ANY				            = new Position(99, 	"Any Position",     "any", false);  // not registered into enum metadata

        #endregion CONSTANTS

        #region FIELDS

        #endregion FIELDS

        #region CONSTRUCTORS

        public Position(int id, string name, string sign) : this(id, name, sign, true)
        {
        }

        public Position(int id, string name, string sign, bool register) : base(id, name, sign, register)
        {
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

		public bool IsSubstitution
		{
            get
            {
                return _id == SUB_DEFENCE.Id ||
                        _id == SUB_MIDFIELD.Id ||
                        _id == SUB_FORWARD.Id;
            }

		}

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        #endregion PUBLIC METHODS

        #region STATIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string[] Names()
        {
            return Enumerators.Names(typeof(Position));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Position[] Values()
        {
            return Enumerators.Values<Position>(typeof(Position));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Position GetById(int id)
        {
            return Enumerators.GetById<Position>(typeof(Position), id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Position GetByName(string name)
        {
            return Enumerators.GetByName<Position>(typeof(Position), name);
        }

        /// <summary>
        /// Search a Position by given PositionEnum
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Position GetBySign(string sign)
        {
            return Enumerators.GetBySign<Position>(typeof(Position), sign);
        }
        
        /// <summary>
        /// Returns the opposed position of the given one
        /// </summary>
        /// <param name="position">Position to retrieve the opposite</param>
        /// <returns>Opposite Position</returns>
		public static Position GetOpposite(Position position)
		{
                 if (DEFENDER_01    == position)    return FORWARD_01;
            else if (DEFENDER_02    == position)    return FORWARD_02;
            else if (MIDFIELD       == position)    return MIDFIELD;
            else if (FORWARD_01     == position)    return DEFENDER_01;
            else if (FORWARD_02     == position)    return DEFENDER_02;
            else throw new ArgumentException("Position " + position.Sign +  " has not an opposite position");
		}

        /// <summary>
        /// Returns the default target Position for a given Position (for an Action)
        /// </summary>
        /// <param name="position">position to retrieve the default target</param>
        /// <returns>Default target Position for the given Position</returns>
		public static Position GetDefaultTarget(Position position)
		{
                 if (DEFENDER_01    == position)    return MIDFIELD;
            else if (DEFENDER_02    == position)    return MIDFIELD;
            else if (MIDFIELD       == position)    return FORWARD_01;
            else if (FORWARD_01     == position)    return MIDFIELD;
            else if (FORWARD_02     == position)    return MIDFIELD;
            else throw new ArgumentException("Position " + position.Sign +  " has not a default target");
		}

        /// <summary>
        /// Retrieves the list of the nearby Positions for the given Position
        /// </summary>
        /// <param name="position">Position to retrieve the nearby available Positions</param>
        /// <returns>List of the nerby Positions</returns>
		public static Position[] GetNearbyPositions(Position position)
		{
			List<Position>positions = new List<Position>();

            if (DEFENDER_01 == position)
            {
				positions.Add(Position.DEFENDER_02);
				positions.Add(Position.MIDFIELD);
				positions.Add(Position.FORWARD_01);
            }
            else if (DEFENDER_02 == position)
            {
				positions.Add(Position.DEFENDER_02);
				positions.Add(Position.MIDFIELD);
				positions.Add(Position.FORWARD_01);
            }
            else if (MIDFIELD == position)
            {
				positions.Add(Position.DEFENDER_01);
				positions.Add(Position.DEFENDER_02);
				positions.Add(Position.FORWARD_01);
				positions.Add(Position.FORWARD_02);
            }
            else if (FORWARD_01 == position)
            {
				positions.Add(Position.DEFENDER_01);
				positions.Add(Position.MIDFIELD);
				positions.Add(Position.FORWARD_02);
            }
            else if (FORWARD_02 == position)
            {
				positions.Add(Position.DEFENDER_02);
				positions.Add(Position.MIDFIELD);
				positions.Add(Position.FORWARD_01);
            }
            else throw new ArgumentException("Position " + position +  "have not an reachable position");

			return positions.ToArray();
		}

        #endregion STATIC METHODS

    }
}
