﻿using System;
using sos.utils;

namespace sos.core
{
    /// <summary>
    /// Base player class
    /// </summary>
    public class Player
    {

        #region CONSTANTS

        #endregion CONSTANTS

        #region FIELDS

        protected uint _id;
        protected string _firstName;
        protected string _lastName;
        protected string _username;
        protected string _email;
        protected DateTime _registration;

        #endregion FIELDS

        #region CONSTRUCTORS

        public Player()
        {
            Clear();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public uint Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public DateTime Registration
        {
            get { return _registration; }
            set { _registration = value; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
		public virtual bool Equals(Player player)
		{
			return	_id				== player.Id 		&&
					_firstName		== player.FirstName	&& 
					_lastName		== player.LastName	&&
					_username		== player.Username	&&
					_email			== player.Email		&&
					_registration	== player.Registration;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="player"></param>
        public virtual void Copy(Player player)
		{
			_id				= player.Id;
			_firstName		= player.FirstName; 
			_lastName		= player.LastName;
			_username		= player.Username;
			_email			= player.Email;
			_registration	= player.Registration;			
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual Player Clone()
		{
			Player player = new Player();
			player.Copy(this);
			return player;
		}
		
        /// <summary>
        /// 
        /// </summary>
		public virtual void Clear()
		{
			_id 			= Defaults.INVALID_ID;
			_firstName		= "";
			_lastName		= "";
			_username		= "";
			_email			= "";
			_registration	= default(DateTime);
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} {1} (id: {2})", GetType().FullName, _username, _id);
        }

        #endregion PUBLIC METHODS

    }
}
