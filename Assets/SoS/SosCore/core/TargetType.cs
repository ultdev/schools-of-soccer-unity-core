﻿using System;
using sos.data;

namespace sos.core
{
    /// <summary>
    /// 
    /// </summary>
    public class TargetType : Enumerator
    {

        #region CONSTANTS

        public static readonly TargetType   NONE					    = new TargetType(0, "NONE",			"none");	    // no TargetType 
		public static readonly TargetType   ROLE				        = new TargetType(1, "ROLE",		    "target");      // can TargetType only defined role
		public static readonly TargetType   EFFECT				        = new TargetType(2, "EFFECT",		"effect");      // can TargetType only defined effects
		public static readonly TargetType   GRAVEYARD			        = new TargetType(3, "GRAVEYARD",    "card");	    // can TargetType only graveyard cards

        #endregion CONSTANTS

        #region CONSTRUCTORS

        protected TargetType(int id, string name, string sign) : base(id, name, sign, true)  {}

        #endregion CONSTRUCTORS

        #region STATIC METHODS

        public static string[] Names()
        {
            return Enumerators.Names(typeof(TargetType));
        }

        public static TargetType[] Values()
        {
            return Enumerators.Values<TargetType>(typeof(TargetType));
        }

        public static TargetType GetById(int id)
        {
            return Enumerators.GetById<TargetType>(typeof(TargetType), id);
        }

        public static TargetType GetByName(string name)
        {
            return Enumerators.GetByName<TargetType>(typeof(TargetType), name);
        }

        public static TargetType GetBySign(string sign)
        {
            return Enumerators.GetBySign<TargetType>(typeof(TargetType), sign);
        }

        #endregion STATIC METHODS

    }
}
