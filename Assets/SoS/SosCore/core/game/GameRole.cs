﻿using System;
using System.Collections.Generic;
using sos.data;

namespace sos.core.game
{

    /// <summary>
    /// Player field side during the game (Enumerator)
    /// </summary>
    public class GameRole : Enumerator
    {

        #region CONSTANTS
		public static readonly GameRole IDLE						= new GameRole(0, "Idle",    "idle");
		public static readonly GameRole ATTACK						= new GameRole(1, "Attack",  "att");
		public static readonly GameRole DEFENCE					    = new GameRole(2, "Defence", "def");

        #endregion CONSTANTS

        #region CONSTRUCTORS

        public GameRole(int id, string name, string sign) : this(id, name, sign, true)
        {
        }

        public GameRole(int id, string name, string sign, bool register) : base(id, name, sign, register)
        {
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        #endregion PUBLIC METHODS

        #region STATIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string[] Names()
        {
            return Enumerators.Names(typeof(GameRole));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static GameRole[] Values()
        {
            return Enumerators.Values<GameRole>(typeof(GameRole));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GameRole GetById(int id)
        {
            return Enumerators.GetById<GameRole>(typeof(GameRole), id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GameRole GetByName(string name)
        {
            return Enumerators.GetByName<GameRole>(typeof(GameRole), name);
        }

        #endregion STATIC METHODS

    }
}
