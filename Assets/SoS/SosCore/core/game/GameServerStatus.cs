﻿using System;
using System.Collections.Generic;
using sos.data;

namespace sos.core.game
{

    /// <summary>
    /// 
    /// </summary>
    public class GameServerStatus : Enumerator
    {

        #region CONSTANTS

		public static readonly GameServerStatus UNKNOWN				            = new GameServerStatus(0, 	"UNKNOWN",				"Game status unknown");									// 
		public static readonly GameServerStatus WAITING				            = new GameServerStatus(1, 	"WAITING",				"Server is waiting for player PLAY request");			// GamePhase.PLAY
		public static readonly GameServerStatus CALCULATING			            = new GameServerStatus(2, 	"CALCULATING",			"Server is waiting for opponent PLAY request");			// GamePhase.RESOLVE_PLAY (polling)
		public static readonly GameServerStatus COMPLETED				        = new GameServerStatus(3, 	"COMPLETED",			"Game completed if last turn, RESYNC otherwise");		// GamePhase.RESYNC or game end
		public static readonly GameServerStatus DISCARD				            = new GameServerStatus(4, 	"DISCARD",				"Server is waiting for player DISCARD request");		// GamePhase.DISCARD
		public static readonly GameServerStatus SUBSTITUTION			        = new GameServerStatus(5, 	"SUBSTITUTION",			"Server is waiting for player SUBSTITUTE request");		// GamePhase.SUBSTITUTE
		public static readonly GameServerStatus SOLVE_LOCK				        = new GameServerStatus(6, 	"SOLVE_LOCK",			"Server PLAY calc lock");								// GamePhase.RESOLVE_PLAY (polling)
		public static readonly GameServerStatus DISCARD_POLLING		            = new GameServerStatus(7, 	"DISCARD_POLLING",		"Server is waiting for opponent DISCARD request");		// GamePhase.RESOLVE_DISCARD (polling)
		public static readonly GameServerStatus DISCARD_LOCK			        = new GameServerStatus(8, 	"DISCARD_LOCK",			"Server DISCARD calc lock");							// GamePhase.RESOLVE_PLAY (polling)
		public static readonly GameServerStatus SUBSTITUTION_POLLING	        = new GameServerStatus(9, 	"SUBSTITUTION_POLLING",	"Server is waiting for opponent SUBSTITUTION request");	// GamePhase.RESOLVE_DISCARD (polling)
		public static readonly GameServerStatus SUBSTITUTION_LOCK		        = new GameServerStatus(10, 	"SUBSTITUTION_LOCK",	"Server SUBSTITUTION calc lock");						// GamePhase.RESOLVE_PLAY (polling)

        #endregion CONSTANTS

        #region CONSTRUCTORS

        public GameServerStatus(int id, string name, string description) : this(id, name, description, true)
        {
        }

        public GameServerStatus(int id, string name, string description, bool register) : base(id, name, string.Empty, register)
        {
            _description = description;
        }

        #endregion CONSTRUCTORS

        #region FIELDS

		private string _description;

        #endregion FIELDS

        #region PROPERTIES

        public string Description
        {
            get { return _description; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        #endregion PUBLIC METHODS

        #region STATIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string[] Names()
        {
            return Enumerators.Names(typeof(GameServerStatus));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static GameServerStatus[] Values()
        {
            return Enumerators.Values<GameServerStatus>(typeof(GameServerStatus));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GameServerStatus GetById(int id)
        {
            return Enumerators.GetById<GameServerStatus>(typeof(GameServerStatus), id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GameServerStatus GetByName(string name)
        {
            return Enumerators.GetByName<GameServerStatus>(typeof(GameServerStatus), name);
        }

        #endregion STATIC METHODS

    }
}
