﻿using System;
using System.Collections.Generic;

using sos.core.cards;
using sos.utils;

namespace sos.core.game
{
    public class GameDeck
    {

        #region CONSTANTS

        #endregion CONSTANTS

        #region FIELDS

		private List<GameActionCard>    _actionCards;
		private List<GamePlayerCard>    _playerCards;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameDeck()
        {
		    _actionCards = new List<GameActionCard>();
		    _playerCards = new List<GamePlayerCard>();            
        }

        public GameDeck(Deck deck) : this()
        {
            Load(deck);
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public List<GameActionCard> ActionCards
        {
            get { return _actionCards; }
        }

        public List<GamePlayerCard> PlayerCards
        {
            get { return _playerCards; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

		protected int GetActionCardIndex(uint instanceId)
		{
			for (int i = 0; i < _actionCards.Count; i++)
			{
				if (_actionCards[i].InstanceId == instanceId) return i;
			}
			return Defaults.INVALID_INDEX;
		}

		protected int GetPlayerCardIndex(uint instanceId)
		{
			for (int i = 0; i < _playerCards.Count; i++)
			{
				if (_actionCards[i].InstanceId == instanceId) return i;
			}
			return Defaults.INVALID_INDEX;
		}

		public bool EqualsActionCards(GameDeck gameDeck)
		{
			// Check cards number
			if (_actionCards.Count != gameDeck.ActionCards.Count) return false;
			// Check each card
			for (int i = 0; i < _actionCards.Count; i++) if (!_actionCards[i].Equals(gameDeck.ActionCards[i])) return false;
			// Equal
			return true;
		}

        public bool EqualsPlayerCards(GameDeck gameDeck)
        {
            // Check cards number
            if (_playerCards.Count != gameDeck.PlayerCards.Count) return false;
            // Check each card
            for (int i = 0; i < _playerCards.Count; i++) if (!_playerCards[i].Equals(gameDeck.PlayerCards[i])) return false;
            // Equal
            return true;
        }

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

		public bool ContainsActionCard(uint instanceId)
		{
			return GetActionCardIndex(instanceId) > Defaults.INVALID_INDEX;
		}
		
		public bool containsPlayerCard(uint instanceId)
		{
            return GetPlayerCardIndex(instanceId) > Defaults.INVALID_INDEX;
		}

		public GameActionCard FindActionCard(uint instanceId)
		{
			GameActionCard card = null;
			int index = GetActionCardIndex(instanceId);
			if (index > -1) card = _actionCards[index];
			return card;
		}
		
		public GamePlayerCard FindPlayerCard(uint instanceId)
		{
			GamePlayerCard card = null;
            int index = GetPlayerCardIndex(instanceId);
			if (index > -1) card = _playerCards[index];
			return card;
		}

		public void Load(Deck deck)
		{
			// Temporary lists
			List<GameActionCard> actions = new List<GameActionCard>();
			List<GamePlayerCard> players = new List<GamePlayerCard>();
			// Loop over deck action cards
			for (int i = 0; i < deck.ActionCardsCount; i++)
			{
				// Create a new GameActionCard and copy data from the original one
				GameActionCard gameActionCard = new GameActionCard();
				gameActionCard.Copy(deck.GetActionCard(i));
				// Add the card to the list
				actions.Add(gameActionCard);
			}
			// Clone all players from deck
			players.Add(new GamePlayerCard(deck.Goalkeeper, Position.GOALKEEPER));
			players.Add(new GamePlayerCard(deck.Defender01, Position.DEFENDER_01));
			players.Add(new GamePlayerCard(deck.Defender02, Position.DEFENDER_02));
			players.Add(new GamePlayerCard(deck.Midfield, Position.MIDFIELD));
			players.Add(new GamePlayerCard(deck.Forward01, Position.FORWARD_01));
			players.Add(new GamePlayerCard(deck.Forward02, Position.FORWARD_02));
			players.Add(new GamePlayerCard(deck.SubstitutionDefence, Position.SUB_DEFENCE));
			players.Add(new GamePlayerCard(deck.SubstitutionMidfield, Position.SUB_MIDFIELD));
			players.Add(new GamePlayerCard(deck.SubstitutionForward, Position.SUB_FORWARD));
			// Clear current cards
			Clear();
			// Popolate cards lists
			for (int x= 0; x < actions.Count; x++) _actionCards.Add(actions[x]);
			for (int y= 0; y < players.Count; y++) _playerCards.Add(players[y]);
		}

		public bool Equals(GameDeck gameDeck)
		{
			// Equal
			return	EqualsActionCards(gameDeck) &&
					EqualsPlayerCards(gameDeck);
		}

		public void Copy(GameDeck gameDeck)
		{
			// Temporary lists
			List<GameActionCard> actions = new List<GameActionCard>();
			List<GamePlayerCard> players = new List<GamePlayerCard>();
			// Loop over deck action cards
			for (int i= 0; i < gameDeck.ActionCards.Count; i++)
			{
				// Add the card to the list
				actions.Add(gameDeck.ActionCards[i].Clone() as GameActionCard);
			}
			// Loop over deck action cards
			for (int j= 0; j < gameDeck.PlayerCards.Count; j++)
			{
				// Add the card to the list
                players.Add(gameDeck.PlayerCards[j].Clone() as GamePlayerCard);
			}
			// Clear current cards			
			Clear();
			// Popolate cards lists
            for (int x = 0; x < actions.Count; x++) _actionCards.Add(actions[x]);
            for (int y = 0; y < players.Count; y++) _playerCards.Add(players[y]);
		}

		public GameDeck Clone()
		{
            GameDeck gameDeck = new GameDeck();
			gameDeck.Copy(this);
			return gameDeck;
		}

		public void Clear()
		{
            _actionCards.Clear();
            _playerCards.Clear();
		}

        #endregion PUBLIC METHODS

    }
}
