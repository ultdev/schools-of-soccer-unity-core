﻿using System;
using System.Collections.Generic;

namespace sos.core.game
{
    public class GameLogRow
    {

        #region CONSTANTS

        #endregion CONSTANTS

        #region FIELDS

		protected GameLogRowType _type;
		protected string _text;
		protected uint _sourcePlayer;
		protected GameCast _cast;
		protected GameProc _proc;
		protected uint _targetId;
		protected Position _targetPos;
		protected uint _targetUser;

        #endregion FIELDS

        #region CONSTRUCTORS

        #endregion CONSTRUCTORS

        #region PROPERTIES

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        #endregion PUBLIC METHODS

    }
}
