﻿using System;
using System.Collections.Generic;

using sos.core.cards;

namespace sos.core.game
{
    public class GamePlayerCard : PlayerCard
    {

        #region CONSTANTS

        #endregion CONSTANTS

        #region FIELDS

		private Position _position;
		private List<GameMod> _mods;
		private GameStats _gameStats;
		private GameStats _modStats;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GamePlayerCard(Position position) : base(CardKind.GAME_INSTANCE)
        {
            // Init
            _position = position;
            // Init objects
            _mods = new List<GameMod>();
		    _gameStats = new GameStats();
		    _modStats = new GameStats ();
        }

        public GamePlayerCard(PlayerCard card, Position position) : this(position)
        {
            Set(card);
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public Position Position
        {
            get { return _position;  }
        }

        public GameStats GameStats
        {
            get { return _gameStats; }
        }

        public GameStats ModStats
        {
            get { return _modStats; }
        }

        public List<GameMod> Mods
        {
            get { return _mods; }
        }

        public bool HasMods
        {
            get { return _mods.Count > 0; }
        }

        public bool CanPlayCards
        {
            get { return _gameStats.CanPlay && !_gameStats.IsExpelled; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

		protected bool ContainsMod(GameMod mod)
		{
			foreach (GameMod m in _mods)
			{
				if (m.Equals(mod)) return true;
			}
			return false;
		}

		public void Set(PlayerCard playerCard)
		{
			// Force card type
            _type = playerCard.Type;
			// Copy the player
            base.Copy(playerCard);
		}

		internal void UpdatePosition(Position position)
		{
            _position = position;
		}

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        public override bool Equals(Card card)
        {
            GamePlayerCard gamePlayerCard = card as GamePlayerCard;
            if (gamePlayerCard != null)
            {
                return base.Equals(card) &&
                        gamePlayerCard.Position == _position;
            }
            else
            {
                return base.Equals(card);
            }
        }

        public override void Copy(Card card)
        {
            // Check card type: only PlayerCards can be copied
            if (card.Type != CardType.PLAYER && card.Type != CardType.GOALKEEPER) throw new ArgumentException("Card should be a PLAYER or GOALKEEPER card");
            // Force card type
            _type = card.Type;
            // Ancestor copy
            base.Copy(card);
			// Cast the card
            GamePlayerCard gamePlayerCard = card as GamePlayerCard;
            // Copy data
            _position = gamePlayerCard.Position;
            // Copy objects
            _mods.Clear();
            foreach (GameMod mod in gamePlayerCard.Mods)
            {
                _mods.Add(mod.Clone());
            }
            _gameStats = gamePlayerCard.GameStats.Clone() as GameStats;
            _modStats = gamePlayerCard.ModStats.Clone() as GameStats;
        }

        public override void Clear()
        {
            base.Clear();
            _mods.Clear();
            _modStats.Clear();
            _gameStats.Clear();
        }

        #endregion PUBLIC METHODS

    }
}
