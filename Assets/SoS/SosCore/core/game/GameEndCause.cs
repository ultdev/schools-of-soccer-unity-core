﻿using System;
using System.Collections.Generic;
using sos.data;

namespace sos.core.game
{

    /// <summary>
    /// 
    /// </summary>
    public class GameEndCause : Enumerator
    {

        #region CONSTANTS

		public static readonly GameEndCause PLAYER_WIN				= new GameEndCause(10, 	true,  "PLAYER_WIN",			"Game won");
		public static readonly GameEndCause PLAYER_FLAWLESS_WIN	    = new GameEndCause(11, 	true,  "PLAYER_FLAWLESS_WIN",	"Game won with flawless victory");
		public static readonly GameEndCause PLAYER_LEAVE			= new GameEndCause(12, 	false, "PLAYER_LEAVE",			"Game lost, user leaved the game (surrender)");
		public static readonly GameEndCause PLAYER_DISCREDITED		= new GameEndCause(13, 	false, "PLAYER_DISCREDITED",	"Game lost, too many turns passed");
		public static readonly GameEndCause OPPONENT_WIN			= new GameEndCause(20, 	false, "OPPONENT_WIN",			"Game lost, waiting for kick off");
		public static readonly GameEndCause OPPONENT_FLAWLESS_WIN	= new GameEndCause(21, 	false, "OPPONENT_FLAWLESS_WIN","Game lost, opponent flawless victory");
		public static readonly GameEndCause OPPONENT_LEAVE			= new GameEndCause(22, 	true,  "OPPONENT_LEAVE",		"Game won, opponent leaved the game (surrender)");
		public static readonly GameEndCause OPPONENT_DISCREDITED	= new GameEndCause(23, 	true,  "OPPONENT_DISCREDITED",	"Game won, opponent passed too many turns");
		public static readonly GameEndCause OPPONENT_DISCONNECTS	= new GameEndCause(24, 	true,  "OPPONENT_DISCONNECTS",	"User won, opponent disconnects");
		public static readonly GameEndCause DRAW					= new GameEndCause(30, 	true,  "DRAW",					"Draw game");
        public static readonly GameEndCause CLIENT_ERROR            = new GameEndCause(90,  false, "CLIENT_ERROR",          "CLIENT ERROR - Check game and server log!");

        #endregion CONSTANTS

        #region CONSTRUCTORS

        public GameEndCause(int id, bool win, string name, string description) : this(id, win, name, description, true)
        {
        }

        public GameEndCause(int id, bool win, string name, string description, bool register) : base(id, name, string.Empty, register)
        {
            _win = win;
            _description = description;
        }

        #endregion CONSTRUCTORS

        #region FIELDS

		private string _description;
		private bool _win;

        #endregion FIELDS

        #region PROPERTIES

        public string Description
        {
            get { return _description; }
        }

        public bool Win
        {
            get { return _win; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        #endregion PUBLIC METHODS

        #region STATIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string[] Names()
        {
            return Enumerators.Names(typeof(GameEndCause));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static GameEndCause[] Values()
        {
            return Enumerators.Values<GameEndCause>(typeof(GameEndCause));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GameEndCause GetById(int id)
        {
            return Enumerators.GetById<GameEndCause>(typeof(GameEndCause), id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GameEndCause GetByName(string name)
        {
            return Enumerators.GetByName<GameEndCause>(typeof(GameEndCause), name);
        }

        #endregion STATIC METHODS

    }
}
