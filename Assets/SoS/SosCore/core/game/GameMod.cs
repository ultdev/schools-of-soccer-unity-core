﻿using System;
using sos.utils;

namespace sos.core.game
{
    public class GameMod
    {

        #region CONSTANTS

		// Default values
		private static readonly uint DEFAULT_ID							            = Defaults.INVALID_ID;
		private static readonly uint DEFAULT_INSTANCE_ID					        = Defaults.INVALID_ID;
		private static readonly string DEFAULT_NAME						            = string.Empty;
		private static readonly string DEFAULT_DESCRIPTION				            = string.Empty;
		private static readonly GameModType DEFAULT_TYPE				            = GameModType.GENERAL;
		private static readonly uint DEFAULT_SOURCE_CARD_ID				            = Defaults.INVALID_ID;

        #endregion CONSTANTS

        #region FIELDS

		private uint _id;
		private uint _instanceId;
		private string _name;
		private string _description;
		private GameModType _type;
		private uint _sourceCardId;

        #endregion FIELDS

        #region CONSTRUCTORS

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public uint Id
        {
            get { return _id; }
            set { _id = value; }
        }
        
        public uint InstanceId
        {
            get { return _instanceId; }
            set { _instanceId = value; }
        }
        
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        
        public GameModType Type
        {
            get { return _type; }
            set { _type = value;  }
        }
        
        public uint SourceCardId
        {
            get { return _sourceCardId; }
            set { _sourceCardId = value; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

		public bool Equals(GameMod mod)
		{
			return	_id == mod.Id;
		}
		
		public void Copy(GameMod mod)
		{
			_id				= mod.Id;
			_instanceId		= mod.InstanceId;
			_name			= mod.Name;
			_description	= mod.Description;
			_type			= mod.Type;
			_sourceCardId	= mod.SourceCardId;
		}
		
		public GameMod Clone()
		{
			GameMod mod = new GameMod();
			mod.Copy(this);
			return mod;
		}
		
		public void Clear()
		{
			// Clear data
			_id 			= DEFAULT_ID;
			_instanceId		= DEFAULT_INSTANCE_ID;
			_name			= DEFAULT_NAME;
			_description	= DEFAULT_DESCRIPTION;
			_type			= DEFAULT_TYPE;
			_sourceCardId	= DEFAULT_SOURCE_CARD_ID;
		}
		
		public override string ToString()
		{
			return string.Format("GameMod {0} (id:{1}, instanceId:{2})", _name, _id, _instanceId);
		}

        #endregion PUBLIC METHODS

    }
}
