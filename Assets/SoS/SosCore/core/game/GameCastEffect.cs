﻿using sos.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game
{
    public class GameCastEffect
    {

        #region CONSTANTS

        #endregion CONSTANTS

        #region FIELDS

		private string _name;
		private Dictionary<string, string> _stats;
		private uint _target;   // id univoco del target

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameCastEffect(string name)
        {
            // Init
            _name = name;
            _target = Defaults.INVALID_ID;
            // Inti objects
            _stats = new Dictionary<string, string>();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public uint Target
        {
            get { return _target; }
            set { _target = value;  }
        }

        public Dictionary<string, string> Stats
        {
            get { return _stats; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

		public void Copy(GameCastEffect gameCastEffect)
		{
			_name = gameCastEffect.Name;
			_target = gameCastEffect.Target;
            _stats.Clear();
            foreach (KeyValuePair<string, string> pair in gameCastEffect.Stats)
            {
                _stats.Add(pair.Key, pair.Value);
            }           
		}
		
		public void Clear()
		{
			// Clear data
            _stats.Clear();
			_target = Defaults.INVALID_ID;
		}

        #endregion PUBLIC METHODS

    }
}
