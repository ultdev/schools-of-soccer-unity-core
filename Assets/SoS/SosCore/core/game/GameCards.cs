﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game
{
    public class GameCards
    {

        #region CONSTANTS

		// Default values
		protected static readonly uint  DEFAULT_CARD_COUNT						    = 0;
		protected static readonly uint  DEFAULT_GRAVEYARD_CARD_COUNT			    = 0;
		protected static readonly uint  DEFAULT_DECK_CARD_COUNT				        = 0;
		protected static readonly uint  DEFAULT_MAX_PLAYABLE					    = GameOptions.DEFAULT_HAND_SIZE;
		protected static readonly uint  DEFAULT_MAX_DISCARDABLE				        = GameOptions.DEFAULT_DISCARD_SIZE;

        #endregion CONSTANTS

        #region FIELDS

		private uint _cardCount;
		private uint _graveyardCardCount;
		private uint _deckCardCount;
		private uint _maxPlayable;
		private uint _maxDiscardable;
		private List<GameActionCard> _hand;
		private List<GameActionCard> _graveyard;
		private GameTeam _gameTeam;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameCards(GameTeam gameTeam)
        {
			// GameTeam reference
			_gameTeam = gameTeam;
			// Init objects
			_hand = new List<GameActionCard>();
			_graveyard = new List<GameActionCard>();
			// init data
			Clear();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        internal GameTeam Team
        {
            get { return _gameTeam; }
        }

        public uint CardCount
        {
            get { return _cardCount; }
            internal set { _cardCount = value; }
        }

        public uint GraveyardCardCount
        {
            get { return _graveyardCardCount; }
            internal set { _graveyardCardCount = value; }
        }

        public uint DeckCardCount
        {
            get { return _deckCardCount; }
            internal set { _deckCardCount = value; }
        }

        public uint MaxPlayable
        {
            get { return _maxPlayable; }
            internal set { _maxPlayable = value; }
        }

        public uint MaxDiscardable
        {
            get { return _maxDiscardable; }
            internal set { _maxDiscardable = value; }
        }

        public List<GameActionCard> Hand
        {
            get { return _hand; }
        }

        public List<GameActionCard> Graveyard
        {
            get { return _graveyard; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

		public bool HasCardInHand(uint instanceId)
		{
			foreach (GameActionCard gac in _hand) {
				if (gac.InstanceId == instanceId) return true;
			}
			return false;
		}

		public bool Equals(GameCards gameCards)
		{
			// Check data
			if (_cardCount			!= gameCards.CardCount 			||
				_graveyardCardCount	!= gameCards.GraveyardCardCount ||
				_deckCardCount		!= gameCards.DeckCardCount 		||
				_maxPlayable		!= gameCards.MaxPlayable 		||
				_maxDiscardable		!= gameCards.MaxDiscardable		||
				_hand.Count 		!= gameCards.Hand.Count		    ||
				_graveyard.Count 	!= gameCards.Graveyard.Count) return false;
			// Loop over hand action cards
			for (int i= 0; i < _hand.Count; i++)
			{
				if (!_hand[i].Equals(gameCards.Hand[i])) return false;
			}
			// Loop over graveyard action cards
            for (int j = 0; j < _graveyard.Count; j++)
			{
				if (!_graveyard[j].Equals(gameCards.Graveyard[j])) return false;
			}
			// Instances are equal
			return 	true;
		}

		public void Update(GameCards gameCards)
		{
			// Check data
			_cardCount			= gameCards.CardCount;
			_graveyardCardCount	= gameCards.GraveyardCardCount;
			_deckCardCount		= gameCards.DeckCardCount;
			_maxPlayable		= gameCards.MaxPlayable;
			_maxDiscardable		= gameCards.MaxDiscardable;
			// Loop over hand action cards
			List<GameActionCard> h = new List<GameActionCard>();
			for (int i= 0; i < gameCards.Hand.Count; i++)
			{
				h.Add(_gameTeam.Game.Deck.FindActionCard(gameCards.Hand[i].InstanceId));
			}
			// Loop over graveyard action cards
			List<GameActionCard> g = new List<GameActionCard>();
			for (int j= 0; j < gameCards.Graveyard.Count; j++)
			{
				g.Add(_gameTeam.Game.Deck.FindActionCard(gameCards.Graveyard[j].InstanceId));
			}
			// Sets hand cards
			_hand.Clear();
            _hand.AddRange(h);
			// Sets graveyard cards
			_graveyard.Clear();
            _graveyard.AddRange(g);
		}

		public void Copy(GameCards gameCards)
		{
			// CHeck same game deck
			if (_gameTeam != gameCards.Team) throw new ArgumentException("Cannot copy from cards of a different GameTeam");
			// Check data
			_cardCount			= gameCards.CardCount;
			_graveyardCardCount	= gameCards.GraveyardCardCount;
			_deckCardCount		= gameCards.DeckCardCount;
			_maxPlayable		= gameCards.MaxPlayable;
			_maxDiscardable		= gameCards.MaxDiscardable;
			// Sets hand cards
			// Sets hand cards
			_hand.Clear();
            _hand.AddRange(gameCards.Hand);
			// Sets graveyard cards
			_graveyard.Clear();
            _graveyard.AddRange(gameCards.Graveyard);
		}

		public GameCards Clone()
		{
            GameCards cards = new GameCards(_gameTeam);
			cards.Copy(this);
			return cards;
		}

        /// <summary>
        /// Duplicates the current game card set using the same GameTeam but clearing all the data.
        /// This method is used inside the GameEngine to create a temporary copy do the data befor to
        /// update the original instance
        /// </summary>
        /// <returns></returns>
        public GameCards Duplicate()
        {
            GameCards cards = new GameCards(_gameTeam);
            cards.Clear();
            return cards;
        }

		public void Clear()
		{
			// Clear data
			_cardCount			= DEFAULT_CARD_COUNT;
			_graveyardCardCount	= DEFAULT_GRAVEYARD_CARD_COUNT;
			_deckCardCount		= DEFAULT_DECK_CARD_COUNT;
			_maxPlayable		= DEFAULT_MAX_PLAYABLE;
			_maxDiscardable		= DEFAULT_MAX_DISCARDABLE;
			// Clear objects
            _hand.Clear();
            _graveyard.Clear();
		}

        #endregion PUBLIC METHODS

    }
}
