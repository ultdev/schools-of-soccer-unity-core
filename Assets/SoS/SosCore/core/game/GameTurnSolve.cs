﻿using System;
using System.Collections.Generic;

using sos.utils;

namespace sos.core.game
{
    public class GameTurnSolve
    {

        #region CONSTANTS

		// Default values
		protected static readonly GameRole  DEFAULT_WINNER					        = GameRole.IDLE;
		protected static readonly uint      DEFAULT_ATTACKER_ROLL				    = 0;
		protected static readonly uint      DEFAULT_ATTACKER_SCORE				    = 0;
        protected static readonly uint      DEFAULT_ATTACKER_ID                     = Defaults.INVALID_ID;
		protected static readonly uint      DEFAULT_DEFENDER_ROLL				    = 0;
		protected static readonly uint      DEFAULT_DEFENDER_SCORE				    = 0;
		protected static readonly uint      DEFAULT_DEFENDER_ID                     = Defaults.INVALID_ID;

        #endregion CONSTANTS

        #region FIELDS

		protected GameRole _winner;
		protected uint _attackerRoll;
		protected uint _defenderRoll;
		protected uint _attackerScore;
		protected uint _defenderScore;
		protected uint _attackerId;
		protected uint _defenderId;
		protected GameRole _autoWinner;
		protected GameAction _action;
		protected Position _target;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameTurnSolve()
        {
            Clear();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public GameRole Winner
        {
            get { return _winner; }
            internal set { _winner = value; }
        }

        public uint AttackerRoll
        {
            get { return _attackerRoll; }
            internal set { _attackerRoll = value; }
        }

        public uint DefenderRoll
        {
            get { return _defenderRoll; }
            internal set { _defenderRoll = value; }
        }

        public uint AttackerScore
        {
            get { return _attackerScore; }
            internal set { _attackerScore = value; }
        }

        public uint DefenderScore
        {
            get { return _defenderScore; }
            internal set { _defenderScore = value; }
        }

        public uint AttackerId
        {
            get { return _attackerId; }
            internal set { _attackerId = value; }
        }

        public uint DefenderId
        {
            get { return _defenderId; }
            internal set { _defenderId = value; }
        }

        public GameRole AutoWinner
        {
            get { return _autoWinner; }
            internal set { _autoWinner = value; }
        }

        public GameAction Action
        {
            get { return _action; }
            internal set { _action = value; }
        }

        public Position Target
        {
            get { return _target; }
            internal set { _target = value; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

		public void Clear()
		{
			_winner			= DEFAULT_WINNER;
			_autoWinner		= DEFAULT_WINNER;
			_attackerRoll	= DEFAULT_ATTACKER_ROLL;
			_defenderRoll	= DEFAULT_DEFENDER_ROLL;
			_attackerScore	= DEFAULT_ATTACKER_SCORE;
			_defenderScore	= DEFAULT_DEFENDER_SCORE;
			_attackerId		= DEFAULT_ATTACKER_ID;
			_defenderId		= DEFAULT_DEFENDER_ID;
		    _action         = GameAction.PASS;
		    _target         = Position.ANY;
		}

        #endregion PUBLIC METHODS

    }
}
