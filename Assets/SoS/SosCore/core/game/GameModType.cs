﻿using System;
using System.Collections.Generic;
using sos.data;

namespace sos.core.game
{
    /// <summary>
    /// 
    /// </summary>
    public class GameModType : Enumerator
    {

        #region CONSTANTS

		public static readonly GameModType GENERAL		                    = new GameModType(1, 	"General");
		public static readonly GameModType BUFF                             = new GameModType(2, 	"Debuff");
		public static readonly GameModType DEBUFF                           = new GameModType(3, 	"Buff");
		public static readonly GameModType GEAR                             = new GameModType(4, 	"Gear");
		public static readonly GameModType FREEKICK                         = new GameModType(5, 	"Freekick");
		public static readonly GameModType SWAP            	                = new GameModType(6, 	"Swap");
		public static readonly GameModType CHILD_EFFECT                     = new GameModType(7, 	"Child Effect");
		public static readonly GameModType FOUL        		                = new GameModType(8, 	"Foul");
		public static readonly GameModType YELLOW_CARD        	            = new GameModType(9, 	"Yellow Card");
		public static readonly GameModType RED_CARD        	                = new GameModType(10, 	"Red Card");
		public static readonly GameModType SCHOOL_BONUS                     = new GameModType(11,	"School Bonus");
		public static readonly GameModType STAMINA_REGEN                    = new GameModType(12,	"Stamina Regen"); 		// Half Time Stamina Regen
		public static readonly GameModType CARD_COST       	                = new GameModType(13,	"Card Cost");
		public static readonly GameModType CHILD_DEBUFF       	            = new GameModType(14,	"Child Debuff");
		public static readonly GameModType CHILD_BUFF       	            = new GameModType(15,	"Child Buff");
		public static readonly GameModType ANALISYS_MARKER                  = new GameModType(16,	"Analisys Marker");		// Defence School analisys marker
		public static readonly GameModType INVISIBLE_MARKER                 = new GameModType(17,	"Invisible Marker"); 	// Used to avoid double procs

        #endregion CONSTANTS

        #region CONSTRUCTORS

        public GameModType(int id, string name) : this(id, name, true)
        {
        }

        public GameModType(int id, string name, bool register) : base(id, name, string.Empty, register)
        {
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        #endregion PUBLIC METHODS

        #region STATIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string[] Names()
        {
            return Enumerators.Names(typeof(GameModType));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static GameModType[] Values()
        {
            return Enumerators.Values<GameModType>(typeof(GameModType));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GameModType GetById(int id)
        {
            return Enumerators.GetById<GameModType>(typeof(GameModType), id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GameModType GetByName(string name)
        {
            return Enumerators.GetByName<GameModType>(typeof(GameModType), name);
        }

        #endregion STATIC METHODS

    }
}
