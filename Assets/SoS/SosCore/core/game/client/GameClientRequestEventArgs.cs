﻿using sos.core.game.requests;

namespace sos.core.game.client
{
    public class GameClientRequestEventArgs : GameClientEventArgs
    {

        #region FIELDS

        private GameRequest _request;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameClientRequestEventArgs(GameClient client, GameRequest request) : base(client)
        {
            _request = request;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        /// <summary>
        /// Client that rises the error
        /// </summary>
        public GameRequest Request
        {
            get { return _request; }
        }

        #endregion

    }
}
