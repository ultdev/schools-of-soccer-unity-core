﻿using System;

namespace sos.core.game.client
{
    public class GameClientException : Exception
    {
        public GameClientException() : base() {}
        public GameClientException(string message) : base(message) {}
        public GameClientException(string message, Exception cause) : base(message, cause) {}
    }
}
