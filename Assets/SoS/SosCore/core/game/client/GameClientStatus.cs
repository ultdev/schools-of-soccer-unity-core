﻿using sos.data;

namespace sos.core.game.client
{
    public class GameClientStatus : Enumerator
    {

        #region CONSTANTS

		public static readonly  GameClientStatus    NOT_CONNECTED	= new GameClientStatus(0, "NOT_CONNECTED");
		public static readonly  GameClientStatus    CONNECTING      = new GameClientStatus(1, "CONNECTING");
		public static readonly  GameClientStatus    READY	        = new GameClientStatus(2, "READY");
		public static readonly  GameClientStatus    REQUEST 		= new GameClientStatus(3, "REQUEST");
		public static readonly  GameClientStatus    POLLING 		= new GameClientStatus(4, "POLLING");
		public static readonly  GameClientStatus    HEARTBEAT       = new GameClientStatus(5, "HEARTBEAT");

        #endregion CONSTANTS

        #region CONSTRUCTORS
        
        protected GameClientStatus(int id, string name) : base(id, name) {}

        protected GameClientStatus(int id, string name, bool register) : base(id, name, null, register) {}

        #endregion CONSTRUCTORS

        #region STATIC METHODS

        public static string[] Names()
        {
            return Enumerators.Names(typeof(GameClientStatus));
        }

        public static GameClientStatus[] Values()
        {
            return Enumerators.Values<GameClientStatus>(typeof(GameClientStatus));
        }

        public static GameClientStatus GetById(int id)
        {
            return Enumerators.GetById<GameClientStatus>(typeof(GameClientStatus), id);
        }

        public static GameClientStatus GetByName(string name)
        {
            return Enumerators.GetByName<GameClientStatus>(typeof(GameClientStatus), name);
        }

        #endregion STATIC METHODS

    }
}
