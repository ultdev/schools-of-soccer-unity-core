﻿using System.Collections.Generic;
using System.Linq;

namespace sos.core.game.client
{

	/// <summary>
    /// 
    /// </summary>
	public class GameClientOptions
	{

		#region CONSTANTS

		// JSON Property names
		protected const	string  JSON_PROPERTY_PARAMS				    = "params";
		
		// Parameters
		public const string     PARAM_GAME_ID					        = "gameID";
		public const string     PARAM_USER_ID					        = "userID";
		public const string     PARAM_PASSWORD					        = "password";
		public const string     PARAM_USER_TOKEN					    = "userToken";
		public const string     PARAM_GAME_TOKEN					    = "gameToken";
		
		// Delays & intervals
		private const int      DEFAULT_WORKER_DELAY				        = 00100;    // 0.1 sec
		// private const int      DEFAULT_INTERVALL_CONNECTION		        = 00250;    // 0.25 sec
		private const int      DEFAULT_HEARTBEAT_INTERVALL		        = 01000;// 15000;    // 15 sec
		private const int      DEFAULT_HEARTBEAT_RETRY_INTERVALL	    = 00250;    // 0.25 sec
		private const int      DEFAULT_REQUEST_RETRY_INTERVALL	        = 01000;    // 1 sec
		private const int      DEFAULT_POLLING_INTERVALL			    = 01000;    // 1 sec

		#endregion CONSTANTS
		
		#region FIELDS

		private int _workerDelay;
		// private int _intervalConnection;
		private int _heartbeatInterval;
		private int _heartbeatRetryInterval;
		private int _pollingInterval;
		private int _requestRetryInterval;
		private Dictionary<string, object> _params = new Dictionary<string, object>();

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameClientOptions()
		{
			// Clear data
			Clear();
		}

		#endregion CONSTRUCTORS
		
		#region PROPERTIES

        /// <summary>
        /// Client internal worker delay, in milliseconds
        /// </summary>
        public int WorkerDelay
        {
            get
            {
                return _workerDelay;
            }

            set
            {
                _workerDelay = value;
            }
        }

        //public uint IntervalConnection
        //{
        //    get
        //    {
        //        return _intervalConnection;
        //    }

        //    set
        //    {
        //        _intervalConnection = value;
        //    }
        //}

        public int HeartbeatInterval
        {
            get
            {
                return _heartbeatInterval;
            }

            set
            {
                _heartbeatInterval = value;
            }
        }

        public int HeartbeatRetryInterval
        {
            get { return _heartbeatRetryInterval; }
            set { _heartbeatRetryInterval = value; }
        }

        public int IntervalPolling
        {
            get
            {
                return _pollingInterval;
            }

            set
            {
                _pollingInterval = value;
            }
        }

        public int IntervalRequestRetry
        {
            get
            {
                return _requestRetryInterval;
            }

            set
            {
                _requestRetryInterval = value;
            }
        }

		public uint GameId
		{
			get { return GetParamAsUInt(PARAM_GAME_ID); }
		}
		
		public string UserId
		{
			get { return GetParamAsString(PARAM_USER_ID); }
		}
		
		public string Password
		{
			get { return GetParamAsString(PARAM_PASSWORD); }
		}

		#endregion PROPERTIES
		
		#region PRIVATE METHODS

		#endregion PRIVATE METHODS

		#region PUBLIC METHODS

		public object GetParam(string key)
		{
			return _params[key];
		}
		
		public string GetParamAsString(string key)
		{
			return _params[key].ToString();
		}
		
		public int GetParamAsInt(string key)
		{
			return (int) _params[key];
		}
		
		public uint GetParamAsUInt(string key)
		{
			return (uint) _params[key];
		}
		
		public void SetParam(string key, object value)
		{
			_params[key] = value;
		}
		
		public bool HasParam(string key)
		{
            return _params.ContainsKey(key);
		}

		public bool Equals(GameClientOptions options)
		{
			return 	_workerDelay			== options.WorkerDelay && 
					// _intervalConnection		== options.IntervalConnection && 
					_heartbeatInterval		== options.HeartbeatInterval && 
					_heartbeatRetryInterval	== options.HeartbeatRetryInterval &&
					_requestRetryInterval	== options.IntervalRequestRetry &&
					_pollingInterval		== options.IntervalPolling &&
					_params.Count           == options._params.Count &&
                    !_params.Except(options._params).Any();
		}
		
		public void Clear()
		{
			_workerDelay			= DEFAULT_WORKER_DELAY;
			// _intervalConnection		= DEFAULT_INTERVALL_CONNECTION;
			_heartbeatInterval		= DEFAULT_HEARTBEAT_INTERVALL;
			_heartbeatRetryInterval	= DEFAULT_HEARTBEAT_RETRY_INTERVALL;
			_requestRetryInterval	= DEFAULT_REQUEST_RETRY_INTERVALL;
			_pollingInterval		= DEFAULT_POLLING_INTERVALL;
			_params.Clear();
		}

		#endregion PUBLIC METHODS
        		
	}


}