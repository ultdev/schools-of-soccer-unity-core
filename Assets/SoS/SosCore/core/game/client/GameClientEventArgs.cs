﻿using System;

namespace sos.core.game.client
{
    /// <summary>
    /// 
    /// </summary>
    public class GameClientEventArgs : EventArgs
    {

        #region FIELDS

        private GameClient _client;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameClientEventArgs(GameClient client) : base()
        {
            _client = client;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        /// <summary>
        /// Client that rises the error
        /// </summary>
        public GameClient Client
        {
            get { return _client; }
        }

        #endregion

    }
}
