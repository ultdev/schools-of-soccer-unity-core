﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game.client
{
    public class GameClientErrorEventArgs : GameClientEventArgs
    {

        #region FIELDS

        private Exception _exception;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameClientErrorEventArgs(GameClient client, Exception exception) : base(client)
        {
            _exception = exception;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        /// <summary>
        /// Client that rises the error
        /// </summary>
        public Exception Exception
        {
            get { return _exception; }
        }

        #endregion

    }
}
