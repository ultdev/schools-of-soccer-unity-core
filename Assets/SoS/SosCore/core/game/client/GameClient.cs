﻿using sos.core.game.requests;
using sos.data.providers.game;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sos.core.game.client
{
    /// <summary>
    /// 
    /// </summary>
	public class GameClient : MonoBehaviour
    {

        #region CONSTANTS

        // Logging
        private const string LOG_MARKER = "GAME CLIENT > ";
        private const string LOG_MARKER_WORKER = LOG_MARKER + "WORKER >";
        private const string LOG_MARKER_REQUEST = LOG_MARKER + "REQUEST > ";
        private const string LOG_MARKER_HEARTBEAT = LOG_MARKER + "HEARTBEAT > ";
        private const string LOG_MARKER_CONNECTION = LOG_MARKER + "CONNECTION > ";

        // Debug keys
        public const string DEBUG_KEY = "sos.core.game.client";
        public const string DEBUG_KEY_WORKER = "sos.core.game.client.worker";
        public const string DEBUG_KEY_REQUEST = "sos.core.game.client.request";
        public const string DEBUG_KEY_HEARTBEAT = "sos.core.game.client.heartbeat";
        public const string DEBUG_KEY_CONNECTION = "sos.core.game.client.connection";

        // Heartbeat contants
        private const int HEARTHBEAT_COUNTER_MIN = 1;
        private const int HEARTHBEAT_FAIL_TO_DISCONNECT = 3;

        // Request contants
        private const int REQUEST_RETRY_MAX = 3;

        #endregion CONSTANTS

        #region FIELDS

        // Client options
        private GameClientOptions _options;
        private User _user;
        // Status
        private GameClientStatus _status = GameClientStatus.NOT_CONNECTED;
        // Heartbeat status
        private int _heartbeatCount;
        private int _heartbeatFailCount;
        private DateTime _heartbeatDate;
        // Requests
        private GameRequest _request;
        private DateTime _requestRetryDate;
        private List<GameRequest> _requests; // currently only one request at time, but is ready for a requests queue
        // Polling status
        private int _pollingCount;
        private DateTime _pollingDate;
        // Calls
        private GameObject _providerHolder;
        private GameProvider _provider;


        #endregion FIELDS

        #region EVENTS AND DELEGATES

        // Delegates
        public delegate void GameClientDelegete(GameClientEventArgs args);
        public delegate void GameClientRequestDelegete(GameClientRequestEventArgs args);
        public delegate void GameClientErrorDelegate(GameClientErrorEventArgs args);

        // Events
        public event GameClientDelegete ConnectionStart;
        public event GameClientDelegete ConnectionStop;
        public event GameClientDelegete ConnectionLost;
        public event GameClientDelegete ConnectionFailed;
        public event GameClientRequestDelegete RequestExecuted;
        public event GameClientRequestDelegete RequestFailed;
        public event GameClientRequestDelegete RequestWaiting;
        public event GameClientErrorDelegate Error;

        #endregion EVENTS AND DELEGATES

        #region CONSTRUCTORS

        private GameClient()
        {
            // Requests list
            _requests = new List<GameRequest>();
            // Initialization
            Reset();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        /// <summary>
        /// Client options
        /// </summary>
		public GameClientOptions Options
        {
            get { return _options; }
            set { _options = value; }
        }

        /// <summary>
        /// Client status
        /// </summary>
		public GameClientStatus Status
        {
            get { return _status; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int HeartbeatCount
        {
            get { return _heartbeatCount; }
        }

        /// <summary>
        /// True if the client is currently connected to the SoS server
        /// </summary>
		public bool IsConnected
        {
            get { return _status.Id > GameClientStatus.CONNECTING.Id; }
        }

        /// <summary>
        /// True if the client is establishing connection with SoS server
        /// </summary>
		public bool IsConnecting
        {
            get { return _status == GameClientStatus.CONNECTING; }
        }

        /// <summary>
        /// True when the client is polling the server waiting for a request async response
        /// </summary>
        public bool IsInPolling
        {
            get { return _status == GameClientStatus.POLLING &&
                          _request != null &&
                          _request.IsPollingRequest;
            }
        }

        /// <summary>
        /// True when the client is handling a request to SoS server
        /// </summary>
        public bool IsRunning
        {
            get { return _request != null; }
        }

        /// <summary>
        /// True if the client has at least one request to handle
        /// </summary>
        public bool HasRequests
        {
            get { return _requests.Count > 0; }
        }

        /// <summary>
        /// True when the client is handling a request that fails and is trying to handle it again
        /// </summary>
        public bool HasRequestsToRetry
        {
            get { return HasRequests && IsRunning && _request.Retries > 0; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #region Heartbeat handling

        /// <summary>
        /// Performs the heartbeat
        /// </summary>
		private void Heartbeat()
        {
            // STATUS > HEARTBEAT (only if connected)
            if (IsConnected) SetStatus(GameClientStatus.HEARTBEAT);
            // Sets the heartbeat date
            _heartbeatDate = DateTime.Now;
            // Execute the call
            _provider.Heartbeat(_user.SessionToken, _heartbeatCount);
        }

        /// <summary>
        /// Handle a successful heartbeat call to the SoS server
        /// </summary>
		private void HeartbeatSuccess()
        {
            try
            {
                // Resets the heartbeat fail counter
                _heartbeatFailCount = 0;
                // Check if this is the first heartbeat, so the connection is no yet opened
                // and client status is CONNECTING
                if (IsConnecting)
                {
                    // Flag connection started and init the heartbeat counter
                    _heartbeatCount = HEARTHBEAT_COUNTER_MIN;
                    // STATUS > READY (connetion up)
                    SetStatus(GameClientStatus.READY);
                    // DEBUG > Connection start
                    if (sos.logging.Logger.CanDebug(DEBUG_KEY_CONNECTION)) sos.logging.Logger.Debug(LOG_MARKER_CONNECTION + "start");
                    // Dispatch EVENT_CONNECT event
                    RaiseConnectionStartEvent(new GameClientEventArgs(this));
                }
                else
                {
                    // Increment the heartbeat
                    _heartbeatCount++;
                    // STATUS > READY (connetion up)
                    SetStatus(GameClientStatus.READY);
                }
                // Restart the worker
                WorkerStart();
            }
            catch (Exception ex)
            {
                // Manage unexpected error
                ManageUnexpectedError(LOG_MARKER_HEARTBEAT + "heartbeatSuccess() unexpected error", ex);
            }
        }

        /// <summary>
        /// Handle a failed heartbeat call to the SoS server
        /// </summary>
		private void HeartbeatFailed()
        {
            try
            {
                // Increment the heartbeat error counter
                _heartbeatFailCount++;
                // Check if the maximum fail count is reached
                if (_heartbeatFailCount < HEARTHBEAT_FAIL_TO_DISCONNECT)
                {
                    // Restart the worker
                    WorkerStart();
                }
                else
                {
                    // If the client is already connected
                    if (IsConnected)
                    {
                        // Force disconnection due too many heartbeat fails
                        ConnectionClose(true);
                    }
                    else
                    {
                        // Raises ConnectionFailed event
                        RaiseConnectionFailedEvent(new GameClientEventArgs(this));
                    }
                }
            }
            catch (Exception ex)
            {
                // Manage unexpected error
                ManageUnexpectedError(LOG_MARKER_HEARTBEAT + "heartbeatFailed() unexpected error", ex);
            }
        }

        /// <summary>
        /// Returns the heartbeat interval or the heartbeat retry interval
        /// </summary>
        /// <returns></returns>
		private int GetHeartbeatInterval()
        {
            return !IsConnecting ? _heartbeatFailCount > 0 ? _options.HeartbeatRetryInterval
                                                            : _options.HeartbeatInterval
                                  : _options.HeartbeatInterval;
        }

        /// <summary>
        /// Heartbeat call success event handler
        /// </summary>
        /// <param name="args"></param>
        private void GameProvider_HeartbeatDone(GameProviderEventArgs args)
        {
            // Heartbeat ok
            HeartbeatSuccess();
            // LOG
            if (sos.logging.Logger.CanDebug(DEBUG_KEY_HEARTBEAT)) sos.logging.Logger.Debug(LOG_MARKER_HEARTBEAT + "Heartbeat (counter:{0})", _heartbeatCount);
        }

        /// <summary>
        /// Heartbeat call fail event handler
        /// </summary>
        /// <param name="args">Call arguments</param>
        private void GameProvider_HeartbeatFailed(GameProviderErrorEventArgs args)
        {
            // Compose the error message
            string msg = LOG_MARKER_HEARTBEAT + "Heartbeat call failed (prog: {0}, error prog: {1}, message: {2})";
            // Log error
            sos.logging.Logger.Error(msg, _heartbeatCount, _heartbeatFailCount, args.ErrorMessage);
            // Manage heartbeat failed
            HeartbeatFailed();
        }

        #endregion Heartbeat handling

        #region Request handling

        /// <summary>
        /// Executes the given request on SoS server
        /// </summary>
        /// <param name="request"></param>
        private void RequestExecute(GameRequest request)
        {
			// Set the current request and call
			_request = request;
			// If is a polling request, store the date and resets the counter
			if (_request.IsPollingRequest)
			{
				// Initialize polling status
				_pollingCount = 0;
				_pollingDate = DateTime.MinValue;
				// STATUS > POLLING
				SetStatus(GameClientStatus.POLLING);
			}
			else
			{
				// STATUS > REQUEST
				SetStatus(GameClientStatus.REQUEST);
			}
			// DEBUG > Trace request call
			if (sos.logging.Logger.CanDebug(DEBUG_KEY_REQUEST)) sos.logging.Logger.Debug(LOG_MARKER_REQUEST + "Execute {0} (id: {1})", _request.Phase.Name, _request.Id);
			// Executes the request call
            _provider.Request(_request);
        }

        /// <summary>
        /// Retry to execute the last request on SoS server
        /// </summary>
        private void RequestRetry()
        {
            // Checks if the client has a request to execute again
            if (_requests == null) throw new GameClientException("GameClient has not a request to retry to execute");
			// DEBUG > Trace request retry
			if (sos.logging.Logger.CanDebug(DEBUG_KEY_REQUEST)) sos.logging.Logger.Debug(LOG_MARKER_REQUEST + "Retry {0} (id: {1}) - retry: {2}", _request.Phase.Name, _request.Id, _request.Retries);
			// Executes the request again
            _provider.Request(_request);
        }

        /// <summary>
        /// Handles the result of a request successfully executed by SoS server
        /// </summary>
        /// <param name="result">Request result</param>
		protected void RequestSucceded(GameRequestResult result)
		{
			try
			{
				// Store the current request
				var request = _request;
				// Process the request results
				request.Process(result);
				// Check if the request is in error
				if (!request.IsInError)
				{
					// Check if the current request is a polling request
					if (request.Waiting)
					{
						// Check if the status is POLLING
						if (!IsInPolling) throw new GameClientException("Request " + request.Phase.Name + " (id:" + request.Id + ") WAIT result but client not in POLLING!");
						
						// Nothing to do, not to clear the request is a polling request, only restarts the worker (later down)
						
						// DEBUG > Request POLLING
						if (sos.logging.Logger.CanDebug(DEBUG_KEY_REQUEST)) sos.logging.Logger.Debug(LOG_MARKER_REQUEST + "WAITING {0} (id: {1}) {2} retries", request.Phase, request.Id, _pollingCount);
						// STATUS > polling
						SetStatus(GameClientStatus.POLLING);
						// Raise RequestWaiting event
                        RaiseRequestWaitingEvent(new GameClientRequestEventArgs(this, request));
					}
					else
					{
						// DEBUG > Request SUCCESS
						if (sos.logging.Logger.CanDebug(DEBUG_KEY_REQUEST)) sos.logging.Logger.Debug(LOG_MARKER_REQUEST + "EXEC {0} success (id: {1}) data: {2}", request.Phase, request.Id, request.Result.Data.ToString());
						// STATUS > ready
						SetStatus(GameClientStatus.READY);
						// Resets the current request, call and queue
						RequestReset();
						// Dispatch RequestExecuted event
                        RaiseRequestExecutedEvent(new GameClientRequestEventArgs(this, request));
					}
				}
				else
				{
					// DEBUG > Request FAILED
					if (sos.logging.Logger.CanDebug(DEBUG_KEY_REQUEST)) sos.logging.Logger.Debug(LOG_MARKER_REQUEST + "EXEC {0} FAILED (id: {1}) - {2} (code:{3})", request.Phase, request.Id, request.Result.Error.Message, request.Result.Error.Code);
					// STATUS > ready
					SetStatus(GameClientStatus.READY);
					// Resets the current request, call and queue
					RequestReset();
					// Dispatch RequestFailed event
                    RaiseRequestFailedEvent(new GameClientRequestEventArgs(this, request));
				}
				// Restart the worker if the client is still connected
				if (IsConnected) WorkerStart();
			}
			catch (Exception ex)
			{
				// Manage unexpected error
				ManageUnexpectedError(LOG_MARKER_REQUEST + "RequestSucceded() in error", ex);
			}
		}

        /// <summary>
        /// Handles a failed request. Fails cause must be provided as a message
        /// </summary>
        /// <param name="message">Message that explains the reason because the request fails</param>
		private void RequestFails(string message)
		{
			// DEBUG > Trace request retry
			if (sos.logging.Logger.CanDebug(DEBUG_KEY_REQUEST)) sos.logging.Logger.Debug(LOG_MARKER_REQUEST + "Fails {0} (id: {1}) - retry: {2}", _request.Phase.Name, _request.Id, _request.Retries);
			// Resets the current request and queue call
			RequestReset();
			// Request in error, raise Error event
            RaiseErrorEvent(new GameClientErrorEventArgs(this, new GameClientException(message)));
			// Force disconnection
			ConnectionClose(false);
		}

        /// <summary>
        /// Handles a failed request and retry to execute it again
        /// </summary>
		private void RequestFailsWithRetry()
		{
			// DEBUG > Trace request retry
			if (sos.logging.Logger.CanDebug(DEBUG_KEY_REQUEST)) sos.logging.Logger.Debug(LOG_MARKER_REQUEST + "Fails, start to retry");
			// Resets the request to execute it again and save the time
			_request.Retry();
			_requestRetryDate = DateTime.Now;
            // Restart the worker if the client is still connected
            if (IsConnected)
            {
                WorkerStart();
            }
            else
            {
			    // DEBUG > Trace request retry
			    if (sos.logging.Logger.CanDebug(DEBUG_KEY_REQUEST)) sos.logging.Logger.Debug(LOG_MARKER_REQUEST + "Can't retry, client is not connected");
            }
		}

        /// <summary>
        /// Resets the current request, waiting for the next execution
        /// </summary>
		private void RequestReset()
		{
			// Removes the request from the queue
            _requests.RemoveAt(0);
			// Clear the reference to the current request
			_request = null;
			_requestRetryDate = DateTime.MinValue;
			// DEBUG > Trace polling request call
			if (sos.logging.Logger.CanDebug(DEBUG_KEY_REQUEST)) sos.logging.Logger.Debug(LOG_MARKER_REQUEST + "reset done");
		}

        /// <summary>
        /// Event handler for GameProvide.RequestDone event: manages a succesfull request to the SoS server
        /// </summary>
        /// <param name="args">Event arguments</param>
        private void GameProvider_RequestDone(GameProviderRequestEventArgs args)
        {
			// LOG
			if (sos.logging.Logger.CanDebug(DEBUG_KEY_REQUEST)) sos.logging.Logger.Debug(LOG_MARKER_REQUEST + "Request {0} (id: {1}) succedeed - params: {2}", _request.Phase.Name, _request.Id, _request.Params.ToString());
			// Request executed
			RequestSucceded(args.Result);
        }

        /// <summary>
        /// Event handler for GameProvide.RequestFailed event: manages a failes request to the SoS server
        /// </summary>
        /// <param name="args">Event arguments</param>
        private void GameProvider_RequestFailed(GameProviderErrorEventArgs args)
        {
            // Build the error message
            var message = string.Format(LOG_MARKER_REQUEST + "Request {0} (id: {1}) failed : {2} (params: {3})", _request.Phase.Name, _request.Id, args.ErrorMessage, _request.Params.ToString());
            // Check if request can be retried and if the current request has cross the retries limit
            if (_request.Retries < REQUEST_RETRY_MAX)
            {
			    // DEBUG > Trace failed request
			    if (sos.logging.Logger.CanDebug(DEBUG_KEY_REQUEST)) sos.logging.Logger.Debug(message);
				// Retry to execute the request
				RequestFailsWithRetry();
            }
            else
            {
			    // ERROR > Trace request failed
			    sos.logging.Logger.Error(message);
                // Request failed
				RequestFails(message);
            }
        }

        #endregion Request handling

        #region Worker handling

        /// <summary>
        /// Starts the client worker
        /// </summary>
        private void WorkerStart()
        {
			// DEBUG > Log worker started
			if (sos.logging.Logger.CanDebug(DEBUG_KEY_WORKER)) sos.logging.Logger.Debug(LOG_MARKER_WORKER + "started");
            // Worker intervall in seconds (float)
            var workerInterval = Convert.ToSingle(_options.WorkerDelay) / 1000f;
            // Starts the workorker handler
            InvokeRepeating("WorkerHandler", 0.1f, workerInterval);
        }

        /// <summary>
        /// Stops the client worker
        /// </summary>
        private void WorkerStop()
        {
			// DEBUG > Log worker started
			if (sos.logging.Logger.CanDebug(DEBUG_KEY_WORKER)) sos.logging.Logger.Debug(LOG_MARKER_WORKER + "stopped");
            // Stops the workorker handler
            CancelInvoke("WorkerHandler");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        // private IEnumerator WorkerHandler()
        private void WorkerHandler()
        {
            // Flag to make the worker running or wait until async execution
            // is performed (at the end of execution the async exec will restart the worker)
            var restartWorker = false;
            // Safe exec
            try
            {
                // Stops the worker
                WorkerStop();
				// Flag to check heartbeat is ok 
				bool connectionValid = _heartbeatFailCount == 0;
                // Check if the client must retry the last request
                if (connectionValid && HasRequestsToRetry)
                {
					// Retrieve the elapsed time from last request retry
                    TimeSpan elapsedRequestRetry = DateTime.Now.Subtract(_requestRetryDate);
					// Check the elapsed ms from the requets retry
					if (elapsedRequestRetry.TotalMilliseconds > _options.IntervalRequestRetry)
					{
						RequestRetry();
					}
					else
					{
						// DEBUG > Polling intervall not reached
						if (sos.logging.Logger.CanDebug(DEBUG_KEY_WORKER)) sos.logging.Logger.Debug(LOG_MARKER_WORKER + "request retry not executed, elapsed: {0}", elapsedRequestRetry.TotalMilliseconds);
						// Ok restart worker
						restartWorker = true;
					}
                }
                // Check the client is currently polling
                else if (connectionValid && IsInPolling)
                {
                    // TODO...
                }
                // Check if any request is in the queue
                else if (connectionValid && HasRequests)
                {
					// Retrieve oldest request
					var request = _requests[0];
					// DEBUG > Request to be executed
					if (sos.logging.Logger.CanDebug(DEBUG_KEY_WORKER)) sos.logging.Logger.Debug(LOG_MARKER_WORKER + "request {0} being executed", request.ToString());
					// Executes the request
					RequestExecute(request);
                }
                // Executes the heartbeat
                else
                {
					// Retrieve the elapsed time from last heartbeat
                    TimeSpan elapsedHeartbeat = DateTime.Now.Subtract(_heartbeatDate);
					// Check the elapsed ms from the last heartbeat
					if (elapsedHeartbeat.TotalMilliseconds > GetHeartbeatInterval())
					{
						// DEBUG > signal hearbeat retry
						if (!connectionValid) if (sos.logging.Logger.CanDebug(DEBUG_KEY_WORKER)) sos.logging.Logger.Debug(LOG_MARKER_WORKER + "heartbeat retry {0}", _heartbeatFailCount);
                        // Before to start new operation, flags the worker as stopped
                        // _workerRunning = false;
						// Ok restart worker
						restartWorker = true;
						// Executes the heartbeat
						Heartbeat();
					}
					else
					{
                        // No exec, starts the worker again
                        restartWorker = true;
						// DEBUG > Polling intervall not reached
						if (sos.logging.Logger.CanDebug(DEBUG_KEY_WORKER)) sos.logging.Logger.Debug(LOG_MARKER_WORKER + "heartbeat not executed, elapsed: {0}", elapsedHeartbeat.Milliseconds);
					}
                }
                // Worker restart
                if (restartWorker) WorkerStart();
            }
            catch (Exception ex)
            {
                // Manage unexpected error (disconnect with error)
                ManageUnexpectedError("Worker handler in error", ex);
            }
        }

        #endregion Worker handling

        #region Events

        private void RaiseConnectionStartEvent(GameClientEventArgs args)
        {
            if (ConnectionStart != null) ConnectionStart(args);
        }

        private void RaiseConnectionStopEvent(GameClientEventArgs args)
        {
            if (ConnectionStop != null) ConnectionStop(args);
        }

        private void RaiseConnectionLostEvent(GameClientEventArgs args)
        {
            if (ConnectionLost != null) ConnectionLost(args);
        }

        private void RaiseConnectionFailedEvent(GameClientEventArgs args)
        {
            if (ConnectionFailed != null) ConnectionFailed(args);
        }

        private void RaiseRequestExecutedEvent(GameClientRequestEventArgs args)
        {
            if (RequestExecuted != null) RequestExecuted(args);
        }

        private void RaiseRequestFailedEvent(GameClientRequestEventArgs args)
        {
            if (RequestFailed != null) RequestFailed(args);
        }

        private void RaiseRequestWaitingEvent(GameClientRequestEventArgs args)
        {
            if (RequestWaiting != null) RequestWaiting(args);
        }

        private void RaiseErrorEvent(GameClientErrorEventArgs args)
        {
            if (Error != null) Error(args);
        }

        #endregion Events

        #region Generic handling

        /// <summary>
        /// Attach event handlers methods used by the client
        /// </summary>
        private void AttachEventHandlers()
        {
            _provider.HeartbeatDone     += GameProvider_HeartbeatDone;
            _provider.HeartbeatFailed   += GameProvider_HeartbeatFailed;
            _provider.RequestDone       += GameProvider_RequestDone;
            _provider.RequestFailed     += GameProvider_RequestFailed;
        }

        /// <summary>
        /// Attach event handlers methods used by the client
        /// </summary>
        private void DetachEventHandlers()
        {
            _provider.HeartbeatDone     -= GameProvider_HeartbeatDone;
            _provider.HeartbeatFailed   -= GameProvider_HeartbeatFailed;
            _provider.RequestDone       -= GameProvider_RequestDone;
            _provider.RequestFailed     -= GameProvider_RequestFailed;
        }

        /// <summary>
        /// Updates the client internal status
        /// </summary>
        /// <param name="newStatus">New client status</param>
        private void SetStatus(GameClientStatus newStatus)
		{
			if (_status != newStatus)
			{
				var oldStatus = _status;
				_status = newStatus;
				// DEBUG > Trace status change
				if (sos.logging.Logger.CanDebug(DEBUG_KEY)) sos.logging.Logger.Debug(LOG_MARKER + "STATUS > changed {0} to {1}", oldStatus, newStatus);
			}
		}

        /// <summary>
        /// Resets the client interal status
        /// </summary>
		private void Reset()
		{
			// Set base status
			_heartbeatCount		= 0;
			_heartbeatFailCount	= 0;
			_heartbeatDate		= DateTime.MinValue;
			// Remove current request reference
			_request = null;
			// Empties the request queue
			_requests.Clear();
            // Request retry
            _requestRetryDate = DateTime.MinValue;
			// Polling
			_pollingCount = 0;
			_pollingDate = DateTime.MinValue;
			// STATUS > NOT CONNECTED
			SetStatus(GameClientStatus.NOT_CONNECTED);
			// DEBUG > Trace polling request call
			if (sos.logging.Logger.CanDebug(DEBUG_KEY)) sos.logging.Logger.Debug(LOG_MARKER + "RESET > done");
		}

        /// <summary>
        /// Game goes in error for unexpected problem or exception
        /// </summary>
        /// <param name="message">Message related to the unexpected error</param>
        /// <param name="ex">Exception that cause the unexpected error</param>
		private void ManageUnexpectedError(string message, Exception ex)
		{
			// Compose the error message
			var msg = "";
			msg += LOG_MARKER;
			if (!string.IsNullOrEmpty(message))
			{
				msg += " " + message;
			}
			// ERROR > log the complete error
			sos.logging.Logger.Error(msg, ex);
			// Dispatch EVENT_GAME_ERROR
			RaiseErrorEvent(new GameClientErrorEventArgs(this, ex));
			// Force the disconnection
			ConnectionClose(true);
		}

        /// <summary>
        /// Closes the connection
        /// </summary>
        /// <param name="closedByError">True if the connection will be close to handle an error</param>
        private void ConnectionClose(bool closedByError)
        {
			// Detach all the event handlers to avoid to handle running server calls
			DetachEventHandlers();
			// Stops the workder
            WorkerStop();
			// Reset the status
			Reset();
            // Dispatch 
            if (closedByError)
            {
			    // DEBUG > Connection closed by an error
			    if (sos.logging.Logger.CanDebug(DEBUG_KEY_CONNECTION)) sos.logging.Logger.Debug(LOG_MARKER_CONNECTION + "connection closed due internal error!");
                // Raises ConnectionLost event
                RaiseConnectionLostEvent(new GameClientEventArgs(this));
            }
            else
            {
			    // DEBUG > Manuale client disconnection
			    if (sos.logging.Logger.CanDebug(DEBUG_KEY_CONNECTION)) sos.logging.Logger.Debug(LOG_MARKER_CONNECTION + "stop");
                // Raises ConnectionStop event
                RaiseConnectionStopEvent(new GameClientEventArgs(this));
            }
        }

        #endregion Generic handling

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// Starts the connection with the server. The connecttion will be opened after the first heartbeat gets a successfull
        /// resposnse by the server.
        /// The connection will be preserver via heartbeat calls, event if the client is not used to send requests to the SoS server;
        /// this connection will preserve the server side session and will continue until the disconnection is manually required or
        /// an error occourrs
        /// </summary>
        public void Connect(User user, GameClientOptions options)
        {
			// Check connecting
			if (IsConnecting) throw new GameClientException("GameClient is connecting, connection can't be opened again!");
			// Check connection
			if (IsConnected) throw new GameClientException("GameClient is already connected, connection can't be opened again!");
            // Setup
            _options = options;
            _user = user;
			// Reset the status
			Reset();
			// Provider
            _providerHolder = new GameObject("GameClientGameObject");
            GameObject.DontDestroyOnLoad(_providerHolder);
            _provider = _providerHolder.AddComponent<GameProvider>();
            // Attach event handlers
            AttachEventHandlers();
			// STATUS > Connectiong
			SetStatus(GameClientStatus.CONNECTING);
			// DEBUG > Connection starting
			if (sos.logging.Logger.CanDebug(DEBUG_KEY_CONNECTION)) sos.logging.Logger.Debug(LOG_MARKER_CONNECTION + "starting...");
			// Starts the heartbeat to check the connection
			Heartbeat();
        }

        /// <summary>
        /// Disconnects the client if it's actually connected
        /// </summary>
        public void Disconnect()
        {
			// Check connection
			if (!IsConnected) throw new GameClientException("GameClient is not connected, connection can't be closed!");
            // Closes the connection
            ConnectionClose(false);
            // Destroy the GameObject
            GameObject.DestroyImmediate(_providerHolder);
        }

        /// <summary>
        /// Sends a GameRequest to be executeds by SoS server.
        /// </summary>
        /// <param name="request">Game request to be executed by the SoS server</param>
		public void Execute(GameRequest request)
		{
			// Check connection established
			if (!IsConnected) throw new GameClientException("GameClient is not connected, the GameRequest can't be executed");
			// Push the request in the list
			_requests.Add(request);
			// DEBUG > Remove the request from the queue
			if (sos.logging.Logger.CanDebug(DEBUG_KEY_REQUEST)) sos.logging.Logger.Debug(LOG_MARKER_REQUEST + "{0} request added", request);
		}

		#endregion PUBLIC METHODS


	}
}
