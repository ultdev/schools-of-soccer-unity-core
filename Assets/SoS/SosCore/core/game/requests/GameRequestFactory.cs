﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game.requests
{
    /// <summary>
    /// 
    /// </summary>
    public static class GameRequestFactory
    {

		#region CONSTANTS

		// Counters limits definition
		private const int	COUNTER_MIN             = 0;
		private const int	COUNTER_MAX             = 999999;

        // 
        private const int	ID_CHAR_NUMBER          = 6;

		#endregion CONSTANTS

		#region FIELDS

        private static int _counter = 0;

		#endregion FIELDS

		#region CONSTRUCTORS

		#endregion CONSTRUCTORS

		#region PROPERTIES

		#endregion PROPERTIES

		#region PRIVATE METHODS

		private static int NextCounter()
		{
			var next = _counter + 1;
			if (next >= COUNTER_MAX)
			{
				next = COUNTER_MIN;
			}
			return next;
		}
		
		private static string NextId()
		{

            // LT_TODO: algoritmo per la generazione dell'id, per ora solo id contatore formattato

			// Increment the counter
			_counter = NextCounter();
			// Returns the generated counter with leading 0
			return _counter.ToString("D6");
		}

		#endregion PRIVATE METHODS

		#region PUBLIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phase"></param>
        /// <param name="pars"></param>
        /// <returns></returns>
		public static GameRequest Create(GamePhase phase, GameRequestParams pars)
		{
			// Output request
			GameRequest request = null;
			// Create the right cation for type		
			     if (phase == GamePhase.SUBSTITUTE)         request = new GameRequestSubstitute(NextId());
			else if (phase == GamePhase.RESOLVE_SUBSTITUTE) request = new GameRequestResolveSubstitute(NextId());
			else if (phase == GamePhase.DISCARD)			request = new GameRequestDiscard(NextId());
			else if (phase == GamePhase.RESOLVE_DISCARD)	request = new GameRequestResolveDiscard(NextId());
			else if (phase == GamePhase.PLAY)				request = new GameRequestPlay(NextId());
			else if (phase == GamePhase.RESOLVE_PLAY)		request = new GameRequestResolvePlay(NextId());
			else if (phase == GamePhase.RESYNC)				request = new GameRequestResync(NextId());
			else throw new ArgumentException("GameRequestFactory.Create() erorr: Unknown game request type for phase '" + phase.Name + "'");
			// Initializes the request
			request.Init(pars);
			// Returns the new request
			return request;
		}

		#endregion PUBLIC METHODS

    }
}
