﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game.requests
{

    /// <summary>
    /// 
    /// </summary>
    public class GameRequestResult
    {

		#region CONSTANTS

		// Default values
		public static readonly GameRequestResultCode    DEFAULT_CODE                    = GameRequestResultCode.UNKNOWN;

		#endregion CONSTANTS

		#region FIELDS

		private GameRequestResultCode _code;
		private GameRequestData _data;
		private GameRequestError _error;

        #endregion FIELDS

        #region CONSTRUCTORS

        /// <summary>
        /// Creates an invalid request results to fill
        /// </summary>
        public GameRequestResult()
        {
            // 
            Clear();
        }

        /*

        /// <summary>
        /// Creates a request results that contains data to handle
        /// </summary>
        /// <param name="raw_data">Raw data reference (by now only json string is handled</param>
        public GameRequestResult(GameRequestResultCode code, object raw_data)
        {
            // 
            Clear();
            // Result code
            _code = code;
            // Creates the data contained by the response result
            _data = new GameRequestData(raw_data);  
        }

        /// <summary>
        /// Creates a request results that contains error data to handle
        /// </summary>
        /// <param name="error_code"></param>
        /// <param name="error_message"></param>
        public GameRequestResult(int error_code, string error_message)
        {
            // 
            Clear();
            // Creates the error data contained by the response result
            _error = new GameRequestError(error_code, error_message);
        }

        */

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public GameRequestResultCode Code
        {
            get { return _code; }
        }

        public GameRequestData Data
        {
            get { return _data; }
        }

		public bool HasData
		{
			get { return _data != null; }
		}

        public GameRequestError Error
        {
            get { return _error; }
        }

		public bool HasError
		{
			get { return _error != null; }
		}

		public bool Waiting
		{
			get {  return _code == GameRequestResultCode.WAIT; }
		}
		
		public bool Succeeded
		{
			get { return _code == GameRequestResultCode.SUCCESS; }
		}
		
		public bool IsInGameEnd
		{
			get {  return _code == GameRequestResultCode.GAME_END; }
		}
		
		public bool IsInError
        {
            get
            {
                return _code == GameRequestResultCode.ERROR ||
                       _code == GameRequestResultCode.EXCEPTION;
            }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        public void Clear()
		{
			_code	= GameRequestResultCode.UNKNOWN;
			_data	= null;
			_error  = null;
		}	

		#endregion PUBLIC METHODS

    }
}
