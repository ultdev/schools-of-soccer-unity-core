﻿using sos.logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game.requests
{
    public class GameRequestDiscard : GameRequest
    {

        #region CONSTRUCTORS

        public GameRequestDiscard(string requestId) : base(GamePhase.DISCARD, requestId) { }

        #endregion CONSTRUCTORS

    }
}
