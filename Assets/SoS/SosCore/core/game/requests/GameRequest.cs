﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game.requests
{
    /// <summary>
    /// 
    /// </summary>+
    public abstract class GameRequest
    {

		#region CONSTANTS

		#endregion CONSTANTS

		#region FIELDS

		private string _id;
		// private string _url; // Rimosso: è uguale per tutte le richieste, se lo gestirà il client
		private GamePhase _phase;
		private GameRequestParams _params;
		private GameRequestResult _result;
		private uint _retries;

		#endregion FIELDS

		#region CONSTRUCTORS

		/**
		 * Creates a new generic action
		 * @param actionType Type of the action
		 * @param actionId Unique id of the action
		 */		
		public GameRequest(GamePhase phase, /* string url, */ string id)
		{
			// Init
			_phase 			= phase;
			// _url			= url;
			_id 			= id;
			_retries		= 0;
		}

		#endregion CONSTRUCTORS

		#region PROPERTIES

		public string Id
		{
			get {  return _id; }
		}
		
		public GamePhase Phase
		{
			get {  return _phase; }
		}
		
		public GameRequestParams Params
		{
			get {  return _params; }
		}
		
		
		public uint Retries
		{
			get { return _retries; }
		}
		
		public GameRequestResult Result
		{
			get { return _result; }
		}
		
		public bool Executed
		{
			get {  return _result != null; }
		}
		
		public bool Succeeded
		{
			get { return _result != null && _result.Succeeded; }
		}
		
		public bool Waiting
		{
			get {  return _result != null && _result.Waiting; }
		}
		
		public bool IsInError
		{
			get { return _result != null && _result.IsInError; }
		}
		
		public bool IsRequest
		{
			get { return !_phase.Polling; }
		}
		
		public bool IsPollingRequest
		{
			get { return _phase.Polling; }
		}

        #endregion PROPERTIES

        #region PRIVATE METHODS

        /**
		 * Virtual method to override in descendant classes to update the status of the 
		 * action after the execution (currently not used)
		 */
        protected virtual void Setup(GameRequestParams pars)
        {
        }

        /**
		 * Virtual method to override in descendant classes to update the status of the 
		 * action after the execution (currently not used)
		 */
        protected virtual void Update(GameRequestResult result)
        {
        }

		#endregion PRIVATE METHODS

		#region PUBLIC METHODS

        /// <summary>
        /// Initializes the request with all the parameters required to execute the request
        /// </summary>
        /// <param name="pars">Parameters to attach to the request</param>
		public void Init(GameRequestParams pars)
		{
			// Setup to validate params for the request
			Setup(pars);
			// Save the param reference
			_params = pars;
		}

        /// <summary>
        /// Process the results data obtained with the request to the server
        /// </summary>
        /// <param name="result">Results object to process</param>
		public void Process(GameRequestResult result)
		{
			// If the action succeeded, update action status
			if (result.Succeeded)
			{
				Update(result);
			}
			// Save the result
			_result = result;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
		public bool Equals(GameRequest request)
		{
			return	_id		== request.Id		&&
					_phase	== request.Phase;
		}

        /// <summary>
        /// Handles the request retry, cancels the current result and increment the retry counter 
        /// </summary>
		public void Retry()
		{
			_retries++;
			_result = null;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0}::{1} (id: {2}, retries: {3})",
                                 GetType().FullName,
                                 _phase.Name,
                                 _id,
                                 _retries);
        }

        #endregion PUBLIC METHODS

    }
}
