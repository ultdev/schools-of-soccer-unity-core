﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game.requests
{
    public class GameRequestDiscardParams : GameRequestParams
    {

		#region CONSTANTS

		#endregion CONSTANTS

		#region FIELDS

        private List<GameActionCard>_cards = new List<GameActionCard>();

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameRequestDiscardParams(uint turn, uint gameID, uint userID, string password, string gameToken, string sessionToken) 
        : base(GamePhase.DISCARD, turn, gameID, userID, password, gameToken, sessionToken)
        {
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public List<GameActionCard> Cards
        {
            get { return _cards; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        #endregion PUBLIC METHODS

    }
}
