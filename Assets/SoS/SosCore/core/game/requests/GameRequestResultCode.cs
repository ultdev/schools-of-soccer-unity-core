﻿using sos.data;

namespace sos.core.game.requests
{
    public class GameRequestResultCode : Enumerator
    {

        #region CONSTANTS

		public static readonly  GameRequestResultCode   UNKNOWN			    = new GameRequestResultCode(9, 	"UNKNOWN");	    //
		public static readonly  GameRequestResultCode   GAME_END			= new GameRequestResultCode(2, 	"GAME_END");	//
		public static readonly  GameRequestResultCode   WAIT				= new GameRequestResultCode(1, 	"WAIT");	    //
		public static readonly  GameRequestResultCode   SUCCESS			    = new GameRequestResultCode(0, 	"SUCCESS");	    //
		public static readonly  GameRequestResultCode   ERROR				= new GameRequestResultCode(-1, "ERROR");	    //
		public static readonly  GameRequestResultCode   EXCEPTION           = new GameRequestResultCode(-2, "EXCEPTION");   //

        #endregion CONSTANTS

        #region CONSTRUCTORS
        
        protected GameRequestResultCode(int id, string name) : base(id, name) {}

        protected GameRequestResultCode(int id, string name, bool register) : base(id, name, null, register) {}

        #endregion CONSTRUCTORS

        #region STATIC METHODS

        public static string[] Names()
        {
            return Enumerators.Names(typeof(GameRequestResultCode));
        }

        public static GameRequestResultCode[] Values()
        {
            return Enumerators.Values<GameRequestResultCode>(typeof(GameRequestResultCode));
        }

        public static GameRequestResultCode GetById(int id)
        {
            return Enumerators.GetById<GameRequestResultCode>(typeof(GameRequestResultCode), id);
        }

        public static GameRequestResultCode GetByName(string name)
        {
            return Enumerators.GetByName<GameRequestResultCode>(typeof(GameRequestResultCode), name);
        }

        #endregion STATIC METHODS

    }
}
