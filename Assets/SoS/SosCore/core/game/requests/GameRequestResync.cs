﻿using sos.logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game.requests
{
    public class GameRequestResync : GameRequest
    {

        #region CONSTRUCTORS

        public GameRequestResync(string requestId) : base(GamePhase.RESYNC, requestId) { }

        #endregion CONSTRUCTORS

    }
}
