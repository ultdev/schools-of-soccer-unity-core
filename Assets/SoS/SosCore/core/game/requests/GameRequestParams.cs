﻿using sos.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game.requests
{
    public class GameRequestParams
    {

		#region CONSTANTS

		#endregion CONSTANTS

		#region FIELDS

		protected GamePhase _phase;
		protected uint _turn;
		protected uint _gameID; 		    // LT_TODO: rimpiazzare al più presto con il game token 
		protected uint _userID;		        // LT_TODO: rimpiazzare al più presto con il token di sessione
		protected string _password; 	    // LT_TODO: rimpiazzare al più presto con il token di sessione
		// protected stirng _actionData;    // LT_TODO: usato solo per scopi di test
		protected string _gameToken;
		protected string _sessionToken;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameRequestParams(GamePhase phase, uint turn, uint gameID, uint userID, string password, string gameToken, string sessionToken)
        {
		    _phase          = phase;
		    _turn           = turn;
		    _gameID         = gameID;
		    _userID         = userID;
		    _password       = password;
		    _gameToken      = gameToken;
		    _sessionToken   = sessionToken;
        }

		#endregion CONSTRUCTORS

		#region PROPERTIES

        public GamePhase Phase
        {
            get { return _phase; }
        }

        public uint Turn
        {
            get { return _turn; }
        }

        public uint GameId
        {
            get { return _gameID; }
        }

        public uint UserId
        {
            get { return _userID; }
        }

        public string Password
        {
            get { return _password; }
        }

        public string GameToken
        {
            get { return _gameToken; }
        }

        public string SessionToken
        {
            get { return _sessionToken; }
        }

        //public string ActionData
        //{
        //    get { return _actionData; }
        //}

		#endregion PROPERTIES

		#region PRIVATE METHODS

		#endregion PRIVATE METHODS

		#region PUBLIC METHODS

		public void Clear()
		{
			_phase			= GamePhase.RESYNC;
            _userID			= Defaults.INVALID_ID;
			_gameID			= Defaults.INVALID_ID; 
			_turn			= Game.INVALID_TURN;
			_password		= "";
			_sessionToken	= "";
			_gameToken		= "";
			// _actionData	= "";
		}

        public override string ToString()
        {
            return string.Format("{0} (phase: {1}, turn: {2}, game token: {3}, session token: {4}, game id {5}, user id: {6}, password: {7})",
                                 GetType().FullName,
                                 _phase.Name,
                                 _turn,
                                 _gameToken,
                                 _sessionToken,
                                 _gameID,
                                 _userID,
                                 _password);
        }

        #endregion PUBLIC METHODS

    }
}
