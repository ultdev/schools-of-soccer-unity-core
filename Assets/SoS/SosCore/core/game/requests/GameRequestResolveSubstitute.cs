﻿using sos.logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game.requests
{
    public class GameRequestResolveSubstitute : GameRequest
    {

        #region CONSTRUCTORS

        public GameRequestResolveSubstitute(string requestId) : base(GamePhase.RESOLVE_SUBSTITUTE, requestId) { }

        #endregion CONSTRUCTORS

    }
}
