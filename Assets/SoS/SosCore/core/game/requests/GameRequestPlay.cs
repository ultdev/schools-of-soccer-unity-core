﻿using sos.logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game.requests
{
    public class GameRequestPlay : GameRequest
    {

        #region CONSTRUCTORS

        public GameRequestPlay(string requestId) : base(GamePhase.PLAY, requestId) { }

        #endregion CONSTRUCTORS

    }
}
