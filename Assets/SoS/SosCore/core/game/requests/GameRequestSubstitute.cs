﻿using sos.logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game.requests
{
    public class GameRequestSubstitute : GameRequest
    {

        #region CONSTRUCTORS

        public GameRequestSubstitute(string requestId) : base(GamePhase.SUBSTITUTE, requestId) { }

        #endregion CONSTRUCTORS

    }
}
