﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game.requests
{
    public class GameRequestSubstituteParams : GameRequestParams
    {

		#region CONSTANTS

		#endregion CONSTANTS

		#region FIELDS

        private List<Position> _positions;

		#endregion FIELDS

		#region CONSTRUCTORS

        public GameRequestSubstituteParams(uint turn, uint gameID, uint userID, string password, string gameToken, string sessionToken) 
        : base(GamePhase.SUBSTITUTE, turn, gameID, userID, password, gameToken, sessionToken)
        {
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public List<Position> Positions
        {
            get
            {
                return _positions;
            }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        #endregion PUBLIC METHODS

    }
}
