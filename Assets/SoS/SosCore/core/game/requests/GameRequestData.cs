﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game.requests
{
    /// <summary>
    /// 
    /// </summary>
    public class GameRequestData
    {

		#region CONSTANTS

		#endregion CONSTANTS

		#region FIELDS

		private object _data;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameRequestData()
        {
            Clear();
        }

        public GameRequestData(object data) : this()
        {
            _data = data;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public object Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public bool IsEmpty
        {
            get { return _data != null; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

		public void Clear()
		{
			_data = null;
		}

        public string AsString()
        {
            return _data.ToString();
        }

        #endregion PUBLIC METHODS

    }
}
