﻿using sos.logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game.requests
{
    public class GameRequestResolveDiscard : GameRequest
    {

        #region CONSTRUCTORS

        public GameRequestResolveDiscard(string requestId) : base(GamePhase.RESOLVE_DISCARD, requestId) { }

        #endregion CONSTRUCTORS

    }
}
