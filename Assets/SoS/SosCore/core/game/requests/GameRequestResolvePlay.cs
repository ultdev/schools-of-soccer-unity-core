﻿using sos.logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game.requests
{
    public class GameRequestResolvePlay : GameRequest
    {

        #region CONSTRUCTORS
        
        public GameRequestResolvePlay(string requestId) : base(GamePhase.RESOLVE_PLAY, requestId) { }

        #endregion CONSTRUCTORS

    }
}
