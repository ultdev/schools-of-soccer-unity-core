﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game.requests
{
    public class GameRequestPlayParams : GameRequestParams
    {

		#region CONSTANTS

		#endregion CONSTANTS

		#region FIELDS

		private GameActionRole _role;
		private GameAction _action;
		private Position _target;
		private Dictionary<GameActionCard, GameActionCardTarget> _cards = new Dictionary<GameActionCard, GameActionCardTarget>();

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameRequestPlayParams(uint turn, uint gameID, uint userID, string password, string gameToken, string sessionToken) 
        : base(GamePhase.PLAY, turn, gameID, userID, password, gameToken, sessionToken)
        {
        }

        #endregion CONSTRUCTORS

		#region PROPERTIES

        public GameActionRole Role
        {
            get
            {
                return _role;
            }

            set
            {
                _role = value;
            }
        }

        public GameAction Action
        {
            get
            {
                return _action;
            }

            set
            {
                _action = value;
            }
        }

        public Position Target
        {
            get
            {
                return _target;
            }

            set
            {
                _target = value;
            }
        }

        public bool HasTarget
        {
            get { return _target != null; }
        }

        public Dictionary<GameActionCard, GameActionCardTarget> PlayedCards
        {
            get
            {
                return _cards;
            }
        }

        public bool HasPlayedCards
        {
            get
            {
                return _cards.Count > 0;
            }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        #endregion PUBLIC METHODS

    }
}
