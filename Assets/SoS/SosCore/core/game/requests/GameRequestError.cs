﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game.requests
{
    /// <summary>
    /// 
    /// </summary>
    public class GameRequestError
    {

		#region CONSTANTS

		// Default values
		public const int        DEFAULT_CODE                            = 0;
		public const string     DEFAULT_MESSAGE                         = "";

		#endregion CONSTANTS

		#region FIELDS

		private int     _code;
		private string  _message;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameRequestError()
        {
            Clear();
        }

        public GameRequestError(int code, string message) : this()
        {
            _code = code;
            _message = message;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public int Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

		public void Clear()
		{
			_code 		= DEFAULT_CODE;
			_message 	= DEFAULT_MESSAGE;			
		}

        #endregion PUBLIC METHODS

    }
}
