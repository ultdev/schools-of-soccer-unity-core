﻿using System;
using System.Collections.Generic;
using sos.data;

namespace sos.core.game
{

    /// <summary>
    /// 
    /// </summary>
    public class GameCastStatus : Enumerator
    {

        #region CONSTANTS

		public static readonly GameCastStatus UNKNOWN				= new GameCastStatus(0, "UNKNOWN");
		public static readonly GameCastStatus PLAYED				= new GameCastStatus(1, "PLAYED");
		public static readonly GameCastStatus COUNTERED			    = new GameCastStatus(2, "COUNTERED");
		public static readonly GameCastStatus CONDITION			    = new GameCastStatus(3, "COND_ERROR");          // (condizioni di giocabilità non soddisfatte)
		public static readonly GameCastStatus MAX_CAST_REACHED		= new GameCastStatus(4, "MAX_CAST_REACHED");    // (raggiunto il massimo numero di carte giocabili)
		public static readonly GameCastStatus LOW_STAMINA			= new GameCastStatus(5, "LOW_STAMINA");

        #endregion CONSTANTS

        #region CONSTRUCTORS

        public GameCastStatus(int id, string name) : this(id, name, true)
        {
        }

        public GameCastStatus(int id, string name, bool register) : base(id, name, string.Empty, register)
        {
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        #endregion PUBLIC METHODS

        #region STATIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string[] Names()
        {
            return Enumerators.Names(typeof(GameCastStatus));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static GameCastStatus[] Values()
        {
            return Enumerators.Values<GameCastStatus>(typeof(GameCastStatus));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GameCastStatus GetById(int id)
        {
            return Enumerators.GetById<GameCastStatus>(typeof(GameCastStatus), id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GameCastStatus GetByName(string name)
        {
            return Enumerators.GetByName<GameCastStatus>(typeof(GameCastStatus), name);
        }

        #endregion STATIC METHODS

    }
}
