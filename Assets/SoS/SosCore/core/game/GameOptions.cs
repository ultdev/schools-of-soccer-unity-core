﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game
{
    /// <summary>
    /// Configuration of the currente playing game
    /// </summary>
    public class GameOptions
    {

        #region CONSTANTS

		// Default values
		public const uint   DEFAULT_TURNS								    = 20;
		public const uint   DEFAULT_DECK_SIZE							    = 40;
		public const uint   DEFAULT_HAND_SIZE							    = 6;
		public const uint   DEFAULT_DISCARD_SIZE						    = 3;
		public const bool   DEFAULT_TIMEOUT_ENABLED				            = false; // ms
		public const uint   DEFAULT_TIMEOUT_PLAY_PHASE					    = 30000; // ms
		public const uint   DEFAULT_TIMEOUT_DISCARD_PHASE				    = 15000; // ms
		public const uint   DEFAULT_TIMEOUT_SUBSTITUTION_PHASE			    = 20000; // ms

        #endregion CONSTANTS

        #region FIELDS

		private uint _turns;
		private uint _deckSize;
		private uint _handSize;
		private uint _discardSize;
		private bool _timeoutEnabled;		    // true if the game handles turns timeout
		private uint _timeoutPlayPhase;			// milliseconds
		private uint _timeoutDiscardPhase;		// milliseconds
		private uint _timeoutSubstitutionPhase;	// milliseconds

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameOptions()
        {
            Clear();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        /// <summary>
        /// Number of turns for a game
        /// </summary>
        public uint Turns
        {
            get { return _turns; }
            set { _turns = value; }
        }

        /// <summary>
        /// Number of turns for period
        /// </summary>
		public uint TurnsForPeriod
		{
			get { return _turns / 2; }
		}
		
        /// <summary>
        /// Index of the first turn of the first period
        /// </summary>
		public uint FirstTurnOfFirstPeriod
		{
			get { return 1; }
		}
		
        /// <summary>
        /// Index of the first turn for the first period for a game
        /// </summary>
		public uint FirstTurnOfSecondPeriod
		{
            get { return FirstTurnOfFirstPeriod + TurnsForPeriod; }
		}

        /// <summary>
        /// Maximum number of cards into the player deck
        /// </summary>
        public uint DeckSize
        {
            get { return _deckSize; }
            set { _deckSize = value;  }
        }

        /// <summary>
        /// Maximum number of cards for the player hand
        /// </summary>
        public uint HandSize
        {
            get { return _handSize; }
            set { _handSize = value; }
        }

        /// <summary>
        /// Maximum number of cards trashable during the discard phase
        /// </summary>
        public uint DiscardSize
        {
            get { return _discardSize; }
            set { _discardSize = value; }
        }

        /// <summary>
        /// true if the game handles turns timeout
        /// </summary>
        public bool TimeoutEnabled
        {
            get { return _timeoutEnabled; }
            set { _timeoutEnabled = value; }
        }

        /// <summary>
        /// Number of milliseconds before the timeout for the play phase
        /// </summary>
        public uint TimeoutPlayPhase
        {
            get { return _timeoutPlayPhase; }
            set { _timeoutPlayPhase = value; }
        }

        /// <summary>
        /// Number of milliseconds before the timeout for the discard phase
        /// </summary>
        public uint TimeoutDiscardPhase
        {
            get { return _timeoutDiscardPhase; }
            set { _timeoutDiscardPhase = value; }
        }

        /// <summary>
        /// Number of milliseconds before the timeout for the substitution phase
        /// </summary>
        public uint TimeoutSubstitutionPhase
        {
            get { return _timeoutSubstitutionPhase; }
            set { _timeoutSubstitutionPhase = value; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// Retrieve the timeout (in milliseconds) for the given game phase
        /// </summary>
        /// <param name="phase">Game phase to retrieve the timeout</param>
        /// <returns>Timeout (in milliseconds) for the given game phase</returns>
		public uint GetGamePhaseTimeout(GamePhase phase)
		{
			// no timeout
            uint timeout = 0;
            // Checks if the given phase is active (requireds an user interaction)
			if (phase.Active)
			{
				     if (phase == GamePhase.PLAY)       timeout	= _timeoutPlayPhase;
				else if (phase == GamePhase.DISCARD)	timeout	= _timeoutDiscardPhase;
				else if (phase == GamePhase.SUBSTITUTE) timeout	= _timeoutSubstitutionPhase;
			}
			return timeout;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameOptions"></param>
        /// <returns></returns>
		public bool Equals(GameOptions gameOptions)
		{
			return	_turns						== gameOptions.Turns &&
					_deckSize					== gameOptions.DeckSize	&& 
					_handSize					== gameOptions.HandSize	&&
					_discardSize				== gameOptions.DiscardSize &&
					_timeoutEnabled				== gameOptions.TimeoutEnabled &&
					_timeoutPlayPhase			== gameOptions.TimeoutPlayPhase && 
					_timeoutDiscardPhase		== gameOptions.TimeoutDiscardPhase &&
					_timeoutSubstitutionPhase	== gameOptions.TimeoutSubstitutionPhase;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameOptions"></param>
		public void Copy(GameOptions gameOptions)
		{
			_turns						= gameOptions.Turns;
			_deckSize					= gameOptions.DeckSize;
			_handSize					= gameOptions.HandSize;
			_discardSize				= gameOptions.DiscardSize;
			_timeoutEnabled				= gameOptions.TimeoutEnabled;
			_timeoutPlayPhase			= gameOptions.TimeoutPlayPhase;
			_timeoutDiscardPhase		= gameOptions.TimeoutDiscardPhase;
			_timeoutSubstitutionPhase	= gameOptions.TimeoutSubstitutionPhase;
		}

		public GameOptions Clone()
		{
            GameOptions options = new GameOptions();
			options.Copy(this);
			return options;
		}	

        /// <summary>
        /// 
        /// </summary>
		public void Clear()
		{
			_turns						= DEFAULT_TURNS;
			_deckSize					= DEFAULT_DECK_SIZE;
			_handSize					= DEFAULT_HAND_SIZE;
			_discardSize				= DEFAULT_DISCARD_SIZE;
			_timeoutEnabled				= DEFAULT_TIMEOUT_ENABLED;
			_timeoutPlayPhase			= DEFAULT_TIMEOUT_PLAY_PHASE;
			_timeoutDiscardPhase		= DEFAULT_TIMEOUT_DISCARD_PHASE;
			_timeoutSubstitutionPhase	= DEFAULT_TIMEOUT_SUBSTITUTION_PHASE;
		}

        #endregion PUBLIC METHODS

    }
}
