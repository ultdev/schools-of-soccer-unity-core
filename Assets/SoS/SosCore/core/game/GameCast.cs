﻿using System;
using System.Collections.Generic;

using sos.utils;

namespace sos.core.game
{
    public class GameCast
    {

        #region CONSTANTS

		// Default values
		protected static readonly string            DEFAULT_NAME                            = "";
		protected static readonly string            DEFAULT_TEXT                            = "";
		protected static readonly uint              DEFAULT_CASTER_ID                       = Defaults.INVALID_ID;
		protected static readonly uint              DEFAULT_CARD_ID                         = Defaults.INVALID_ID;
		protected static readonly GameActionRole    DEFAULT_CASTER_ROLE                     = GameActionRole.NONE;
		protected static readonly GameCastStatus    DEFAULT_STATUS			                = GameCastStatus.UNKNOWN;
		protected static readonly int               DEFAULT_COST                            = 0;
		protected static readonly bool              DEFAULT_PERMANENT                       = false;

        #endregion CONSTANTS

        #region FIELDS

		private string _name;
		private uint _cardId;
		private string _text;
		private uint _casterId; 				// card instance id
		private GameActionRole _casterRole;
		private GameCastStatus _status;
		private int _cost;
		private bool _permanent;
		private List<GameCastEffect>_effects;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameCast()
        {
            // Init objects
            _effects = new List<GameCastEffect>();
            // 
            Clear();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public string Name
        {
            get { return _name;  }
            set { _name = value; }
        }

        public uint CardId
        {
            get { return _cardId; }
            set { _cardId = value; }
        }

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public uint CasterId
        {
            get { return _casterId; }
            set { _casterId = value; }
        }

        public GameActionRole CasterRole
        {
            get { return _casterRole; }
            set { _casterRole = value; }
        }

        public GameCastStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public int Cost
        {
            get { return _cost; }
            set { _cost = value; }
        }

        public bool Permanent
        {
            get { return _permanent; }
            set { _permanent = value; }
        }

        public List<GameCastEffect> Effects
        {
            get { return _effects; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

		public void Clear()
		{
			// Clear data
			_name		= DEFAULT_NAME;
			_cardId		= DEFAULT_CARD_ID;
			_text		= DEFAULT_TEXT;
			_casterId	= DEFAULT_CASTER_ID;
			_casterRole	= DEFAULT_CASTER_ROLE;
			_status		= DEFAULT_STATUS;
			_cost		= DEFAULT_COST;
			_permanent	= DEFAULT_PERMANENT;
			// Clear objects
            _effects.Clear();
		}

        #endregion PUBLIC METHODS

    }
}
