﻿using sos.utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game
{
    /// <summary>
    /// 
    /// </summary>
    public class GamePlayerProc
    {

        #region CONSTANTS

		// Default values
		protected static readonly uint  DEFAULT_PLAYERID					    = Defaults.INVALID_ID;

        #endregion CONSTANTS

        #region FIELDS

		private uint _playerId;
        private List<GameProc> _procs;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GamePlayerProc()
        {
			_procs = new List<GameProc>();
			// clears data
			Clear();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public uint PlayerId
        {
            get { return _playerId; }
            internal set { _playerId = value; }
        }

        public List<GameProc> Procs
        {
            get { return _procs; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

		public void Clear()
		{
			// Clear data
			_playerId	= DEFAULT_PLAYERID;	
			// Clear objects
			_procs.Clear();
		}

        #endregion PUBLIC METHODS

    }
}
