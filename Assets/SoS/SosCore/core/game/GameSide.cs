﻿using System;
using System.Collections.Generic;
using sos.data;

namespace sos.core.game
{

    /// <summary>
    /// Player field side during the game (Enumerator)
    /// </summary>
    public class GameSide : Enumerator
    {

        #region CONSTANTS

		public static readonly GameSide NONE         	        = new GameSide(0, "none");
		public static readonly GameSide HOME                 	= new GameSide(1, "home");
		public static readonly GameSide AWAY                	= new GameSide(2, "away");

        #endregion CONSTANTS

        #region CONSTRUCTORS

        public GameSide(int id, string name) : this(id, name, true)
        {
        }

        public GameSide(int id, string name, bool register) : base(id, name, string.Empty, register)
        {
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        #endregion PUBLIC METHODS

        #region STATIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string[] Names()
        {
            return Enumerators.Names(typeof(GameSide));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static GameSide[] Values()
        {
            return Enumerators.Values<GameSide>(typeof(GameSide));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GameSide GetById(int id)
        {
            return Enumerators.GetById<GameSide>(typeof(GameSide), id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GameSide GetByName(string name)
        {
            return Enumerators.GetByName<GameSide>(typeof(GameSide), name);
        }

        #endregion STATIC METHODS

    }
}
