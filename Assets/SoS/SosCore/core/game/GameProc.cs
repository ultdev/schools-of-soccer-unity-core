﻿using sos.utils;
using System;
using System.Collections.Generic;

namespace sos.core.game
{
    public class GameProc
    {

        #region CONSTANTS

		// Default values
		protected static readonly string    DEFAULT_LABEL               = "";
		protected static readonly uint      DEFAULT_SOURCE              = Defaults.INVALID_ID;

        #endregion CONSTANTS

        #region FIELDS

		private string _label;
		private uint _sourceId; 				// = 0 if basic player ability, instanceId of buff/gear/debuff otherwise
		private uint _sourcePlayer;
		private uint _sourceCard;
        private List<GameCastEffect> _effects;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameProc()
        {
			// Init objects
			_effects = new List<GameCastEffect>();
			// clears data
			Clear();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public string Label
        {
            get { return _label; }
            internal set { _label = value; }
        }

        public uint SourceId
        {
            get { return _sourceId; }
            internal set { _sourceId = value; }
        }

        public uint SourcePlayer
        {
            get { return _sourcePlayer; }
            internal set { _sourcePlayer = value; }
        }

        public uint SourceCard
        {
            get { return _sourceCard; }
            internal set { _sourceCard = value; }
        }

        public List<GameCastEffect> Effects
        {
            get { return _effects; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

		public void Clear()
		{
			// Clear data
			_label			= DEFAULT_LABEL;
			_sourceId		= DEFAULT_SOURCE;
			_sourcePlayer	= DEFAULT_SOURCE;
			_sourceCard		= DEFAULT_SOURCE;
			// Clear objects
            _effects.Clear();
		}

        #endregion PUBLIC METHODS

    }
}
