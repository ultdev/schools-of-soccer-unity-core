﻿using System;
using System.Collections.Generic;
using sos.core.cards;

namespace sos.core.game
{
    public class GameActionCard : ActionCard
    {

        #region CONSTANTS

        #endregion CONSTANTS

        #region FIELDS

        private List<GameTarget> _validTargets;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameActionCard() : base(CardKind.GAME_INSTANCE)
        {
            _validTargets = new List<GameTarget>();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public GameTarget[] ValidTargets
        {
            get { return _validTargets.ToArray(); }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        public void AddTarget(GameTarget target)
        {
            _validTargets.Add(target);
        }

        public override void Clear()
        {
            base.Clear();
            _validTargets.Clear();
        }

        #endregion PUBLIC METHODS

    }
}
