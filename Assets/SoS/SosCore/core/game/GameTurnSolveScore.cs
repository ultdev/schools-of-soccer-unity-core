﻿using sos.utils;
using System;
using System.Collections.Generic;

namespace sos.core.game
{
    public class GameTurnSolveScore
    {

        #region CONSTANTS

		// Default values
		protected static readonly uint      DEFAULT_GOALSCORED					    = 0;
		protected static readonly uint      DEFAULT_ATTACKER_ROLL				    = 0;
		protected static readonly uint      DEFAULT_ATTACKER_SCORE				    = 0;
		protected static readonly uint      DEFAULT_DEFENDER_ROLL				    = 0;
		protected static readonly uint      DEFAULT_DEFENDER_SCORE				    = 0;
        protected static readonly uint      DEFAULT_SHOOTER_ID                      = Defaults.INVALID_ID;
        protected static readonly uint      DEFAULT_GOALKEEPER_ID                   = Defaults.INVALID_ID;
		protected static readonly GameRole  DEFAULT_WINNER					        = GameRole.IDLE;

        #endregion CONSTANTS

        #region FIELDS

        protected uint _goalScored;
        protected uint _attackerRoll;
        protected uint _defenderRoll;
        protected uint _attackerScore;
        protected uint _defenderScore;
        protected uint _shooterId;
        protected uint _goalkeeperId;
        protected GameRole _autoWinner;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameTurnSolveScore()
        {
            Clear();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public uint AttackerRoll
        {
            get { return _attackerRoll; }
            internal set { _attackerRoll = value; }
        }

        public uint DefenderRoll
        {
            get { return _defenderRoll; }
            internal set { _defenderRoll = value; }
        }

        public uint AttackerScore
        {
            get { return _attackerScore; }
            internal set { _attackerScore = value; }
        }

        public uint DefenderScore
        {
            get { return _defenderScore; }
            internal set { _defenderScore = value; }
        }

        public uint GoalScored
        {
            get { return _goalScored; }
            internal set { _goalScored = value; }
        }

        public uint ShooterId
        {
            get { return _shooterId; }
            internal set { _shooterId = value; }
        }

        public uint GoalkeeperId
        {
            get { return _goalkeeperId; }
            internal set { _goalkeeperId = value; }
        }

        public GameRole AutoWinner
        {
            get { return _autoWinner; }
            internal set { _autoWinner = value; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        public void Clear()
        {
            _goalScored     = DEFAULT_GOALSCORED;
            _autoWinner     = DEFAULT_WINNER;
            _attackerRoll   = DEFAULT_ATTACKER_ROLL;
            _defenderRoll   = DEFAULT_DEFENDER_ROLL;
            _attackerScore  = DEFAULT_ATTACKER_SCORE;
            _defenderScore  = DEFAULT_DEFENDER_SCORE;
            _shooterId      = DEFAULT_SHOOTER_ID;
            _goalkeeperId   = DEFAULT_GOALKEEPER_ID;
        }

        #endregion PUBLIC METHODS   

    }
}
