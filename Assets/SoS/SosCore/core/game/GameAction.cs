﻿using System;
using sos.data;
using sos.utils;

namespace sos.core.game
{
    /// <summary>
    /// 
    /// </summary>
    public class GameAction : Enumerator
    {

        #region CONSTANTS

		public static readonly GameAction PASS						= new GameAction(1, "pass", 	new Position[] { Position.DEFENDER_01, Position.DEFENDER_02, Position.MIDFIELD, Position.FORWARD_01, Position.FORWARD_02});
		public static readonly GameAction DRIBBLE                	= new GameAction(2, "dribble", 	new Position[] { Position.DEFENDER_01, Position.DEFENDER_02, Position.MIDFIELD, Position.FORWARD_01, Position.FORWARD_02});
        public static readonly GameAction CROSS                     = new GameAction(3, "cross",    new Position[] { Position.FORWARD_01, Position.FORWARD_02 });
		public static readonly GameAction SHOOT                	    = new GameAction(4, "shoot", 	new Position[] { Position.FORWARD_01, Position.FORWARD_02 });

        #endregion CONSTANTS

        #region FIELDS

        private Position[] _positions;	// posizioni da cui l'azione è giocabile

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameAction(int id, string name, Position[] positions) : this(id, name, positions, true)
        {
        }

        public GameAction(int id, string name, Position[] positions, bool register) : base(id, name, string.Empty, register)
        {
            _positions = positions;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public Position[] Positions
        {
            get { return _positions; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// Check if an action type is allowed in a given position 
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
		public bool Allowed(Position position)
		{
            return Array.IndexOf<Position>(_positions, position) > Defaults.INVALID_INDEX;
		}

        #endregion PUBLIC METHODS

        #region STATIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string[] Names()
        {
            return Enumerators.Names(typeof(GameAction));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static GameAction[] Values()
        {
            return Enumerators.Values<GameAction>(typeof(GameAction));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GameAction GetById(int id)
        {
            return Enumerators.GetById<GameAction>(typeof(GameAction), id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GameAction GetByName(string name)
        {
            return Enumerators.GetByName<GameAction>(typeof(GameAction), name);
        }

        #endregion STATIC METHODS

    }
}
