﻿using System;
using System.Collections.Generic;

namespace sos.core.game
{
    public class GameFormation
    {

        #region CONSTANTS

        #endregion CONSTANTS

        #region FIELDS

		private Dictionary<Position, GamePlayerCard> _formation;
		private GameTeam _gameTeam;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameFormation(GameTeam gameTeam)
        {
            // Init
            _gameTeam = gameTeam;
            // Init objects
            _formation = new Dictionary<Position, GamePlayerCard>();
            _formation.Add(Position.GOALKEEPER,     null);
            _formation.Add(Position.DEFENDER_01,    null);
            _formation.Add(Position.DEFENDER_02,    null);
            _formation.Add(Position.MIDFIELD,       null);
            _formation.Add(Position.FORWARD_01,     null);
            _formation.Add(Position.FORWARD_01,     null);
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public GamePlayerCard Goalkeeper
        {
            get { return GetPlayer(Position.GOALKEEPER); }
        }

        public GamePlayerCard Defender01
        {
            get { return GetPlayer(Position.DEFENDER_01); }
        }

        public GamePlayerCard Defender02
        {
            get { return GetPlayer(Position.DEFENDER_02); }
        }

        public GamePlayerCard Midfield
        {
            get { return GetPlayer(Position.MIDFIELD); }
        }

        public GamePlayerCard Forward01
        {
            get { return GetPlayer(Position.FORWARD_01); }
        }

        public GamePlayerCard Forward02
        {
            get { return GetPlayer(Position.FORWARD_02); }
        }

        public bool HasGoalkeeper
        {
            get { return HasPlayerByPosition(Position.GOALKEEPER); }
        }

        public bool HasDefender01
        {
            get { return HasPlayerByPosition(Position.DEFENDER_01); }
        }

        public bool HasDefender02
        {
            get { return HasPlayerByPosition(Position.DEFENDER_02); }
        }

        public bool HasMidfield
        {
            get { return HasPlayerByPosition(Position.MIDFIELD); }
        }

        public bool HasForward01
        {
            get { return HasPlayerByPosition(Position.FORWARD_01); }
        }

        public bool HasForward02
        {
            get { return HasPlayerByPosition(Position.FORWARD_02); }
        }

		public bool IsPlayerFormation
		{
            get { return _gameTeam.IsPlayerTeam; }
		}

		public bool IsValid
		{
            get
            {
                return HasGoalkeeper &&
                        HasDefender01 &&
                        HasDefender02 &&
                        HasMidfield &&
                        HasForward01 &&
                        HasForward02;
            }
		}

        #endregion PROPERTIES

        #region PRIVATE METHODS

        /// <summary>
        /// Updates a fomation player card status using the provided player and provided destination
        /// position 
        /// </summary>
        /// <param name="updatedPosition"></param>
        /// <param name="updatedPlayer"></param>
		protected void UpdatePlayer(Position updatedPosition, GamePlayerCard updatedPlayer)
		{
			// Locals
			bool changed = false;
			bool toSwap = false;
			GamePlayerCard player = null; 
			// Retrieve the player from the deck using the id if the team represents player team,
			// otherwise uses the internal reference, if is not loaded uses the provided reference
			if (IsPlayerFormation)
			{
				// Uses deck reference, player team
				player = _gameTeam.Game.Deck.FindPlayerCard(updatedPlayer.InstanceId);
			}
			else if (HasPlayerById(updatedPlayer.InstanceId))
			{
				// Uses internal reference, already loaded for opponent team
				player = GetPlayerById(updatedPlayer.InstanceId);
			}
			else
			{
				// Uses the external player reference, first load for opponent team
				player = updatedPlayer;
				changed = true;
			}
			// Generic position
            Position playerPosition = Position.ANY;
			// Check if the player exists
			if (player != null)
			{
				// Apply the change only if the player is changed
				if (!changed && !player.Equals(updatedPlayer))
				{
					// Updates the card with the current values
					player.Copy(updatedPlayer);
					// Player changed
					changed = true;
				}
				// Check if the formation already has the player
				if (!changed && HasPlayerById(player.InstanceId))
				{
					// Retrieve the player current position in the formation
					playerPosition = GetPlayerPosition(player);
					// Check if the player position is changed
					toSwap = (updatedPosition != playerPosition);
				}
				else
				{
					// Sets the player reference into the right position
					_formation[updatedPosition] = player;
					// Apply the player position
					player.UpdatePosition(updatedPosition);
				}
				// If player position is changed, performs players swap 
				if (toSwap)
				{
					// Swap the players
					SwapPlayers(playerPosition, updatedPosition);
				}
			}
			else
			{
				// ERROR > Player not found in the deck
                throw new Exception(string.Format("GamePlayerCard with id {0} does not exists in GameDeck", updatedPlayer.InstanceId));
			}
		}

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

		public bool HasPlayerByPosition(Position position)
		{
			return _formation[position] != null;
		}

		public bool HasPlayerById(uint instanceId)
		{
			return GetPlayerById(instanceId) != null;
		}

		public GamePlayerCard GetPlayer(Position position)
		{
			return _formation[position];
		}

		public Position GetPlayerPosition(GamePlayerCard player)
		{
            foreach (KeyValuePair<Position, GamePlayerCard> pair in _formation)
            {
                if (pair.Value.InstanceId == player.InstanceId) return pair.Key;
            }
            throw new ArgumentException("GamePlayerCard position can't be retrieved, formation doesn't contains the given GamePlayerCard");
		}

		public GamePlayerCard GetPlayerById(uint instanceId)
		{
            foreach (KeyValuePair<Position, GamePlayerCard> pair in _formation)
			{
                if (pair.Value.InstanceId == instanceId)
				{
                    return pair.Value;
				}
			}
            throw new ArgumentException(string.Format("Formation doesn't contains a GamePlayerCard for the given instanceId ({0})", instanceId));
		}

		public List<GamePlayerCard> GetPlayers(Boolean includeSubstitutions)
		{
            List<GamePlayerCard> players = new List<GamePlayerCard>();
            foreach (KeyValuePair<Position, GamePlayerCard> pair in _formation)
			{
                if (includeSubstitutions || (!includeSubstitutions && pair.Key.IsSubstitution == false))
                {
                    players.Add(pair.Value);
                }
            }
            return players;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
		public void SwapPlayers(Position source, Position destination)
		{
			// Goalkeeper check
			if (source == Position.GOALKEEPER) 		throw new ArgumentException("GOALKEEPER can't be a swap source");
			if (destination == Position.GOALKEEPER) throw new ArgumentException("GOALKEEPER can't be a swap destination");
			// Retrieve players to swap
            GamePlayerCard sourcePlayer         = GetPlayer(source);
            GamePlayerCard destinationPlayer    = GetPlayer(source);
			// Swap players
			_formation[source] = destinationPlayer;
			_formation[destination] = sourcePlayer;
			// Update player positions
			destinationPlayer.UpdatePosition(source);
			sourcePlayer.UpdatePosition(destination);
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameTeamFormation"></param>
        /// <returns></returns>
		public bool Equals(GameFormation gameTeamFormation)
		{
			return  ( (!HasGoalkeeper	&& !gameTeamFormation.HasGoalkeeper)    || GetPlayer(Position.GOALKEEPER).Equals(gameTeamFormation.Goalkeeper)  ) &&
                    ( (!HasDefender01   && !gameTeamFormation.HasDefender01)    || GetPlayer(Position.DEFENDER_01).Equals(gameTeamFormation.Defender01) ) &&
                    ( (!HasDefender02   && !gameTeamFormation.HasDefender02)    || GetPlayer(Position.DEFENDER_02).Equals(gameTeamFormation.Defender02) ) &&
                    ( (!HasMidfield     && !gameTeamFormation.HasMidfield)      || GetPlayer(Position.MIDFIELD).Equals(gameTeamFormation.Midfield)      ) &&
                    ( (!HasForward01    && !gameTeamFormation.HasForward01)     || GetPlayer(Position.FORWARD_01).Equals(gameTeamFormation.Forward01)   ) &&
                    ( (!HasForward02    && !gameTeamFormation.HasForward02)		|| GetPlayer(Position.FORWARD_02).Equals(gameTeamFormation.Forward02)	);
		}

        /// <summary>
        /// Updated all the GamePlayerCard in the formation, update player positions
        /// </summary>
        /// <param name="formation"></param>
		public void Update(GameFormation formation)
		{
			// Updates the player cards
			UpdatePlayer(Position.GOALKEEPER, 		formation.Goalkeeper);
			UpdatePlayer(Position.DEFENDER_01,		formation.Defender01);
			UpdatePlayer(Position.DEFENDER_02,		formation.Defender02);
			UpdatePlayer(Position.MIDFIELD,			formation.Midfield);
			UpdatePlayer(Position.FORWARD_01,		formation.Forward01);
			UpdatePlayer(Position.FORWARD_02,		formation.Forward02);
		}

		public void Clear()
		{
			// Clear all game player cards
			_formation[Position.GOALKEEPER] 	= null;
			_formation[Position.DEFENDER_01]	= null;
			_formation[Position.DEFENDER_02]	= null;
			_formation[Position.MIDFIELD]   	= null;
			_formation[Position.FORWARD_01] 	= null;
			_formation[Position.FORWARD_02] 	= null;
		}

        #endregion PUBLIC METHODS

    }
}
