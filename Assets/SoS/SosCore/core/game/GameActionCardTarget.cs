﻿using sos.utils;
using System;
using System.Collections.Generic;

namespace sos.core.game
{
    /// <summary>
    /// 
    /// </summary>
    public class GameActionCardTarget
    {

        #region FIELDS

        private TargetType _type;
        private List<GameActionCardTargetParam> _params;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameActionCardTarget(TargetType type)
        {
            _type = type;
            _params = new List<GameActionCardTargetParam>();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public TargetType Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public GameActionCardTargetParam[] Params
        {
            get { return _params.ToArray(); }
        }

        public bool HasParams
        {
            get { return _params.Count > 0; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        protected int GetParamIndexByName(string name)
        {
            for (int i = 0; i < _params.Count; i++)
            {
                if (_params[i].Name.Equals(name)) return i;
            }
            return Defaults.INVALID_INDEX;
        }

        /// <summary>
        /// Validate a card target param, checking if the passed value is an instance of the required class
        /// </summary>
        /// <param name="name">Name of the param to validate</param>
        /// <param name="value">Value of the param to validate</param>
        /// <returns>True if the value is valid per the current target type</returns>
		protected bool ValidateParam(string name, object value)
		{
            /* DISABILITATO IN ATTESA DI VERIFICA
			bool valid = false;

            // Check action card target type to allow params of the correc type
            if (_type == TargetType.POSITION)
            {
                valid = value is Position;
            }
            else if (_type == TargetType.EFFECT)
            {
                valid = value is GameMod;
            }
            else if (_type == TargetType.GRAVEYARD)
            {
                valid = value is GameActionCard;
            }
			return valid;
             * */
            return true;
		}

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        public GameActionCardTargetParam GetParam(string name)
        {
            GameActionCardTargetParam param = null;
            foreach (GameActionCardTargetParam p in _params) { if ( p.Name.Equals(name) ) param = p; break; }
            return param;
        }

        public void AddParam(string name, object value)
        {
            if (ValidateParam(name, value))
            {
                _params.Add(new GameActionCardTargetParam(name, value));
            }
            else throw new ArgumentException("Param " + name + " of is not valid for TargetType " + _type.ToString());
        }

        public void RemoveParam(int index)
        {
            if (index < 0 || index >= _params.Count) throw new ArgumentException("Param for index " + string.Format("{0}", index) + " doesn't exists");
            _params.RemoveAt(index);
        }

        public void RemoveParamByName(string name)
        {
            int index = GetParamIndexByName(name);
            if (index != Defaults.INVALID_INDEX)
            {
                _params.RemoveAt(index);
            }
            else throw new ArgumentException("Param with name " + name + " doesn't exists");
        }

        public void ClearParams()
        {
            _params.Clear();
        }

        #endregion PUBLIC METHODS

    }
}
