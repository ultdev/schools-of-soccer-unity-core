﻿using System;
using System.Collections.Generic;

namespace sos.core.game
{
    public class GameTurns
    {

        #region CONSTANTS

        #endregion CONSTANTS

        #region FIELDS

        private List<GameTurn> _turns;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameTurns()
        {
            _turns = new List<GameTurn>();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public GameTurn this[int index]
        {
            get { return _turns[index]; }
        }

        public int Count
        {
            get { return _turns.Count; }
        }

        public GameTurn LastTurn
        {
            get { return _turns[_turns.Count - 1]; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        public void Add(GameTurn turn)
        {
            // LT_TODO: verifica esistenza turno
            _turns.Add(turn);
        }

        public void Clear()
        {
            _turns.Clear();
        }

        #endregion PUBLIC METHODS

    }
}
