﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game
{
    public class GameProcHolder
    {

        #region CONSTANTS

		// Property names
		public static readonly string   PHASE_START_OF_TURN				= "sot";
		public static readonly string   PHASE_SCORE				        = "score";
		public static readonly string   PHASE_END_OF_TURN				= "eot";		

        #endregion CONSTANTS

        #region FIELDS

        public Dictionary<string, List<GamePlayerProc>> _procs;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameProcHolder()
        {
            _procs = new Dictionary<string, List<GamePlayerProc>>();
            _procs.Add(PHASE_START_OF_TURN,  new List<GamePlayerProc>());
            _procs.Add(PHASE_END_OF_TURN,    new List<GamePlayerProc>());
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public List<GamePlayerProc> StartOfTurnProcs
        {
            get { return _procs[PHASE_START_OF_TURN]; }
        }

        public List<GamePlayerProc> EndOfTurnProcs
        {
            get { return _procs[PHASE_END_OF_TURN]; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        public GamePlayerProc GetPhaseProcsByPlayer(string phase, uint playerId)
        {
            foreach (GamePlayerProc gpc in _procs[phase])
            {
                if ((gpc.PlayerId == playerId) && (gpc.Procs.Count > 0))
                    return gpc;
            }
            return null;
        }

		public void Clear()
		{
			// Clear objects
			_procs[PHASE_START_OF_TURN].Clear();
			_procs[PHASE_END_OF_TURN].Clear();
		}

        #endregion PUBLIC METHODS

    }
}
