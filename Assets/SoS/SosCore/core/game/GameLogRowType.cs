﻿using System;
using System.Collections.Generic;
using sos.data;

namespace sos.core.game
{
    /// <summary>
    /// 
    /// </summary>
    public class GameLogRowType : Enumerator
    {

        #region CONSTANTS

		public static readonly GameLogRowType   DEBUG			        = new GameLogRowType(1,     "debug", 		    0);
		public static readonly GameLogRowType   CAST				    = new GameLogRowType(2,     "cast",			    1000);
		public static readonly GameLogRowType   CAST_INIT		        = new GameLogRowType(3,     "castInit",		    0);
		public static readonly GameLogRowType   MAIN				    = new GameLogRowType(4,     "solveMain",	    2800);		
		public static readonly GameLogRowType   SCORE			        = new GameLogRowType(5,     "solveScore",	    2800);
		public static readonly GameLogRowType   INIT				    = new GameLogRowType(6,     "init",			    1000);
		public static readonly GameLogRowType   ABILITY			        = new GameLogRowType(7,     "ability",		    3000);
		public static readonly GameLogRowType   SWAP				    = new GameLogRowType(8,     "swap",			    1500);
		public static readonly GameLogRowType   CAST_END			    = new GameLogRowType(9,     "castEnd",		    4000);
		public static readonly GameLogRowType   DISCARD			        = new GameLogRowType(10,    "discard",		    700);
		public static readonly GameLogRowType   PROC				    = new GameLogRowType(11,    "proc",		        700);
		public static readonly GameLogRowType   END_MAIN			    = new GameLogRowType(12,    "endSolveMain",     3000);
		public static readonly GameLogRowType   END_SCORE		        = new GameLogRowType(13,    "endSolveScore",    3000);
		public static readonly GameLogRowType   INTERRUPT		        = new GameLogRowType(14,    "interrupt",	    3000);

        #endregion CONSTANTS

        #region CONSTRUCTORS

        public GameLogRowType(int id, string name, uint delay) : this(id, name, delay, true)
        {
        }

        public GameLogRowType(int id, string name, uint delay, bool register) : base(id, name, string.Empty, register)
        {
            _delay = delay;
        }

        #endregion CONSTRUCTORS

        #region FIELDS

        private uint _delay;

        #endregion FIELDS

        #region PROPERTIES

        public uint Delay
        {
            get { return _delay; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        #endregion PUBLIC METHODS

        #region STATIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string[] Names()
        {
            return Enumerators.Names(typeof(GameLogRowType));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static GameLogRowType[] Values()
        {
            return Enumerators.Values<GameLogRowType>(typeof(GameLogRowType));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GameLogRowType GetById(int id)
        {
            return Enumerators.GetById<GameLogRowType>(typeof(GameLogRowType), id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GameLogRowType GetByName(string name)
        {
            return Enumerators.GetByName<GameLogRowType>(typeof(GameLogRowType), name);
        }

        #endregion STATIC METHODS

    }
}
