﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game
{
    /// <summary>
    /// 
    /// </summary>
    public class GameStats : Stats
    {

        #region CONSTANTS

		// Default values
		private const int	DEFAULT_YELLOW_CARDS                    = 0;
		private const int   DEFAULT_RED_CARDS                       = 0;

        #endregion CONSTANTS

        #region FIELDS

		protected uint _yellowCards;
		protected uint _redCards;

        #endregion FIELDS

        #region CONSTRUCTORS

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public uint YellowCards
        {
            get { return _yellowCards; }
            set { _yellowCards = value; }
        }

        public uint RedCards
        {
            get { return _redCards; }
            set { _redCards = value; }
        }

		public bool IsExpelled
		{
			get { return _redCards > 0; }
		}
		
		public bool CanPlay
		{
			get { return _stamina > 0; }
		}

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        ///  Recalculates the status, merging the base stats and the total modifiers contained
        ///  into the 'mod' GameStats perameter
        /// </summary>
        /// <param name="stats">Base stats (fixed)</param>
        /// <param name="mod">Total stats modifiers to apply to the base stats</param>
        public void Set(Stats stats, GameStats mod)
        {
            // Temporary stats
            GameStats temp = new GameStats();
            // Copy the base stats
            temp.Copy(stats);
            // Update stats with the mods totals
            temp.Update(mod);
            // Copy in the current status
            Copy(temp);
        }

        /// <summary>
        /// Creates a new Stats object as the result of the merge (sum) of the values of each Stat value
        /// </summary>
        /// <param name="stats"></param>
        /// <returns></returns>
		public override Stats Merge(Stats stats)
		{
			// Check stats type
			if (stats is GameStats)
			{
				// Cast Stats to GameStats
				GameStats gameStats = stats as GameStats;
				// Create the new object
                GameStats delta = new GameStats();
				// Stats merge
				delta.Stamina 		= _stamina		+ stats.Stamina;
				delta.Dribbling 	= _dribiling	+ stats.Dribbling;
				delta.Passing 		= _passing		+ stats.Passing;
				delta.Scoring 		= _scoring		+ stats.Scoring;
				delta.Defense 		= _defense		+ stats.Defense;
				delta.Goalkeeping	= _goalkeeping	+ stats.Goalkeeping;
				// Cars values are not merged but copied
				delta.YellowCards = gameStats.YellowCards;
				delta.RedCards 	  = gameStats.RedCards;
				// Returns the new object
				return delta;
			}
			else
			{
				return base.Merge(stats);
			}
		}

        /// <summary>
        /// Updates the current status, merging the values of the given object (sum)
        /// </summary>
        /// <param name="stats"></param>
		public override void Update(Stats stats)
		{
			// Check stats type
			if (stats is GameStats)
			{
				// Merges the current stats with the provided one
                GameStats merge = Merge(stats) as GameStats;
				// Copies data from the merged object
				Copy(merge);
			}
			else
			{
				base.Update(stats);
			}
		}

        /// <summary>
        /// Copies the the given Stats object
        /// </summary>
        /// <param name="stats"></param>
		public override void Copy(Stats stats)
		{
			if (stats is GameStats)
			{
				// Cast to game stats
                GameStats gameStats = stats as GameStats;
				// Check if data is different from the current 
				if (!Equals(gameStats))
				{
					// Ancestor copy
					base.Copy(stats);
					// Copy data
					_yellowCards 	= gameStats.YellowCards; 
					_redCards		= gameStats.RedCards;
				}
			}
			else
			{
				base.Copy(stats);
			}
		}

        /// <summary>
        /// Clones the current object inot a new object
        /// </summary>
        /// <returns></returns>
		public override Stats Clone()
		{
			GameStats stats = new GameStats();
			stats.Copy(this);
			return stats;
		}
		
		public override void Clear()
		{
			// Ancestor clear
			base.Clear();
			// Clear data
			_yellowCards 	= DEFAULT_YELLOW_CARDS; 
			_redCards		= DEFAULT_RED_CARDS;
		}

        #endregion PUBLIC METHODS

    }
}
