﻿using System;
using sos.data;

namespace sos.core.game
{
    public class GamePhase : Enumerator
    {

        #region CONSTANTS

		public static readonly GamePhase    SUBSTITUTE				        = new GamePhase(1, "SUBSTITUTE", 			false, 	true);
		public static readonly GamePhase    RESOLVE_SUBSTITUTE              = new GamePhase(2, "RESOLVE_SUBSTITUTE", 	true, 	false);
		public static readonly GamePhase    DISCARD                	        = new GamePhase(3, "DISCARD", 				false, 	true);
		public static readonly GamePhase    RESOLVE_DISCARD        	        = new GamePhase(4, "RESOLVE_DISCARD", 		true, 	false);
		public static readonly GamePhase    PLAY                		    = new GamePhase(5, "PLAY", 					false, 	true);
		public static readonly GamePhase    RESOLVE_PLAY                    = new GamePhase(6, "RESOLVE_PLAY", 			true, 	false);
		public static readonly GamePhase    RESYNC                	        = new GamePhase(8, "RESYNC", 				false, 	false);
		
		/* SERVER VALUES
		1 = sendSubs
		2 = solveSubs [polling]
		3 = sendDiscard
		4 = solveDiscard [polling]
		5 = sendPlay
		6 = solvePlay [polling]
		7 = getGameInfo [NO-WRAP]
		8 = getGameInfo 
		9 = getRoundLog [NO-WRAP]
		10 = getRoundLog 
		*/

        #endregion CONSTANTS

        #region CONSTRUCTORS

        public GamePhase(int id, string name, bool polling, bool active) : this(id, name, polling, active, true)
        {
        }

        public GamePhase(int id, string name, bool polling, bool active, bool register) : base(id, name, string.Empty, register)
        {
            _polling = polling;
            _active = active;
        }

        #endregion CONSTRUCTORS

        #region FIELDS

		private bool _polling;	// Polling flag, if true the client repeat the request until a valid result is returned
		private bool _active;	// Active flag, if false the engine automatically sends the request when the phase starts

        #endregion FIELDS

        #region PROPERTIES

        /// <summary>
        /// Polling flag, if true the client repeat the request until a valid result is returned
        /// </summary>
        public bool Polling
        {
            get { return _polling; }
        }

        /// <summary>
        /// Active flag, if false the engine automatically sends the request when the phase starts
        /// </summary>
        public bool Active
        {
            get { return _active; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        #endregion PUBLIC METHODS

        #region STATIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string[] Names()
        {
            return Enumerators.Names(typeof(GamePhase));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static GamePhase[] Values()
        {
            return Enumerators.Values<GamePhase>(typeof(GamePhase));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GamePhase GetById(int id)
        {
            return Enumerators.GetById<GamePhase>(typeof(GamePhase), id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GamePhase GetByName(string name)
        {
            return Enumerators.GetByName<GamePhase>(typeof(GamePhase), name);
        }

        #endregion STATIC METHODS

    }
}
