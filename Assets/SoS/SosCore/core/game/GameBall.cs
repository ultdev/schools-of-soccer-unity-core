﻿using System;

namespace sos.core.game
{
    /// <summary>
    /// Represents the ball into the game field
    /// </summary>
    public class GameBall
    {

        #region FIELDS

        private Position _position;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameBall()
        {
            Clear();
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public Position Position
        {
            get { return _position; }
            set { _position = value; }
        }

        public Position OppositePosition
        {
            get { return Position.GetOpposite(_position); }
        }

        #endregion PROPERTIES

        #region PUBLIC METHODS

        public bool Equals(GameBall ball)
        {
            return _position == ball.Position;
        }

        public void Clear()
        {
            _position = Position.MIDFIELD;
        }

        #endregion PUBLIC METHODS

    }
}
