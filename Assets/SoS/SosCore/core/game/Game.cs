﻿
using System;
using System.Collections.Generic;
using System.Text;

using sos.core.cards;
using sos.logging;
using sos.utils;

namespace sos.core.game
{
    public class Game
    {

        #region CONSTANTS

		// Logging
		public const string                     LOG_MARKER                      = "GAME > ";
		public const string                     DEBUG_KEY                       = "core.game";

        /// <summary>
        /// Invalid turn index
        /// </summary>
		public const	uint                    INVALID_TURN                    = 0;
		
        // Defaults
        protected static readonly uint          DEFAULT_ID                      = Defaults.INVALID_ID;
        protected static readonly GameSide      DEFAULT_SIDE					= GameSide.NONE;
		protected static readonly uint          DEFAULT_TURN					= INVALID_TURN;
		protected static readonly GameStatus    DEFAULT_STATUS                  = GameStatus.UNKNOWN;
        protected static readonly uint          DEFAULT_PASSED_PHASES           = 0;

        #endregion CONSTANTS

        #region FIELDS

		private uint _id;
		private GameStatus _status;
		private GameOptions _options;
		private User _player;
		private Player _opponent;
		private GameDeck _deck;     // Deck reference which the player starts the game
		private GameBall _ball;
		private GameSide _side;
		private GameTeam _homeTeam;
		private GameTeam _awayTeam;
        private GameTurns _turns;
		private uint _turn;         // Current turn
        private uint _passedPhases;  // Number of passed phases

        #endregion FIELDS

        #region CONSTRUCTORS

        public Game(User player, Deck playerDeck, GameOptions options) : this(player, new GameDeck(playerDeck), options)
        {
        }

		public Game(User player, GameDeck gameDeck, GameOptions options)
		{
			// Options
			_options = options;
			// Init player
			_player = player;
			// Init deck
            _deck = gameDeck;
			// Init object
			_ball = new GameBall();
			_homeTeam = new GameTeam(this, GameSide.HOME);
			_awayTeam = new GameTeam(this, GameSide.AWAY);
			_turns = new GameTurns();
			// Init data
			Clear();
		}

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public uint Id
        {
            get { return _id; }
            internal set { _id = value; }
        }

        public GameStatus Status
        {
            get { return _status; }
        }

        public GameOptions Options
        {
            get { return _options; }
        }

        public string Name
        {
            get 
            { 
                return  IsValid ? _homeTeam.Name + " vs " + _awayTeam.Name
							    : "Game unknown";
            }
        }

        public string Score
        {
            get
            {
                return IsValid ? _homeTeam.Score + " vs " + _awayTeam.Score
                                : "0 - 0";
            }
        }

        public uint Turn
        {
            get { return _turn; }
            internal set { _turn = value; }
        }

        public uint PassedPhases
        {
            get { return _passedPhases; }
            internal set { _passedPhases = value; }
        }

        public User Player
        {
            get { return _player; }
        }

        public Player Opponent
        {
            get { return _opponent; }
        }

        public GameBall Ball
        {
            get { return _ball; }
        }

        public GameSide Side
        {
            get { return _side; }
        }

        public GameTeam HomeTeam
        {
            get { return _homeTeam; }
        }

        public GameTeam AwayTeam
        {
            get { return _awayTeam; }
        }

		public GameTeam PlayerTeam
		{
			get
            {
                return (_side == GameSide.HOME) ? _homeTeam
                                                : _awayTeam;
            }
		}

        public GameTeam OpponentTeam
        {
            get
            {
                return (_side == GameSide.HOME) ? _awayTeam
                                                : _homeTeam;
            }
        }

        public GamePlayerCard PlayerActiveGamePlayerCard
        {
            get
            {
                GameTeam team = PlayerTeam;
                return team.GetPlayer(team.IsAttacking ? _ball.Position : _ball.OppositePosition);
            }
        }

        public GamePlayerCard OpponentActiveGamePlayerCard
        {
            get
            {
                GameTeam team = OpponentTeam;
                return team.GetPlayer(team.IsAttacking ? _ball.Position : _ball.OppositePosition);
            }
        }

		public Position PlayerActivePosition
		{
            get
            {
                return PlayerTeam.IsAttacking ? _ball.Position : _ball.OppositePosition; 
            }
		}
		
		public Position OpponentActivePosition
		{
            get
            {
                return PlayerTeam.IsAttacking ? _ball.OppositePosition : _ball.Position; 
            }
		}

		public GameTeam AttackingTeam
		{
            get
            {
                return PlayerTeam.IsAttacking ? PlayerTeam : OpponentTeam;
            }
			
		}
		
		public GameTeam DefendingTeam
		{
            get
            {
                return PlayerTeam.IsAttacking ? OpponentTeam : PlayerTeam;
            }
		}

        public GameDeck Deck
        {
            get { return _deck; }
        }

        public GameTurns Turns
        {
            get { return _turns; }
        }

        public bool IsStarted
        {
            get { return _turn > INVALID_TURN; }
        }

		public bool IsInProgress
		{
            get
            {
			    return	_status	==	GameStatus.SUBSTITUTION				||
					    _status	==	GameStatus.RESOLVE_SUBSTITUTION		||
					    _status	==	GameStatus.DISCARD					||
					    _status	==	GameStatus.RESOLVE_DISCARD			||
					    _status	==	GameStatus.PLAY						||
					    _status	==	GameStatus.RESOLVE_PLAY 			||
					    _status	==	GameStatus.RESYNC;
            }
		}

		public bool isEnded
		{
			get { return _status == GameStatus.DONE; }
		}
		
		public bool isInDesync
		{
			get { return _status == GameStatus.DESYNC; }
		}
		
		public bool isInError
		{
            get { return _status == GameStatus.ERROR; }
		}

		public bool isFirstTurn
		{
			get { return _turn == 1; }
		}
		
		public bool isLastTurn
		{
            get { return _turn == _options.Turns; }
		}

		public bool IsFirstTurnForPeriod
		{
            get
            {
			    return	_turn == _options.FirstTurnOfFirstPeriod || 
					    _turn == _options.FirstTurnOfSecondPeriod;
            }
		}
		
		public bool IsInFirstPeriod
		{
            get
            {
                return	_turn != INVALID_TURN && _turn <= _options.TurnsForPeriod;
            }
		}
		
		public bool IsInSecondPeriod
		{
            get
            {
                return _turn != INVALID_TURN && _turn > _options.TurnsForPeriod;
            }
		}

		public bool isDiscardToDo
		{
            get
            {
                return !IsFirstTurnForPeriod && IsInFirstPeriod;
            }
		}
		
		public bool IsHalfTime
		{
            get
            {
                return IsInSecondPeriod && IsFirstTurnForPeriod;
            }
		}

		public bool IsPlayerAttacking
		{
            get
            {
                return PlayerTeam.Role == GameRole.ATTACK;
            }
		}
		
		public bool IsPlayerDefending
		{
            get
            {
                return PlayerTeam.Role == GameRole.DEFENCE;
            }
		}
		
		public bool IsPlayerWinning
		{
            get
            {
                return PlayerTeam.Score > OpponentTeam.Score;
            }
		}

		public bool IsOpponentWinning
		{
            get
            {
                return OpponentTeam.Score > PlayerTeam.Score;
            }
		}

		public bool IsValid
		{
            get
            {
                return _side != GameSide.NONE &&
                        _homeTeam.IsValid &&
                        _awayTeam.IsValid;
            }
		}

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        /// <summary>
        /// Change the game status 
        /// </summary>
        /// <param name="newStatus">Game new status</param>
		public void ChangeStatus(GameStatus newStatus)
		{
			// Old status
            GameStatus oldStatus = _status;
			// Apply the new status
			_status = newStatus;
			// DEBUG > Trace status change
			if (Logger.CanDebug(DEBUG_KEY)) Logger.Debug(LOG_MARKER + " STATUS > changet {0} to {1}", oldStatus, newStatus);
		}

        /// <summary>
        /// Return the list of action the player can choose for PLAY phase
        /// </summary>
        /// <returns></returns>
		public GameAction[] GetActionsForPlayPhase()
		{
			// Action list
            List<GameAction> actions = new List<GameAction>();
			// Check if the player is attacking
			if (PlayerTeam.IsAttacking)
			{
				// Chek the allowd actions for the current position
				if (GameAction.PASS.Allowed(PlayerActivePosition)) 		actions.Add(GameAction.PASS);
				if (GameAction.DRIBBLE.Allowed(PlayerActivePosition)) 	actions.Add(GameAction.DRIBBLE);
				if (GameAction.CROSS.Allowed(PlayerActivePosition)) 	actions.Add(GameAction.CROSS);
				if (GameAction.SHOOT.Allowed(PlayerActivePosition)) 	actions.Add(GameAction.SHOOT);
			}
			return actions.ToArray();
		}

        /// <summary>
        /// Return the list of action the player can choose for PLAY phase
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
		public Position[] GetTargetsForGameAction(GameAction action)
		{
			// Positions list
			List<Position> positions = new List<Position>();
			// Check if the player is attacking
			if (PlayerTeam.IsAttacking)
			{
				// Shoot action is always to the goalkeeper, no chooises
				if ((action != GameAction.SHOOT) && (action != GameAction.CROSS))
				{
                    positions.AddRange(Position.GetNearbyPositions(PlayerActivePosition));
				}
			}
			return positions.ToArray();
		}

        /// <summary>
        /// Returns the list of the playable card from the player hand, excluding the selected ones
        /// </summary>
        /// <param name="selected">List of the selected cards</param>
        /// <returns></returns>
		public GameActionCard[] GetCardsToPlay(GameActionCard[] selected)
		{
			// Action card list
			List<GameActionCard> cards = new List<GameActionCard>();
			GameActionCard card = null;
			// Valid flag
			bool valid = true;
			
			// Player stamina check
			if (PlayerActiveGamePlayerCard.CanPlayCards)
			{
				// Filter cards by by role
				for (int i = 0; i < PlayerTeam.Cards.Hand.Count; i++)
				{
					// reset validity flag
					valid = true;
					// Card
					card = PlayerTeam.Cards.Hand[i];
					// Card school check
                    if (valid) valid = card.MatchSchools(PlayerActiveGamePlayerCard.Schools);
					// Role check
					if (valid) valid = (card.Role == Role.ALL || card.Role == Role.NONE || card.Role == PlayerActiveGamePlayerCard.Role);
					// Ball condition
					if (valid)	valid =	card.BallCondition == CardBallCondition.ALWAYS ||
										(PlayerTeam.IsAttacking 	&& card.BallCondition == CardBallCondition.WITH_BALL) ||
										(!PlayerTeam.IsAttacking	&& card.BallCondition == CardBallCondition.WITHOUT_BALL);
					
					// LT_TODO: target conditions check
					
					// Current card is not already contained into the selected list
					if (valid) valid = Array.IndexOf<GameActionCard>(selected, card) == Defaults.INVALID_INDEX;
					// Adds the card to the output list
					if (valid) cards.Add(card);
				}
			}
			return cards.ToArray();
		}

        /// <summary>
        /// Data una carta scelta come input, restituisce la lista di tutte le carte targettabili 
        /// </summary>
        /// <param name="card"></param>
        /// <returns></returns>
		public List<GameActionCard> GetTargetsActionsForCard(GameActionCard card)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="card"></param>
		/// <returns></returns>
	
		public GamePlayerCard[] GetTargetsPlayersForCard(GameActionCard card)
		{
			throw new NotImplementedException();
		}
		
        /// <summary>
        /// Data una carta scelta come input, restituisce la lista di tutte le carte targettabili 
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
		public Position[] GetTargetsPositionsForCard(GameActionCard card)
		{
            throw new NotImplementedException();
		}

        /// <summary>
        /// Restituisce le carte giocatore disponibili per la sostituzione
        /// </summary>
        /// <returns></returns>
        public GamePlayerCard[] getPlayersToSubstitute()
		{
            throw new NotImplementedException();
		}

		// LT_TODO: to remove DEBUG ONLY
		public void UpdateId(uint id)
		{
			_id = id;
		}
		
		// LT_TODO: to remove DEBUG ONLY
		public void UpdateOpponent(Player opponent)
		{
			_opponent = opponent;
		}
		
        /// <summary>
        /// Updates Game
        /// </summary>
        /// <param name="playerFormation"></param>
        /// <param name="opponentFormation"></param>
		public void UpdateFormations(GameFormation playerFormation, GameFormation opponentFormation)
		{
			PlayerTeam.Formation.Update(playerFormation);
			OpponentTeam.Formation.Update(opponentFormation);
		}

        /// <summary>
        /// Updates the status of the cards for player and opponent, usually called
        /// after a discard phase
        /// </summary>
        /// <param name="playerCards">Status of the player cards</param>
        /// <param name="opponentCards">Status of the opponent cards</param>
		public void UpdateCards(GameCards playerCards, GameCards opponentCards)
		{
			// Update cards
			PlayerTeam.Cards.Update(playerCards);
			OpponentTeam.Cards.Update(opponentCards);
		}

        /// <summary>
        /// Updates the game turn list
        /// </summary>
        /// <param name="gameTurn"></param>
		public void UpdateTurns(GameTurn gameTurn)
		{
			// Add the turn to turn list
			_turns.Add(gameTurn);
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public void PassPhase()
        {
            _passedPhases++;
        }

		public bool Equals(Game game)
		{
			return	_id				== game.Id			    &&
					_status			== game.Status		    &&
					_side 			== game.Side		    &&
					_turn 			== game.Turn 		    &&
					_passedPhases	== game.PassedPhases    &&
					_options.Equals(game.Options)		    &&
					_player.Equals(game.Player)			    &&
					_opponent.Equals(game.Opponent)		    &&
					_deck.Equals(game.Deck)				    &&
					_ball.Equals(game.Ball)				    &&
					PlayerTeam.Equals(game.PlayerTeam)	    &&
					OpponentTeam.Equals(game.OpponentTeam);
			
			// LT_TODO: add GameTurns to the check
			
		}

		public void Clear()
		{
			// Clear data
			_id		        = DEFAULT_ID;
			_turn 	        = DEFAULT_TURN;
			_side 	        = DEFAULT_SIDE;
			_status         = DEFAULT_STATUS;
            _passedPhases   = DEFAULT_PASSED_PHASES;
			// Clear objects
			_ball.Clear();
			_homeTeam.Clear();
			_awayTeam.Clear();
			_turns.Clear();
		}

        #endregion PUBLIC METHODS

    }
}
