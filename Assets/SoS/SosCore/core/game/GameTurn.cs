﻿using sos.utils;
using System;
using System.Collections.Generic;

namespace sos.core.game
{
    public class GameTurn
    {

        #region CONSTANTS

		// Default values
		protected static readonly GameAction    DEFAULT_ACTION				        = GameAction.PASS;
		protected static readonly Position      DEFAULT_ACTION_TARGET			    = Position.MIDFIELD;
		protected static readonly uint          DEFAULT_ATTACKER_CARD_ID            = Defaults.INVALID_ID;
		protected static readonly uint          DEFAULT_DEFENDER_CARD_ID            = Defaults.INVALID_ID;

        #endregion CONSTANTS

        #region FIELDS

		private GameSide _playerSide;
		private GameRole _playerRole;
		private GameAction _action;
		private Position _target;
		private uint _targetCardId;
		private uint _attackerCardId;
		private uint _defenderCardId;
		private GameProcHolder _procHolder;
		private List<GameCast> _casts;
		private GameTurnSolve _solve;
		private GameTurnSolveScore _solveScore;
		private string _output;
        private List<GameLogRow> _logRows;

        #endregion FIELDS

        #region CONSTRUCTORS

		public GameTurn(GameSide playerSide, GameRole playerRole)
		{
			// Init objects
			_casts = new List<GameCast>();
			// Ruolo e "lato" del giocatore
			if (playerSide != null) _playerSide = playerSide;
			if (playerRole != null) _playerRole = playerRole;
			// Init objects
			_solve 		= new GameTurnSolve();
			_solveScore = new GameTurnSolveScore();
			_logRows	= new List<GameLogRow>();
			// Clear
			Clear();
		}

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public GameRole PlayerRole
        {
            get { return _playerRole; }
        }

        public GameSide PlayerSide
        {
            get { return _playerSide; }
        }

        public GameAction Action
        {
            get { return _action; }
            internal set { _action = value; }
        }

        public Position Target
        {
            get { return _target; }
            internal set { _target = value; }
        }

        public uint AttackerCardId
        {
            get { return _attackerCardId; }
            internal set { _attackerCardId = value; }
        }

        public uint DefenderCardId
        {
            get { return _defenderCardId; }
            internal set { _defenderCardId = value; }
        }

        public uint TargetCardId
        {
            get { return _targetCardId; }
            internal set { _targetCardId = value; }
        }

        public List<GameCast> Casts
        {
            get { return _casts; }
        }

        public GameProcHolder ProcHolder
        {
            get { return _procHolder; }
            internal set { _procHolder = value; }
        }

        public List<GameLogRow> LogRows
        {
            get { return _logRows; }
        }

        public string Output
        {
            get { return _output; }
            internal set { _output = value; }
        }

        public GameTurnSolve Solve
        {
            get { return _solve; }
            internal set { _solve = value; }
        }

        public GameTurnSolveScore SolveScore
        {
            get { return _solveScore; }
            internal set { _solveScore = value; }
        }

        public bool HasPlayerData
        {
            get { return _playerRole != null && _playerSide != null; }
        }

		public bool IsPlayerAttacking
		{
			get { return _playerRole == GameRole.ATTACK; }
		}

		public bool PlayerWins
		{
			get { return HasPlayerData ? _solve.Winner == _playerRole : false; }
		}
		
		public uint PlayerRoll
		{
			get { return HasPlayerData ? IsPlayerAttacking ? _solve.AttackerRoll : _solve.DefenderRoll : 0; }
		}
		
		public uint PlayerScore
		{
			get { return HasPlayerData ? IsPlayerAttacking ? _solve.AttackerScore : _solve.DefenderScore : 0; }
		}
		
		public uint OpponentRoll
		{
			get { return HasPlayerData ? IsPlayerAttacking ? _solve.DefenderRoll : _solve.AttackerRoll : 0; }
		}
		
		public uint OpponentScore
		{
			get { return HasPlayerData ? IsPlayerAttacking ? _solve.DefenderScore : _solve.AttackerScore : 0; }
		}

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

		public void Clear()
		{
			// Clear data
			_action 		= DEFAULT_ACTION;
			_target 		= DEFAULT_ACTION_TARGET;
			_attackerCardId	= DEFAULT_ATTACKER_CARD_ID;
			_defenderCardId	= DEFAULT_DEFENDER_CARD_ID;
			_output			= "";
			// Clear objects
			_solve.Clear();
			_solveScore.Clear();
			// Clear objects
            _casts.Clear();
            _logRows.Clear();
			
		}

        #endregion PUBLIC METHODS

    }
}
