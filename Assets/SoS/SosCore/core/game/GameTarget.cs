﻿using System;
using System.Collections.Generic;

namespace sos.core.game
{
    /// <summary>
    /// 
    /// </summary>
    public class GameTarget
    {

        #region CONSTANTS

        #endregion CONSTANTS

        #region FIELDS

		protected string _type;
		protected string _name;
		protected List<string> _validTargets;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameTarget()
        {
            _validTargets = new List<string>();
            Clear();
        }

        public GameTarget(string type, string name, string[] validTargets) : this()
        {
		    _type = type;
		    _name = name;
		    _validTargets.AddRange(validTargets);
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string[] ValidTargets
        {
            get { return _validTargets.ToArray(); }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS


        public void Clear()
        {
            _type = string.Empty;
            _name = string.Empty;
            _validTargets.Clear();
        }

        #endregion PUBLIC METHODS

    }
}
