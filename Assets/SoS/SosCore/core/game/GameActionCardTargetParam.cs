﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace sos.core.game
{
    public class GameActionCardTargetParam
    {

        #region FIELDS

        public string _name;
        public object _value;

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameActionCardTargetParam(string name, object value)
        {
            _name = name;
            _value = value;
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public object Value
        {
            get { return _value; }
            set { _value = value; }
        }

        #endregion PROPERTIES

    }
}
