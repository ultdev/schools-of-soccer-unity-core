﻿using sos.utils;
using System;
using System.Collections.Generic;

namespace sos.core.game
{
    public class GameTeam
    {

        #region CONSTANTS

        #endregion CONSTANTS

        #region FIELDS

		private string _name;
		private uint _score;
		private GameRole _role;
		private GameFormation _formation;
		private GameCards _cards;
		private DateTime _roundTime;
		private GameSide _side;
		private Game _game; // Game reference

        #endregion FIELDS

        #region CONSTRUCTORS

		public GameTeam(Game game, GameSide side)
		{
			// side of the game represented by this team
			_side = side;
			// Game reference
			_game = game;
			// Init
			_formation = new GameFormation(this);
			_cards = new GameCards(this);
			// Clear data
			Clear();
		}

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public GameSide Side
        {
            get { return _side; }
            internal set { _side = value; }
        }

        public Game Game
        {
            get { return _game; }
        }

        public string Name
        {
            get { return _name; }
            internal set { _name = value;}
        }

        public uint Score
        {
            get { return _score; }
            internal set { _score = value ;}
        }

        public GameRole Role
        {
            get { return _role; }
            internal set { _role = value; }
        }

        public GameFormation Formation
        {
            get { return _formation; }
        }

        public GameCards Cards
        {
            get { return _cards; }
        }

        public DateTime RoundTime
        {
            get { return _roundTime; }
        }

        public bool IsAttacking
        {
            get { return _role == GameRole.ATTACK; }
        }

		public bool IsPlayerTeam
		{
            get
            {
                if (_game == null) throw new Exception("GameTeam.isPlayerTeam() error, Game reference is null!");
                if (_game.Side == GameSide.NONE) throw new Exception("GameTeam.isPlayerTeam() error, Game.side == GameSide.NONE");
                return _side == _game.Side;
            }
		}

		public bool IsValid
		{
            get
            {
                return  _formation.IsValid &&
                        _role != GameRole.IDLE &&
                        _roundTime != Defaults.INVALID_DATE &&
                        !string.IsNullOrEmpty(_name);
            }
		}

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

		public GamePlayerCard GetPlayer(Position position)
		{
			return _formation.GetPlayer(position);
		}
		
		public Position GetPlayerPosition(GamePlayerCard player)
		{
			return _formation.GetPlayerPosition(player);
		}
		
		public bool Equals(GameTeam gameTeam)
		{
			return	_side  	== 	gameTeam.Side		&&
					_game 	== 	gameTeam.Game		&&
					_name 	== 	gameTeam.Name		&&
					_score	==	gameTeam.Score		&&
					_role	==	gameTeam.Role		&&
					_formation.Equals(gameTeam.Formation)	&&
					_cards.Equals(gameTeam.Cards);
		}
		
		public void Clear()
		{
			// Clears data
			_name		= "";
			_score		= 0;
			_role		= GameRole.IDLE;
			_roundTime	= Defaults.INVALID_DATE;
			// Clears objects
			_formation.Clear();
			_cards.Clear();
		}

        #endregion PUBLIC METHODS

    }
}
