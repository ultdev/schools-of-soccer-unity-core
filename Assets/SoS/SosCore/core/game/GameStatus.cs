﻿using System;
using System.Collections.Generic;
using sos.data;

namespace sos.core.game
{

    /// <summary>
    /// List of all the status that a Game can assume during gametime
    /// </summary>
    public class GameStatus : Enumerator
    {

        #region CONSTANTS

		public static readonly GameStatus   UNKNOWN				    = new GameStatus(00, 	"UNKNOWN",				"Game status unknown");						// no data or not valid, waiting for data
		public static readonly GameStatus   WAITING				    = new GameStatus(05, 	"WAITING",				"Game created, waiting for an opponent");	// game created, waiting for an opponent
		public static readonly GameStatus   READY				    = new GameStatus(10, 	"READY",				"Game created, waiting first resync");		// game init and ready to be started
		public static readonly GameStatus   SUBSTITUTION			= new GameStatus(20, 	"SUBSTITUTION",			"User must choose player to substitute");	// turn > substitution phase
		public static readonly GameStatus   RESOLVE_SUBSTITUTION	= new GameStatus(21, 	"RESOLVE_SUBSTITUTION",	"User must choose player to substitute");	// turn > resolve substitution phase
		public static readonly GameStatus   DISCARD				    = new GameStatus(22, 	"DISCARD",				"User must choose action card to discard");	// turn > discard phase
		public static readonly GameStatus   RESOLVE_DISCARD		    = new GameStatus(23, 	"RESOLVE_DISCARD",		"User must choose action card to discard");	// turn > resolve discard phase
		public static readonly GameStatus   PLAY					= new GameStatus(24, 	"PLAY",					"User must choose action to do");			// turn > action phase
		public static readonly GameStatus   RESOLVE_PLAY			= new GameStatus(25, 	"RESOLVE_PLAY",			"Waiting for turn resolution");				// turn > resolve action phase 
		public static readonly GameStatus   RESYNC				    = new GameStatus(26,	"RESYNC",				"Game in resyn with server");				// turn > resync with server (status check)
		public static readonly GameStatus   DONE					= new GameStatus(50, 	"DONE",					"Game ended");
		public static readonly GameStatus   DESYNC				    = new GameStatus(80,	"DESYNC",				"Game client status desync from the server status");
		public static readonly GameStatus   ERROR				    = new GameStatus(90,	"ERROR",				"Game in error");

        #endregion CONSTANTS

        #region CONSTRUCTORS

        public GameStatus(int id, string name, string description) : this(id, name, description, true)
        {
        }

        public GameStatus(int id, string name, string description, bool register) : base(id, name, string.Empty, register)
        {
            _description = description;
        }

        #endregion CONSTRUCTORS

        #region FIELDS

        private string _description;

        #endregion FIELDS

        #region PROPERTIES

        public string Description
        {
            get { return _description; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        #endregion PUBLIC METHODS

        #region STATIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string[] Names()
        {
            return Enumerators.Names(typeof(GameStatus));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static GameStatus[] Values()
        {
            return Enumerators.Values<GameStatus>(typeof(GameStatus));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GameStatus GetById(int id)
        {
            return Enumerators.GetById<GameStatus>(typeof(GameStatus), id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GameStatus GetByName(string name)
        {
            return Enumerators.GetByName<GameStatus>(typeof(GameStatus), name);
        }

        #endregion STATIC METHODS

    }
}
