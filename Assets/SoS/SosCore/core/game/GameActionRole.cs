﻿using sos.data;

namespace sos.core.game
{
    public class GameActionRole : Enumerator
    {

        #region CONSTANTS

		public static readonly GameActionRole NONE					        = new GameActionRole(0, "NONE", 	"none");
		public static readonly GameActionRole ATTACKER				        = new GameActionRole(1, "ATTACKER", "att");
		public static readonly GameActionRole DEFENDER                      = new GameActionRole(2, "DEFENDER",	"def");

        #endregion CONSTANTS

        #region FIELDS

        #endregion FIELDS

        #region CONSTRUCTORS

        public GameActionRole(int id, string name, string sign) : this(id, name, sign, true)
        {
        }

        public GameActionRole(int id, string name, string sign, bool register) : base(id, name, sign, register)
        {
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

        #endregion PUBLIC METHODS

        #region STATIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string[] Names()
        {
            return Enumerators.Names(typeof(GameActionRole));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static GameActionRole[] Values()
        {
            return Enumerators.Values<GameActionRole>(typeof(GameActionRole));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GameActionRole GetById(int id)
        {
            return Enumerators.GetById<GameActionRole>(typeof(GameActionRole), id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static GameActionRole GetByName(string name)
        {
            return Enumerators.GetByName<GameActionRole>(typeof(GameActionRole), name);
        }

        #endregion STATIC METHODS

    }
}
