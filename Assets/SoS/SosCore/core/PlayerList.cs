﻿using System;
using System.Collections.Generic;
using sos.utils;

namespace sos.core
{
    public class PlayerList
    {

        #region CONSTANTS

        #endregion CONSTANTS

        #region FIELDS

        private List<Player> _list;

        #endregion FIELDS

        #region CONSTRUCTORS

        public PlayerList()
        {
            _list = new List<Player>();
        }

        public PlayerList(List<Player> players) : this()
        {
            AddList(players);
        }

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public int Count
        {
            get { return _list.Count; }
        }

        public Player this[int index]
        {
            get { return _list[index]; }
        }

        public Player this[uint id]
        {
            get 
            { 
                int index = IndexOf(id);
                if (index == Defaults.INVALID_INDEX) throw new ArgumentException(string.Format("Player with id {0} not found", id));
                return _list[index]; 
            }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        /// <summary>
        /// returns a Player instance by given ID
        /// </summary>
        /// <param name="id">ID of the Player to retrieve</param>
        /// <returns>Player instance contained into the list with the given id, returns Defaults.INVALID_INDEX if no Players is found for the given id</returns>
		private int IndexOf(uint id)
		{
			for (var i=0; i<_list.Count; i++) if (_list[i].Id == id) return i;
			return Defaults.INVALID_INDEX;
		}

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS

		public void Add(Player item)
		{
			_list.Add(item);
		}

        public void AddList(List<Player> players)
        {
            _list.AddRange(players);
        }
		
		public Player Get(int index)
		{
			return _list[index];
		}
		
		public Player GetById(uint id)
		{
			int index = IndexOf(id);
			Player player = null;
			if (index != Defaults.INVALID_INDEX) player = _list[index];
			return player;
		}
		
		public void Copy(PlayerList playerList)
		{
			// Value parsing and casting
			List<Player> list = new List<Player>();
			// Objects parsing
			for (int i = 0; i < playerList.Count; i++)
			{
				list.Add(playerList[i].Clone());
			}
			// Updating player list
			_list.Clear();
            _list.AddRange(list);
		}
		
		public PlayerList Clone()
		{
			PlayerList playerList = new PlayerList();
			playerList.Copy(this);
			return playerList;
		}

        public void Clear()
		{
            _list.Clear();
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} (player count: {1})", GetType().FullName, _list.Count);
        }

        #endregion PUBLIC METHODS

    }
}
