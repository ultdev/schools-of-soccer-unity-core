﻿using sos.core.game;
using sos.utils;

namespace sos.core
{
    public class UserGame
    {

        #region CONSTANTS

        #endregion CONSTANTS

        #region FIELDS

        private uint _id;
        private GameSide _side;
		private Player _opponent;
        private uint _playerDeckId;
        private string _playerDeckName;
        private uint _opponentDeckId;
        private string _opponentDeckName;

        #endregion FIELDS

        #region CONSTRUCTORS

        #endregion CONSTRUCTORS

        #region PROPERTIES

        public uint Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public GameSide Side
        {
            get { return _side; }
            set { _side = value; }
        }

        public Player Opponent
        {
            get { return _opponent; }
            set { _opponent = value; }
        }

        public uint PlayerDeckId
        {
            get { return _playerDeckId; }
            set { _playerDeckId = value; }
        }

        public string PlayerDeckName
        {
            get { return _playerDeckName; }
            set { _playerDeckName = value; }
        }

        public uint OpponentDeckId
        {
            get { return _opponentDeckId; }
            set { _opponentDeckId = value; }
        }

        public string OpponentDeckName
        {
            get { return _opponentDeckName; }
            set { _opponentDeckName = value; }
        }

        #endregion PROPERTIES

        #region PRIVATE METHODS

        #endregion PRIVATE METHODS

        #region PUBLIC METHODS
		
        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameReference"></param>
		public void Copy(UserGame gameReference)
		{
			_id 				= gameReference.Id;
			_side				= gameReference.Side;
			_opponent			= gameReference.Opponent;
			_playerDeckId		= gameReference.PlayerDeckId;
			_playerDeckName		= gameReference.PlayerDeckName;
			_opponentDeckId		= gameReference.OpponentDeckId;
			_opponentDeckName	= gameReference.OpponentDeckName;
		}
		
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
		public UserGame Clone()
		{
            UserGame clone = new UserGame();
            clone.Copy(this);
            return clone;
		}

        /// <summary>
        /// 
        /// </summary>
        public void Clear()
        {
            _id = Defaults.INVALID_ID;
            _side = GameSide.NONE;
            _opponent = null;
            _playerDeckId = Defaults.INVALID_ID;
            _playerDeckName = string.Empty;
            _opponentDeckId = Defaults.INVALID_ID;
            _opponentDeckName = string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} (id : {1}, opponent: {2}, deck: {3} (id: {4}), side: {5})",
                                 GetType().FullName,
                                 _id,
                                 _opponent.Username,
                                 _playerDeckName,
                                 _playerDeckId,
                                 _side);
        }

        #endregion PUBLIC METHODS

    }
}
